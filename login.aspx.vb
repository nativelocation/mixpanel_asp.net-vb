﻿
Partial Class login
    Inherits System.Web.UI.Page
    Public cni As New Data.SqlClient.SqlConnection
    Public SQLi As Data.SqlClient.SqlCommand
    Public rsi As Data.SqlClient.SqlDataReader
    Public inversionistas As New ICE_clientes, g As New generales

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cni.ConnectionString = inversionistas.conectdb
    End Sub

    Protected Sub btn_ingresar_Click(sender As Object, e As System.EventArgs) Handles btn_ingresar.Click
        Dim sSQL, idinversionista As String, encontrado As Integer
        mensaje_alerta.Visible = False
        If g.IsValidEmail(txt_correoElectronico.Text) Then
            sSQL = "select count(idinversionista)existe from inversionistas where correo='" & LCase(txt_correoElectronico.Text) & "' and pass='" & txt_Contrasena.Text & "'"
            SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
            cni.Open() : encontrado = SQLi.ExecuteScalar : cni.Close()
            If encontrado > 0 Then
                sSQL = "select idinversionista from inversionistas  where correo='" & LCase(txt_correoElectronico.Text) & "' and pass='" & txt_Contrasena.Text & "'"
                SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
                cni.Open()
                rsi = SQLi.ExecuteReader
                While rsi.Read
                    idinversionista = rsi("idinversionista")
                    Session("idinversionista") = idinversionista
                    Session("nombre_inversionista") = inversionistas.nombre_inversionista(idinversionista)
                    Session("correo_registro") = LCase(txt_correoElectronico.Text)
                    Session("invesionista_login") = "autorized"
                End While
                cni.Close()
                If (String.IsNullOrEmpty(Request.QueryString(g.base64_encode("Estrategia")))) Or (String.IsNullOrEmpty(Request.QueryString(g.base64_encode("Experto")))) Then
                    Response.Redirect("inversionistas/portafolio.aspx")
                Else
                    Dim idestrategia, idexperto, proviene As String
                    idestrategia = Request.QueryString(g.base64_encode("Estrategia"))
                    idexperto = Request.QueryString(g.base64_encode("Experto"))
                    idestrategia = g.base64_decode(idestrategia)
                    idexperto = g.base64_decode(idexperto)
                    proviene = Request.QueryString(g.base64_encode("proviene"))
                    If proviene = "buscarn" Then
                        Response.Redirect("inversionistas/detalle.aspx?" & g.base64_encode("Estrategia") & "=" & g.base64_encode(idestrategia) & "&" & g.base64_encode("Experto") & "=" & g.base64_encode(idexperto) & "&" & g.base64_encode("proviene") & "=" & proviene)
                    ElseIf proviene = "buscarexp" Then
                        Response.Redirect("inversionistas/detalle.aspx?" & g.base64_encode("Estrategia") & "=" & g.base64_encode(idestrategia) & "&" & g.base64_encode("Experto") & "=" & g.base64_encode(idexperto) & "&" & g.base64_encode("proviene") & "=" & proviene & "&search=" & Request.QueryString("search") & "&execution=" & Request.QueryString("execution"))
                    End If
                End If
                Dim jsonproduct = JsonConvert.SerializeObject(New With {
                    Key .event = "login",
                    Key .properties = New With {
                        Key .distinct_id = txt_distinct.Text,
                        Key .token = "79a3138bc6cd79dea43bb30e334f8709",
                        Key .email = txt_correoElectronico.Text,
                        Key .user_type = "investor"
                    }
                })
                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(jsonproduct)
                Dim webClient As New System.Net.WebClient
                Dim result As String = webClient.DownloadString("http://api.mixpanel.com/track/?data=" & System.Convert.ToBase64String(byt))

            Else
                'Mensaje de error (Datos incorrectos)
                mensaje_alerta.Visible = True
            End If
        Else

        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txt_Contrasena.Attributes.Add("type", "password") : txt_Contrasena.Attributes.Add("required", "required")
        txt_correoElectronico.Attributes.Add("type", "email") : txt_correoElectronico.Attributes.Add("required", "required")
    End Sub
    Protected Sub btn_recover_Click(sender As Object, e As EventArgs) Handles btn_recover.Click
        Response.Redirect("password-recovery.aspx")
    End Sub
End Class
