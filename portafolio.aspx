﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="portafolio.aspx.vb" Inherits="portafolio" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Mi Portafolio</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/portafolio.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar first"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar last"></span>
                </button>
                <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li style="margin-right: 18px;"><a href="preguntas.aspx">FAQs</a></li>
                    <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                    <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                    <li><a href="Micuenta.aspx">Mi Cuenta</a></li>
                    <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
                </ul>
            </div>
        </div>
    </nav>

<section class="search_wrap">
    <div class="search">
        <form id="searchForm">
            <input type="text" class="search_input" placeholder="Buscar Experto / Estrategia">
            <span class="search_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
            <input type="submit" style="display: none">
        </form>
    </div>
</section>

<section class="container porta_cont">
    <h1 class="block_title">Mi Portafolio</h1>
    <hr class="block_divider">
    <h2 class="block_subtitle">Confirma las estrategias en donde quieres invertir y los importes de inversión
    </h2>
    <div class="estrats_wrap">
        <div class="estrats_single">
            <div class="estrats_single_head">
                <p class="estrats_single_head_title">
                    <span>Estrategia</span> AE120 fede11</p>
            </div>
            <div class="estrats_single_block">
                <p class="estrats_single_percent">20.5%</p>
                <p class="estrats_single_term">365 days</p>
            </div>
            <div class="estrats_single_block">
                <span class="estrats_single_subtitle">Comisión</span>
                <div class="estrats_single_value_wrap">
                    <span class="estrats_single_big-value estrats_single_comision_value">1.20%</span>
                    <span class="estrats_single_value_sm-text">(anual)</span>
                </div>
                <span class="estrats_single_subtitle">Minomo</span>
                <div class="estrats_single_value_wrap">
                    <span class="estrats_single_big-value estrats_single_minimo_value">50.000</span>
                    <span class="estrats_single_value_sm-text">pesos</span>
                </div>
            </div>
            <div class="estrats_single_block">
                <span class="estrats_single_subtitle">Objetivo de Inversión</span>
                <span class="estrats_single_objetivo_value">4</span>
                <div>
                    <span class="estrats_single_subtitle">Liquidez:</span>
                    <span class="estrats_single_liquidez_value">72hrs</span>
                </div>
            </div>
            <div class="estrats_single_block">
                <span class="estrats_single_subtitle">Diversificación</span>
                <span class="estrats_single_big-value estrats_single_fondos_value">5 Fondos</span>
                <span class="estrats_single_big-value estrats_single_operadoras_value">2 Operadoras</span>
            </div>
            <div class="estrats_single_block">
                <div class="estrats_single_sum_wrap">
                    <span class="estrats_single_sum_value">$25,000</span>
                </div>
            </div>
        </div>
    </div>
    <div class="estrats_total">
        <span class="estrats_total_title">Importe Total para</span>
        <div class="estrats_total_sum">
                <span class="estrats_total_sum_value">$25,000</span>
        </div>
    </div>
    <div class="porta_actualisar">
        <span class="porta_actualisar_text">Cambiaste los importes, haz clic en </span>
        <button class="btn porta_actualisar_btn">Actualizar</button>
    </div>
</section>

<section class="conformes_wrap">
    <div class="container conformes_cont">
        <p class="conformes_subtitle">No te conformes con la persona que te asignó tu institución financiera, mejor
            contrata a los mejores, empieza hoy y gana más.</p>
        <hr class="block_divider">
        <button class="btn conformes_btn">Invierte en tu Portafolio
        </button>
    </div>
</section>

<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>

<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>

                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
</footer>
<script src="js/general_bundle.js"></script>
</body>
</html>