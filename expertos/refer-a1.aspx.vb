﻿
Partial Class expertos_refer_a1
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto As Integer
    Public cadena, fecha As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Dim invitaciones_restantes As Integer
        If String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        End If
        idexperto = Session("idexperto")
        cne.ConnectionString = expertos.conectdb : cne2.ConnectionString = expertos.conectdb
        If idexperto <> 504 Then
            If referidos_check() < 5 Then
                invitaciones_restantes = 5 - referidos_check()
                txt_invitaciones.Text = " (te quedan <strong>" & invitaciones_restantes & "</strong> invitaciones)"
            Else

                txt_invitaciones.Text = " (te quedan <strong>0</strong> invitaciones)<br><br>No puedes referir a mas expertos. Has agotado todas las invitaciones."
                txt_invitaciones.ForeColor = Drawing.Color.Red
                panel_referir.Visible = False
            End If
        Else
            txt_invitaciones.Text = " Has enviado <strong>" & referidos_check() & "</strong> invitaciones."
        End If
    End Sub

    Protected Sub btn_recomendar_Click(sender As Object, e As System.EventArgs) Handles btn_recomendar.Click
        If Not String.IsNullOrEmpty(txt_correos.Value) Then
            validarCorreo()
            txt_correos.Value = ""
        End If
    End Sub

    Private Function CuentaCaracter(ByVal cadena As String, ByVal caracter As Char) As Integer
        Dim contador As Integer
        contador = 0
        For Each c As Char In cadena.ToCharArray
            If c.ToString.Equals(caracter.ToString) Then
                contador = contador + 1
            End If
        Next
        Return contador
    End Function

    Private Function validarCorreo()
        Dim contador, existencia As Integer, sSQL, sSQL2, codigo, mensaje As String
        If referidos_check() > 25 And idexperto <> 504 Then
            Exit Function
        End If
        cadena = txt_correos.Value
        cadena = Replace(cadena, vbCrLf, ";")
        cadena = Replace(cadena, ";", ",")
        Session("other_itemscall") = 0
        mensaje = "<p>Te acaban de invitar a que formes parte de la primera plataforma para expertos en fondos de inversión, mediante la cual puedes ganar dinero permitiendo que miles de inversionistas repliquen tus estrategias de inversión. " &
        "<b>Tus datos personales son confidenciales, todos los usuarios te conocerán por tu alias (nombre de usuario)</b>. </p>" &
        "<p>La plataforma no está abierta al público; solamente las personas que reciben esta invitación pueden formar parte de ella.</p>" &
        "<p>Esta invitación tiene una vigencia de 30 días, por lo que te recomendamos que te registres lo antes posible.</p>" &
        "<p>Utiliza tu correo (dirección de email) para registrarte en la plataforma. <br>Ingresa a: <a href='http://invierteconexpertos.mx/expertos'>www.invierteconexpertos.mx/expertos</a> y haz clic en Regístrate.</p>"
        For Each mail As String In cadena.Split(",")
            If (IsValidEmail(LCase(Trim(mail))) = True) And (contador < 25) Then
                contador += 1
                'Revisar si ya esta en los referidos
                sSQL = "select count(*)existe from referidos_expertos where referido_email='" & Trim(mail) & "'"
                SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                cne.Open() : existencia = SQLe.ExecuteScalar : cne.Close()
                If existencia = 0 Then
                    sSQL = "insert into referidos_expertos (experto_refiere,referido_email,fecha_referido,estatus) values('" & idexperto & "','" & Trim(mail) & "','" & expertos.hora_local & "',1)"
                    SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                    cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
                    'revisar si ya se envuentra registrado como Experto en la plataforma1
                    sSQL = "select idexperto from expertos where correo='" & Trim(mail) & "'"
                    SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                    cne.Open()
                    rse = SQLe.ExecuteReader
                    If rse.Read Then
                        sSQL2 = "update referidos_expertos set estatus=-1 where referido_email='" & Trim(mail) & "'"
                        SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
                        cne2.Open() : SQLe2.ExecuteNonQuery() : cne2.Close()
                    Else
                        expertos.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Experos", Trim(mail), "Experto en Fondos de Inversión", " Te invitaron a Invierte con Expertos", "", mensaje)
                    End If
                    cne.Close()
                End If
            End If
        Next
        If contador > 0 Then
            Session("refer_expertos") = g.base64_encode(contador & " solicitud(es) enviada(s).")
            lbl_mensaje.Text = contador & " solicitud(es) enviada(s)."
            Response.Redirect("refer-a2.aspx?msj=" & Session("refer_expertos"))
        Else
            lbl_mensaje.Text = "No se ha enviado nunguna solicitud."
        End If
    End Function

    Public Function IsValidEmail(ByVal email As String) As Boolean
        Dim expresion As String
        expresion = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        Return Regex.IsMatch(email, expresion)
    End Function

    Function referidos_check() As Integer
        Dim sSQL As String
        sSQL = "select count(*)referidos from referidos_expertos where experto_refiere='" & idexperto & "' "
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : referidos_check = SQLe.ExecuteScalar : cne.Close()
    End Function
End Class
