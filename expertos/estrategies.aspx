﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="estrategies.aspx.vb" Inherits="expertos_estrategies" %>

<!DOCTYPE html>
<html data-wf-page="58a2ccc2aa68ad20244cf7fc" data-wf-site="5898439cf29bebaa63a0fb15">
<head> 
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Mi cuenta" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script type="text/javascript" src="../fusioncharts/fusioncharts.js"></script>                    
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="fondo-window-wrapper" id="show_message">
    
  </div>
  <form id="form1" runat="server">
    <div class="brown-section est height padding-top">
    <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="estrategies.aspx"><img src="images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="estrategies.aspx">Mis Estrategias</a><a class="nav-link w-nav-link" href="comisions.aspx">Mis Comisiones</a><a class="menu-bar-button w-button" href="refer.aspx">Recomienda y Gana</a>
          <div class="dd w-dropdown" data-delay="0">
            <div class="dd-toogle w-dropdown-toggle">
              <div><%=Session("alias_login")%></div>
              <div class="w-icon-dropdown-toggle"></div>
            </div>
            <nav class="dd-list w-dropdown-list"><a class="dd-link w-dropdown-link" href="refer-a2b.aspx">Datos Bancarios</a><a class="dd-link w-dropdown-link" href="password-1.aspx">Cambiar password</a><a class="dd-link w-dropdown-link" href="logout.aspx">Log out</a>
            </nav>
          </div>
        </nav>
        <div class="menu-text mt-mobile">Portal
          <br>de Expertos</div>
        <div class="menu-text mt-hide">Portal de Expertos</div>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="estrategies-top">
      <h2 class="heading-white-28px left-align">Mis Estrategias</h2>
        <%If cuenta_datos() = 0 Then %>
        <%--<div class="email-confirm hide"><div class="text-block-4"><a href="refer-a2b.aspx">Datos Bancarios</a></div></div>--%>
        <%End If %>
      <%validate_estrategies()%>
    </div>
    <div class="info-blocks-wrapper">
      <div class="info-block">
        <div class="info-price">$0</div>
        <div class="info-question-wrapper">
          <div class="info-text">Recursos</div><img class="question-icon" data-ix="popup-appear" height="16" src="images/Question-Icon_1.svg" width="16">
          <div class="popup-wrapper">
            <p>La cantidad de dinero invertido en cuentas de inversión de diferentes inversionistas y que está replicando cualquiera de tus estrategias.</p>
          </div>
        </div>
      </div>
      <div class="info-block">
        <div class="info-price"><% =seguidores%></div>
        <div class="info-question-wrapper">
          <div class="info-text">Seguidores</div><img class="question-icon" data-ix="popup-appear" height="16" src="images/Question-Icon_1.svg" width="16">
          <div class="popup-wrapper">
            <p>Cantidad de personas que tienen su dinero invertido en todas tus estrategias.</p>
          </div>
        </div>
      </div>
      <div class="info-block">
        <div class="info-price"><%=cuantas_estrategias()%></div>
        <div class="info-question-wrapper">
          <div class="info-text">Estrategias</div><img class="question-icon" data-ix="popup-appear" height="16" src="images/Question-Icon_1.svg" width="16">
          <div class="popup-wrapper">
            <p>Número de estrategias que tienes creadas en el sistema.</p>
          </div>
        </div>
      </div>
    </div>
    <asp:PlaceHolder ID="estrategies_zone" runat="server"></asp:PlaceHolder>
  </div>
  </form>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="estrategies.aspx">Mi Cuenta</a><a class="footer-link" href="comisions.aspx">Mis Comisiones</a><a class="footer-link" href="refer.aspx">Recomienda y Gana</a><a class="footer-link" href="faq.aspx">FAQs</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <script>
      function a_onClick() {$('#show_message').css("display","none");}
      $(document).ready(function () {
          $('.valid-mod').click(function () {
              var xEstrategia = $(this).attr("data-ixEstrategia");
              var cadena = "<div class='fondo-close' data-ix='fondo-close' onClick='a_onClick()'></div>" +
              "<div class='fondo-block'>" +
              "<div class='input-label-block'>" +
              "<h3 class='brown-bold-14px'>" + xEstrategia + "</h3>" +
              "<div class='brown-bold-14px' style='color:Red; margin-top:25px; margin-bottom:25px;'>No es posible modificar la estragia ya que esta en Actualización</div>" +
              "<a class='small-button w-button' style='margin-left: auto; margin-right: auto;' href='#' onClick='a_onClick()'> Cerrar</a></div>";
              $('#show_message').html(cadena);
          });
          $('.valid-mod-pub').click(function () {
              var xEstrategia = $(this).attr("data-ixEstrategia");
              var cadena = "<iframe src='strategy-mod.aspx?elemento=" + xEstrategia + "&type=modificacion' border='0' scrolling='no' style='width:100%; height:100%; border:0; overflow:hidden;'></iframe>"
              $('#show_message').html(cadena);
          });
          $('.del-estrategy').click(function () {
              var xEstrategia = $(this).attr("data-ixEstrategia");
              var cadena = "<iframe src='strategy-delete.aspx?elemento=" + xEstrategia + "&type=eliminacion' border='0' scrolling='no' style='width:100%; height:100%; border:0; overflow:hidden;'></iframe>"
              $('#show_message').html(cadena);
          });
          $('.deny-new-strategy').click(function () {
              var xEstrategia = "Invierte con Expertos";
              var cadena = "<div class='fondo-close' data-ix='fondo-close' onClick='a_onClick()'></div>" +
              "<div class='fondo-block'>" +
              "<div class='input-label-block'>" +
              "<h3 class='brown-bold-14px'>" + xEstrategia + "</h3>" +
              "<div class='brown-bold-14px' style='color:Red; margin-top:25px; margin-bottom:25px;'>No es posible agregar mas estrategias, ya has registrado las 5 permitidas</div>" +
              "<a class='small-button w-button' style='margin-left: auto; margin-right: auto;' href='#' onClick='a_onClick()'> Cerrar</a></div>";
              $('#show_message').html(cadena);
          });
          $('.promover-btn').click(function () {
              var pos = $(this).attr("data-ixEstrategia");
              if ($("#promover-wrapper_" + pos).css("display") == "none") {
                  $("#promover-wrapper_" + pos).css("display", "block");
                  $("#composition-wrapper_" + pos).css("display", "none");
                  $("#cambios-wrapper_" + pos).css("display", "none");
                  $("#operaciones-wrapper_" + pos).css("display", "none");
              }
              else if ($("#promover-wrapper_" + pos).css("display") == "block")
                  $("#promover-wrapper_" + pos).css("display", "none");
          });
          $('.composition-btn').click(function () {
              var pos = $(this).attr("data-ixEstrategia");
              if ($("#composition-wrapper_" + pos).css("display") == "none") {
                  $("#promover-wrapper_" + pos).css("display", "none");
                  $("#composition-wrapper_" + pos).css("display", "block");
                  $("#cambios-wrapper_" + pos).css("display", "none");
                  $("#operaciones-wrapper_" + pos).css("display", "none");
              }
              else if ($("#composition-wrapper_" + pos).css("display") == "block")
                  $("#composition-wrapper_" + pos).css("display", "none");
          });
          $('.cambios-btn').click(function () {
              var pos = $(this).attr("data-ixEstrategia");
              if ($("#cambios-wrapper_" + pos).css("display") == "none") {
                  $("#promover-wrapper_" + pos).css("display", "none");
                  $("#composition-wrapper_" + pos).css("display", "none");
                  $("#cambios-wrapper_" + pos).css("display", "block");
                  $("#operaciones-wrapper_" + pos).css("display", "none");
              }
              else if ($("#cambios-wrapper_" + pos).css("display") == "block")
                  $("#cambios-wrapper_" + pos).css("display", "none");
          });
          $('.operaciones-btn').click(function () {
              var pos = $(this).attr("data-ixEstrategia");
              if ($("#operaciones-wrapper_" + pos).css("display") == "none") {
                  $("#promover-wrapper_" + pos).css("display", "none");
                  $("#composition-wrapper_" + pos).css("display", "none");
                  $("#cambios-wrapper_" + pos).css("display", "none");
                  $("#operaciones-wrapper_" + pos).css("display", "block");
              }
              else if ($("#operaciones-wrapper_" + pos).css("display") == "block")
                  $("#operaciones-wrapper_" + pos).css("display", "none");
          });
          $('.header-btn').click(function () {
              var pos = $(this).attr("data-ixEstrategia");
              $("#promover-wrapper_" + pos).css("display", "none");
              $("#composition-wrapper_" + pos).css("display", "none");
              $("#cambios-wrapper_" + pos).css("display", "none");
              $("#operaciones-wrapper_" + pos).css("display", "none");
          });
      });
  </script>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>