﻿
Partial Class expertos_logout
    Inherits System.Web.UI.Page

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Session.Abandon()
        Session.Clear()
        Response.Redirect("index.aspx")
    End Sub
End Class
