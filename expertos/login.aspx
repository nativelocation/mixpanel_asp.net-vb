﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="expertos_login" %>

<!DOCTYPE html>
<html data-wf-site="5898439cf29bebaa63a0fb15">
<head runat="server">
    <meta charset="utf-8">
    <title>Invierte con Expertos - Expertos</title>
    <meta content="Webflow" name="generator">
    <link href="css/normalize.css" rel="stylesheet" type="text/css">
    <link href="css/webflow.css" rel="stylesheet" type="text/css">
    <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
    <script type="text/javascript">
        WebFont.load({
            google: {
                families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
            }
        });
    </script>
    <script src="js/modernizr.js" type="text/javascript"></script>
</head>
<body style="background-color:transparent;">
    <div class="close" data-ix="close"></div>
    <div class="login-wrapper">
          <div class="login-photo"></div>
          <div class="form w-tabs">
            <div class="login-back w-tab-menu">
              <a class="register-round w-inline-block w-tab-link" data-w-tab="Tab 2">
                <div class="login-text">Regístrate</div>
              </a>
              <a class="login-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
                <div class="login-text">Inicia Sesión</div>
              </a>
            </div>
            <div class="w-tab-content">
              <div class="w-tab-pane" data-w-tab="Tab 2">
                <div class="login-back-bottom register">
                  <p class="brown-center-14px">Crea tu cuenta en el portal de Expertos y comienza a registrar tus estrategias</p><a class="button reg w-button" href="new-account.aspx" target="_top">Regístrate Gratis</a>
                </div>
              </div>
              <div class="w--tab-active w-tab-pane" data-w-tab="Tab 1">
                <div class="login-back-bottom">
                  <div class="w-form">
                    <div class="w-form-fail" runat="server" id="login_fail">
                      <div style="text-align:center;"><asp:Label ID="msg_error" runat="server" Text=""></asp:Label></div>
                    </div>
                    <form class="login-form" data-name="Email Form" id="email_form" name="email-form" runat="server">
                      <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="images/message.svg" width="20">
                          <asp:TextBox ID="txt_email" name="txt_email" runat="server" class="text-field w-input" data-name="correo electrónico" MaxLength="256"></asp:TextBox>
                      </div>
                      <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="images/password.svg" width="20">
                        <asp:TextBox ID="txt_contrasena" name="txt_contrasena" runat="server" class="text-field w-input" data-name="contrasena" MaxLength="256"></asp:TextBox>
                      </div>
                      <div class="checkbox-field w-checkbox w-clearfix">
                        <asp:CheckBox ID="chk_recordar" runat="server" class="checkbox w-checkbox-input" data-name="chk recordar" name="chk_recordar" AutoPostBack="False" />
                        <label class="small-right-text w-form-label" for="chk_recordar">Recuérdame (Solo actívalo en equipos privados)</label>
                      </div>
                      <asp:Button ID="btn_ingredar" runat="server" Text="Ingresar" class="button login w-button" data-wait="Please wait..." />
                    </form>
                  </div><a class="small-center-text" href="password-recovery.aspx" target="_top">¿Olvidaste tu contraseña?</a>
                </div>
              </div>
            </div>
          </div>
        </div>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <script>
        $(document).ready(function () {
            $('.close').click(function () {
                $('.login-form-wrapper', parent.document).css("display", "none");
            });
        });
    </script>
</body>
</html>
