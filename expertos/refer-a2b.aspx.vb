﻿
Partial Class expertos_refer_a2b
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public g As New generales, expertos As New Ice_expertos
    Public idexperto As Integer

    Private Sub expertos_refer_a2b_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        idexperto = Session("idexperto")
        llenar_bancos()
        lbl_titular.Text = expertos.nombre_experto(Session("idexperto"))
        If cuenta_datos() > 0 Then
            lbl_encabezado.Text = "Datos bancarios proporcionados para poderte depositar las comisiones que generes por recomendar a otros expertos."
            read_data.Visible = True
            input_data.Visible = False
            datos_experto()
        Else
            lbl_encabezado.Text = "Proporciona la siguiente información para poderte depositar las comisiones que generes por recomendar a otros expertos."
            read_data.Visible = False
            input_data.Visible = True
        End If
    End Sub

    Function llenar_bancos()
        Dim sSQL As String
        sSQL = "select id,nombrebanco from bancos"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        cmb_banco.Items.Add(New ListItem("Banco...", ""))
        While rse.Read
            cmb_banco.Items.Add(New ListItem(rse("nombrebanco"), rse("id")))
        End While
        cne.Close()
    End Function

    Private Sub expertos_refer_a2b_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("index.aspx") : End If
    End Sub

    Private Sub btn_enviar_Click(sender As Object, e As EventArgs) Handles btn_enviar.Click
        Dim sSQL As String
        sSQL = "insert into datos_bank (idexperto,idbanco,clabe) values('" & idexperto & "','" & cmb_banco.SelectedItem.Value & "','" & g.base64_encode(txt_clabe.Text) & "')"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        Response.Redirect("refer-a2b.aspx")
    End Sub

    Function cuenta_datos() As Integer
        Dim sSQL As String
        sSQL = "select count(*) from datos_bank where idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : cuenta_datos = SQLe.ExecuteScalar() : cne.Close()
    End Function

    Function datos_experto()
        Dim sSQL As String
        sSQL = "select datos_bank.idbanco,datos_bank.clabe,bancos.nombrebanco from datos_bank inner join bancos on datos_bank.idbanco=bancos.id  where idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            lbl_banco.Text = rse("nombrebanco")
            lbl_clabe.Text = g.base64_decode(rse("clabe"))
        End While
        cne.Close()
        lbl_read_name.Text = expertos.nombre_experto(idexperto)
    End Function

    Private Sub btn_estrategias_Click(sender As Object, e As EventArgs) Handles btn_estrategias.Click
        Response.Redirect("estrategies.aspx")
    End Sub
End Class
