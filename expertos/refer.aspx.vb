﻿
Partial Class expertos_refer
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto As Integer

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        End If
        idexperto = Session("idexperto")
        cne.ConnectionString = expertos.conectdb
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        refer_inversionistas.Visible = False
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("index.aspx") : End If
    End Sub
End Class
