﻿Option Infer On
Imports System.IO
Imports System.Net
Imports System.Threading.Tasks
Imports MailChimp.Net
Imports Newtonsoft.Json

Partial Class expertos_new_account_invitation
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public origen As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb : cne2.ConnectionString = expertos.conectdb
        origen = g.base64_decode(Request.QueryString("source"))
        If origen <> "IceLkdn" Then : Response.Redirect("index.aspx") : End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Session.Abandon()
        Session.RemoveAll()
        Session.Clear()
        correo_registro.Attributes.Add("type", "email")
        correo_registro.Attributes.Add("placeholder", "correo electrónico")
        correo_registro.Attributes.Add("required", "required")
    End Sub

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL, sSQL2, registro, opcion As String, encontrado, registrado, ya_referido As Integer
        registro = LCase(Trim(correo_registro.Text))
        Session("correo_registro") = registro
        Dim myCookie As HttpCookie = New HttpCookie("attempt")
        myCookie(g.base64_encode("correo")) = g.base64_encode(registro)
        Response.Cookies.Add(myCookie)
        'Evaluar si cumple con los requitos para el registro
        sSQL = "select count(*) from referidos_expertos where referido_email='" & registro & "' and estatus=1"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : encontrado = SQLe.ExecuteScalar : cne.Close()
        If encontrado = 0 Then
            'Inserta la referencia con el Experto LKDN
            '553
            sSQL = "insert into referidos_expertos (experto_refiere,referido_email,fecha_referido,estatus) values('553','" & Trim(LCase(correo_registro.Text)) & "','" & expertos.hora_local & "',1)"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
            'revisar si ya se envuentra registrado como Experto en la plataforma1
            sSQL = "select idexperto from expertos where correo='" & Trim(LCase(correo_registro.Text)) & "'"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open()
            rse = SQLe.ExecuteReader
            If rse.Read Then
                sSQL2 = "update referidos_expertos set estatus=-1 where referido_email='" & Trim(LCase(correo_registro.Text)) & "'"
                SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
                cne2.Open() : SQLe2.ExecuteNonQuery() : cne2.Close()
            End If
            cne.Close()
            encontrado = 1
        End If


        sSQL = "select count(*) from expertos where correo='" & registro & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : registrado = SQLe.ExecuteScalar : cne.Close()

        If encontrado > 0 And registrado = 0 Then
            Response.Redirect("new-account-1.aspx")
        Else
            'Revisa si ya esta registrado
            sSQL = "select count(*) from referidos_expertos where referido_email='" & registro & "' and estatus=2"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : ya_referido = SQLe.ExecuteScalar : cne.Close()

            sSQL = "select count(*) from expertos where correo='" & registro & "'"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : registrado = SQLe.ExecuteScalar : cne.Close()
            If registrado > 0 Or ya_referido > 0 Then
                opcion = "EstaRegistrado"
            Else
                'sSQL = "insert into attempt_registro (fecha,correo) values('" & expertos.hora_local & "','" & registro & "')"
                'SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                'cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
                opcion = "SinInvitacion"
                'AddOrUpdateListMember("us16", "8e4e8013d9f90ca0e02df34423f4b6fd-us16", "3ec8f1431d", registro, expertos.hora_local)
            End If
            Response.Redirect("new-account-no.aspx?msj=" & g.base64_encode(opcion))
        End If

    End Sub

    Private Shared Function AddOrUpdateListMember(dataCenter As String, apiKey As String, listId As String, subscriberEmail As String, subscriberDate As String) As String
        Dim sampleListMember = JsonConvert.SerializeObject(New With {
        Key .email_address = subscriberEmail,
        Key .merge_fields = New With {
            Key .EMAIL = subscriberEmail,
            Key .FECHA = subscriberDate
        },
        Key .status_if_new = "subscribed"
    })

        Dim hashedEmailAddress = If(String.IsNullOrEmpty(subscriberEmail), "", CalculateMD5Hash(subscriberEmail.ToLower()))
        Dim uri = String.Format("https://{0}.api.mailchimp.com/3.0/lists/{1}/members/{2}", dataCenter, listId, hashedEmailAddress)
        Try
            Using webClient = New WebClient()
                webClient.Headers.Add("Accept", "application/json")
                webClient.Headers.Add("Authorization", Convert.ToString("apikey ") & apiKey)

                Return webClient.UploadString(uri, "PUT", sampleListMember)
            End Using
        Catch we As WebException
            Using sr = New StreamReader(we.Response.GetResponseStream())
                Return sr.ReadToEnd()
            End Using
        End Try
    End Function

    Private Shared Function CalculateMD5Hash(input As String) As String
        ' Step 1, calculate MD5 hash from input.
        Dim md5 = System.Security.Cryptography.MD5.Create()
        Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(input)
        Dim hash As Byte() = md5.ComputeHash(inputBytes)
        ' Step 2, convert byte array to hex string.
        Dim sb = New StringBuilder()
        For Each [byte] As Byte In hash
            sb.Append([byte].ToString("X2"))
        Next
        Return sb.ToString()
    End Function

    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Response.Redirect("index.aspx")
    End Sub
End Class
