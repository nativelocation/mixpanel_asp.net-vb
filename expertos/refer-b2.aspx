﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="refer-b2.aspx.vb" Inherits="expertos_refer_b2" %>

<!DOCTYPE html>
<html data-wf-page="58b86132f1c8a96150a3472c" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Refiere y Gana" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-form-wrapper">
    <div class="close" data-ix="close"></div>
    <div class="login-wrapper">
      <div class="login-photo"></div>
      <div class="form w-tabs">
        <div class="login-back w-tab-menu">
          <a class="register-round w-inline-block w-tab-link" data-w-tab="Tab 2">
            <div class="login-text">Regístrate</div>
          </a>
          <a class="login-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
            <div class="login-text">Inicia Sesión</div>
          </a>
        </div>
        <div class="w-tab-content">
          <div class="w-tab-pane" data-w-tab="Tab 2">
            <div class="login-back-bottom register">
              <p class="brown-center-14px">Crea tu cuenta en el portal de Expertos y comienza a registrar tus estrategias</p><a class="button reg w-button" href="new-account.aspx">Regístrate Gratis</a>
            </div>
          </div>
          <div class="w--tab-active w-tab-pane" data-w-tab="Tab 1">
            <div class="login-back-bottom">
              <div class="w-form">
                <form class="login-form" data-name="Email Form" id="email-form" name="email-form">
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="images/Envelope---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Correo Electr Nico 2" id="correo-electr-nico-2" maxlength="256" name="correo-electr-nico-2" placeholder="correo electrónico" required="required" type="email">
                  </div>
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="images/lock---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Contrase A 2" id="contrase-a-2" maxlength="256" name="contrase-a-2" placeholder="contraseña" type="password">
                  </div>
                  <div class="checkbox-field w-checkbox w-clearfix">
                    <input class="checkbox w-checkbox-input" data-name="Checkbox 5" id="checkbox-5" name="checkbox-5" type="checkbox">
                    <label class="small-right-text w-form-label" for="checkbox-5">Recuérdame</label>
                  </div>
                  <input class="button login w-button" data-wait="Please wait..." type="submit" value="Ingresar">
                </form>
                <div class="w-form-done">
                  <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                  <div>Oops! Something went wrong while submitting the form</div>
                </div>
              </div><a class="small-center-text" href="#">¿Olvidaste tu contraseña?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="brown-section padding-top">
    <div class="white-small-wrapper">
      <h3 class="heading-right-24px">Recomienda a Inversionistas</h3>
      <h4 class="heading-left-brown-14px">Recibe $500 pesos por cada persona que recomiendes e invierta 
en cualquier estrategia.</h4>
      <p class="brown-right-14px">Ingresa los correos electrónicos de los expertos que quieres recomendar. (separa las direcciones con comas)</p>
      <div class="w-form">
        <form data-name="Email Form 2" id="email-form-2" name="email-form-2">
          <textarea class="w-input" id="field" maxlength="5000" name="field" placeholder="Example Text"></textarea>
        </form>
        <div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form</div>
        </div>
      </div>
      <div class="buttons-wrapper last-reg top"><a class="small-button w-button wider" href="#">Enviar Invitaciones</a>
      </div>
      <div class="line"></div>
      <div class="small-right-text">Tu correo electronico ni tu alias serán compartidos con ninguna persona.
        <a href="#" class="small-link"></a>
      </div>
      <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
        <div class="menu-container w-container">
          <a class="menu-logo w-nav-brand" href="estrategies.aspx"><img src="images/Logo.svg">
          </a>
          <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="estrategies.aspx">Mis Estrategias</a><a class="nav-link w-nav-link" href="comisions.aspx">Mis Comisiones</a><a class="menu-bar-button w-button" href="refer.aspx">Recomienda y Gana</a>
            <div class="dd w-dropdown" data-delay="0">
              <div class="dd-toogle w-dropdown-toggle">
                <div>aliasxxxxxxx</div>
                <div class="w-icon-dropdown-toggle"></div>
              </div>
              <nav class="dd-list w-dropdown-list"><a class="dd-link w-dropdown-link" href="#">Change password</a><a class="dd-link w-dropdown-link" href="#">Log out</a>
              </nav>
            </div>
          </nav>
          <div class="menu-text mt-mobile">Portal
            <br>de Expertos</div>
          <div class="menu-text mt-hide">Portal de Expertos</div>
          <div class="menu-button w-nav-button">
            <div class="w-icon-nav-menu"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="index.aspx">Inicio</a><a class="footer-link" href="new-account.aspx">Regístrate como Experto</a><a class="footer-link" href="estrategies.aspx">Mi Cuenta</a><a class="footer-link" href="faq.aspx">FAQs</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>