﻿
Partial Class expertos_new_strategy
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        End If
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then
            Response.Redirect("index.aspx")
        End If
        Session.Remove("EstrategiaUser")
        Session.Timeout = 50
    End Sub

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL As String
        If Not String.IsNullOrEmpty(txt_objetivo.Text) Then
            sSQL = "select id,objetivo,rv from objetivos where objetivo='" & txt_objetivo.Text & "' order by id"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open()
            rse = SQLe.ExecuteReader
            While rse.Read
                Session("ne_IDobjetivo") = rse("id")
                Session("ne_objetivo") = rse("objetivo")
                Session("ne_RV") = rse("rv")
            End While
            cne.Close()
            Response.Redirect("new-strategy-2.aspx")
        End If
    End Sub

    Function objetivos()
        Dim sSQL As String
        sSQL = "select id,objetivo,rv from objetivos order by id"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            Response.Write("<div id='" & rse("objetivo") & "' class='estrategia-block'>" & _
            "<div class='brown-center-bold-12px'>" & rse("objetivo") & "</div>" & _
            "<div class='brown-center-11px'>Máximo en RV</div>" & _
            "<div class='ec-gray estr-circle'>" & _
            "<div class='bc-bold brown-center-14px'>" & rse("rv") & "%</div>" & _
            "</div>" & _
            "</div>")
        End While
        cne.Close()
    End Function

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("estrategies.aspx") : End If
        Session("ne_IDobjetivo") = Nothing
        Session("ne_objetivo") = Nothing
        Session("ne_RV") = Nothing
    End Sub
End Class
