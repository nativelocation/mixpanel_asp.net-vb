﻿
Partial Class expertos_new_strategy_4
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idestrategia, idexperto, limite_rv, objetivo, max_rv, rv_acum As Integer
    Public estrategia_name, objetivo_name, accion As String, comision As Double

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then : Response.Redirect("index.aspx") : End If
        If String.IsNullOrEmpty(Request.QueryString("elemento")) Then : Response.Redirect("index.aspx") : End If
        Session.Timeout = 30
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        idexperto = Session("idexperto")
        accion = g.base64_decode(Request.QueryString("type"))
        datos_estrategia()
    End Sub

    Function operadoras()
        Dim sSQL As String, contador As Integer = 1
        sSQL = "select id,nombrecorto from operadora where activado=1 and distribuir=1"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            If contador = 1 Then
                Response.Write("<div class='estrategias-wrapper'>")
            End If
            Response.Write("<div id='" & rse("id") & "' class='eb-trans estrategia-block'><img src='images/operadora/" & rse("nombrecorto") & ".png' width='100%'>")
            'Response.Write("<div class='brown-center-11px'>" & rse("nombrecorto") & "</div>")
            Response.Write("</div>")
            If contador = 5 Then : Response.Write("</div>") : End If
            contador += 1
            If contador > 5 Then : contador = 1 : End If
        End While
        cne.Close()
        If contador > 1 And contador <= 5 Then
            For i = contador To 5
                Response.Write("<div class='estrategia-block-blank'>")
                Response.Write("<div class='brown-center-11px'>&nbsp;</div>")
                Response.Write("</div>")
            Next
            Response.Write("</div>")
        End If
    End Function

    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
        End While
        cne.Close()

        sSQL = "select objetivo from objetivos where id='" & objetivo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            objetivo_name = rse("objetivo")
        End While
        cne.Close()

    End Function

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL As String
        If Not String.IsNullOrEmpty(txt_operadora.Text) Then
            Response.Redirect("new-strategy-5.aspx?elemento=" & Request.QueryString("elemento") & "&dist=" & g.base64_encode(Trim(txt_operadora.Text)) & "&type=" & Request.QueryString("type"))
        End If
    End Sub
End Class
