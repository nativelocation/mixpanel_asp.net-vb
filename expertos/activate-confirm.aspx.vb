﻿
Partial Class expertos_activate_confirm
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public correo1, dominio, correo, sSQL, idexperto As String
    Public g As New generales, expertos As New Ice_expertos

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        correo1 = Request.QueryString("verify")
        dominio = Request.QueryString("accion")
        idexperto = Request.QueryString("tipo")
        If String.IsNullOrEmpty(correo1) Or String.IsNullOrEmpty(dominio) Or String.IsNullOrEmpty(idexperto) Then
            Response.Redirect("index.aspx")
        Else
            cne.ConnectionString = expertos.conectdb
            correo = g.base64_decode(correo1) & "@" & g.base64_decode(dominio)
            If g.IsValidEmail(correo) Then
                Dim sSQL, correo_base As String
                If IsNumeric(g.base64_decode(idexperto)) Then
                    sSQL = "select correo from expertos where idexperto='" & g.base64_decode(idexperto) & "'"
                    SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                    cne.Open() : correo_base = SQLe.ExecuteScalar : cne.Close()
                    If Not String.IsNullOrEmpty(correo_base) Then
                        If LCase(Trim(correo)) = LCase(Trim(correo_base)) Then
                            sSQL = "update expertos set activada=1 where idexperto='" & g.base64_decode(idexperto) & "'"
                            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                            cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
                            Dim experto_refiere As String
                            sSQL = "select experto_refiere from referidos_expertos where referido_email='" & correo & "' and estatus=1"
                            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                            cne.Open() : experto_refiere = SQLe.ExecuteScalar : cne.Close()
                            Session("experto_refiere") = experto_refiere
                            'Se actualiza la tabla de referencias de Expertos
                            sSQL = "update referidos_expertos set estatus=2 where referido_email='" & correo & "' and experto_refiere='" & experto_refiere & "'"
                            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                            cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
                            'Generando sesiones
                            Session("correo_registro") = correo
                            Session("con_login") = 1 : Session("correo_login") = Session("correo_registro")
                            Session("alias_login") = expertos.alias_experto(g.base64_decode(idexperto))
                            Session("idexperto") = g.base64_decode(idexperto)
                        End If
                    Else
                        fail_activate.Visible = True
                        done_activate.Visible = False
                    End If
                Else
                    fail_activate.Visible = True
                    done_activate.Visible = False
                End If
            Else
                fail_activate.Visible = True
                done_activate.Visible = False
            End If
        End If
    End Sub

End Class
