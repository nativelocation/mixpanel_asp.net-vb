﻿Imports System.Data

Partial Class expertos_strategy_fundel
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idestrategia, idexperto, idfondo, idoperacion As Integer, origen, accion As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Dim sSQL As String
        cne.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Request.QueryString("elemento")) Or String.IsNullOrEmpty(Request.QueryString("it")) Then : Response.Redirect("index.aspx") : End If
        Session.Timeout = 30
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        idfondo = g.base64_decode(Request.QueryString("it"))
        accion = g.base64_decode(Request.QueryString("type"))
        idexperto = Session("idexperto")
        CarritoQuitar(idfondo)
        Response.Redirect("new-strategy-3.aspx?elemento=" & Request.QueryString("elemento") & "&type=" & Request.QueryString("type"))
    End Sub

    Protected Sub CarritoQuitar(ByVal idfondo As Integer)
        Try
            Dim tabla As DataTable = Session("EstrategiaUser")
            Dim id As Integer = idfondo
            tabla.Rows.Find(id).Delete()
        Catch ex As Exception
            Response.Write("A ocurrido un error: " & ex.Message)
        End Try
    End Sub

End Class
