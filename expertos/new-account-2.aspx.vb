﻿
Partial Class expertos_new_account_2
    Inherits System.Web.UI.Page

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        End If
        Session.Timeout = 50
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetExpires(DateTime.Now - New TimeSpan(1, 0, 0))
        Response.Cache.SetLastModified(DateTime.Now)
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        Response.Headers.Add("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store")
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("estrategies.aspx") : End If
    End Sub
End Class
