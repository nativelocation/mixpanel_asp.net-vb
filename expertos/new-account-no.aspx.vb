﻿
Partial Class expertos_new_account_no
    Inherits System.Web.UI.Page
    Public opcion As String
    Public expertos As New Ice_expertos, g As New generales

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        opcion = g.base64_decode(Request.QueryString("msj"))
        If opcion = "EstaRegistrado" Then
            YaRegistrado.Visible = True
            Sin_invitar.Visible = False
        End If
        If opcion = "SinInvitacion" Then
            Sin_invitar.Visible = True
            YaRegistrado.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then
            Response.Redirect("index.aspx")
        End If
    End Sub
End Class
