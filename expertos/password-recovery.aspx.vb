﻿
Partial Class expertos_password_recovery
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public g As New generales, expertos As New Ice_expertos

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
    End Sub

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL, correoExperto, mensaje, idexperto As String, SiRegistro As Integer
        'Verificar que el correo existe
        correoExperto = Trim(correo_registro.Text)
        If g.IsValidEmail(correoExperto) Then
            sSQL = "select count(*)registros from expertos where correo='" & correoExperto & "'"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : SiRegistro = SQLe.ExecuteScalar : cne.Close()
            If SiRegistro > 0 Then
                Dim NewPass As String
                sSQL = "select idexperto from expertos where correo='" & correoExperto & "'"
                SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                cne.Open() : idexperto = SQLe.ExecuteScalar : cne.Close()

                NewPass = g.numeros_aleatorios()
                mensaje = "<p>Hemos recibido tu solicitud para recuperar tu contraseña, por lo que te hemos asignado una nueva contraseña para que puedas ingresar a tu cuenta.</p>" &
                "<p>Tu nueva contraseña es: <b>" & NewPass & "</b></p>" &
                "<p>Una vez que hayas ingresado a tu cuenta, puedes cambiar tu contraseña</p>"
                expertos.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Expertos", correoExperto, expertos.nombre_experto(idexperto), "Recupera tu Contraseña", "", mensaje)

                sSQL = "update expertos set pass='" & NewPass & "' where correo='" & correoExperto & "' and idexperto='" & idexperto & "'"
                SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
                form_done.Attributes.Add("style", "display:block;")
                form_fail.Attributes.Add("style", "display:none")
                form_contenido.Visible = False
            Else
                form_done.Attributes.Add("style", "display:none;")
                form_fail.Attributes.Add("style", "display:block")
                form_contenido.Visible = False
                Exit Sub
            End If
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        correo_registro.Attributes.Add("placeholder", "correo electrónico")
        correo_registro.Attributes.Add("required", "required")
    End Sub

    Protected Sub btn_back_Click(sender As Object, e As System.EventArgs) Handles btn_back.Click
        Response.Redirect("password-recovery.aspx")
    End Sub
End Class
