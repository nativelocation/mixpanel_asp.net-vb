﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="expertos_index" %>

<!DOCTYPE html>
<html data-wf-page="5898439cf29bebaa63a0fb16" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-form-wrapper">
    <iframe src="login.aspx" border="0" scrolling="no" style="width:100%; height:100%; border:0; overflow:hidden;"></iframe>      
  </div>
  <div class="main photo-section">
    <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="index.aspx"><img src="images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="new-account.aspx">Regístrate como Experto</a><a class="nav-link w-nav-link" data-ix="open" href="#">Mi Cuenta</a><a class="nav-link w-nav-link" href="faq.aspx">FAQs</a>
        </nav>
        <div class="menu-text">Portal de Expertos</div>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="main-anim" data-ix="main-anim">
      <h1 class="heading-1">¿Eres experto en fondos de inversión?</h1>
      <p class="margin-25px white-center-18px">Gana dinero permitiendo que miles de inversionistas
        <br>repliquen tu forma de invertir</p><a class="button w-button" href="new-account.aspx">Regístrate como Experto</a>
      <div class="small-text">(Solo por invitación)</div>
    </div>
  </div>
  <div class="brown-section video-section">
    <div class="video-wrapper">
      <div class="div-block-5">
        <div class="video w-embed w-video" style="padding-top: 56.17021276595745%;">
          <iframe class="embedly-embed" src="https://www.youtube.com/embed/Lo9oy_I2by0" scrolling="no" frameborder="0" allowfullscreen=""></iframe>
        </div>
      </div>
      <div class="video-info">
        <h1 class="heading-2 video-text">Invierte con Expertos</h1>
        <p class="video-text white-center-14px">Transformamos la industria de fondos de inversión</p>
      </div>
    </div>
  </div>
  <div class="orange-section">
    <h2 class="heading-2">Beneficios</h2>
    <div class="ben-wrapper">
      <div class="ben-block padding-40px"><img src="images/price-tag.svg">
        <h4 class="heading-white-18px">Inscripción Gratis</h4>
        <p class="white-center-14px">Registra gratis tus estrategias&nbsp;
          <br>de inversión</p>
      </div>
      <div class="ben-block padding-40px"><img src="images/smartphone.svg">
        <h4 class="heading-white-18px">Disponbilidad</h4>
        <p class="white-center-14px">100% Online, desde cualquier&nbsp; computadora las 24 horas</p>
      </div>
      <div class="ben-block padding-40px"><img src="images/get-money.svg">
        <h4 class="heading-white-18px">Gana dinero</h4>
        <p class="white-center-14px">Decides cuánto quieres ganar&nbsp; por permitir que te repliquen</p>
      </div>
      <div class="ben-block"><img src="images/user.svg">
        <h4 class="heading-white-18px">Acceso a Inversionistas</h4>
        <p class="white-center-14px">Miles de inversionistas&nbsp; podrán replicarte</p>
      </div>
    </div>
  </div>
  <div class="brown-section">
    <form data-name="Cuanto podrias ganar" id="wf_form_Cuanto_podrias_ganar" name="wf-form-Cuanto-podrias-ganar">
    <div class="cuanto-wrapper" data-ix="cuanto-appear">
      <h2 class="heading-white-28px">Cuánto podrías ganar</h2>
      <h4 class="heading-white-18px">Comisión por Estrategía:</h4>
      <p class="white-center-14px">(entre 0.5% y 1.5% anual sobre el dinero invertido)</p>
      <div class="w-form">
        <input class="field w-input" data-name="%" id="cuanto_comision" maxlength="256" placeholder="% de comisión" type="text">
      </div>
      <h4 class="heading-white-18px">Dinero en tus Estrategias:</h4>
      <p class="white-center-14px">(dinero de todas las personas que replican estrategias)</p>
      <div class="w-form">
        <input class="field w-input" data-name="%" id="cuanto_estrategias" maxlength="256" placeholder="Importe (ej. 25000)" type="text">
      </div>
      <div class="price-wrapper">
        <div class="price-bold-text">Calcular</div>
        <%--<div class="question-mark" data-ix="qm-wrapper-appear">?</div>
        <div class="question-mark-block">
          <p class="_12px brown-center-14px">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
        </div>--%>
      </div>
    </div>
    </form>
  </div>
  <div class="como-funciona photo-section">
    <h2 class="heading-2">Cómo funciona en 5 pasos</h2>
    <div class="pasos-wrapper">
      <div class="pasos-block">
        <div class="step-block">
          <div class="step-number">1</div>
        </div>
        <h4 class="heading-white-18px">Regístrate</h4>
        <p class="white-center-14px">Inscríbete gratis en la plataforma</p>
      </div>
      <div class="pasos-block">
        <div class="step-block">
          <div class="step-number">2</div>
        </div>
        <h4 class="heading-white-18px">Crea tus Estrategias</h4>
        <p class="white-center-14px">Miles de personas las podrán conocer,
          <br>comparar y replicar</p>
      </div>
      <div class="pasos-block">
        <div class="step-block">
          <div class="step-number">3</div>
        </div>
        <h4 class="heading-white-18px">Promueve</h4>
        <p class="white-center-14px">Comparte tus estrategias
          <br>con tus amigos y familiares</p>
      </div>
      <div class="bottom-30px pasos-block">
        <div class="step-block">
          <div class="step-number">4</div>
        </div>
        <h4 class="heading-white-18px">Monitorea</h4>
        <p class="white-center-14px">Revisa tus estrategias y cámbialas
          <br>cuando lo consideres necesario</p>
      </div>
      <div class="bottom-30px pasos-block">
        <div class="step-block">
          <div class="step-number">5</div>
        </div>
        <h4 class="heading-white-18px">Gana</h4>
        <p class="white-center-14px">Recibe comisiones de las personas
          <br>que replican tus estrategias</p>
      </div>
    </div>
  </div>
  <div class="brown-section">
    <h2 class="heading-2">Estamos cambiando la forma de invertir</h2>
    <div class="estamos-wrapper">
      <div class="estamos-block">
        <div class="estamos-top">
          <h4 class="estamos heading-white-18px">Confidencialidad</h4>
        </div>
        <p class="brown-center-14px">Tus datos personales son confidenciales, los inversionistas te conocerán por tu alias (nombre de usuario)</p>
      </div>
      <div class="estamos-block">
        <div class="estamos-top">
          <h4 class="estamos heading-white-18px">Apertura de Cuentas</h4>
        </div>
        <p class="brown-center-14px">Cuando un inversionista seleccione tu estrategia, nosotros nos encargamos de apertura su contrato y de que pueda replicar tu estrategia</p>
      </div>
      <div class="bottom estamos-block">
        <div class="estamos-top">
          <h4 class="estamos heading-white-18px">Comisión de por vida</h4>
        </div>
        <p class="brown-center-14px">Recibes comisiones perpetuas,  ya que mientras las personas continúen invirtiendo su dinero con tus estrategias,  tú recibes comisiones</p>
      </div>
      <div class="bottom estamos-block">
        <div class="estamos-top">
          <h4 class="estamos heading-white-18px">Protección</h4>
        </div>
        <p class="brown-center-14px">Solamente las personas que invierten y copian tus estrategias conocen su composición detallada, ningún otro experto puede copiarte</p>
      </div>
    </div>
  </div>
  <div class="opinan photo-section">
    <h2 class="heading-2">Los Expertos opinan</h2>
    <div class="opinan-wrapper">
      <div class="opinan-block" data-ix="opinan-appear">
        <div class="photo-border"><img class="photo-opinan" height="80" src="images/Ophoto-1.png" width="80">
        </div>
        <h4 class="heading-brown-18px">fede22</h4>
        <p class="brown-center-14px">Excelente plataforma, gracias por permitirme ganar dinero haciendo lo que me gusta</p>
      </div>
      <div class="opinan-block" data-ix="opinan-appear-2">
        <div class="photo-border"><img class="photo-opinan" height="80" src="images/Ophoto-2.png" width="80">
        </div>
        <h4 class="heading-brown-18px">virginiaA1</h4>
        <p class="brown-center-14px">Son serios y profesionales,&nbsp; los felicito por la plataforma</p>
      </div>
      <div class="opinan-block" data-ix="opinan-appear-3">
        <div class="photo-border"><img class="photo-opinan" height="80" src="images/Ophoto-3.png" width="80">
        </div>
        <h4 class="heading-brown-18px">JoseCastro</h4>
        <p class="brown-center-14px">El mejor lugar para los que&nbsp; sabemos de fondos, mis&nbsp; conocimientos me generan&nbsp; ingresos
        </p>
      </div>
    </div>
  </div>
  <div class="brown-section">
    <div class="conoces-wrapper">
      <h2 class="heading-white-28px less-margin">¿Conoces a otros expertos?</h2>
      <h4 class="heading-white-18px">Recomienda y Gana</h4>
        <p class="margin-more white-center-14px">Por cada experto que recomiendes, tú ganas <strong>200 pesos</strong> y los expertos <strong>200 pesos</strong><br />
        Gana el <strong>10%</strong> de las comisiones que generen los expertos que tú recomiendes</p><a class="button w-button" href="new-account-1.aspx">Regístrate como Experto</a>
    </div>
    <h2 class="heading-white-28px less-margin">Cuota por Inversionista</h2>
    <p class="space white-center-14px">Te cobramos una cuota única de <strong>35 pesos por cada inversionista</strong> que quiera replicar tu estrategia, que serán destinados a la apertura  del contrato de inversión en la instrucción financiera (solicitud de documentos, generación de contactos). Este pago será descontado de tus primeras comisiones.</p>
  </div>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="index.aspx">Inicio</a><a class="footer-link" href="new-account.aspx">Regístrate como Experto</a><a class="footer-link" data-ix="open" href="#">Mi Cuenta</a><a class="footer-link" href="faq.aspx">FAQs</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <script>
      $(document).ready(function () {
          $('.price-wrapper').css("cursor", "pointer");
          function addCommas(nStr) {
              nStr += '';
              x = nStr.split('.');
              x1 = x[0];
              x2 = x.length > 1 ? '.' + x[1] : '';
              var rgx = /(\d+)(\d{3})/;
              while (rgx.test(x1)) {
                  x1 = x1.replace(rgx, '$1' + ',' + '$2');
              }
              return x1 + x2;
          }
          function CuantoComision() {
              $('.price-wrapper').css("cursor", "pointer");
              var comision = $('#cuanto_comision').val();
              var dineros = $('#cuanto_estrategias').val();
              if ((comision >= 0.5 && comision <= 1.5) && (dineros > 0)) {
                  var calculos = (((dineros * comision) / 100) * (.70)) / 12;
                  calculos = calculos.toFixed(2);
                  $('.price-bold-text').html("$" + addCommas(calculos) + " al mes");
              } else {
                  if (comision && dineros) {
                      if ((comision < 0.5 || comision > 1.5) && (dineros <= 0)) {
                          alert("Se debe indicar una comisión entre 0.5% y 1.5%, y un importe mayor a $0 pesos");
                      } else if (comision < 0.5 || comision > 1.5) {
                          alert("Se debe indicar una comisión entre 0.5% y 1.5%");
                      } else if (dineros <= 0) {
                          alert("Se debe indicar un importe mayor a $0 pesos");
                      } else {
                          alert("Se debe indicar valores numéricos para realizar el cálculo");
                      }
                  } else {
                      $('.price-wrapper').css("cursor", "pointer");
                      $('.price-bold-text').html("Calcular");
                  }
              }
          }
          $('#cuanto_comision').change(function () { CuantoComision(); });
          $('#cuanto_estrategias').change(function () { CuantoComision(); });
          $('#cuanto_comision').keyup(function () {
              $('.price-wrapper').css("cursor", "pointer");
              $('.price-bold-text').html("Calcular");
          });
          $('#cuanto_estrategias').keyup(function () {
              $('.price-wrapper').css("cursor", "pointer");
              $('.price-bold-text').html("Calcular");
          });
          $('.price-wrapper').click(function () {
              CuantoComision();
          });
      });
  </script>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>