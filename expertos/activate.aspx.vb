﻿
Partial Class expertos_activate
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public correo1, dominio, correo, sSQL, idexperto As String
    Public g As New generales, expertos As New Ice_expertos

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        correo1 = Request.QueryString("verify")
        dominio = Request.QueryString("type")
        idexperto = Request.QueryString("accion")
        If String.IsNullOrEmpty(correo1) Or String.IsNullOrEmpty(dominio) Or String.IsNullOrEmpty(idexperto) Then
            Response.Redirect("index.aspx")
        Else
            cne.ConnectionString = expertos.conectdb
            correo = g.base64_decode(correo1) & "@" & g.base64_decode(dominio)
            If g.IsValidEmail(correo) Then
                Dim sSQL, correo_base As String, activada As Integer
                If IsNumeric(g.base64_decode(idexperto)) Then
                    sSQL = "select correo,activada from expertos where idexperto='" & g.base64_decode(idexperto) & "'"
                    SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                    cne.Open()
                    rse = SQLe.ExecuteReader
                    While rse.Read
                        activada = rse("activada")
                        correo_base = rse("correo")
                    End While
                    cne.Close()
                    If Not String.IsNullOrEmpty(correo_base) Then
                        If LCase(Trim(correo)) = LCase(Trim(correo_base)) Then
                            If activada = 1 Then
                                'Muestra en pantalla la advertencia que indica se puede realizar la acción de activación
                                done_activate.Visible = False
                                fail_activate.Visible = True
                                lbl_advertencia.Text = "Tú cuenta ya ha sido activada previamente"
                            Else
                                Session("correo_registro") = correo
                                Session("con_login") = 1 : Session("correo_login") = Session("correo_registro")
                                Session("alias_login") = expertos.alias_experto(g.base64_decode(idexperto))
                                Session("idexperto") = g.base64_decode(idexperto)
                            End If
                        Else
                            'Muestra en pantalla la advertencia que indica se puede realizar la acción de activación
                            done_activate.Visible = False
                            fail_activate.Visible = True
                            lbl_advertencia.Text = "No se puede realizar la activación solicitada"
                        End If
                    Else
                        'Muestra en pantalla la advertencia que indica se puede realizar la acción de activación
                        done_activate.Visible = False
                        fail_activate.Visible = True
                        lbl_advertencia.Text = "No se puede realizar la activación solicitada"
                    End If
                Else
                    'Muestra en pantalla la advertencia que indica se puede realizar la acción de activación
                    done_activate.Visible = False
                    fail_activate.Visible = True
                    lbl_advertencia.Text = "No se puede realizar la activación solicitada"
                End If
            Else
                'Muestra en pantalla la advertencia que indica se puede realizar la acción de activación
                done_activate.Visible = False
                fail_activate.Visible = True
                lbl_advertencia.Text = "No se puede realizar la activación solicitada"
            End If
        End If


    End Sub
End Class
