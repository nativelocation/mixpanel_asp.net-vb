﻿Imports FusionCharts.Charts

Partial Class expertos_estrategies
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto, seguidores As Integer

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        End If
        idexperto = Session("idexperto")
        cne.ConnectionString = expertos.conectdb
        cne2.ConnectionString = expertos.conectdb
        Dim rnd As New Random
        seguidores = 0
        mis_estrategias()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Session.Remove("EstrategiaUser")
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("index.aspx") : End If
        strategias_fail()
        Session.Timeout = 50
    End Sub

    Function mis_estrategias()
        Dim sSQL, sSQL2, fecha, idestrategia, sHTML As String, counter As Integer
        sSQL = "select fechaact from actualizacion where id=4"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : fecha = Format(CDate(SQLe.ExecuteScalar), "yyyy-MM-dd") : cne.Close()
        sSQL = "select a.* from asesorias a inner join clientes c on a.idcliente=c.idcliente where a.idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        counter = 1
        While rse.Read
            idestrategia = rse("idasesoria")
            Dim myGraphArea, EstrategiesArea As New Literal
            myGraphArea.ID = "strategy" & counter
            sHTML = "<div class='graphic-wrapper'>"
            sHTML &= "<div class='graphic-menu'>"
            sHTML &= "<div class='graphic-heading header-btn' data-ix='' data-ixEstrategia='" & counter & "' >" & rse("clave") & "</div>"
            sHTML &= "<div class='graphic-menu-points'>"
            'Selecciona el estatus de la estrategia dentro de la tabla de publicación
            Dim estado_publicacion As Integer, strEstatus As String
            sSQL2 = "select t1.estatus,t1.idestrategia,t1.fecha,t1.idpublicacion " & _
            "from (select Rank() over(order by idestrategia)NroRank,row_number() over(order by idestrategia,fecha desc)NroRow," & _
            "idestrategia,fecha,idpublicacion,estatus from publicacion_paquete)t1 where t1.NroRank=t1.NroRow and t1.idestrategia='" & idestrategia & "'"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            estado_publicacion = SQLe2.ExecuteScalar
            cne2.Close()
            sHTML &= "<a class='graphic-menu-point w-inline-block promover-btn' data-ix='' data-ixEstrategia='" & counter & "' href='#'><img class='menu-icon' src='images/Grid---simple-line-icons.svg' width='16'>"
            sHTML &= "<div class='graphic-menu-text'>Promover</div>"
            sHTML &= "</a>"
            sHTML &= "<a class='graphic-menu-point w-inline-block composition-btn' data-ix='' data-ixEstrategia='" & counter & "' href='#'><img class='menu-icon' src='images/composition.svg' width='16'>"
            sHTML &= "<div class='graphic-menu-text'>Composición</div>"
            sHTML &= "</a>"
            If estado_publicacion = 1 Then
                strEstatus = "En Actualización"
                sHTML &= "<a class='graphic-menu-point w-inline-block valid-mod' data-ix='fondo-open' data-ixEstrategia='" & rse("clave") & "' href='#'><img class='menu-icon' src='images/settings.svg' width='16'>"
            End If
            If estado_publicacion = 2 Then
                strEstatus = "Publicada"
                sHTML &= "<a class='graphic-menu-point w-inline-block valid-mod-pub' data-ix='fondo-open' data-ixEstrategia='" & g.base64_encode(idestrategia) & "' href='#'><img class='menu-icon' src='images/settings.svg' width='16'>"
            End If
            sHTML &= "<div class='graphic-menu-text' >Modificar</div>"
            sHTML &= "</a>"
            sHTML &= "<a class='graphic-menu-point w-inline-block cambios-btn' data-ix='' data-ixEstrategia='" & counter & "' href='#'><img class='menu-icon' src='images/Graph---simple-line-icons.svg' width='16'>"
            sHTML &= "<div class='graphic-menu-text'>Cambios</div>"
            sHTML &= "</a>"
            sHTML &= "<a class='graphic-menu-point w-inline-block operaciones-btn' data-ix='' data-ixEstrategia='" & counter & "' href='#'><img class='menu-icon' src='images/Layers---simple-line-icons.svg' width='16'>"
            sHTML &= "<div class='graphic-menu-text'>Operaciones</div>"
            sHTML &= "</a>"
            sHTML &= "<a class='graphic-menu-point w-inline-block del-estrategy' data-ix='fondo-open' data-ixEstrategia='" & g.base64_encode(idestrategia) & "' href='#'><img class='menu-icon' src='images/garbage.svg' width='16'>"
            sHTML &= "<div class='graphic-menu-text'>Eliminar</div>"
            sHTML &= "</a>"
            sHTML &= "</div>"
            sHTML &= "</div>"

            sHTML &= "<div class='graphic'>"
            sHTML &= "<div class='graphic-section' id='SE" & counter & "'>"
            sHTML &= "<div class='stats-wrapper'>"
            sHTML &= "<div class='stat-block'><div class='info-text it-small'>Estatus:<div class='info-price ip-14px'>" & strEstatus & "</div></div></div>"
            sHTML &= "<div class='stat-block'><div class='info-text it-small'>Fecha de alta:<div class='info-price ip-14px'>" & g.ConvierteFecha2(rse("fecha")) & "</div></div></div>"
            sHTML &= "<div class='stat-block'><div class='info-text it-small'>Comisión:</div><div class='info-price ip-14px'>" & FormatNumber(rse("comision")) & "%</div></div>"
            Dim sObjetivo As String
            sSQL2 = "select id,objetivo from objetivos where id='" & rse("perfil2") & "'"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                sObjetivo = rse2("id") & " " & rse2("objetivo")
            End While
            cne2.Close()
            sHTML &= "<div class='stat-block'><div class='info-text it-small'>Objetivo:<div class='info-price ip-14px'>" & sObjetivo & "</div></div></div></div>"
            '///////////////////////////////////////
            Dim xmlData2 As New StringBuilder(), fechatmp As String
            Dim maximo, minimo As Integer
            maximo = 135000
            minimo = 80000
            xmlData2.Append("<chart caption='' subcaption='' lineThickness='4' showValues='0'  showPercentValues='1' xAxisName='' yAxisName='' ")
            xmlData2.Append("basefontcolor='FFFFFF' canvasBgAlpha='0' bgColor='403936' bgAlpha='0' borderAlpha='0' showBorder='0' showAlternateVGridColor='0' ")
            xmlData2.Append("DivlineThickness='1' canvasBorderAlpha='0' lineColor='e07941' toolTipColor='403936' xAxisNameFontSize='6' ")
            xmlData2.Append("formatNumberScale='0' anchorRadius='0' divLineAlpha='20' divLineColor='FFFFFF' divLineIsDashed='0' showAlternateHGridColor='0' numberPrefix='$' ")
            xmlData2.Append("numdivlines='10' numvdivlines='0' adjustDiv='0' yAxisValueDecimals='0' yAxisMaxValue='" & maximo & "' yAxisMinValue='" & minimo & "' decimals='3' forcedecimals='3' labelDisplay='ROTATE' slantLabels='1'> ")
            Dim hay_posicion As Integer
            sSQL2 = "select count(fecha)registros from posicion_cliente where idcliente='" & idestrategia & "'"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open() : hay_posicion = SQLe2.ExecuteScalar : cne2.Close()

            sSQL2 = "select fecha from posicion_cliente where idcliente='" & idestrategia & "' order by fecha"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                fechatmp = rse2("fecha")
                xmlData2.Append("<set label='" & fechatmp & "' value='" & posicion_un_dia(fechatmp, idestrategia) & "'/>")
            End While
            cne2.Close()
            xmlData2.Append("</chart>")
            ' Create the chart
            Dim draws As New Chart
            ' Setting chart id
            draws.SetChartParameter(Chart.ChartParameter.chartId, "Estrategia" & counter)
            ' Setting chart type to Column 3D chart
            draws.SetChartParameter(Chart.ChartParameter.chartType, "line")
            ' Setting chart width to 800px
            draws.SetChartParameter(Chart.ChartParameter.chartWidth, "100%")
            ' Setting chart height to 500px
            draws.SetChartParameter(Chart.ChartParameter.chartHeight, "400px")
            ' Setting chart background color 
            draws.SetChartParameter(Chart.ChartParameter.bgColor, "transparent")
            ' Setting chart data as XML String 
            draws.SetData(xmlData2.ToString)
            ' Render chart
            myGraphArea.Text = draws.Render()
            If hay_posicion > 0 Then
                sHTML &= myGraphArea.Text
            Else
                sHTML &= "<div style='font-size:smaller; color:gray; height:400px; padding-top:125px; text-align:center;'>Tu estrategia se publicará en 24 horas hábiles siguientes a su fecha de alta, a partir de este momento empezaremos a contabilizar las plusvalías/minusvalías que genere durante el tiempo.</div>"
            End If
            '///////////////////////////////////////
            sHTML &= "</div>"
            sHTML &= "<div class='stats-wrapper'>"
            sHTML &= "<div class='stat-block top'>"
            Dim rnd As New Random()
            sHTML &= "<div class='info-price ip-18px'>$" & FormatNumber(0, 2) & "</div>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>La cantidad de dinero que está invertido en cuentas de inversión de diferentes inversionistas y que está replicando tus estrategias.</p>"
            sHTML &= "</div>"
            sHTML &= "<div class='info-question-wrapper iq-mobile'>"
            sHTML &= "<div class='info-text it-small'>Recursos</div><img class='question-icon' data-ix='popup-appear' height='16' src='images/Question-Icon_1.svg' width='16'>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>La cantidad de dinero que está invertido en cuentas de inversión de diferentes inversionistas y que está replicando tus estrategias.</p>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='stat-block'>"
            sHTML &= "<div class='info-price ip-18px'>" & 0 & "</div>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>Cantidad de personas que tienen su dinero invertido en tus estrategias.</p>"
            sHTML &= "</div>"
            sHTML &= "<div class='info-question-wrapper iq-mobile'>"
            sHTML &= "<div class='info-text it-small'>Seguidores</div><img class='question-icon' data-ix='popup-appear' height='16' src='images/Question-Icon_1.svg' width='16'>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>Cantidad de personas que tienen su dinero invertido en tus estrategias.</p>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='stat-block'>"
            Dim RendimientoDacum As Double
            sSQL2 = "select isnull(rendimientoDacum,0)rendimientoDacum from posicion_cliente where idcliente=" & idestrategia & " and fecha='" & fecha & "'"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open() : RendimientoDacum = SQLe2.ExecuteScalar : cne2.Close()
            sHTML &= "<div class='info-price ip-18px'>" & FormatNumber(RendimientoDacum, 2) & "&nbsp;%</div>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>Plusvalía o minusvalía que ha generado tu estrategia desde que la diste de alta.</p>"
            sHTML &= "</div>"
            sHTML &= "<div class='info-question-wrapper iq-mobile'>"
            sHTML &= "<div class='info-text it-small'>Rendimiento</div><img class='question-icon' data-ix='popup-appear' height='16' src='images/Question-Icon_1.svg' width='16'>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>Plusvalía o minusvalía que ha generado tu estrategia desde que la diste de alta.</p>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='stat-block'>"
            sHTML &= "<div class='info-price ip-18px'>$0.00</div>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>Cantidad de dinero que han generado las personas que invierten y replican tu estrategia.</p>"
            sHTML &= "</div>"
            sHTML &= "<div class='info-question-wrapper iq-mobile'>"
            sHTML &= "<div class='info-text it-small'>Plusvalía/Minusvalía</div><img class='question-icon' data-ix='popup-appear' height='16' src='images/Question-Icon_1.svg' width='16'>"
            sHTML &= "<div class='popup-wrapper'>"
            sHTML &= "<p>Cantidad de dinero que han generado las personas que invierten y replican tu estrategia.</p>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "</div>"

            sHTML &= "<div id='promover-wrapper_" & counter & "' class='promover-wrapper'>"
            sHTML &= "<div class='info-text' style='height:400px; padding:180px;color:gray; text-align:center;'>Proximamente</div>"
            sHTML &= "</div>"

            sHTML &= "<div id='composition-wrapper_" & counter & "' class='composition-wrapper'>"
            sHTML &= "<div class='estrrr table-block tb-integrate'>"
            sHTML &= "<div class='table-heading'>"
            sHTML &= "<h3 class='table-heading'>Composición de " & rse("clave") & "</h3>"
            sHTML &= "<div class='table-date'>" & g.ConvierteFecha2(fecha) & "</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-underline'></div>"
            sHTML &= "<div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table'>"
            sHTML &= "<div class='head-string table-string'>"
            sHTML &= "<div class='table-item ti-first'>"
            sHTML &= "<div>%</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-item ti-second'>"
            sHTML &= "<div>Fondo</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-item ti-third'>"
            sHTML &= "<div>Operadora</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-item ti-fourth'>"
            sHTML &= "<div>Rendimiento</div>"
            sHTML &= "</div>"
            'sHTML &= "<div class='_1231 table-item ti-fourth'>"
            'sHTML &= "<div>Pluslavía</div>"
            'sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-underline'></div>"
            '/////// Presentar la composicion
            '// Obtener el importe total de fondos
            Dim total_fondos As Double
            sSQL2 = "select isnull(sum(pos.importe),0)importe " & _
            "from (select pos.idfondo,pos.idoperacion,sum(pos.titulos)titulos,(sum(pos.titulos)*pf.precio) as importe,pf.nombrefondo,pf.precio,op.nombrecorto " & _
            "from (((select cyv.idfondo,cyv.idoperacion,case when cyv.tipoO=2 then (sum(cyv.importeNcod))*-1 else sum(cyv.importeNcod) end as importeNcod," & _
            "case when cyv.tipoO=2 then (sum(cyv.tituloscod))*-1 else sum(cyv.tituloscod) end as titulos,cyv.tipoO " & _
            "from cyv where idcliente='" & idestrategia & "' and idfondo<>'1442' and estatus=4  and fechaLcod<='" & fecha & "' " & _
            "group by cyv.idfondo,cyv.idoperacion,cyv.tipoO)pos inner join posicion_fondo pf on pos.idoperacion=pf.idoperacion " & _
            "and pf.idcliente='" & idestrategia & "' and pos.idfondo=pf.idfondo) inner join fondo f on pos.idfondo=f.id) inner join operadora op on f.idoperadora=op.id " & _
            "where pf.fecha='" & fecha & "' group by pos.idfondo,pos.idoperacion,pf.nombrefondo,pf.precio,op.nombrecorto)pos"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open() : total_fondos = SQLe2.ExecuteScalar : cne2.Close()

            sSQL2 = "select sum(pf.importe) as ImporteOld,sum(pf.ImporteNew) as ImporteNew,pf.idfondo,sum(pf.porcentaje) as porcentaje,f.nombrefondo," & _
            "sum(pf.porcentajediafondo) as porcentajediafondo,f.idoperadora,isnull(rendimientodacum,0) as RendimientoD,isnull(rendimientoaacum,0) as RendimientoA " & _
            "from posicion_fondo pf inner join fondo f on pf.idfondo=f.id " & _
            "where pf.idcliente='" & idestrategia & "' and pf.fecha='" & fecha & "' and pf.idfondo<>'1442' " & _
            "group by pf.idfondo,f.nombrefondo,f.idoperadora,pf.rendimientodacum,pf.rendimientoaacum"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                sHTML &= "<div class='table-string ts-first'>"
                sHTML &= "<div class='table-item ti-first'>"
                sHTML &= "<div>" & FormatNumber(rse2("porcentajediafondo"), 2) & "%</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item ti-second'>"
                sHTML &= "<div>" & UCase(rse2("nombrefondo")) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item ti-third'>"
                sHTML &= "<div>" & UCase(expertos.operadora_nombrecorto(rse2("idoperadora"))) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item'>"
                sHTML &= "<div>" & FormatNumber(rse2("RendimientoA"), 2) & "%</div>"
                sHTML &= "</div>"
                'sHTML & ="<div class='_1231 table-item ti-fourth'>"
                'sHTML & ="<div>$25,250</div>"
                'sHTML & ="</div>"
                sHTML &= "</div>"
            End While
            cne2.Close()
            sHTML &= "</div>"
            sHTML &= "</div>"
            'mobile section
            sHTML &= "<div class='mobile table-block tbb-trans'>"
            sHTML &= "<div>"
            sHTML &= "<h3 class='table-heading'>Composición de " & rse("clave") & "</h3>"
            sHTML &= "</div>"
            sHTML &= "<div class='mobile-table'>" '--2
            sHTML &= "<div class='mobile-table-string mt-heading-string'>" '--3
            sHTML &= "<div>" & g.ConvierteFecha2(fecha) & "</div>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-underline'></div>"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>%:</div>"
                sHTML &= "<div class='mt-info'>" & FormatNumber(rse2("porcentajediafondo"), 0) & "%</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Fondo:</div>"
                sHTML &= "<div class='mt-info'>" & UCase(rse2("nombrefondo")) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Operadora:</div>"
                sHTML &= "<div class='mt-info'>" & UCase(expertos.operadora_nombrecorto(rse2("idoperadora"))) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Rendimiento:</div>"
                sHTML &= "<div class='mt-info'>" & FormatNumber(rse2("RendimientoA"), 2) & "%</div>"
                sHTML &= "</div>"
            End While
            cne2.Close()
            sHTML &= "</div>"
            sHTML &= "<div class='table-underline'></div>"
            sHTML &= "</div>"
            sHTML &= "</div>"
            sHTML &= "<div id='cambios-wrapper_" & counter & "' class='cambios-wrapper'>" '--1
            sHTML &= "<div class='estrrr table-block tb-integrate'>" '--2
            sHTML &= "<div class='table-heading'>"
            sHTML &= "<h3 class='table-heading'>Cambios de " & rse("clave") & "</h3>"
            sHTML &= "</div>" 'table-heading
            sHTML &= "<div class='table-underline'></div>"
            sHTML &= "<div></div>"
            sHTML &= "<div class='table'>"
            sHTML &= "<div class='head-string table-string'>"
            'Item para la cabecera de la tabla
            sHTML &= "<div class='table-item ti-first'><div>Fecha</div></div>"
            sHTML &= "<div class='table-item ti-second'><div>Estatus</div></div>"
            sHTML &= "<div class='table-item ti-third'><div>&nbsp;</div></div>"
            'Fin Items cabecera
            sHTML &= "</div>" 'table-string

            sHTML &= "<div class='table-underline'></div>"
            '------------------------------------------------------------------------------------
            sSQL2 = "select idpublicacion,fecha,estatus from publicacion_paquete where idestrategia='" & idestrategia & "' order by fecha desc"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                sHTML &= "<div class='table-string ts-first'>"
                sHTML &= "<div class='table-item ti-first'>"
                sHTML &= "<div>" & g.ConvierteFecha2(rse2("fecha")) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item ti-second'>"
                Dim estatus_publicacion As String
                If rse2("estatus") = 1 Then : estatus_publicacion = "En Actualización" : End If
                If rse2("estatus") = 2 Then : estatus_publicacion = "Publicada" : End If
                sHTML &= "<div>" & estatus_publicacion & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item ti-third'>"
                sHTML &= "<div><a href='#' class='table-link'>Detalle</a></div>"
                sHTML &= "</div>"
                sHTML &= "</div>"
            End While
            cne2.Close()
            '------------------------------------------------------------------------------------
            sHTML &= "</div>" 'Class: Table
            sHTML &= "</div>" '--2
            '******************************************************************************************************
            'mobile section
            sHTML &= "<div class='mobile table-block tbb-trans'>"
            sHTML &= "<div>"
            sHTML &= "<h3 class='table-heading'>Cambios " & rse("clave") & "</h3>"
            sHTML &= "</div>"
            sHTML &= "<div class='mobile-table'>" '--2
            sHTML &= "<div class='mobile-table-string mt-heading-string'>" '--3
            sHTML &= "</div>"
            sHTML &= "<div class='table-underline'></div>"
            '####
            sSQL2 = "select idpublicacion,fecha,estatus from publicacion_paquete where idestrategia='" & idestrategia & "' order by fecha desc"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Fecha:</div>"
                sHTML &= "<div class='mt-info'>" & g.ConvierteFecha2(rse2("fecha")) & "</div>"
                sHTML &= "</div>"
                Dim estatus_publicacion As String
                If rse2("estatus") = 1 Then : estatus_publicacion = "En Actualización" : End If
                If rse2("estatus") = 2 Then : estatus_publicacion = "Publicada" : End If
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Estatus:</div>"
                sHTML &= "<div class='mt-info'>" & estatus_publicacion & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Ver:</div>"
                sHTML &= "<div class='mt-info'><a href='#' class='table-link'>Detalle</a></div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-underline'></div>"
            End While
            cne2.Close()
            '####
            sHTML &= "</div>"
            sHTML &= "</div>"
            'End Mobile section
            '******************************************************************************************************
            sHTML &= "</div>" '--1
            sHTML &= "<div id='operaciones-wrapper_" & counter & "' class='operaciones-wrapper'>" '--1
            sHTML &= "<div class='estrrr table-block tb-integrate'>" '--2
            sHTML &= "<div class='table-heading'>"
            sHTML &= "<h3 class='table-heading'>Operaciones " & rse("clave") & "</h3>"
            sHTML &= "</div>" 'table-heading
            sHTML &= "<div class='table-underline'></div>"
            sHTML &= "<div></div>"
            sHTML &= "<div class='table'>"
            sHTML &= "<div class='head-string table-string'>"
            'Item para la cabecera de la tabla
            sHTML &= "<div class='table-item ti-first'><div>Solicitud - Liquidación</div></div>"
            sHTML &= "<div class='table-item ti-second' style='width:60%;'><div>Operación</div></div>"
            sHTML &= "<div class='table-item ti-third'><div>Porcentaje</div></div>"
            'Fin Items cabecera
            sHTML &= "</div>" 'table-string

            sHTML &= "<div class='table-underline'></div>"
            '------------------------------------------------------------------------------------
            sSQL2 = "select C.idcyv, C.idcliente, C.estatus, C.contratofm, C.nombrefondo, C.tipo1, C.tipo2, isnull(C.tipoV,'')tipoV, isnull(C.tipoAj,'')tipoAj, C.importeB, C.fechaE, C.fechaL, C.modalidad, C.ver, " & _
            "C.moneda, C.idsolicitud, C.bancoclabe, isnull(C.ajuste,'')ajuste from cyv C where idcliente='" & idestrategia & "' and ver=1 order by C.fechaE desc"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                Dim tipo1, tipo2, tipox, tipoV, tipoAj, bancoclabe As String
                bancoclabe = rse2("bancoclabe")
                If (rse2("tipo1") = "3" And rse2("tipo2") = "1") Then
                    rse2.Read()
                End If
                tipo1 = rse2("tipo1") : tipo2 = rse2("tipo2") : tipoAj = rse2("tipoAj") : tipoV = rse2("tipoV")
                If tipo1 = "1" Then tipox = "COMPRA"
                If tipo1 = "2" Then tipox = "VENTA"
                If tipo1 = "2" And tipoV = "1" Then tipox = "VENTA TOTAL"
                If tipo1 = "3" And tipo2 = "1" Then tipox = "DEPOSITO"
                If tipo1 = "3" And tipo2 = "2" Then tipox = "COMPRA"
                If tipo1 = "4" And tipo2 = "1" Then tipox = "VENTA"
                If tipo1 = "4" And tipo2 = "3" And tipoAj = "" Then tipox = "RETIRO"
                If tipo1 = "4" And tipo2 = "3" And tipoAj = "1" Then
                    If IsDBNull(rse2("ajuste")) Then
                        tipox = "AJUSTE A FAVOR"
                    ElseIf Trim(rse2("ajuste")) = "(sin comentarios del asesor)" Then
                        tipox = "AJUSTE A FAVOR"
                    Else
                        tipox = Trim(rse2("ajuste"))
                    End If
                End If
                If tipo1 = "4" And tipo2 = "3" And tipoAj = "2" Then
                    If IsDBNull(rse2("ajuste")) Then
                        tipox = "AJUSTE EN CONTRA"
                    ElseIf Trim(rse2("ajuste")) = "(sin comentarios del asesor)" Then
                        tipox = "AJUSTE EN CONTRA"
                    Else
                        tipox = Trim(rse2("ajuste"))
                    End If
                End If
                sHTML &= "<div class='table-string ts-first'>"
                sHTML &= "<div class='table-item ti-first'>"
                sHTML &= "<div>" & g.ConvierteFecha2(rse2("fechaE")) & "-" & g.ConvierteFecha2(rse2("fechaL")) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item ti-second' style='width:60%;'>"
                sHTML &= "<div>" & tipox & "&nbsp;DE&nbsp;" & rse2("nombrefondo") & "&nbsp;<br></div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-item ti-third'>"
                sHTML &= "<div><a class='table-link'>" & FormatNumber(((rse2("importeB") / 100000) * 100), 0) & "%</a></div>"
                sHTML &= "</div>"
                sHTML &= "</div>"
            End While
            cne2.Close()
            '------------------------------------------------------------------------------------
            sHTML &= "</div>" 'Class: Table
            sHTML &= "</div>" '--2
            '****************************************
            'mobile section
            sHTML &= "<div class='mobile table-block tbb-trans'>"
            sHTML &= "<div>"
            sHTML &= "<h3 class='table-heading'>Operaciones " & rse("clave") & "</h3>"
            sHTML &= "</div>"
            sHTML &= "<div class='table-underline'></div>"
            sHTML &= "<div class='mobile-table'>" '--2
            sHTML &= "<div class='mobile-table-string mt-heading-string'>" '--3
            sHTML &= "</div>"
            '####
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                Dim tipo1, tipo2, tipox, tipoV, tipoAj, bancoclabe As String
                bancoclabe = rse2("bancoclabe")
                If (rse2("tipo1") = "3" And rse2("tipo2") = "1") Then
                    rse2.Read()
                End If
                tipo1 = rse2("tipo1") : tipo2 = rse2("tipo2") : tipoAj = rse2("tipoAj") : tipoV = rse2("tipoV")
                If tipo1 = "1" Then tipox = "COMPRA"
                If tipo1 = "2" Then tipox = "VENTA"
                If tipo1 = "2" And tipoV = "1" Then tipox = "VENTA TOTAL"
                If tipo1 = "3" And tipo2 = "1" Then tipox = "DEPOSITO"
                If tipo1 = "3" And tipo2 = "2" Then tipox = "COMPRA"
                If tipo1 = "4" And tipo2 = "1" Then tipox = "VENTA"
                If tipo1 = "4" And tipo2 = "3" And tipoAj = "" Then tipox = "RETIRO"
                If tipo1 = "4" And tipo2 = "3" And tipoAj = "1" Then
                    If IsDBNull(rse2("ajuste")) Then
                        tipox = "AJUSTE A FAVOR"
                    ElseIf Trim(rse2("ajuste")) = "(sin comentarios del asesor)" Then
                        tipox = "AJUSTE A FAVOR"
                    Else
                        tipox = Trim(rse2("ajuste"))
                    End If
                End If
                If tipo1 = "4" And tipo2 = "3" And tipoAj = "2" Then
                    If IsDBNull(rse2("ajuste")) Then
                        tipox = "AJUSTE EN CONTRA"
                    ElseIf Trim(rse2("ajuste")) = "(sin comentarios del asesor)" Then
                        tipox = "AJUSTE EN CONTRA"
                    Else
                        tipox = Trim(rse2("ajuste"))
                    End If
                End If
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Solicitud - Liquidación:</div>"
                sHTML &= "<div class='mt-info'>" & g.ConvierteFecha2(rse2("fechaE")) & " - " & g.ConvierteFecha2(rse2("fechaL")) & "</div>"
                sHTML &= "</div>"
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Operación:</div>"
                sHTML &= "<div class='mt-info'>" & tipox & "&nbsp;DE&nbsp;" & rse2("nombrefondo") & "&nbsp;<br></div>"
                sHTML &= "</div>"
                sHTML &= "<div class='mobile-table-string'>"
                sHTML &= "<div class='mt-label'>Porcentaje:</div>"
                sHTML &= "<div class='mt-info'><a class='table-link'>" & FormatNumber(((rse2("importeB") / 100000) * 100), 0) & "%</a></div>"
                sHTML &= "</div>"
                sHTML &= "<div class='table-underline'></div>"
            End While
            cne2.Close()
            '####
            sHTML &= "</div>"
            sHTML &= "</div>"
            'End Mobile section
            '****************************************
            sHTML &= "</div>" '--1
            sHTML &= "</div>"

            EstrategiesArea.Text = sHTML
            estrategies_zone.Controls.Add(EstrategiesArea)
            counter += 1
        End While
        cne.Close()
    End Function

    Function strategias_fail()
        Dim sSQL As String
        'Limpia la tabla asesorias
        sSQL = "delete asesorias where idasesoria in (select a.idasesoria from asesorias a left join clientes c on a.idcliente=c.idcliente where a.idexperto='" & idexperto & "' and c.idcliente is null)"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        'Limpia la tabla Operaciones
        sSQL = "delete operaciones where idasesoria in (select distinct op.idasesoria from operaciones op left join asesorias a on (a.idasesoria=op.idasesoria and a.idcliente=op.idcliente) where op.idexperto='" & idexperto & "' and a.idasesoria is null)"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
    End Function

    Function recursos_totales() As Double
        Dim sSQL, sSQL2, fechatmp As String
        sSQL = "select max(fecha)fecha from posicion_cliente"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : fechatmp = SQLe.ExecuteScalar : cne.Close()
        sSQL = "select isnull(sum(isnull(pc.saldoconef,0)),0)recursos from clientes c left join posicion_cliente pc on c.idcliente=pc.idcliente where c.idexperto='" & idexperto & "' and pc.fecha='" & fechatmp & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : recursos_totales = SQLe.ExecuteScalar : cne.Close()
    End Function

    Function cuantas_estrategias() As Integer
        Dim sSQL As String
        sSQL = "select count (c.idcliente)estrategias from clientes c inner join asesorias a on a.idasesoria=c.idcliente where a.idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : cuantas_estrategias = SQLe.ExecuteScalar : cne.Close()
    End Function

    Function habiles(ByVal fecha As String) As Integer
        Dim vSQLx As String, encontrado As Integer
        Dim cn As New Data.SqlClient.SqlConnection
        Dim SQL1 As Data.SqlClient.SqlCommand
        Dim rs As Data.SqlClient.SqlDataReader
        cn.ConnectionString = expertos.conectdb
        If Weekday(fecha) = 1 Or Weekday(fecha) = 7 Then
            habiles = 0
        Else
            vSQLx = "Select count(diafestivo) as EXPR from diasfestivos where diafestivo='" & fecha & "'"
            SQL1 = New Data.SqlClient.SqlCommand(vSQLx, cn)
            cn.Open()
            encontrado = SQL1.ExecuteScalar
            cn.Close()
            If encontrado = 1 Then
                habiles = 0
            Else
                habiles = 1
            End If
        End If
    End Function

    Function posicion_un_dia(ByVal fechap As String, ByVal idcliente As String) As Double
        Dim cny As New Data.SqlClient.SqlConnection
        Dim SQLy As Data.SqlClient.SqlCommand
        Dim rsy As Data.SqlClient.SqlDataReader
        Dim vSQLy, fechaptmp As String
        Dim total_fondos, importeefectivo, importe As Double
        cny.ConnectionString = expertos.conectdb
        'Verifica si es habil
        If habiles(fechap) = 0 Then
            fechaptmp = fechaCalculadaV(fechap, 1)
        Else
            fechaptmp = fechap
        End If
        'Se obtiene la posicion total en fondos
        vSQLy = "select isnull(sum(pos.importe),0)importe " & _
        "from (select pos.idfondo,pos.idoperacion,sum(pos.titulos)titulos,(sum(pos.titulos)*pf.precio) as importe,pf.nombrefondo,pf.precio,op.nombrecorto " & _
        "from (((select cyv.idfondo,cyv.idoperacion,case when cyv.tipoO=2 then (sum(cyv.importeNcod))*-1 else sum(cyv.importeNcod) end as importeNcod," & _
        "case when cyv.tipoO=2 then (sum(cyv.tituloscod))*-1 else sum(cyv.tituloscod) end as titulos,cyv.tipoO " & _
        "from cyv where idcliente='" & idcliente & "' and idfondo<>'1442' and estatus=4  and fechaLcod<='" & fechaptmp & "' " & _
        "group by cyv.idfondo,cyv.idoperacion,cyv.tipoO)pos inner join posicion_fondo pf on pos.idoperacion=pf.idoperacion " & _
        "and pf.idcliente='" & idcliente & "' and pos.idfondo=pf.idfondo) inner join fondo f on pos.idfondo=f.id) inner join operadora op on f.idoperadora=op.id " & _
        "where pf.fecha='" & fechaptmp & "' group by pos.idfondo,pos.idoperacion,pf.nombrefondo,pf.precio,op.nombrecorto)pos"
        SQLy = New Data.SqlClient.SqlCommand(vSQLy, cny)
        cny.Open() : total_fondos = SQLy.ExecuteScalar : cny.Close()
        
        'Se indica el importe en efectivo de las operaciones liquidadas
        vSQLy = "select pos.idfondo,sum(importeNcod)importe,pf.nombrefondo " & _
        "from (((select cyv.idfondo,cyv.idoperacion,case when cyv.tipoO=2 then (sum(cyv.importeNcod))*-1 else sum(cyv.importeNcod) end as importeNcod, " & _
        "case when cyv.tipoO=2 then (sum(cyv.tituloscod))*-1 else sum(cyv.tituloscod) end as titulos,cyv.tipoO " & _
        "from cyv where idcliente='" & idcliente & "' and idfondo='1442' and estatus=4 and fechaLcod<='" & fechaptmp & "' " & _
        "group by cyv.idfondo,cyv.idoperacion,cyv.tipoO)pos inner join posicion_fondo pf on pos.idoperacion=pf.idoperacion " & _
        "and pf.idcliente='" & idcliente & "' and pos.idfondo=pf.idfondo) inner join fondo f on pos.idfondo=f.id) inner join operadora op on f.idoperadora=op.id " & _
        "where pf.fecha='" & fechaptmp & "' group by pos.idfondo,pf.nombrefondo"
        SQLy = New Data.SqlClient.SqlCommand(vSQLy, cny)
        cny.Open()
        rsy = SQLy.ExecuteReader
        While rsy.Read
            importeefectivo = CDbl(rsy("importe"))
        End While
        cny.Close()
        importe = importeefectivo + total_fondos
        posicion_un_dia = importe
    End Function


    Function fechaCalculadaV(ByVal fechaI As String, ByVal dias As Integer) As String
        Dim fecha1, cadenafecha, fechaq As String, combinacionfecha, validacion, cuentadias, encontrado As Integer
        fecha1 = fechaI
        combinacionfecha = dias
        Dim cnx As New Data.SqlClient.SqlConnection
        Dim SQLx As Data.SqlClient.SqlCommand
        Dim rsx As Data.SqlClient.SqlDataReader
        Dim vSQLx As String
        cnx.ConnectionString = expertos.conectdb
        validacion = 0
        Do While validacion = 0
            cuentadias = 1
            Do While cuentadias <= CInt(combinacionfecha)
                fecha1 = DateAdd("d", -1, fecha1)
                cadenafecha = Month(fecha1) & "/" & Day(fecha1) & "/" & Year(fecha1)
                fecha1 = CDate(cadenafecha)
                fechaq = Month(fecha1) & "/" & Day(fecha1) & "/" & Year(fecha1)
                vSQLx = "Select count(diafestivo) as EXPR from diasfestivos where diafestivo='" & fechaq & "'"
                SQLx = New Data.SqlClient.SqlCommand(vSQLx, cnx)
                cnx.Open() : encontrado = SQLx.ExecuteScalar : cnx.Close()
                If Weekday(fecha1) = 1 Or Weekday(fecha1) = 7 Or encontrado = 1 Then
                Else
                    cuentadias = cuentadias + 1
                End If
            Loop
            validacion = 1
        Loop
        fechaCalculadaV = fecha1
    End Function

    Function validate_estrategies()
        If cuantas_estrategias() >= 5 Then
            Response.Write("<a class='small-button w-button deny-new-strategy' data-ix='fondo-open'  href='#'>Alta nueva Estrategia</a>")
        Else
            Response.Write("<a class='small-button w-button' href='new-strategy.aspx'>Alta nueva Estrategia</a>")
        End If
    End Function

    Function cuenta_datos() As Integer
        Dim sSQL As String
        sSQL = "select count(*) from datos_bank where idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : cuenta_datos = SQLe.ExecuteScalar() : cne.Close()
    End Function

End Class
