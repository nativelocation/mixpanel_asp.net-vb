﻿
Partial Class expertos_new_account_1a
    Inherits System.Web.UI.Page
    Public g As New generales, expertos As New Ice_expertos
    Public correo As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Dim cadena(), parte1, part2, url, idexperto, mensaje As String
        correo = Session("correo_login")
        If String.IsNullOrEmpty(Session("correo_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        Else
            cadena = Split(correo, "@", 2)
            idexperto = Session("idexperto")
            url = "http://invierteconexpertos.mx/expertos/activate.aspx?verify=" & g.base64_encode(cadena(0)) & "&accion=" & g.base64_encode(idexperto) & "&type=" & g.base64_encode(cadena(1))
            mensaje = "<p>Bienvenido a Invierte con Expertos. Para poder comenzar a dar de alta tus estrategias, te pedimos que confirmes tu dirección de email. " & _
            "De este modo mejoramos la seguridad de tu cuenta y te aseguras de que podrás recuperar " & _
            "tu contraseña si llegaras a olvidarla. </p><p><a href='" & url & "'>Haz clic aquí para activar tu cuenta</a></p>"
            expertos.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Expertos", correo, expertos.nombre_experto(idexperto), "Bienvenido a Invierte con Expertos", "", mensaje)
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("new-account.aspx") : End If
    End Sub
End Class
