﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="activate-confirm.aspx.vb" Inherits="expertos_activate_confirm" %>

<!DOCTYPE html>
<html data-wf-page="589c50e12c4095a15f067a83" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Activa tu cuenta - Confirmación" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div id="done_activate" style="padding: 20px; box-shadow: none; text-align:center; margin: 0 auto;" runat="server">
      <h3 class="heading-right-24px">Tu cuenta ha sido activada satisfactoriamente.</h3>
      <p class="brown-right-14px">A partir de ahora puedes registrar tu estrategias. <br />Podrás registrar hasta 5 estrategias.</p>
      <div class="buttons-wrapper last-reg" style="    margin-left: auto; margin-right: auto;"><a class="small-button w-button" href="new-strategy.aspx" target="_top">Registra tu Estrategia</a><a class="lineal small-button w-button" href="refer.aspx" target="_top">Recomienda y Gana</a>
      </div>
      <p class="brown-right-14px">Gana por recomendar a otros expertos</p>
  </div>
  <div id="fail_activate" style="padding: 20px; box-shadow: none; text-align:center; margin: 0 auto;" runat="server" visible="false">
      <h3 class="heading-right-24px">No se puede realizar la acción solicitada.</h3>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>