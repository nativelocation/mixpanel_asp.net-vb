﻿
Partial Class expertos_new_strategy_5
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idestrategia, idexperto, idoperadora, objetivo, max_rv As Integer
    Public estrategia_name, objetivo_name, nombre_operadora, accion As String, comision As Double

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb : cne2.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then : Response.Redirect("index.aspx") : End If
        If String.IsNullOrEmpty(Request.QueryString("elemento")) Or String.IsNullOrEmpty(Request.QueryString("dist")) Then : Response.Redirect("index.aspx") : End If
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        idexperto = Session("idexperto")
        idoperadora = g.base64_decode(Request.QueryString("dist"))
        accion = g.base64_decode(Request.QueryString("type"))
        datos_estrategia()
        nombre_operadora = expertos.operadora_nombre(idoperadora)
    End Sub

    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
        End While
        cne.Close()

        sSQL = "select objetivo from objetivos where id='" & objetivo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            objetivo_name = rse("objetivo")
        End While
        cne.Close()
    End Function

    Function fondos_operadora(ByVal operadora As String)
        Dim sSQL, rendimiento, calificacion As String, contador As Integer = 1
        sSQL = "select f.id,f.nombrefondo,f.sia_idtipofondo,tf.tipofondo,cf.clasificacion,f.rend360a,cal.tipocalificacion,r.tipoRiesgo " & _
        "from (((fondo f left join sia_tipofondo tf on f.sia_idtipofondo=tf.id) left join sia_clasificacion cf on f.sia_idclasificacion=cf.idclasificacion) " & _
        "left join calificacion cal on f.idcalificacion=cal.id)left join riesgo r on f.idriesgo=r.id " & _
        "where f.idoperadora = '" & operadora & "' and (f.distribuir=1 and f.activado=1 and f.ICE_vista=1) And (f.sia_idpersona1 = 1 Or f.sia_idpersona2 = 1 Or f.sia_idpersona3 = 1) " & _
        "order by f.nombrefondo,f.serie"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            If rse("sia_idtipofondo") = 2 Then
                calificacion = "Máximo de Renta Variable: <strong>" & FormatNumber(rv_fondo(rse("id")), 0) & "%</strong>"
            Else
                calificacion = "Calificación: <strong>" & rse("tipocalificacion") & "/" & rse("tipoRiesgo") & "</strong>"
            End If
            Response.Write("<div class='santander-block sb-margin'>")
            Response.Write("<div class='brown-bold-14px'>" & rse("nombrefondo") & "</div>")
            Response.Write("<div class='lblock line'></div>")
            Response.Write("<div class='brown-12px'>" & rse("tipofondo") & "")
            Response.Write("</div>")
            Response.Write("<div class='brown-12px'>" & rse("clasificacion") & "</div>")
            If IsDBNull(rse("rend360a")) Then : rendimiento = "N/D" : Else : rendimiento = FormatNumber(rse("rend360a"), 2) & "%" : End If
            Response.Write("<div class='brown-12px'>Últimos 365 días: <strong>" & rendimiento & "</strong>")
            Response.Write("</div>")
            Response.Write("<div class='blast brown-12px'>" & calificacion & "</div>" & _
            "<a class='lineal sbl-left small-button w-button' href='new-strategy-6.aspx?elemento=" & Request.QueryString("elemento") & "&dist=" & Request.QueryString("dist") & "&itf=" & g.base64_encode(rse("id")) & "&type=" & Request.QueryString("type") & "'>Ver más</a>")
            Response.Write("</div>")
        End While
        cne.Close()
    End Function

    Function rv_fondo(ByVal idfondo As Integer) As Integer
        Dim sSQL As String, maxrv As Integer
        sSQL = "select isnull(max_rv,0)max_rv from fondo where id='" & idfondo & "'"
        SQLe2 = New Data.SqlClient.SqlCommand(sSQL, cne2)
        cne2.Open() : maxrv = SQLe2.ExecuteScalar : cne2.Close()
        rv_fondo = maxrv
    End Function
End Class
