﻿Imports FusionCharts.Charts
Imports System.Text

Partial Class expertos_new_strategy_6
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idestrategia, idexperto, idoperadora, objetivo, max_rv, idfondo As Integer
    Public estrategia_name, objetivo_name, nombre_operadora As String, comision, p_deuda, p_rv As Double
    Public tipofondo, clasificacion, calificacion, liquidez, fondo, fechaact, sFecha As String
    Public rend30, rend60, rend90, rend_anterior, rend_actual As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb : cne2.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then : Response.Redirect("index.aspx") : End If
        If String.IsNullOrEmpty(Request.QueryString("elemento")) Or String.IsNullOrEmpty(Request.QueryString("dist")) Then : Response.Redirect("index.aspx") : End If
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        idexperto = Session("idexperto")
        idoperadora = g.base64_decode(Request.QueryString("dist"))
        idfondo = g.base64_decode(Request.QueryString("itf"))
        datos_estrategia() : detalle_fondo()
        nombre_operadora = expertos.operadora_nombre(idoperadora)
    End Sub

    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
        End While
        cne.Close()

        sSQL = "select objetivo from objetivos where id='" & objetivo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            objetivo_name = rse("objetivo")
        End While
        cne.Close()
    End Function

    Function detalle_fondo()
        Dim sSQL As String
        sSQL = "select fechaact from actualizacion where id=4"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : fechaact = SQLe.ExecuteScalar : cne.Close()
        sFecha = g.funFechaNombre(fechaact)

        sSQL = "select f.id,f.nombrefondo,tf.tipofondo,cf.clasificacion,tf.id as sia_tipofondo," & _
        "f.rend30a,f.rend60a,f.rend90a,f.rend360a,f.renda,f.rend" & Year(Now()) - 1 & ",cal.tipocalificacion,r.tipoRiesgo," & _
        "f.administrador2 as sol_compra,f.administrador1 as sol_venta,f.administrador4 as oper_compra," & _
        "f.administrador3 as oper_venta,f.administrador6 as liq_compra,f.administrador5 as liq_venta " & _
        "from (((fondo f left join sia_tipofondo tf on f.sia_idtipofondo=tf.id) left join sia_clasificacion cf on f.sia_idclasificacion=cf.idclasificacion) " & _
        "left join calificacion cal on f.idcalificacion=cal.id)left join riesgo r on f.idriesgo=r.id " & _
        "where (f.idoperadora = " & idoperadora & " And f.distribuir = 1 And f.activado = 1 And f.id =" & idfondo & ") " & _
        "order by f.nombrefondo,f.serie"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            fondo = rse("nombrefondo")
            tipofondo = rse("tipofondo")
            clasificacion = rse("clasificacion")
            If rse("sia_tipofondo") = 2 Then
                calificacion = "Máximo de Renta Variable: <strong>" & FormatNumber(rv_fondo(rse("id")), 0) & "%</strong>"
            Else
                calificacion = "Calificación: <strong>" & rse("tipocalificacion") & "/" & rse("tipoRiesgo") & "</strong>"
            End If

            liquidez = rse("liq_venta")
            If IsDBNull(rse("rend30a")) Then : rend30 = "N/D" : Else : rend30 = FormatNumber(rse("rend30a"), 2) : End If
            If IsDBNull(rse("rend60a")) Then : rend60 = "N/D" : Else : rend60 = FormatNumber(rse("rend60a"), 2) : End If
            If IsDBNull(rse("rend90a")) Then : rend90 = "N/D" : Else : rend90 = FormatNumber(rse("rend90a"), 2) : End If
            If IsDBNull(rse("renda")) Then : rend_actual = "N/D" : Else : rend_actual = FormatNumber(rse("renda"), 2) : End If
            If IsDBNull(rse("rend" & Year(Now()) - 1 & "")) Then : rend_anterior = "N/D" : Else : rend_anterior = FormatNumber(rse("rend" & Year(Now()) - 1 & ""), 2) : End If
        End While
        cne.Close()
    End Function

    Function top_instrumentos()
        Dim sSQL, nombreemisora, clave As String, contador, idfondocar As Integer

        'Obtener el idfondo de carteras
        sSQL = "select clave from fondo where id='" & idfondo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : clave = SQLe.ExecuteScalar : cne.Close()
        sSQL = "select top 1 id from fondo where clave='" & clave & "' order by serie"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : idfondocar = SQLe.ExecuteScalar : cne.Close()

        sSQL = "select top 10 idemisora,nombreemisora,totalemisora,porcentaje " & _
        "from (select c.idemisora,e.nombreemisora,sum(c.valortotal)totalemisora,gt.totalcartera,((sum(c.valortotal)/totalcartera)*100)porcentaje " & _
        "from (carteras c inner join (select '" & idfondocar & "' as idfondo,sum(valortotal)totalcartera from carteras " & _
        "where fechaperiodo='" & expertos.periodo_carteras & "' and idfondo='" & idfondocar & "')gt " & _
        "on c.idfondo=gt.idfondo) " & _
        "left join emisora e on c.idemisora=e.idemisora where c.idfondo='" & idfondocar & "' and c.fechaperiodo='" & expertos.periodo_carteras & "' " & _
        "group by c.idemisora,e.nombreemisora,gt.totalcartera)t1 order by t1.porcentaje desc"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        Response.Write("<div class='table'>")
        While rse.Read
            If IsDBNull(rse("nombreEmisora")) Then : nombreemisora = "N/D" : Else : nombreemisora = UCase(rse("nombreemisora")) : End If
            If contador = 0 Then : Response.Write("<div class='table-string ts-first'>") : Else : Response.Write("<div class='table-string'>") : End If
            Response.Write("<div class='ns1 table-item'>")
            Response.Write("<div>" & nombreemisora & "</div>")
            Response.Write("</div>")
            Response.Write("<div class='table-item table-right'>")
            Response.Write("<div>" & FormatNumber(rse("porcentaje"), 2) & "%</div>")
            Response.Write("</div>")
            Response.Write("</div>")
            contador += 1
        End While
        Response.Write("</div>")
        cne.Close()
    End Function

    Function cartera_fondo()
        Dim sSQL, nombreInstrumento As String, total As Double, contador As Integer
        total = total_fondo()
        sSQL = "select t1.idTipoIntrumento,t1.nombreIntrumento,((parcial/total) *100) as porcentaje " & _
        "from (select distinct carteras.idTipoIntrumento,intrumentoCartera.nombreIntrumento,sum(isnull(carteras.valorTotal,0)) parcial, (" & total & ") as total " & _
        "from (carteras inner join fondo on fondo.id = carteras.idFondo) inner join intrumentoCartera " & _
        "on carteras.idTipoIntrumento = intrumentoCartera.idIntrumento " & _
        "where carteras.idfondo='" & idfondo & "' and carteras.fechaPeriodo ='" & expertos.periodo_carteras & "' " & _
        "Group by carteras.idTipoIntrumento,intrumentoCartera.nombreIntrumento)t1 "
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        Response.Write("<div class='table'>")
        While rse.Read
            If IsDBNull(rse("nombreIntrumento")) Then : nombreInstrumento = "N/D" : Else : nombreInstrumento = UCase(rse("nombreIntrumento")) : End If
            If contador = 0 Then : Response.Write("<div class='table-string ts-first'>") : Else : Response.Write("<div class='table-string'>") : End If
            Response.Write("<div class='ns1 table-item'>")
            Response.Write("<div>" & nombreInstrumento & "</div>")
            Response.Write("</div>")
            Response.Write("<div class='table-item table-right'>")
            Response.Write("<div>" & FormatNumber(rse("porcentaje"), 2) & "%</div>")
            Response.Write("</div>")
            Response.Write("</div>")
            contador += 1
        End While
        Response.Write("</div>")
        cne.Close()
    End Function

    Function grafica_fondo_precio()
        Dim maximo, minimo, preciof As Double
        Dim bandera As Integer
        Dim sSQL, fecha2, fechatmp As String
        Dim cn As New Data.SqlClient.SqlConnection
        Dim SQL As Data.SqlClient.SqlCommand
        Dim rs As Data.SqlClient.SqlDataReader
        Dim xmlData2 As New StringBuilder()
        cn.ConnectionString = expertos.conectdb

        sSQL = "select max(anio.precio)preciomax from " & _
        "(select top 365 fechaold,precio from splitprecios where  idfondo=" & idfondo & " order by fechaold desc)anio "
        SQL = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open()
        maximo = SQL.ExecuteScalar
        cn.Close()
        maximo = maximo * 1.1

        sSQL = "select min(anio.precio)preciomax from " & _
        "(select top 365 fechaold,precio from splitprecios where  idfondo=" & idfondo & " order by fechaold desc)anio "
        SQL = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open()
        minimo = SQL.ExecuteScalar
        cn.Close()
        minimo = minimo - (minimo * 0.1)

        xmlData2.Append("<chart caption='' subcaption='' lineThickness='4' showValues='0'  showPercentValues='1' xAxisName='' yAxisName='' ")
        xmlData2.Append("basefontcolor='FFFFFF' canvasBgAlpha='0' bgColor='403936' bgAlpha='0' borderAlpha='0' showBorder='0' showAlternateVGridColor='0' ")
        xmlData2.Append("DivlineThickness='1' canvasBorderAlpha='0' lineColor='e07941' toolTipColor='403936' ")
        xmlData2.Append("formatNumberScale='0' anchorRadius='0' divLineAlpha='20' divLineColor='FFFFFF' divLineIsDashed='0' showAlternateHGridColor='0' numberPrefix='$' ")
        xmlData2.Append("numdivlines='5' numvdivlines='0' adjustDiv='0' yAxisValueDecimals='2' yAxisMaxValue='" & maximo & "' yAxisMinValue='" & minimo & "' decimals='3' forcedecimals='3' labelDisplay='ROTATE' slantLabels='1'> ")

        sSQL = "select anio.fechaold,anio.precio from " & _
        "(select top 250 fechaold,precio from splitprecios where  idfondo='" & idfondo & "' order by fechaold desc)anio " & _
        "order by anio.fechaold "
        SQL = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open()
        rs = SQL.ExecuteReader
        While rs.Read
            fecha2 = g.funnombremes(rs("fechaold"))
            preciof = FormatNumber(rs("precio"), 3)
            If bandera = 0 Then
                fechatmp = fecha2
                bandera = 1
            ElseIf bandera = 1 Then
                If fecha2 = fechatmp Then
                    xmlData2.Append("<set label='" & fecha2 & "' value='" & preciof & "'/>")
                Else
                    xmlData2.Append("<set label='" & fecha2 & "' value='" & preciof & "'/>")
                    bandera = 0
                End If
            End If
        End While
        cn.Close()
        xmlData2.Append("</chart>")
        ' Create the chart
        Dim draws As New Chart
        ' Setting chart id
        draws.SetChartParameter(Chart.ChartParameter.chartId, "desempeno")
        ' Setting chart type to Column 3D chart
        draws.SetChartParameter(Chart.ChartParameter.chartType, "line")
        ' Setting chart width to 800px
        draws.SetChartParameter(Chart.ChartParameter.chartWidth, "100%")
        ' Setting chart height to 500px
        draws.SetChartParameter(Chart.ChartParameter.chartHeight, "400px")
        ' Setting chart background color 
        draws.SetChartParameter(Chart.ChartParameter.bgColor, "transparent")
        ' Setting chart data as XML String 
        draws.SetData(xmlData2.ToString)
        ' Render chart
        Literal1.Text = draws.Render()
    End Function

    Function composicion_fondo()
        Dim sSQL, sDeuda, sRentaV As String, total As Double
        total = total_fondo()
        'Consulta empleando la cartera
        'sSQL = "select sum(((parcial/total) *100)) as porcentaje,t1.deuda_renta " & _
        '"from (select distinct sum(isnull(carteras.valorTotal,0)) parcial, (" & total & ") as total ,carteras.idTipoValor,tv.deuda_renta " & _
        '"from ((carteras inner join fondo on fondo.id = carteras.idFondo) inner join intrumentoCartera " & _
        '"on carteras.idTipoIntrumento = intrumentoCartera.idIntrumento) left join TipoValor tv on carteras.idTipoValor=tv.idtipoValor " & _
        '"where carteras.idfondo='" & idfondo & "' and carteras.fechaPeriodo ='" & expertos.periodo_carteras & "' " & _
        '"Group by carteras.idTipoValor,tv.deuda_renta) t1 group by t1.deuda_renta"
        'SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        'cne.Open()
        'rse = SQLe.ExecuteReader
        'While rse.Read
        '    If rse("deuda_renta") = 1 Then
        '        p_deuda = FormatNumber(rse("porcentaje"), 0)
        '        If p_deuda < 10 Then : sDeuda = "0" & p_deuda : Else : sDeuda = p_deuda : End If
        '    End If
        '    If rse("deuda_renta") = 2 Then
        '        p_rv = FormatNumber(rse("porcentaje"), 0)
        '        If p_rv < 10 Then : sRentaV = "0" & p_rv : Else : sRentaV = p_rv : End If
        '    End If
        'End While
        'cne.Close()

        'Consulta con el maximo en RV
        sSQL = "select isnull(max_rv,0)max_rv from fondo where id='" & idfondo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            p_deuda = FormatNumber((100 - rse("max_rv")), 0)
            If p_deuda < 10 Then : sDeuda = "0" & p_deuda : Else : sDeuda = p_deuda : End If
            
            p_rv = FormatNumber(rse("max_rv"), 0)
            If p_rv < 10 Then : sRentaV = "0" & p_rv : Else : sRentaV = p_rv : End If
        End While
        cne.Close()

        Dim xmlData2 As New StringBuilder
        xmlData2.Append("<chart palettecolors='#c7c7c7,#e07941' enableMultiSlicing='0' ")
        xmlData2.Append("bgcolor='#ffffff' bgAlpha='0' showborder='0' use3dlighting='0' showshadow='0' enablesmartlabels='0' startingangle='50' ")
        xmlData2.Append("showlabels='0' alignLegendWithCanvas='1' legendAllowDrag='0' showValues='0' ")
        xmlData2.Append("showpercentvalues='1' showlegend='0' legendBgAlpha='0' legendshadow='0' legendborderalpha='0' showtooltip='0' decimals='0' ")
        xmlData2.Append("usedataplotcolorforlabels='1'>")
        xmlData2.Append("<set label='Deuda' value='" & sDeuda & "' isSliced='1' />")
        xmlData2.Append("<set label='Renta Variable' value='" & sRentaV & "' />")
        xmlData2.Append("</chart>")

        ' Create the chart
        Dim draws As New Chart
        ' Setting chart id
        draws.SetChartParameter(Chart.ChartParameter.chartId, "composicion")
        ' Setting chart type to Column 3D chart
        draws.SetChartParameter(Chart.ChartParameter.chartType, "doughnut2d")
        ' Setting chart width to 800px
        draws.SetChartParameter(Chart.ChartParameter.chartWidth, "300px")
        ' Setting chart height to 500px
        draws.SetChartParameter(Chart.ChartParameter.chartHeight, "200px")
        ' Setting chart background color 
        draws.SetChartParameter(Chart.ChartParameter.bgColor, "transparent")
        ' Setting chart data as XML String 
        draws.SetData(xmlData2.ToString)
        ' Render chart
        Literal2.Text = draws.Render() 

    End Function

    Function total_fondo() As Double
        Dim sSQL As String
        sSQL = "select isnull(sum(t1.suma),0)val_total from (select distinct carteras.idTipoIntrumento,sum(isnull(carteras.valorTotal,0)) SUMA " & _
        "from (carteras inner join fondo on fondo.id = carteras.idFondo) inner join intrumentoCartera " & _
        "on carteras.idTipoIntrumento = intrumentoCartera.idIntrumento " & _
        "where carteras.idfondo='" & idfondo & "' and carteras.fechaPeriodo ='" & expertos.periodo_carteras & "' " & _
        "Group by carteras.idTipoIntrumento)t1"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : total_fondo = SQLe.ExecuteScalar : cne.Close()
    End Function

    Function rv_fondo(ByVal idfondo As Integer) As Integer
        Dim sSQL As String, maxrv As Integer
        sSQL = "select isnull(max_rv,0)max_rv from fondo where id='" & idfondo & "'"
        SQLe2 = New Data.SqlClient.SqlCommand(sSQL, cne2)
        cne2.Open() : maxrv = SQLe2.ExecuteScalar : cne2.Close()
        rv_fondo = maxrv
    End Function

End Class
