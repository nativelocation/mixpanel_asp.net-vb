﻿
Partial Class expertos_strategy_mod
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public EstrategiaName, idestrategia, accion, estrategia_name, idexperto, fecha_estrategia As String
    Public objetivo, max_rv As Integer, comision As Double

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Request.QueryString("elemento")) Then : Response.Redirect("index.aspx") : End If
        cne.ConnectionString = expertos.conectdb
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        accion = Request.QueryString("type")
        idexperto = Session("idexperto")
        If accion <> "modificacion" Then : Response.Redirect("estrategies.aspx") : End If
        datos_estrategia()
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As System.EventArgs) Handles Me.LoadComplete
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("estrategies.aspx") : End If
    End Sub
    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv,fecha from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
            fecha_estrategia = rse("fecha")
        End While
        cne.Close()
        cuantas_modificaciones(fecha_estrategia)
    End Function

    Protected Sub btn_modificar_Click(sender As Object, e As System.EventArgs) Handles btn_modificar.Click
        Dim s As New StringBuilder
        s.Append("window.top.location.replace('new-strategy-3.aspx?elemento=" & g.base64_encode(idestrategia) & "&type=" & g.base64_encode("modificarEstrategia") & "');")
        Page.ClientScript.RegisterStartupScript(GetType(String), "goModify", s.ToString, True)
    End Sub

    Function cuantas_modificaciones(ByVal fecha_alta As String)
        Dim sSQL As String, modificaciones_del_mes As Integer

        sSQL = "select count(*)modificaciones from publicacion_paquete where (month(fecha)=" & Month(expertos.hora_local) & " and year(fecha)=" & Year(expertos.hora_local) & ") and idestrategia='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : modificaciones_del_mes = 2 - SQLe.ExecuteScalar : cne.Close()

        If (Month(expertos.hora_local) = Month(fecha_alta)) And (Year(expertos.hora_local) = Year(fecha_alta)) Then
            modificaciones_del_mes += 1
        End If

        lbl_mensaje.Text = "<div class='brown-bold-12px'>Te recordamos que solo se puedes realizar 2 modificaiones al mes a tu Estrategia. <br /><br />Te quedan <strong>" & modificaciones_del_mes & "</strong> modificacion(es)</div>"
        If modificaciones_del_mes <= 0 Then
            allow_mod.Visible = False
            deny_mod.Visible = True
        Else
            allow_mod.Visible = True
            deny_mod.Visible = False
        End If
    End Function
End Class
