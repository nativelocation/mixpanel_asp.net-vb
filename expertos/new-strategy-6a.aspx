﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="new-strategy-6a.aspx.vb" Inherits="expertos_new_strategy_6a" %>

<!DOCTYPE html>
<html data-wf-page="58cc50f6e175a0d719c4e568" data-wf-site="5898439cf29bebaa63a0fb15">
<head runat="server">
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Nueva Estrategia" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>                    
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body style="background-color:transparent;">
    <form id="form1" runat="server">
    <div class="fondo-close" data-ix="fondo-close"></div>
    <div class="fondo-block">
      <div class="input-label-block">
        <h3 class="brown-bold-14px"><%=fondo%></h3>
        <div class="brown-bold-12px">Asigna el porcentaje a tu estrategia<br />
            <asp:Label ID="lbl_fondo_info" runat="server" Text=""></asp:Label></div>
        <div class="input-block no-margin" data-ix="icons">
          <asp:TextBox ID="txt_porcentaje" runat="server" class="estrat text-field w-input" data-name="%" MaxLength="256" style="width:50%;"></asp:TextBox>
          <asp:Label ID="lbl_mensaje" runat="server" Text="Solo número sin decimales" ForeColor="Red" Font-Size="X-Small" style="display: inline-table;"></asp:Label>
          <asp:RegularExpressionValidator ID="REV_porcentaje" runat="server" ForeColor="Red"
                ErrorMessage="***" ControlToValidate="txt_porcentaje" 
                ValidationExpression="^\d+$" Width="100%" Font-Size="X-Small"></asp:RegularExpressionValidator>
        </div>
      </div>
      <asp:Button ID="btn_agregar" runat="server" Text="Asignar Fondo" CssClass="small-button w-button" />
      <div style="margin-top:10px;">
        <asp:Label ID="lbl_validacion" runat="server" Text="" Font-Size="10px" ForeColor="Red"></asp:Label>
      </div>
    </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <script>
        $(document).ready(function () {
            $('.fondo-close').click(function () {
                $('.fondo-window-wrapper', parent.document).css("display", "none");
            });
        });
    </script>
</body>
</html>
