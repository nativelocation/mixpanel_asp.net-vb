﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="password-1.aspx.vb" Inherits="expertos_password_1" %>

<!DOCTYPE html>
<html data-wf-page="58bc79049d6c401e73a266db" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Cambiar contraseña" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <form id="form1" runat="server">
  <div class="brown-section padding-top">
    <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="estrategies.aspx"><img src="images/Logo.svg">
        </a>
        <div class="centered-mt menu-text">Portal de Expertos</div>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="white-small-wrapper">
      <h3 class="heading-right-24px">Cambiar tu Contraseña</h3>
      <div id="form_contenedor" runat="server">
        <div class="brown-bold-12px place">Contraseña Anterior:</div>
        <div class="input-block no-margin" data-ix="icons">
          <asp:TextBox ID="txt_OldPass" Cssclass="estrat text-field w-input" runat="server" maxlength="256" required="required" type="password"></asp:TextBox>
        </div>
        <div class="brown-bold-12px place">Nueva Contraseña:</div>
        <div class="dob input-block no-margin" data-ix="icons">
          <asp:TextBox ID="txt_NewPass" Cssclass="estrat text-field w-input" runat="server" maxlength="256" required="required" type="password"></asp:TextBox>
          <asp:RegularExpressionValidator ID="REV_NewPass" runat="server" ErrorMessage="6-15 caracteres (Letras y Números)" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)([A-Za-z\d]|){6,15}$" ControlToValidate="txt_NewPass" Width="100%" Font-Size="X-Small" Display="Static"></asp:RegularExpressionValidator>
        </div>
        <div class="brown-bold-12px place">Confirma la Nueva Contraseña:</div>
        <div class="input-block no-margin" data-ix="icons">
          <asp:TextBox ID="txt_confirmar" Cssclass="estrat text-field w-input" runat="server" maxlength="256" required="required" type="password"></asp:TextBox>
          <asp:RegularExpressionValidator ID="REV_Confirm" runat="server" ErrorMessage="6-15 caracteres (Letras y Números)" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)([A-Za-z\d]|){6,15}$" ControlToValidate="txt_confirmar" Width="100%" Font-Size="X-Small" Display="Static"></asp:RegularExpressionValidator>
        </div>
      </div>
      <div id="form_aviso" runat="server" class="w-form">
        <asp:Button ID="btn_cambiar" class="reggg small-button w-button" data-wait="Please wait..." runat="server" Text="Cambia Contraseña" />
        <div id="form_done" runat="server" class="w-form-done">
          <div>¡Tu contraseña fue cambiada correctamente!</div>
          <div class="input-block no-margin" style="margin-left:auto; margin-right:auto; margin-top:20px;"><br />
          <a class="reggg small-button w-button" style="margin: 0 auto;" href="estrategies.aspx">Mis Estrategias</a></div>
        </div>
        <div id="form_fail" runat="server" class="w-form-fail">
          <div><asp:Label ID="lbl_error" runat="server" Text=""></asp:Label></div>
        </div>
      </div>
    </div>
  </div>
  </form>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="estrategies.aspx">Mi Cuenta</a><a class="footer-link" href="comisions.aspx">Mis Comisiones</a><a class="footer-link" href="refer.aspx">Recomienda y Gana</a><a class="footer-link" href="faq.aspx">FAQs</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>