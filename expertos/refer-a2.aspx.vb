﻿
Partial Class expertos_refer_a2
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto As Integer

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Session("idexperto")) Then
            Response.Redirect("index.aspx")
        End If
        idexperto = Session("idexperto")
        cne.ConnectionString = expertos.conectdb
    End Sub

    Function listado()
        Dim sSQL, estatus As String
        sSQL = "select referido_email,fecha_referido,estatus from referidos_expertos where experto_refiere='" & idexperto & "' order by fecha_referido desc"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            Select Case rse("estatus")
                Case -1 : estatus = "No califica"
                Case 1 : estatus = "Enviado (<a href='refer-a2a.aspx?action=" & g.base64_encode(rse("referido_email")) & " ' class='table-link'>Recordar</a>)"
                Case 2 : estatus = "Registrado"
            End Select
            Response.Write("<div class='table-string ts-first' style='font-size:smaller'>")
            Response.Write("<div class='first-one table-item'>")
            Response.Write("<div><a href='#' class='table-link'>" & rse("referido_email") & "</a>")
            Response.Write("</div>")
            Response.Write("</div>")
            Response.Write("<div class='second-one table-item'>")
            Response.Write("<div>" & g.ConvierteFecha2(rse("fecha_referido")) & "</div>")
            Response.Write("</div>")
            Response.Write("<div class='table-item tti-third'>")
            Response.Write("<div>" & estatus & "</div>")
            Response.Write("</div>")
            Response.Write("<div class='_222 table-item ti-fourth'>")
            Response.Write("<div>No Aplica</div>")
            Response.Write("</div>")
            Response.Write("</div>")
        End While
        cne.Close()
    End Function

    Function listado_mobile()
        Dim sSQL, estatus, sHTML As String, contador As Integer
        sSQL = "select referido_email,fecha_referido,estatus from referidos_expertos where experto_refiere='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            Select Case rse("estatus")
                Case -1 : estatus = "No califica"
                Case 1 : estatus = "Enviado (<a class='table-link'>Recordar</a>)"
                Case 2 : estatus = "Registrado"
            End Select
            If contador > 0 Then
                Response.Write("'<div class='table-underline'></div>")
            End If
            Response.Write("<div class='mobile-table'>")
            Response.Write("<div class='mobile-table-string mt-heading-string'>")
            Response.Write("<div><a href='#' class='table-big-link'>" & rse("referido_email") & "</a>")
            Response.Write("</div>")
            Response.Write("</div>")
            Response.Write("<div class='mobile-table-string'>")
            Response.Write("<div class='mt-label'>Envío:</div>")
            Response.Write("<div class='mt-info'>" & g.ConvierteFecha2(rse("fecha_referido")) & "</div>")
            Response.Write("</div>")
            Response.Write("<div class='mobile-table-string'>")
            Response.Write("<div class='mt-label'>Estatus:</div>")
            Response.Write("<div class='mt-info'>" & estatus & "</div>")
            Response.Write("</div>")
            Response.Write("<div class='mobile-table-string'>")
            Response.Write("<div class='mt-label'>Beneficio:</div>")
            Response.Write("<div class='mt-info'>$0</div>")
            Response.Write("</div>")
            Response.Write("</div>")
            contador += 1
        End While
        cne.Close()
    End Function

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("estrategies.aspx") : End If
    End Sub
End Class
