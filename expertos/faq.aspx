﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="faq.aspx.vb" Inherits="expertos_faq" %>

<!DOCTYPE html>
<html data-wf-page="58988ec6bbccb1ff2bf336f7" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="FAQ - Pregusntas Frecuentes" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-form-wrapper">
    <iframe src="login.aspx" border="0" scrolling="no" style="width:100%; height:100%; border:0; overflow:hidden;"></iframe>      
  </div>
  <div class="brown-section faq">
    <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="index.aspx"><img src="images/Logo.svg">
        </a>
          <%If Session("con_login") = 1 %>
            <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="new-strategy.aspx">Registra tu Estrategia</a><a class="nav-link w-nav-link" href="estrategies.aspx">Mi Cuenta</a><a class="nav-link w-nav-link" href="faq.aspx">FAQs</a></nav>
          <%Else %>
            <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="new-account.aspx">Regístrate como Experto</a><a class="nav-link w-nav-link" data-ix="open" href="#">Mi Cuenta</a><a class="nav-link w-nav-link" href="faq.aspx">FAQs</a></nav>
          <%End If %>
        
        <div class="menu-text">Portal de Expertos</div>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <h2 class="heading-2-center">Preguntas Frecuentes</h2>
    <div class="faq-wrapper">
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué es Invierte con Expertos?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Invierte con Expertos es la primera plataforma en México que te permite ganar dinero únicamente utilizando tus conocimientos financieros en fondos de inversión.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Por qué me conviene registrarme?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Si tus estrategias generan altos rendimientos, te conviene registrarte para que las personas que tengan dinero y deseen invertirlo puedan replicar tus estrategias, y por ello te pagarán una comisión. </p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cómo puedo registrarme como Experto?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Para registrarte como Experto debes ser invitado por algún conocido, ya que el sistema no está abierto al público en general. Si no cuentas con una invitación y deseas comunicarte con nosotros, envíanos un correo a expertos@invierteconexpertos.mx.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Puedo invitar a mis conocidos a que se inscriban?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Sí. Debes estar ya registrado e ingresar a la sección “Recomienda y Gana”, en donde podrás proporcionar hasta 5 direcciones de correo electrónico de tus conocidos, los cuales recibirán un correo avisándoles que fueron invitados y que ya pueden registrarse.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué beneficios obtengo al recomendar a mis conocidos?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Por cada experto que recomiendes, tú ganas 200 pesos y los expertos 200 pesos. También recibirás el 10% de la comisión que tu conocido genere durante los primeros 6 meses, comenzando desde la fecha en que se inscribió. En la sección “Recomienda y Gana” podrás consultar el estatus de cada uno de los conocidos a los que les enviaste una invitación.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cuánto puedo ganar por mis estrategias?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Al momento de crear tu estrategia, indicas el porcentaje que cobras por permitir que otras personas repliquen tu forma de invertir. Es un porcentaje anual en función a la cantidad de recursos que estén invertidos y replicando tu estrategia. Multiplicamos la comisión que tú hayas indicado por el saldo promedio mensual de todas las personas que repliquen tu estrategia, menos el porcentaje que Invierte con Expertos te retiene, el cual es del 30%. Tú te quedas con el 70%.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cómo y cada cuánto me pagan?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Recibes las comisiones por transferencia bancaria mensualmente durante los primeros 15 días naturales siguientes al mes vencido. Te pedimos que nos proporciones los datos bancarios para que te podamos depositar.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cuál es la comisión mínima que tengo que generar?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Debes generar un mínimo de 500 pesos para que te podamos hacer una transferencia; en caso de que generes un importe menor durante algún mes, te lo acumularemos en el próximo mes hasta que el importe llegue al mínimo.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué es una estrategia?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Es un conjunto de fondos de inversión que tú seleccionaste; tú indicas el porcentaje que cada fondo ocupa en tu estrategia. Al momento de crearla por primera vez, deberás elegir uno de los 5 objetivos que tenemos para indicar el máximo en renta variable que tendrá tu estrategia y también deberás indicar el porcentaje de comisión que le cobrarás a las personas que quieran replicar tu estrategia.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Otros expertos pueden copiar mi portafolio?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">No, nadie puede copiar tu estrategia, únicamente las personas que decidan contratarte, ya que la composición detallada de tu estrategia no se publica en la plataforma. Las únicas personas que podrán conocer tu estrategia son las personas que te hayan contratado y estén replicándola.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cuántas estrategias puedo dar de alta?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Puedes dar de alta un máximo de 5 estrategias diferentes.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Puedo cancelar mi estrategia?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Sí, la puedes cancelar. Pero en caso de que tengas inversionistas que la estén replicando, Invierte con Expertos quedaría asignado como el Experto encargado y les avisaríamos a todas las personas que te estaban replicando para que estén informadas.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué tipo de fondos puedo dar de alta?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Puedes dar de alta fondos de inversión de Deuda y Renta Variable de diferentes Operadoras. Te pedimos que en caso de que no encuentres el fondo y/o la Operadora que quieres, nos envíes un correo a <a href="mailto:espertos@invierteconexpertos.mx"></a>expertos@invierteconexpertos.mx.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cuántas veces puedo hacer cambios a mi estrategia?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Puedes cambiar los fondos y/o porcentajes de cada fondo un máximo de 4 veces por mes. Ten en cuenta que los cambios que solicites pueden tardar varios días en realizarse debido al periodo de liquidez que el fondo tiene indicado en su prospecto de información (nosotros te lo informamos), y que no podrás hacer un cambio hasta que el cambio anterior haya quedado realizado.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cómo miden el rendimiento de mi estrategia?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">El sistema de Invierte con Expertos calcula diariamente los rendimientos de cada uno de los fondos contenidos en tu estrategia, tomando en cuenta los cambios que hayas realizado en la estrategia y la ponderación que cada fondo tenga en la misma. </p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué es un objetivo?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Un objetivo indica el porcentaje máximo que cada estrategia puede invertir en renta variable. Contamos con 5 diferentes objetivos: 1. Conservador (de 0% hasta un 20%), 2. Moderado (desde 21% hasta un 40%), 3. Emprendedor (desde 41% hasta un 60%), 4. Agresivo (desde 61% hasta un 80%) y 5. Especulativo (desde 81% hasta un 100%).</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué información puedo cambiar en mis estrategias?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">En cada una de las estrategias puedes modificar: (1) la composición de los fondos y/o los porcentajes, y (2) reducir la cuota que cobras a los inversionistas que repliquen tus estrategias.<br />
          <br />No podrás cambiar el perfil de tu estrategia ni incrementar la comisión que cobras a los inversionistas.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Tiene costo registrarme en el portal?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">No tiene ningún costo registrarte como Experto y publicar tus estrategias.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Qué comisiones debo pagar?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Cuando un inversionista decida replicar tus estrategias, te descontaremos 49 pesos por única vez de tus comisiones para cubrir gastos de apertura.<br />
          <br />Posteriormente, la comisión que estipulaste en tu estrategia será cobrada a cada uno de los inversionistas en base a su saldo promedio y te será depositada en tu cuenta bancaria menos el porcentaje que Invierte con Expertos te retiene, el cual es del 30%. Tú te quedas con el 70%.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Los inversionistas y Expertos conocerán mi nombre completo?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Todos los usuarios de la comunidad (Expertos e inversionistas) te conocerán únicamente por tu alias, un nombre de usuario que tú indicas al momento de registrarte.</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Cómo un inversionista puede replicar mi estrategia?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Cuando tú creas tus estrategias, ningún inversionista ni Experto puede conocer los nombres de los fondos ni los porcentajes que asignaste, por lo que en Invierte con Expertos nos encargamos de que cada persona que quiera replicar tus estrategias pueda hacerlo de forma sencilla. 
          Cuando un inversionista decide replicar alguna de tus estrategias, nosotros le tramitamos un contrato en una Distribuidora Integral de Fondos de Inversión, la cual será la institución que recibirá sus recursos y se encargará de que estén invertidos de acuerdo al portafolio que hayas definido, y también realizará el cambio de portafolio cuando tú hayas realizado cambios a tu estrategia..</p>
        </div>
      </div>
      <div class="question-wrapper">
        <div class="question-top" data-ix="answer-appear">
          <h4 class="heading-white-18px questin">¿Puedo invitar a mis conocidos a que inviertan con mi estrategia?</h4>
        </div>
        <div class="question-bottom">
          <p class="brown-center-14px">Sí. Dentro de la página “Mis Estrategias”, haz clic en “Promoción” para que puedas: (1) obtener la liga directa a tu estrategia y enviarla a tus amigos para que puedan conocer tu estrategia, (2) puedes enviar un mensaje a través de Facebook y Twitter, y (3) si tienes cuenta en Gmail o Yahoo! puedes enviar de forma automática un correo electrónico a todos tus conocidos.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="index.aspx">Inicio</a><a class="footer-link" href="new-account.aspx">Regístrate como Experto</a><a class="footer-link" href="estrategies.aspx">Mi Cuenta</a><a class="footer-link" href="faq.aspx">FAQs</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>