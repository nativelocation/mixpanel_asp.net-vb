﻿Option Infer On
Imports System.IO
Imports System.Net
Imports System.Threading.Tasks
Imports MailChimp.Net
Imports Newtonsoft.Json

Partial Class expertos_new_account_invitation_b
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public origen As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb : cne2.ConnectionString = expertos.conectdb
        origen = g.base64_decode(Request.QueryString("source"))
        If origen <> "IceBoom" Then : Response.Redirect("index.aspx") : End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Session.Abandon()
        Session.RemoveAll()
        Session.Clear()
        correo_registro.Attributes.Add("type", "email")
        correo_registro.Attributes.Add("placeholder", "correo electrónico")
        correo_registro.Attributes.Add("required", "required")
    End Sub

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL, sSQL2, registro, opcion As String, encontrado, registrado, ya_referido As Integer
        registro = LCase(Trim(correo_registro.Text))
        Session("correo_registro") = registro
        Dim myCookie As HttpCookie = New HttpCookie("attempt")
        myCookie(g.base64_encode("correo")) = g.base64_encode(registro)
        Response.Cookies.Add(myCookie)
        'Evaluar si cumple con los requitos para el registro
        sSQL = "select count(*) from referidos_expertos where referido_email='" & registro & "' and estatus=1"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : encontrado = SQLe.ExecuteScalar : cne.Close()
        If encontrado = 0 Then
            'Inserta la referencia con el Experto LKDN
            '574
            sSQL = "insert into referidos_expertos (experto_refiere,referido_email,fecha_referido,estatus) values('574','" & Trim(LCase(correo_registro.Text)) & "','" & expertos.hora_local & "',1)"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
            'revisar si ya se envuentra registrado como Experto en la plataforma1
            sSQL = "select idexperto from expertos where correo='" & Trim(LCase(correo_registro.Text)) & "'"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open()
            rse = SQLe.ExecuteReader
            If rse.Read Then
                sSQL2 = "update referidos_expertos set estatus=-1 where referido_email='" & Trim(LCase(correo_registro.Text)) & "'"
                SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
                cne2.Open() : SQLe2.ExecuteNonQuery() : cne2.Close()
            End If
            cne.Close()
            encontrado = 1
        End If

        sSQL = "select count(*) from expertos where correo='" & registro & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : registrado = SQLe.ExecuteScalar : cne.Close()

        If encontrado > 0 And registrado = 0 Then
            Response.Redirect("new-account-1.aspx")
        Else
            'Revisa si ya esta registrado
            sSQL = "select count(*) from referidos_expertos where referido_email='" & registro & "' and estatus=2"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : ya_referido = SQLe.ExecuteScalar : cne.Close()

            sSQL = "select count(*) from expertos where correo='" & registro & "'"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : registrado = SQLe.ExecuteScalar : cne.Close()
            If registrado > 0 Or ya_referido > 0 Then
                opcion = "EstaRegistrado"
            Else
                opcion = "SinInvitacion"
            End If
            Response.Redirect("new-account-no.aspx?msj=" & g.base64_encode(opcion))
        End If

    End Sub

    Private Sub btn_home_Click(sender As Object, e As EventArgs) Handles btn_home.Click
        Response.Redirect("index.aspx")
    End Sub
End Class
