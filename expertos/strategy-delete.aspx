﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="strategy-delete.aspx.vb" Inherits="expertos_strategy_delete" %>

<!DOCTYPE html>
<html data-wf-page="58a2ccc2aa68ad20244cf7fc" data-wf-site="5898439cf29bebaa63a0fb15">
<head runat="server">
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Eliminar estrategia" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>                    
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body style="background-color:transparent;">
    <form id="form1" runat="server">
    <div class="fondo-close" data-ix="fondo-close"></div>
    <div class="fondo-block">
      <div class="input-label-block">
        <h3 class="brown-bold-14px"><%=estrategia_name%></h3>
        <div style="margin-bottom:25px;"></div>
        <div class="brown-bold-12px">Estas tratando de eliminar esta estrategia <br />¿Deseas eliminar esta Estrategia?</div>
        <div style="margin-bottom:25px;"></div>
      </div>
      <asp:Button ID="btn_eliminar" runat="server" Text="Eliminar Estrategia" CssClass="small-button w-button display-button-left" />
      <a href="#Close-Button" class="small-button w-button display-button-right">Cancelar</a>
    </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
    <script src="js/webflow.js" type="text/javascript"></script>
    <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <script>
        $(document).ready(function () {
            $('.fondo-close').click(function () {
                $('.fondo-window-wrapper', parent.document).css("display", "none");
            });
            $('a[href="#Close-Button"]').click(function () {
                $('.fondo-close').trigger("click");
            });
        });
    </script>
</body>
</html>
