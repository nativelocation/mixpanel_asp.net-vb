﻿
Partial Class expertos_password_1
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto As Integer

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then : Response.Redirect("index.aspx") : End If
        cne.ConnectionString = expertos.conectdb
        idexperto = Session("idexperto")
    End Sub

    Protected Sub btn_cambiar_Click(sender As Object, e As System.EventArgs) Handles btn_cambiar.Click
        Dim sSQL, oldpass, newPass, confirmPass As String
        sSQL = "select pass from expertos where idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : oldpass = SQLe.ExecuteScalar : cne.Close()

        If txt_OldPass.Text = oldpass Then
            If txt_NewPass.Text = txt_confirmar.Text Then
                sSQL = "update expertos set pass='" & txt_NewPass.Text & "' where idexperto='" & idexperto & "'"
                SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
                form_contenedor.Visible = False
                btn_cambiar.Visible = False
                form_done.Attributes.Add("style", "display:block")
                form_fail.Attributes.Add("style", "display:none")
                'Envía correo avisando del cambio
                Dim mensaje As String
                mensaje = "<p>Te informamos que la contraseña para ingresar a tu cuenta ha sido modificada.<br>" & _
                "Si tú no has solicitado el cambio, te pedimos que nos escribas inmediatamente a <a href='mailto:expertos@invierteconexpertos.mx'>expertos@invierteconexpertos.mx</a></p>"
                expertos.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Expertos", expertos.correo_experto(idexperto), expertos.nombre_experto(idexperto), "Tu contraseña ha sido modificada", "", mensaje)
            Else
                lbl_error.Text = "La Nueva Contraseña no coinciden, vuelve a escribirla"
                form_fail.Attributes.Add("style", "display:block")
            End If
        Else
            lbl_error.Text = "La contraseña anterior no coincide"
            form_fail.Attributes.Add("style", "display:block")
            Exit Sub
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        
    End Sub
End Class
