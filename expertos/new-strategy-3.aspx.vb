﻿Imports System.Data

Partial Class expertos_new_strategy_3
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idestrategia, idexperto, objetivo, max_rv, rv_acum, min_rv As Integer
    Public estrategia_name, objetivo_name, accion As String, comision As Double

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then : Response.Redirect("index.aspx") : End If
        If String.IsNullOrEmpty(Request.QueryString("elemento")) Then : Response.Redirect("index.aspx") : End If
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        accion = g.base64_decode(Request.QueryString("type"))
        idexperto = Session("idexperto")
        datos_estrategia()
        '//////////
        If accion = "nuevaEstrategia" Then
            If Session("EstrategiaUser") Is Nothing Then
                CrearEstrategia()
            End If
        End If
        If accion = "modificarEstrategia" Then
            Dim sSQL As String
            If Session("EstrategiaUser") Is Nothing Then
                sSQL = "select pp.idpublicacion,pe.idfondo,pe.porcentaje,op.idoperadora,op.idoperacion " & _
                "from (publicacion_paquete pp inner join publicacion_estrategias pe on pp.idpublicacion=pe.idpublicacion)left join " & _
                "operaciones op on pe.idfondo=op.idfondo " & _
                "where pp.idestrategia='" & idestrategia & "' and pp.idexperto='" & idexperto & "'"
                SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                cne.Open()
                rse = SQLe.ExecuteReader
                While rse.Read
                    AgregarFondo(rse("idfondo"), rse("idoperadora"), rse("porcentaje"))
                End While
                cne.Close()
            End If
        End If
        '//////////
        rv_acum = rv_calculada() : min_rv = max_rv - 19 : review_percent() : cuenta_fondos()
        Session.Timeout = 50
    End Sub

    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
        End While
        cne.Close()

        sSQL = "select objetivo from objetivos where id='" & objetivo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            objetivo_name = rse("objetivo")
        End While
        cne.Close()

    End Function

    Function rv_calculada() As Integer
        rv_calculada = 0
        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            rv_calculada = rv_calculada + rv_fondo(xRegistro("idfondo"), xRegistro("porcentaje"))
        Next
    End Function

    Function rv_fondo(ByVal idfondo As Integer, ByVal percent As Integer) As Integer
        Dim sSQL As String, maxrv As Integer
        sSQL = "select max_rv from fondo where id='" & idfondo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : maxrv = SQLe.ExecuteScalar : cne.Close()
        rv_fondo = (percent * maxrv) / 100
    End Function

    Function draw_bar()
        Dim color As String, size_bar As Integer
        color = ""
        If porcentaje_acum() > 100 Then : color = "background-color: #e04141;" : size_bar = 100 : Else : size_bar = porcentaje_acum() : color = "" : End If
        Response.Write("" & _
        "<div class='color ns3-bar' style='width:" & size_bar & "%; " & color & "'>" & _
        "<div class='ns3-tet'>" & porcentaje_acum() & "%</div>" & _
        "</div>" & _
        "")
    End Function

    Function composicion()
        Dim tabla As DataTable = Session("EstrategiaUser")
        Dim contador, linea, cuantos_fondos As Integer
        For Each xRegistro As DataRow In tabla.Rows
            If linea = 0 Then
                If add_fund.Visible = True Then : contador = 1 : Else : contador = 0 : End If
            End If
            Response.Write(
            "<div class='eba-red fondo-selector' data-ix='fondo-open' data-ixfon='" & g.base64_encode(xRegistro("idfondo")) & "' data-ixoper='" & g.base64_encode(xRegistro("idoperadora")) & "'>" &
              "<div class='eb-add eba-red estrategia-block'>" &
                "<div class='brown-bold-12px'>" & expertos.nombre_fondo(xRegistro("idfondo")) & "</div>" &
                "<div class='brown-12px'>Asignado: " & xRegistro("porcentaje") & "%<br>Renta Variable: " & rv_fondo(xRegistro("idfondo"), xRegistro("porcentaje")) & "%</div>" &
                "<div class='w-form'>" &
                "</div>" &
                "<div class='x'>")
            Response.Write("<a href='strategy-fundel.aspx?elemento=" & g.base64_encode(idestrategia) & "&type=" & Request.QueryString("type") & "&it=" & g.base64_encode(xRegistro("idfondo")) & "'>X</a>")
            Response.Write("</div>" &
              "</div>" &
            "</div>")
            cuantos_fondos += 1
            contador += 1
            If contador > 4 Then
                Response.Write("</div>")
                Response.Write("<div class='estrategias-wrapper ew-2'>")
                contador = 0
            End If
            linea += 1
        Next
    End Function

    Protected Sub btn_publicar_Click(sender As Object, e As System.EventArgs) Handles btn_publicar.Click
        Dim sSQL As String, idpublicacion As Integer
        If error_percent.Visible = True Or error_percent2.Visible = True Then : Exit Sub : End If
        If accion = "nuevaEstrategia" Then
            Publica_nueva()
        End If
        sSQL = "insert into publicacion_paquete (idestrategia,idexperto,fecha,estatus) " & _
        "values ('" & idestrategia & "','" & idexperto & "','" & expertos.hora_local & "','1')"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        Dim rows As Integer = SQLe.ExecuteNonQuery()
        If rows > 0 Then
            Dim sqlIdentity As String = "SELECT @@IDENTITY"
            Dim Identity As New Data.SqlClient.SqlCommand(sqlIdentity, cne)
            idpublicacion = (Convert.ToInt32(Identity.ExecuteScalar()))
        End If
        cne.Close()

        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            sSQL = "insert into publicacion_estrategias (idpublicacion,idfondo,porcentaje) " & _
            "values ('" & idpublicacion & "','" & xRegistro("idfondo") & "','" & xRegistro("porcentaje") & "')"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        Next
        If accion = "nuevaEstrategia" Then
            Response.Redirect("new-strategy-8.aspx")
        End If
        If accion = "modificarEstrategia" Then
            Response.Redirect("estrategies.aspx")
        End If
    End Sub

    Function cuenta_fondos()
        Dim cuantos As Integer
        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            cuantos += 1
        Next
        If cuantos >= 8 Then
            add_fund.Visible = False
        End If
    End Function

    Function review_percent()
        Dim porcentaje As Integer

        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            porcentaje = porcentaje + xRegistro("porcentaje")
        Next
        If porcentaje > 100 Then
            error_percent.Visible = True
        else If porcentaje < 100 Then
            error_percent2.Visible = True
            error_percent2.InnerHtml = "Para publicar la estrategia completa el 100%"
        Else
            If (rv_calculada() >= min_rv) And (rv_calculada() <= max_rv) Then
                btn_publicar.Visible = True : add_fund.Visible = False
            Else
                add_fund.Visible = False
                error_percent2.InnerHtml = "Para publicar tu estrategia debes asignar un mínimo de " & min_rv & "% y un máximo de " & max_rv & "% de renta variable."
                error_percent2.Visible = True
            End If
        End If
        
    End Function

    Function porcentaje_acum() As Integer
        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            porcentaje_acum = porcentaje_acum + xRegistro("porcentaje")
        Next
    End Function

    Function Publica_nueva()
        Dim sSQL, idtemp As String
        Dim rowsAffected, idoperacionef, nuevoIdS, idcliente, idpaquete, importe_estrategia As Integer
        Dim vSQL As String
        idcliente = idestrategia
        'El minímo de inversion
        sSQL = "select isnull(valor,100000)valor from sysconfig where id=2"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : importe_estrategia = SQLe.ExecuteScalar : cne.Close()
        If idestrategia < 10 Then : idtemp = "000" & idestrategia
        ElseIf idestrategia < 100 Then : idtemp = "00" & idestrategia
        ElseIf idestrategia < 1000 Then : idtemp = "0" & idestrategia : End If
        'Inserta la estrategia como cliente
        sSQL = "insert into clientes (idcliente,fecha,idexperto,nombre,tipo_persona,perfil_propuesta) " & _
        "values ('" & idestrategia & "','" & expertos.hora_local & "','" & idexperto & "','Estrategia E-" & idtemp & "','1','" & objetivo & "')"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        '////////////////////////////////////////////////////////
        Dim q1, idoperadora, idcodistribuidor As String, idcontrato As Integer
        idoperadora = "40037" : idcodistribuidor = "40038"
        q1 = "insert into contratos_cliente (idcliente, idoperadora, fecha, idpromotor, idasesoria, tramitado, idproducto, titular) " & _
        "values('" & idcliente & "','" & idoperadora & "','" & Format(expertos.hora_local, "yyyy-MM-dd HH:mm:ss") & "','" & idexperto & "','" & idestrategia & "','0', " & _
        "'10000','" & estrategia_name & "')"
        SQLe = New Data.SqlClient.SqlCommand(q1, cne)
        cne.Open()
        Dim rows As Integer = SQLe.ExecuteNonQuery()
        If rows > 0 Then
            Dim sqlIdentity As String = "SELECT @@IDENTITY"
            Dim Identity As New Data.SqlClient.SqlCommand(sqlIdentity, cne)
            idcontrato = (Convert.ToInt32(Identity.ExecuteScalar()))
        End If
        cne.Close()
        q1 = "update asesorias set idcodistribuidor2='" & idoperadora & "',aceptada='1' where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(q1, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        '////////////////////////////////////////////////////////
        'Representa la activación el contrato
        sSQL = "update contratos_cliente set tramitado=1,fecha_activacion='" & expertos.hora_local & "',activacion='" & idexperto & "' where id=" & idcontrato & " and idcliente='" & idcliente & "' and idoperadora='" & idoperadora & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()

        sSQL = "Insert into contratos (contrato, idcodistribuidor, idoperadora, idcliente, idorigen, titular, id, fecha, idcontratocliente) " & _
        "values('ICE-E" & idtemp & "','" & idcodistribuidor & "','" & idoperadora & "','" & idcliente & "','40000','" & estrategia_name & "','" & idexperto & "','" & g.ConvierteFecha(expertos.hora_local) & "','" & idcontrato & "')"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        '////////////////////////////////////////////////////////
    End Function

    Protected Sub AgregarFondo(ByVal idfondo As Integer, ByVal idoperadora As Integer, ByVal porcentaje As Integer)
        Dim id As Integer = idfondo
        If Session("EstrategiaUser") Is Nothing Then
            CrearEstrategia()
        End If
        Dim tabla As DataTable = Session("EstrategiaUser")
        If tabla.Rows.Contains(id) Then
            tabla.Rows.Find(id).Item("porcentaje") = porcentaje
        Else
            Dim fila As DataRow = tabla.NewRow
            fila("idfondo") = id
            fila("idoperadora") = idoperadora
            fila("porcentaje") = porcentaje
            tabla.Rows.Add(fila)
        End If
    End Sub
    Protected Sub CrearEstrategia()
        Dim Tabla As New DataTable
        Tabla.Columns.Add("idfondo", GetType(Integer))
        Tabla.Columns.Add("porcentaje", GetType(Integer))
        Tabla.Columns.Add("idoperadora", GetType(Integer))
        Dim Clave() As DataColumn = {Tabla.Columns("idfondo")}
        Tabla.PrimaryKey = Clave
        Session("EstrategiaUser") = Tabla
    End Sub

End Class
