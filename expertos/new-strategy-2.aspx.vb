﻿
Partial Class expertos_new_strategy_2
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto,idestrategia As Integer

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then
            Response.Redirect("index.aspx")
        End If
        idexperto = Session("idexperto")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txt_comision.Attributes.Add("placeholder", "% de comisión")
        txt_comision.Attributes.Add("required", "required")
    End Sub

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL As String
        If IsNumeric(txt_comision.Text) Then
            Session("ne_comision") = txt_comision.Text
            idestrategia = nueva_estrategia()
            Response.Redirect("new-strategy-3.aspx?elemento=" & g.base64_encode(idestrategia) & "&type=" & g.base64_encode("nuevaEstrategia"))
        End If
    End Sub

    Function nueva_estrategia() As Integer
        Dim sSQL, idtemp As String, importe, ne_rendimiento, objetivo As Integer
        objetivo = Session("ne_IDobjetivo")
        'El minímo de inversion
        sSQL = "select isnull(valor,100000)valor from sysconfig where id=2"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : importe = SQLe.ExecuteScalar : cne.Close()
        'Selecciona el rendimietno
        sSQL = "select isnull(valor,360)valor from sysconfig where id=6"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : ne_rendimiento = SQLe.ExecuteScalar : cne.Close()
        '// Registro de la estrategia
        '//////////////////////////////////
        sSQL = "insert into asesorias (fecha,metodo,opera,clave,monto,perfil2,rendimiento,idexperto,max_rv,comision) " & _
        "values ('" & expertos.hora_local & "','Directo ICE_expertos','40038','Extrategia','" & importe & "','" & objetivo & "','" & ne_rendimiento & "','" & idexperto & "'," & _
        "'" & Session("ne_rv") & "','" & Session("ne_comision") & "')"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        Dim rowsAffected, idestrategia As Integer
        rowsAffected = SQLe.ExecuteNonQuery()
        If rowsAffected > 0 Then
            Dim sqlIdentity As String = "SELECT @@IDENTITY"
            Dim cmdIdentity As New Data.SqlClient.SqlCommand(sqlIdentity, cne)
            idestrategia = (Convert.ToInt32(cmdIdentity.ExecuteScalar()))
        End If
        cne.Close()
        'Actualizacion del nombre de la estrategia
        If idestrategia < 10 Then : idtemp = "000" & idestrategia
        ElseIf idestrategia < 100 Then : idtemp = "00" & idestrategia
        ElseIf idestrategia < 1000 Then : idtemp = "0" & idestrategia : End If
        sSQL = "update asesorias set idcliente='" & idestrategia & "',clave='Estrategia E-" & idtemp & "' where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        ''Inserta la estrategia como cliente
        'sSQL = "insert into clientes (idcliente,fecha,idexperto,nombre,tipo_persona,perfil_propuesta) " & _
        '"values ('" & idestrategia & "','" & expertos.hora_local & "','" & idexperto & "','Estrategia E-" & idtemp & "','1','" & objetivo & "')"
        'SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        'cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        ''Inserta el fondo de efectivo para la estrategia
        'sSQL = "insert into operaciones (status,fecha,idcliente,idfondo,monto,porcentaje,idasesoria,idcodistribuidor,tipocliente) " & _
        '"values('3','" & expertos.hora_local & "','" & idestrategia & "','1442','0','0','" & idestrategia & "','40038','2')"
        'SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        'cne.Open() : SQLe.ExecuteNonQuery() : cne.Close()
        '//////////////////////////////////
        Session.Timeout = 30
        Return idestrategia
    End Function
End Class
