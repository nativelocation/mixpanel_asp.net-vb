﻿
Partial Class expertos_login
    Inherits System.Web.UI.Page
    Public expertos As New Ice_expertos
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public g As New generales

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        If (Request.Cookies("ExpertSettings") IsNot Nothing) Then
            If (Request.Cookies("ExpertSettings")(g.base64_encode("UserExperto")) IsNot Nothing) Then
                txt_email.Text = g.base64_decode(Request.Cookies("ExpertSettings")(g.base64_encode("UserExperto")))
            End If
            If (Request.Cookies("ExpertSettings")(g.base64_encode("PassWord")) IsNot Nothing) Then
                txt_contrasena.Text = g.base64_decode(Request.Cookies("ExpertSettings")(g.base64_encode("PassWord")))
            End If
            chk_recordar.Checked = True
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txt_email.Attributes.Add("type", "email") : txt_email.Attributes.Add("required", "required") : txt_email.Attributes.Add("placeholder", "correo electrónico")
        txt_contrasena.Attributes.Add("type", "password") : txt_contrasena.Attributes.Add("placeholder", "contraseña") : txt_contrasena.Attributes.Add("required", "required")
        If Request.UrlReferrer Is Nothing Then
            Session.RemoveAll()
            Session.Abandon()
            Session.Clear()
            Response.Redirect("index.aspx")
        End If
        MaintainScrollPositionOnPostBack = True
    End Sub

    Protected Sub btn_ingredar_Click(sender As Object, e As System.EventArgs) Handles btn_ingredar.Click
        Dim sSQL, alias_user, correo, pass As String, existe, activada As Integer
        correo = LCase(txt_email.Text)
        pass = txt_contrasena.Text
        If g.IsValidEmail(correo) And Len(pass) <= 15 Then
            sSQL = "select count(*)existe from expertos where correo='" & correo & "' and pass='" & pass & "'"
            SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
            cne.Open() : existe = SQLe.ExecuteScalar : cne.Close()
            If existe > 0 Then
                sSQL = "select idexperto,alias,activada from expertos where correo='" & LCase(txt_email.Text) & "' and pass='" & txt_contrasena.Text & "'"
                SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
                cne.Open()
                rse = SQLe.ExecuteReader
                While rse.Read
                    activada = rse("activada")
                    alias_user = rse("alias")
                    Session("alias_login") = alias_user
                    Session("idexperto") = rse("idexperto")
                End While
                cne.Close()
                If activada = 1 Then
                    If chk_recordar.Checked = True Then
                        Dim myCookie As HttpCookie = New HttpCookie("ExpertSettings")
                        myCookie(g.base64_encode("UserExperto")) = g.base64_encode(Trim(LCase(txt_email.Text)))
                        myCookie(g.base64_encode("PassWord")) = g.base64_encode(Trim(txt_contrasena.Text))
                        myCookie.Expires = Now.AddDays(60)
                        Response.Cookies.Add(myCookie)
                    Else
                        If (Request.Cookies("ExpertSettings") IsNot Nothing) Then
                            Dim myCookie As HttpCookie = New HttpCookie("ExpertSettings")
                            myCookie(g.base64_encode("UserExperto")) = Nothing
                            myCookie(g.base64_encode("PassWord")) = Nothing
                            myCookie.Expires = Now.AddDays(-7)
                            Response.Cookies.Add(myCookie)
                        End If
                    End If
                    Session("con_login") = 1
                    Session("correo_login") = LCase(txt_email.Text)
                    Dim s As New StringBuilder
                    s.Append("window.top.location.replace('estrategies.aspx');")
                    Page.ClientScript.RegisterStartupScript(GetType(String), "Ingresar", s.ToString, True)
                Else
                    Dim cadena(), idexperto, url, mensaje As String
                    msg_error.Text = "Cuenta inactiva<br>Te enviamos un correo para activar tu cuenta."
                    login_fail.Attributes.Add("style", "display:block")
                    login_fail.Visible = True
                    correo = txt_email.Text
                    cadena = Split(correo, "@", 2)
                    idexperto = Session("idexperto")
                    url = "http://invierteconexpertos.mx/expertos/activate.aspx?verify=" & g.base64_encode(cadena(0)) & "&accion=" & g.base64_encode(idexperto) & "&type=" & g.base64_encode(cadena(1))
                    mensaje = "<p>Bienvenido a Invierte con Expertos. Para poder comenzar a dar de alta tus estrategias, te pedimos que confirmes tu dirección de email. " & _
            "De este modo mejoramos la seguridad de tu cuenta y te aseguras de que podrás recuperar " & _
            "tu contraseña si llegaras a olvidarla </p><p><a href='" & url & "'>Haz clic aquí para activar</a></p>"
                    expertos.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Expertos", correo, expertos.nombre_experto(idexperto), "Bienvenido a Invierte con Expertos", "", mensaje)
                End If
            Else
                msg_error.Text = "Datos Incorrectos"
                login_fail.Attributes.Add("style", "display:block")
                login_fail.Visible = True
            End If
        Else
            msg_error.Text = "Datos Incorrectos"
            login_fail.Attributes.Add("style", "display:block")
            login_fail.Visible = True
        End If
        
    End Sub
End Class
