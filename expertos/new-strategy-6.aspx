﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="new-strategy-6.aspx.vb" Inherits="expertos_new_strategy_6" %>

<!DOCTYPE html>
<html data-wf-page="58cc50f6e175a0d719c4e568" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Nueva Estrategia" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script type="text/javascript" src="../fusioncharts/fusioncharts.js"></script>                    
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <form id="form1" runat="server">
  <div class="fondo-window-wrapper">
    <iframe src="new-strategy-6a.aspx?elemento=<%=Request.QueryString("elemento") %>&dist=<%=Request.QueryString("dist") %>&itf=<%=Request.QueryString("itf") %>&type=<% =Request.QueryString("type")%>" border="0" scrolling="no" style="width:100%; height:100%; border:0; overflow:hidden;"></iframe>
  </div>
  <div class="brown-section est height padding-top">
    <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="estrategies.aspx"><img src="images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="estrategies.aspx">Mis Estrategias</a><a class="nav-link w-nav-link" href="comisions.aspx">Mis Comisiones</a><a class="menu-bar-button w-button" href="refer.aspx">Recomienda y Gana</a>
          <div class="dd w-dropdown" data-delay="0">
            <div class="dd-toogle w-dropdown-toggle">
              <div><%=Session("alias_login")%></div>
              <div class="w-icon-dropdown-toggle"></div>
            </div>
            <nav class="dd-list w-dropdown-list"><a class="dd-link w-dropdown-link" href="refer-a2b.aspx">Datos Bancarios</a><a class="dd-link w-dropdown-link" href="password-1.aspx">Cambiar password</a><a class="dd-link w-dropdown-link" href="logout.aspx">Cerrar sesión</a>
            </nav>
          </div>
        </nav>
        <div class="menu-text mt-mobile">Portal
          <br>de Expertos</div>
        <div class="menu-text mt-hide">Portal de Expertos</div>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="estrategies-top">
      <div>
        <h2 class="heading-white-28px left-align"><%=estrategia_name%></h2>
        <h4 class="wcleft white-center-14px"><%=fondo%></h4>
        <div class="white-14px">Clasificación: <strong><%=clasificacion%></strong>
        </div>
        <div class="white-14px">Tipo: <strong><%=tipofondo%></strong>
        </div>
        <div class="white-14px"><%=calificacion%>
        </div>
        <div class="white-14px">Liquidez: <strong><%=liquidez%></strong>
        </div>
      </div>
      <div class="buttons-wrapper-2"><a class="small-button w-button wider" data-ix="fondo-open" href="#">Agregar Fondo</a><a class="small-button w-button wider" href="new-strategy-4.aspx?elemento=<%=request.querystring("elemento") %>&type=<%=request.querystring("type") %>">Regresar a la Operadora</a>
      </div>
    </div>
    <%grafica_fondo_precio()%>
    <div class="graphic-wrapper">
        
      <div class="graphic ns">
        <h3 class="heading-white-18px">Desempeño</h3>
        <div class="graphic-section">
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
      </div>
    </div>
    <div class="remdcomp-wrapper">
      <div class="show table-block">
        <h3 class="heading-white-18px hwleft">Rendimientos</h3>
        <div class="table">
          <div class="opacity table-underline"></div>
          <div class="head-string table-string">
            <div class="ns1 table-item">
              <div>Al <%=sFecha%></div>
            </div>
            <div class="table-item tti-second">
              <div>Fondo</div>
            </div>
          </div>
          <div class="opacity table-underline"></div>
          <div class="table-string ts-first">
            <div class="ns1 table-item">
              <div>Últimos 30 días</div>
            </div>
            <div class="table-item ti-fourth">
              <div><%=rend30%>%</div>
            </div>
          </div>
          <div class="table-string">
            <div class="ns1 table-item">
              <div>Últimos 60 días</div>
            </div>
            <div class="table-item ti-fourth">
              <div><%=rend60%>%</div>
            </div>
          </div>
          <div class="table-string">
            <div class="ns1 table-item">
              <div>Últimos 90 días</div>
            </div>
            <div class="table-item ti-fourth">
              <div><%=rend90%>%</div>
            </div>
          </div>
          <div class="table-string">
            <div class="ns1 table-item">
              <div><%=Year(Now())-1%></div>
            </div>
            <div class="table-item ti-fourth">
              <div><%=rend_anterior%>%</div>
            </div>
          </div>
          <div class="table-string">
            <div class="ns1 table-item">
              <div><%=Year(Now())%></div>
            </div>
            <div class="table-item ti-fourth">
              <div><%=rend_actual%>%</div>
            </div>
          </div>
        </div>
      </div>
      <%composicion_fondo %>
      <div class="show table-block">
        <h3 class="heading-white-18px hwleft">Composición</h3>
        <div class="table-underline"></div>
        <div class="round-graph-wrapper">
          <div style="margin-left: -70px;"><asp:Literal ID="Literal2" runat="server"></asp:Literal></div>
          <div class="round-graph-info" style="float:left;">
            <div class="round-percent-block">
              <div class="wbold white-14px"><%=p_deuda%>%</div>
              <div class="round-graph-color"></div>
              <div class="w11 white-14px">Deuda</div>
            </div>
            <div class="round-percent-block">
              <div class="wbold white-14px"><%=p_rv%>%</div>
              <div class="orange round-graph-color"></div>
              <div class="w11 white-14px">Renta Variable</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="remdcomp-wrapper">
      <div class="show-2 table-block">
        <h3 class="heading-white-18px hwleft">Cartera</h3>
        <div class="table-underline"></div>
        <%cartera_fondo()%>
      </div>
      <div class="show-2 table-block">
        <h3 class="heading-white-18px hwleft">Top 10 Instrumentos</h3>
        <div class="table-underline"></div>
        <%top_instrumentos()%>
      </div>
    </div>
    <div class="estrategies-bottom">
      <div class="buttons-wrapper-bottom"><a class="small-button w-button wider" data-ix="fondo-open" href="#">Agregar Fondo</a><a class="small-button w-button wider" href="new-strategy-4.aspx?elemento=<%=request.querystring("elemento") %>&type=<%=request.querystring("type") %>">Regresar a la Operadora</a>
      </div><a class="big-link white" href="#">Cartera Semanal</a><a class="big-link white" href="#">Prospecto de Información</a><a class="big-link white" href="#">Documento CLAVE</a>
    </div>
  </div>
  </form>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="index.aspx">Inicio</a><a class="footer-link" href="#">Registra tu Estrategia</a><a class="footer-link" href="#">Mi Cuenta</a><a class="footer-link" href="faq.aspx">FAQs</a><a class="footer-link" data-ix="open">Login</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>