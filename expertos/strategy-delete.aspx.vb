﻿
Partial Class expertos_strategy_delete
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public EstrategiaName, idestrategia, accion, estrategia_name, idexperto As String
    Public objetivo, max_rv As Integer, comision As Double

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Or String.IsNullOrEmpty(Request.QueryString("elemento")) Then : Response.Redirect("index.aspx") : End If
        cne.ConnectionString = expertos.conectdb
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        accion = Request.QueryString("type")
        idexperto = Session("idexperto")
        If accion <> "eliminacion" Then : Response.Redirect("estrategies.aspx") : End If
        datos_estrategia()
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As System.EventArgs) Handles Me.LoadComplete
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("estrategies.aspx") : End If
    End Sub
    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
        End While
        cne.Close()
    End Function

    Protected Sub btn_eliminar_Click(sender As Object, e As System.EventArgs) Handles btn_eliminar.Click
        'Realizar update de las tablas:
        'asesorias,clientes,operaciones,publicacion_paquete
        'Experto de servicio 504
        Dim sSQL As String
        cne.Open()
        sSQL = "update asesorias set idexperto='504' where idasesoria='" & idestrategia & "' and idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne) : SQLe.ExecuteNonQuery()
        sSQL = "update clientes set idexperto='504' where idcliente='" & idestrategia & "' and idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne) : SQLe.ExecuteNonQuery()
        sSQL = "update operaciones set idexperto='504' where idasesoria='" & idestrategia & "' and idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne) : SQLe.ExecuteNonQuery()
        sSQL = "update publicacion_paquete set idexperto='504' where idestrategia='" & idestrategia & "' and idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne) : SQLe.ExecuteNonQuery()
        sSQL = "insert into estrategias_eliminadas ('" & idestrategia & "','" & idexperto & "','" & expertos.hora_local & "')"
        cne.Close()

        Dim s As New StringBuilder
        s.Append("window.top.location.replace('estrategies.aspx');")
        Page.ClientScript.RegisterStartupScript(GetType(String), "goHome", s.ToString, True)
    End Sub
End Class
