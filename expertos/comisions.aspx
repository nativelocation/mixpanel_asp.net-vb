﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="comisions.aspx.vb" Inherits="expertos_comisions" %>

<!DOCTYPE html>
<html data-wf-page="58a714f5629f8f091e797b62" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Invierte con Expertos - Expertos</title>
  <meta content="Comisiones del Experto" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="brown-section padding-bottom padding-top">
    <div class="nav-bar w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="estrategies.aspx"><img src="images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link w-nav-link" href="estrategies.aspx">Mis Estrategias</a><a class="nav-link w-nav-link" href="comisions.aspx">Mis Comisiones</a><a class="menu-bar-button w-button" href="refer.aspx">Recomienda y Gana</a>
          <div class="dd w-dropdown" data-delay="0">
            <div class="dd-toogle w-dropdown-toggle">
              <div><%=Session("alias_login") %></div>
              <div class="w-icon-dropdown-toggle"></div>
            </div>
            <nav class="dd-list w-dropdown-list"><a class="dd-link w-dropdown-link" href="refer-a2b.aspx">Datos Bancarios</a><a class="dd-link w-dropdown-link" href="password-1.aspx">Cambiar password</a><a class="dd-link w-dropdown-link" href="logout.aspx">Cerrar sesión</a>
            </nav>
          </div>
        </nav>
        <div class="menu-text mt-mobile">Portal
          <br>de Expertos</div>
        <div class="menu-text mt-hide">Portal de Expertos</div>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="hide-mob kkoa table-block">
      <h3 class="table-heading">Comisiones</h3>
      <div class="table-underline"></div>
      <div class="table">
        <div class="head-string table-string">
          <div class="table-item tti-first">
            <div>Mes</div>
          </div>
          <div class="table-item tti-second">
            <div>Comisión Generada</div>
          </div>
          <div class="table-item tti-third">
            <div>Saldo Promedio</div>
          </div>
          <div class="table-item tti-fourth">
            <div>Seguidores</div>
          </div>
        </div>
        <div class="table-underline"></div>
        <div style="color:White;"><h3>No tienes comisiones generadas</h3></div>
        <%--<div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>
        <div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>
        <div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>
        <div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>
        <div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>
        <div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>
        <div class="table-string ts-first">
          <div class="table-item tti-first">
            <div><a href="comisions-2.aspx" class="table-link">ENERO 2017</a>
            </div>
          </div>
          <div class="table-item tti-second">
            <div>$21,105</div>
          </div>
          <div class="table-item tti-third">
            <div>$35,052,025</div>
          </div>
          <div class="_222 table-item ti-fourth">
            <div>250</div>
          </div>
        </div>--%>
      </div>
    </div>
    <div class="kkoa mobile table-block">
      <h3 class="table-heading">Comisiones</h3>
      <div class="table-underline"></div>
      <%--<div class="mobile-table">
        <div class="mobile-table-string mt-heading-string">
          <div><a href="#" class="table-big-link">ENERO 2017</a>
          </div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Comisión Generada:</div>
          <div class="mt-info">$21,105</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Saldo Promedio:</div>
          <div class="mt-info">$35,052,025</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Seguidores:</div>
          <div class="mt-info">250</div>
        </div>
      </div>
      <div class="table-underline"></div>
      <div class="mobile-table">
        <div class="mobile-table-string mt-heading-string">
          <div><a href="#" class="table-big-link">FEBRERO 2017</a>
          </div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Comisión Generada:</div>
          <div class="mt-info">$21,105</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Saldo Promedio:</div>
          <div class="mt-info">$35,052,025</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Seguidores:</div>
          <div class="mt-info">250</div>
        </div>
      </div>
      <div class="table-underline"></div>
      <div class="mobile-table">
        <div class="mobile-table-string mt-heading-string">
          <div><a href="#" class="table-big-link">MARZO 2017</a>
          </div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Comisión Generada:</div>
          <div class="mt-info">$21,105</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Saldo Promedio:</div>
          <div class="mt-info">$35,052,025</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label">Seguidores:</div>
          <div class="mt-info">250</div>
        </div>
      </div>--%>
    </div>
  </div>
  <div class="footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="footer-link" href="index.aspx">Inicio</a><a class="footer-link" href="new-account.aspx">Regístrate como Experto</a><a class="footer-link" href="estrategies.aspx">Mi Cuenta</a><a class="footer-link" href="faq.aspx">FAQs</a>
      </div>
      <div class="footer-column">
        <p class="footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>México CDMX 11000
          <a href="mailto:expertos@invierteconexpertos.mx" class="footer-link">expertos@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="https://www.facebook.com/invierteconexpertos/"><img height="28" src="images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="https://twitter.com/icexpertos"><img height="24" src="images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="https://www.youtube.com/channel/UCvR1sPPQcoru1Gaft-knolg"><img height="25" src="images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <!--<p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>-->
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 8883249;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
    </script>
    <!-- End of LiveChat code -->
</body>
</html>
