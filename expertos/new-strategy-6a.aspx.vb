﻿Imports System.Data

Partial Class expertos_new_strategy_6a
    Inherits System.Web.UI.Page
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idestrategia, idexperto, idoperadora, objetivo, max_rv, idfondo, rv_acum As Integer
    Public estrategia_name, objetivo_name, nombre_operadora As String, comision, p_deuda, p_rv As Double
    Public tipofondo, clasificacion, calificacion, liquidez, fondo, fechaact, sFecha, accion As String
    Public rend30, rend60, rend90, rend_anterior, rend_actual As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If String.IsNullOrEmpty(Session("idexperto")) Or String.IsNullOrEmpty(Session("alias_login")) Then : Response.Redirect("index.aspx") : End If
        If String.IsNullOrEmpty(Request.QueryString("elemento")) Or String.IsNullOrEmpty(Request.QueryString("dist")) Then : Response.Redirect("index.aspx") : End If
        cne.ConnectionString = expertos.conectdb
        idestrategia = g.base64_decode(Request.QueryString("elemento"))
        idexperto = Session("idexperto")
        idoperadora = g.base64_decode(Request.QueryString("dist"))
        idfondo = g.base64_decode(Request.QueryString("itf"))
        accion = g.base64_decode(Request.QueryString("type"))
        datos_estrategia() : fondo = expertos.nombre_fondo(idfondo) : tipofondo = tipo_fondo()
        If tipofondo = 2 Then
            If prevent_fondoRV(idfondo) = 0 Then
                lbl_fondo_info.Text = "No puedes agregar mas Renta Variable para esta estrategia"
                btn_agregar.Visible = False
                txt_porcentaje.Visible = False
            Else
                lbl_fondo_info.Text = "Tu estrategia solo permite agregar hasta el " & prevent_fondoRV(idfondo) & "% de este fondo"
            End If
        Else
            lbl_fondo_info.Text = "Tu estrategia solo permite agregar hasta el " & (100 - (estrategia_porcent() - porcentajeFondo(idfondo))) & "% de este fondo"
        End If
    End Sub

    Function datos_estrategia()
        Dim sSQL As String
        sSQL = "select clave,perfil2,comision,max_rv from asesorias where idasesoria='" & idestrategia & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            estrategia_name = rse("clave")
            objetivo = rse("perfil2")
            comision = rse("comision")
            max_rv = rse("max_rv")
        End While
        cne.Close()
    End Function

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        txt_porcentaje.Attributes.Add("required", "requierd")
        txt_porcentaje.Attributes.Add("type", "text")
        txt_porcentaje.Attributes.Add("placeholder", "%")
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("estrategies.aspx") : End If
    End Sub

    Protected Sub btn_agregar_Click(sender As Object, e As System.EventArgs) Handles btn_agregar.Click
        Dim sSQL As String, porcentaje_estrategia, restante, rv_esperado As Integer
        'Determina el porcentaje que lleva la estrategia
        porcentaje_estrategia = estrategia_porcent()
        restante = 100 - (porcentaje_estrategia - porcentajeFondo(idfondo))
        If IsNumeric(txt_porcentaje.Text) Then
            If rv_fondo(idfondo, CInt(txt_porcentaje.Text)) > 0 Then
                'Sacar lo que representaría agregar el fondo en RV para la estrategia (%poderado)
                rv_esperado = rv_fondo(idfondo, CInt(txt_porcentaje.Text)) + rv_calculada()
                'Si la suma del porcentaje que se va asignar rebasa el max permitido de RV -!Aviso
                If (rv_esperado > max_rv) Or (CInt(txt_porcentaje.Text > prevent_fondoRV(idfondo))) Then
                    If prevent_fondoRV(idfondo) = 0 Then
                        lbl_validacion.Text = "No puedes agregar mas Renta Variable para esta estrategia"
                    Else
                        'Realizar el calulo de lo que se puede agregar del fondo
                        lbl_validacion.Text = "Tu estrategia solo permite agregar hasta el " & prevent_fondoRV(idfondo) & "% de este fondo"
                    End If

                    Exit Sub
                End If
            Else
                If CInt(txt_porcentaje.Text) > restante Then
                    If restante = 0 Then
                        lbl_validacion.Text = "No puedes agregar más fondos. Ya tienes el 100% asignado."
                    Else
                        lbl_validacion.Text = "Solo se puede agregar hasta el " & restante & "% de este fondo"
                    End If
                    Exit Sub
                Else

                End If
            End If
            AgregarFondo(idfondo, idoperadora, txt_porcentaje.Text)
            Dim s As New StringBuilder
            s.Append("window.top.location.replace('new-strategy-3.aspx?elemento=" & Request.QueryString("elemento") & "&type=" & Request.QueryString("type") & "');")
            Page.ClientScript.RegisterStartupScript(GetType(String), "Ingresar", s.ToString, True)
        Else
            lbl_validacion.Text = "Se deben indicar solo valores numéricos"
            Exit Sub
        End If
    End Sub

    Function estrategia_porcent() As Integer
        estrategia_porcent = 0
        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            estrategia_porcent = estrategia_porcent + xRegistro("porcentaje")
        Next
    End Function

    Function rv_calculada() As Integer
        rv_calculada = 0
        Dim tabla As DataTable = Session("EstrategiaUser")
        For Each xRegistro As DataRow In tabla.Rows
            If xRegistro("idfondo") <> idfondo Then
                rv_calculada = rv_calculada + rv_fondo(xRegistro("idfondo"), xRegistro("porcentaje"))
            End If
        Next
    End Function

    Function rv_fondo(ByVal idfondo As Integer, ByVal percent As Integer) As Integer
        Dim sSQL As String, maxrv As Integer
        sSQL = "select max_rv from fondo where id='" & idfondo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : maxrv = SQLe.ExecuteScalar : cne.Close()
        rv_fondo = (percent * maxrv) / 100
    End Function

    Protected Sub AgregarFondo(ByVal idfondo As Integer, ByVal idoperadora As Integer, ByVal porcentaje As Integer)
        Dim id As Integer = idfondo
        If Session("EstrategiaUser") Is Nothing Then
            CrearEstrategia()
        End If
        Dim tabla As DataTable = Session("EstrategiaUser")
        If tabla.Rows.Contains(id) Then
            tabla.Rows.Find(id).Item("porcentaje") = porcentaje
        Else
            Dim fila As DataRow = tabla.NewRow
            fila("idfondo") = id
            fila("idoperadora") = idoperadora
            fila("porcentaje") = porcentaje
            tabla.Rows.Add(fila)
        End If
    End Sub
    Protected Sub CrearEstrategia()
        Dim Tabla As New DataTable
        Tabla.Columns.Add("idfondo", GetType(Integer))
        Tabla.Columns.Add("porcentaje", GetType(Integer))
        Tabla.Columns.Add("idoperadora", GetType(Integer))
        Dim Clave() As DataColumn = {Tabla.Columns("idfondo")}
        Tabla.PrimaryKey = Clave
        Session("EstrategiaUser") = Tabla
    End Sub

    Function porcentajeFondo(ByVal idfondo As Integer) As Integer
        Dim id As Integer = idfondo
        If Session("EstrategiaUser") Is Nothing Then
            CrearEstrategia()
        End If
        Dim tabla As DataTable = Session("EstrategiaUser")
        If tabla.Rows.Contains(id) Then
            porcentajeFondo = tabla.Rows.Find(id).Item("porcentaje")
        Else
            porcentajeFondo = 0
        End If
    End Function

    Function prevent_fondoRV(ByVal idfondo As Integer) As Integer
        Dim sSQL As String, rest_estrategy, rv_esperado, falta_rv As Integer
        'Cuanto Falta por cubrir de la estrategia
        rest_estrategy = 100 - (estrategia_porcent() - porcentajeFondo(idfondo))
        falta_rv = max_rv - rv_calculada()
        rv_esperado = ((falta_rv * 100) / rv_fondo(idfondo, 100))
        If rv_esperado < rest_estrategy Then
            prevent_fondoRV = rv_esperado
        Else
            prevent_fondoRV = rest_estrategy
        End If
    End Function

    Function tipo_fondo() As Integer
        Dim sSQL As String
        sSQL = "select sia_idtipofondo from fondo where id='" & idfondo & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : tipo_fondo = SQLe.ExecuteScalar : cne.Close()
    End Function

End Class
