﻿
Partial Class expertos_refer_a2a
    Inherits System.Web.UI.Page
    Public g As New generales
    Public expertos As New Ice_expertos
    Public mail As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        If Not String.IsNullOrEmpty(Request.QueryString("action")) Then
            Dim mensaje As String
            mensaje = "<p>Te acaban de invitar a que formes parte de la primera plataforma para expertos en fondos de inversión, mediante la cual puedes ganar dinero permitiendo que miles de inversionistas repliquen tu forma de invertir. " & _
            "<b>Tus datos personales son confidenciales, todos los usuarios te conocerán por tu alias (nombre de usuario)</b>. </p>" & _
            "<p>La plataforma no está abierta al público; solamente las personas que reciben esta invitación pueden formar parte de ella.</p>" & _
            "<p>Esta invitación tiene una vigencia de 30 días, por lo que te recomendamos que te registres lo antes posible.</p>" & _
            "<p>Utiliza tu correo (dirección de email) para registrarte en la plataforma. <br>Ingresa a: <a href='http://invierteconexpertos.mx/expertos'>www.invierteconexpertos.mx/expertos</a> y haz clic en Regístrate.</p>"
            mail = g.base64_decode(Request.QueryString("action"))
            If g.IsValidEmail(mail) Then
                expertos.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Experos", Trim(mail), "Experto en Fondos de Inversión", " Te invitaron a Invierte con Expertos", "", mensaje)
                Response.Redirect("refer-a2.aspx")
            End If
        End If

    End Sub
End Class
