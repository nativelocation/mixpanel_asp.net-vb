﻿Option Infer On
Imports System.IO
Imports System.Net
Imports System.Threading.Tasks
Imports MailChimp.Net
Imports Newtonsoft.Json

Partial Class expertos_new_account_1
    Inherits System.Web.UI.Page
    Public expertos As New Ice_expertos, g As New generales
    Public cne As New Data.SqlClient.SqlConnection
    Public SQLe, SQL1e As Data.SqlClient.SqlCommand
    Public rse As Data.SqlClient.SqlDataReader

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Dim sSQL As String, experto_refiere As Integer
        cne.ConnectionString = expertos.conectdb
        If (Request.Cookies("attempt") IsNot Nothing) Then
            If (Request.Cookies("attempt")(g.base64_encode("correo")) IsNot Nothing) Then
                Session("correo_registro") = g.base64_decode(Request.Cookies("attempt")(g.base64_encode("correo")))
            End If
        End If
        If String.IsNullOrEmpty(Session("correo_registro")) Then : Response.Redirect("index.aspx") : End If
        sSQL = "select experto_refiere from referidos_expertos where referido_email='" & Session("correo_registro") & "' and estatus=1"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : experto_refiere = SQLe.ExecuteScalar : cne.Close()
        Session("experto_refiere") = experto_refiere
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then : Response.Redirect("new-account.aspx") : End If
        txt_alias.Attributes.Add("type", "text") : txt_alias.Attributes.Add("required", "required") : txt_alias.Attributes.Add("placeholder", "Alias")
        txt_contrasena.Attributes.Add("type", "password") : txt_contrasena.Attributes.Add("required", "required") : txt_contrasena.Attributes.Add("placeholder", "Contraseña")
        txt_nombre.Attributes.Add("type", "text") : txt_nombre.Attributes.Add("required", "required") : txt_nombre.Attributes.Add("placeholder", "Nombre")
    End Sub

    Protected Sub btn_crear_Click(sender As Object, e As System.EventArgs) Handles btn_crear.Click
        Dim sSQL As String, idexperto As Integer
        If String.IsNullOrEmpty(Session("correo_registro")) Then : Response.Redirect("new-account.aspx") : End If
        sSQL = "insert into expertos (fecha,nombre,correo,alias,pass) values ('" & expertos.hora_local & "','" & UCase(txt_nombre.Text) & "','" & Session("correo_registro") & "','" & txt_alias.Text & "','" & txt_contrasena.Text & "')"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        Dim rows As Integer = SQLe.ExecuteNonQuery()
        If rows > 0 Then
            Dim sqlIdentity As String = "SELECT @@IDENTITY"
            Dim Identity As New Data.SqlClient.SqlCommand(sqlIdentity, cne)
            idexperto = (Convert.ToInt32(Identity.ExecuteScalar()))
        End If
        cne.Close()
        Session("idexperto") = idexperto
        'Se generan las sessiones para indicar que se encuntra con Login el usuario
        Session("con_login") = 1 : Session("correo_login") = Session("correo_registro") : Session("alias_login") = txt_alias.Text
        If Not String.IsNullOrEmpty(Session("idexperto")) Then
            AddOrUpdateListMember("us16", "8e4e8013d9f90ca0e02df34423f4b6fd-us16", "3448f73748", Session("correo_registro"), UCase(txt_nombre.Text), txt_alias.Text)
            Response.Redirect("new-account-1a.aspx")
        End If
    End Sub

    Private Shared Function AddOrUpdateListMember(dataCenter As String, apiKey As String, listId As String, subscriberEmail As String, subscriberName As String, subscriberPhone As String) As String
        Dim sampleListMember = JsonConvert.SerializeObject(New With {
        Key .email_address = subscriberEmail,
        Key .merge_fields = New With {
            Key .EMAIL = subscriberEmail,
            Key .NOMBRE = subscriberName,
            Key .CELULAR = subscriberPhone
        },
        Key .status_if_new = "subscribed"
    })

        Dim hashedEmailAddress = If(String.IsNullOrEmpty(subscriberEmail), "", CalculateMD5Hash(subscriberEmail.ToLower()))
        Dim uri = String.Format("https://{0}.api.mailchimp.com/3.0/lists/{1}/members/{2}", dataCenter, listId, hashedEmailAddress)
        Try
            Using webClient = New WebClient()
                webClient.Headers.Add("Accept", "application/json")
                webClient.Headers.Add("Authorization", Convert.ToString("apikey ") & apiKey)

                Return webClient.UploadString(uri, "PUT", sampleListMember)
            End Using
        Catch we As WebException
            Using sr = New StreamReader(we.Response.GetResponseStream())
                Return sr.ReadToEnd()
            End Using
        End Try
    End Function

    Private Shared Function CalculateMD5Hash(input As String) As String
        ' Step 1, calculate MD5 hash from input.
        Dim md5 = System.Security.Cryptography.MD5.Create()
        Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(input)
        Dim hash As Byte() = md5.ComputeHash(inputBytes)
        ' Step 2, convert byte array to hex string.
        Dim sb = New StringBuilder()
        For Each [byte] As Byte In hash
            sb.Append([byte].ToString("X2"))
        Next
        Return sb.ToString()
    End Function

End Class
