﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="funciona.aspx.vb" Inherits="funciona" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Cómo Funciona</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/funciona.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-100596471-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-53D5L8T');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<section class="hero-area">
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar first"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar last"></span>
                </button>
                <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="funciona.aspx" class="active">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li style="margin-right: 18px;"><a href="preguntas.aspx">FAQs</a></li>
                    <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                    <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                    <li><a href="Micuenta.aspx">Mi Cuenta</a></li>
                    <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="hero-content">
            <h1>Invierte con Expertos</h1>
            <p>
                es la primera plataforma que conecta a personas que quieren invertir, con los mejores expertos
                financieros.
                Estamos transformando la forma de invertir.
            </p>
        </div>  <!-- end hero content -->
    </div>
</section>

<section class="expertos_wrap">
    <div class="expertos_single">
        <div class="expertos_single_cont container">
            <img src="img/funciona/pics/encontramos_pic.jpg" alt="Encontramos a los mejores Expertos"
                 class="expertos_cont_img"/>
            <div class="expertos_cont_text">
                <h2>Encontramos a los mejores Expertos</h2>
                <ul class="expertos_cont_list">
                    <li>Somos la primera comunidad de expertos creada para que ganes mayores rendimientos.</li>
                    <li>Los expertos registran sus estrategias de inversión y nosotros comparamos sus rendimientos para
                        identificar a los más capaces.
                    </li>
                    <li>Todas las estrategias están compuestas por fondos de inversión, autorizados y regulados por la
                        CNBV.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="expertos_single expertos_yellow">
        <div class="expertos_single_cont container">
            <div class="expertos_cont_text">
                <h2>Tú Eliges a los Expertos</h2>
                <ul class="expertos_cont_list">
                    <li>Utiliza nuestro buscador inteligente para identificar las mejores estrategias, de acuerdo a tus
                        objetivos y necesidades.
                    </li>
                    <li>Diversifica tus inversiones y selecciona a más de un asesor, lo que te permitirá reducir tu
                        riesgo y aumentar tu rendimiento .
                    </li>
                    <li>Siempre tendrás la libertad de cambiar de asesor, cuando quieras y sin penalidades.
                    </li>
                </ul>
            </div>
            <img src="img/funciona/pics/eliges_pic.jpg" alt="Tu Eliges a los Expertos"
                 class="expertos_cont_img"/>
        </div>
    </div>
    <div class="expertos_single">
        <div class="expertos_single_cont container">
            <img src="img/funciona/pics/dinero_pic.jpg" alt="Tu dinero en instituciones autorizadas
                    por la CNBV"
                 class="expertos_cont_img"/>
            <div class="expertos_cont_text">
                <h2>Tu dinero en instituciones autorizadas
                    por la CNBV</h2>
                <ul class="expertos_cont_list">
                    <li>Envíanos tus documentos personales para tramitar tu contrato en la institución financiera autorizada para recibir tu dinero.
                    </li>
                    <li>Firma tu contrato y deposita tu dinero directamente en la institución financiera, utilizando la
                        referencia personal que se te proporcione.
                    </li>
                    <li>Recibe mensualmente tu estado de cuenta emitido por la institución financiera.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="expertos_single expertos_blue">
        <div class="expertos_single_cont container">
            <div class="expertos_cont_text">
                <h2>Control total de tus inversiones
                </h2>
                <ul class="expertos_cont_list">
                    <li>Monitorea y controla tus inversiones en un mismo lugar de forma fácil y rápida, desde tu celular
                        o computadora.
                    </li>
                    <li>Revisa en tiempo real el desempeño de tus inversiones, saldos y movimientos de cada una de tus
                        estrategias.
                    </li>
                    <li>Cámbiate de asesor de forma fácil y rápida. Solicita ayuda a uno de nuestros especialistas para
                        que te resuelva cualquier duda.
                    </li>
                </ul>
            </div>
            <img src="img/funciona/pics/control_pic.jpg" alt="Control total de tus inversiones"
                 class="expertos_cont_img"/>
        </div>
    </div>
</section>

<section class="cards">
    <div class="container cards_cont hidden-sm hidden-xs">
        <div class="cards_single">
            <img src="img/funciona/icons/comisiones_icon.svg" alt="Control total de tus inversiones"
                 class="cards_single_img"/>
            <h2 class="cards_single_title">Comisiones</h2>
            <hr class="block_divider">
            <ul class="cards_single_list">
                <li>
                    Cada experto establece una comisión por sus estrategias
                </li>
                <li>La comisión es anual y se paga cada mes sobre el saldo promedio que haya tenido tu cuenta en el mes
                    anterior.
                </li>
                <li>Invierte con Expertos te cobra una cuota única de 149 pesos por apertura
                </li>
            </ul>
        </div>
        <div class="cards_single">
            <img src="img/funciona/icons/minomos_icon.svg" alt="Control total de tus inversiones"
                 class="cards_single_img"/>
            <h2 class="cards_single_title">Mínimos de Inversión</h2>
            <hr class="block_divider">
            <ul class="cards_single_list">
                <li>
                    El importe mínimo para invertir en Invierte con Expertos es de 25,000 pesos.
                </li>
                <li>Cada estrategia tiene un importe mínimo definido previamente por cada Experto, por lo que en algunos casos podría
                    ser mayor a los 25,000 pesos.
                </li>
            </ul>
        </div>
        <div class="cards_single">
            <img src="img/funciona/icons/manejo_icon.svg" alt="Control total de tus inversiones"
                 class="cards_single_img"/>
            <h2 class="cards_single_title">Manejo de Dinero</h2>
            <hr class="block_divider">
            <ul class="cards_single_list">
                <li>Invierte con Expertos NO recibe el dinero que vas a invertir.
                </li>
                <li>Los depósitos los realizas a la institución financiera con la cual abres tu cuenta.
                </li>
                <li>Tu dinero es custodiado por instituciones financieras autorizadas y reguladas por la CNBV.
                </li>
            </ul>
        </div>
    </div>
    <div id="cardsCarousel" class="carousel slide visible-xs visible-sm" data-ride="carousel" data-interval="false">
        <ol class="carousel-indicators">
            <li data-target="#cardsCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#cardsCarousel" data-slide-to="1"></li>
            <li data-target="#cardsCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <div class="cards_single">
                    <img src="img/funciona/icons/comisiones_icon.svg" alt="Control total de tus inversiones"
                         class="cards_single_img"/>
                    <h2 class="cards_single_title">Comisiones</h2>
                    <ul class="cards_single_list">
                        <li>
                            Cada experto establece una comisión por sus estrategias
                        </li>
                        <li>La comisión es anual y se paga cada mes sobre el saldo promedio que haya tenido tu cuenta en
                            el mes
                            anterior.
                        </li>
                        <li>Invierte con Expertos te cobra una cuota única de 149 pesos por apertura
                        </li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="cards_single">
                    <img src="img/funciona/icons/minomos_icon.svg" alt="Control total de tus inversiones"
                         class="cards_single_img"/>
                    <h2 class="cards_single_title">Mínimos de Inversión</h2>
                    <ul class="cards_single_list">
                        <li>
                            El importe mínimo para invertir en Invierte con Expertos es de 25,000 pesos.
                        </li>
                        <li>Cada estrategia tiene un importe mínimo definido previamente por cada Experto, por lo que en algunos
                            casos podría
                            ser mayor a los 25,000 pesos.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="item">
                <div class="cards_single">
                    <img src="img/funciona/icons/manejo_icon.svg" alt="Control total de tus inversiones"
                         class="cards_single_img"/>
                    <h2 class="cards_single_title">Manejo de Dinero</h2>
                    <ul class="cards_single_list">
                        <li>Invierte con Expertos NO recibe el dinero que vas a invertir.
                        </li>
                        <li>Los depósitos los realizas a la institución financiera con la cual abres tu cuenta.
                        </li>
                        <li>Tu dinero es custodiado por instituciones financieras autorizadas y reguladas por la CNBV.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#cardsCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#cardsCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<section class="objetivos_wrap">
    <div class="container objetivos_cont">
        <h1 class="block_title">Objetivos de Inversión que se adaptan a tus necesidades</h1>
        <hr class="block_divider">
        <h2 class="block_subtitle">Todas las estrategias tienen un objetivo de inversión, el cual nunca cambia.</h2>
        <div class="tabs_wrap hidden-xs">
            <ul class="tabs_heads">
                <li class="tabs_heads_single tabs_heads_single_active" data-content="cons">CONSERVADOR</li>
                <li class="tabs_heads_single" data-content="moder">MODERADO</li>
                <li class="tabs_heads_single" data-content="empr">EMPRENDEDOR</li>
                <li class="tabs_heads_single" data-content="agres">AGRESIVO</li>
                <li class="tabs_heads_single" data-content="espec">ESPECULATIVO</li>
            </ul>
            <div class="tabs_cont">
                <div class="tabs_single" data-head="cons">
                    <h3 class="tabs_single_title">Conservador:</h3>
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos bajos asumiendo un riesgo bajo.</p>
                        <ul>
                            <li>Invierten hasta un 20% en Renta Variable</li>
                            <li>Baja volatilidad</li>
                            <li>Horizonte de inversión a 1 año</li>
                        </ul>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/conservador_icon.svg"/>
                </div>
                <div class="tabs_single" data-head="moder">
                    <h3 class="tabs_single_title">Moderado:</h3>
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos moderados asumiendo un riesgo medio.</p>
                        <ul>
                            <li>Invierten hasta un 40% en Renta Variable</li>
                            <li>Media volatilidad</li>
                            <li>Horizonte de inversión a 2 años</li>
                        </ul>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/moderado_icon.svg"/>
                </div>
                <div class="tabs_single" data-head="empr">
                    <h3 class="tabs_single_title">Emprendedor:</h3>
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos muy altos asumiendo un riesgo alto.</p>
                        <ul>
                            <li>Invierten hasta un 60% en Renta Variable</li>
                            <li>Alta volatilidad</li>
                            <li>Horizonte de inversión a 3 años</li>
                        </ul>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/emprendedor_icon.svg"/>
                </div>
                <div class="tabs_single" data-head="agres">
                    <h3 class="tabs_single_title">Agresivo:</h3>
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos altos asumiendo un riesgo medio-alto.</p>
                        <ul>
                            <li>Invierten hasta un 80% en Renta Variable</li>
                            <li>Media-Alta volatilidad</li>
                            <li>Horizonte de inversión a 4 años</li>
                        </ul>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/agresivo_icon.svg"/>
                </div>
                <div class="tabs_single" data-head="espec">
                    <h3 class="tabs_single_title">Especulativo:</h3>
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir los más altos rendimientos asumiendo un riesgo muy alto.</p>
                        <ul>
                            <li>Invierten hasta un 100% en Renta Variable</li>
                            <li>Muy alta volatilidad</li>
                            <li>Horizonte de inversión a 5 años</li>
                        </ul>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/especulativo_icon.svg"/>
                </div>
            </div>
        </div>
        <div class="mob-tabs_wrap visible-xs">
            <button class="mob-tab_head mob-tab_head_active" data-toggle="collapse" data-target="#mobTab0">CONSERVADOR
            </button>
            <div id="mobTab0" class="collapse in mob-tab_cont">
                <div class="tabs_single" data-head="cons">
                    <h3 class="tabs_single_title">Lorem ipsum dolor sit amet, consectetur</h3>
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos moderados asumiendo
                            un riesgo medio.</p>
                        <ul>
                            <li>Invierten hasta un 20% en Renta Variable</li>
                            <li>Mediana volatilidad</li>
                        </ul>
                        <p>Horizonte de inversión a 2 años</p>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/conservador_icon.svg"/>
                </div>
            </div>
            <button class="mob-tab_head" data-toggle="collapse" data-target="#mobTab1">MODERADO</button>
            <div id="mobTab1" class="collapse mob-tab_cont">
                <div class="tabs_single" data-head="cons">
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos moderados asumiendo
                            un riesgo medio.</p>
                        <ul>
                            <li>Invierten hasta un 20% en Renta Variable</li>
                            <li>Mediana volatilidad</li>
                        </ul>
                        <p>Horizonte de inversión a 2 años</p>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/conservador_icon.svg"/>
                </div>
            </div>
            <button class="mob-tab_head" data-toggle="collapse" data-target="#mobTab2">EMPRENDEDOR</button>
            <div id="mobTab2" class="collapse mob-tab_cont">
                <div class="tabs_single" data-head="cons">
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos moderados asumiendo
                            un riesgo medio.</p>
                        <ul>
                            <li>Invierten hasta un 20% en Renta Variable</li>
                            <li>Mediana volatilidad</li>
                        </ul>
                        <p>Horizonte de inversión a 2 años</p>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/conservador_icon.svg"/>
                </div>
            </div>
            <button class="mob-tab_head" data-toggle="collapse" data-target="#mobTab3">AGRESIVO</button>
            <div id="mobTab3" class="collapse mob-tab_cont">
                <div class="tabs_single" data-head="cons">
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos moderados asumiendo
                            un riesgo medio.</p>
                        <ul>
                            <li>Invierten hasta un 20% en Renta Variable</li>
                            <li>Mediana volatilidad</li>
                        </ul>
                        <p>Horizonte de inversión a 2 años</p>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/conservador_icon.svg"/>
                </div>
            </div>
            <button class="mob-tab_head" data-toggle="collapse" data-target="#mobTab4">ESPECULATIVO</button>
            <div id="mobTab4" class="collapse mob-tab_cont">
                <div class="tabs_single" data-head="cons">
                    <div class="tabs_single_text">
                        <p>Estrategias recomendadas para personas que quieren recibir rendimientos moderados asumiendo
                            un riesgo medio.</p>
                        <ul>
                            <li>Invierten hasta un 20% en Renta Variable</li>
                            <li>Mediana volatilidad</li>
                        </ul>
                        <p>Horizonte de inversión a 2 años</p>
                    </div>
                    <img class="tabs_single_img" src="img/funciona/icons/conservador_icon.svg"/>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="buscar_wrap">
    <div class="container buscar_cont" style="min-height:350px;">
        <p class="buscar_subtitle">No te conformes con la persona que te asignó tu institución financiera, mejor
            contrata a los mejores. Comienza hoy y gana más.</p>
        <hr class="block_divider">
        <button class="btn buscar_btn" onclick="location.href='buscar-expertos.aspx';">Buscar Expertos</button>
    </div>
    <hr class="main_divider">
</section>
<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>
<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
    </div>
</footer>

<script src="js/general_bundle.js"></script>
<script src="js/funciona_bundle.js"></script>
</body>
</html>