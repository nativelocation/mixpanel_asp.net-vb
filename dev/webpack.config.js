const path = require('path');
const webpack = require('webpack');

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        general: ['../node_modules/bootstrap/dist/js/bootstrap.min.js', './general.js'],
        index: ['./index.js'],
        funciona: ['./funciona.js'],
        contactenos: ['./contactenos.js'],
        buscar: ['./buscar.js'],
        detalle: ['./detalle.js']
    },
    output: {
        path: path.resolve(__dirname, './dist/js'),
        filename: '[name]_bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [path.resolve(__dirname, "src")],
                exclude: [/node_modules/],
                options: {presets: ['es2015']}
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jquery: "jquery",
            "window.jQuery": "jquery",
            jQuery:"jquery"
        }),
    ]
};
