import {sticky} from 'jquery-sticky';

$(function () {
    $(".navbar").sticky({ topSpacing: 0 });

    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });
    let splitLocation = location.pathname.split('/');
    let page = splitLocation[2];

    $(`.navbar-nav a[href="${page}"]`).addClass("activeLink");

});