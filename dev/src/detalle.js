let d3 = require("d3");

$(function () {
    //Use Google Charts to draw graphs
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(drawLineGraph);
    google.charts.setOnLoadCallback(drawDonutChart);
    function drawLineGraph() {
        let data = new google.visualization.arrayToDataTable([
         ['Date', 'Manager (ner or fes)', 'S&P 500'],
         [new Date(2016, 7, 10), 0, 0],
         [new Date(2016, 7, 11), 0, 0],
         [new Date(2016, 7, 12), 25, 15],
         [new Date(2016, 7, 13), 22, 15],
         [new Date(2016, 7, 14), 65, 60],
         [new Date(2016, 7, 15), 30, 45],
         [new Date(2016, 7, 17), 90, 50],
         [new Date(2016, 7, 18), 110, 30]
         ]);

         //Graph adaptivity
         let graphWidth,
         fontSize;
         if ($(window).width() <= 768) {
         graphWidth = '80%';
         fontSize = 12;
         }
         else {
         graphWidth = '90%';
         fontSize = 14;
         }

         let options = {
         enableInteractivity: false,
         hAxis: {
         textStyle: {color: '#333', fontSize: fontSize},
         format: 'MMM d y',
         gridlines: {color: 'transparent'}
         },
         vAxis: {
         textStyle: {color: '#333', fontSize: fontSize},
         ticks: [-25, 0, 25, 50, 75, 100, 125],
         baseline: -30,
         format: '#\'%\'',
         gridlines: {count: 7}
         },
         lineWidth: 4,
         legend: {position: 'bottom', alignment: 'start', textStyle: {fontSize: fontSize}},
         series: {
         0: {color: '#f6b447', areaOpacity: 0.6},
         1: {color: '#535f6f', areaOpacity: 0.2}
         },
         chartArea: {width: graphWidth}
         };

         let chart = new google.visualization.AreaChart(document.getElementById('desempeno_graph'));
         chart.draw(data, options);

        //New Chart
        /*let data = [
                {date: '2016-07-10', percentage: 0},
                {date: '2016-07-11', percentage: 0},
                {date: '2016-07-12', percentage: 25},
                {date: '2016-07-13', percentage: 22},
                {date: '2016-07-14', percentage: 65},
                {date: '2016-07-15', percentage: 30},
                {date: '2016-07-17', percentage: 90},
                {date: '2016-07-18', percentage: 110}
            ],
            margin = {top: 20, right: 20, bottom: 40, left: 50},
            width = $('#desempeno_graph').width() - margin.left - margin.right,
            height = $('#desempeno_graph').height() - margin.top - margin.bottom,
            parseTime = d3.timeParse('%Y-%m-%d');

        data.forEach(function (d) {
            d.date = parseTime(d.date);
            return d
        });

        let x = d3.scaleTime()
            .domain(d3.extent(data, function(d) { return d.date; }))
            .range([0, width]);

        let y = d3.scaleLinear()
            .domain([0, d3.max(data, function(d) { return d.percentage; })])
            .range([height, 0]);

        let xAxis = d3.axisBottom(x);
        let yAxis = d3.axisLeft(y);

        var area = d3.area()
            .x(function(d) { return x(d.date); })
            .y0(height)
            .y1(function(d) { return y(d.percentage); });

        var svg = d3.select('#desempeno_graph')
            .append('svg')
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.append("path")
            .datum(data)
            .attr("class", "area")
            .attr("d", area);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis);*/

    }

    function drawDonutChart() {
        let dataset = [
                {label: 'Deuda', percentage: 75.55}
            ],
            padding = 20,
            width = $('#composicion_chart').width() - 2 * padding,
            height = $('#composicion_chart').height(),
            radius = height / 2.5;
        if ($(window).width() <= 425) {
            radius = height / 3.5
        }

        let dataPercents = dataset.map(function (data) {
            return data.percentage;
        });
        dataPercents = dataPercents.reduce(function (a, b) {
            return a + b
        });
        let restPercents = 100 - dataPercents;
        let datasetWithRest = dataset.concat({label: 'Rest', percentage: restPercents});


        let color = d3.scaleOrdinal().range(['#f5a623', '#f0f2f5']);

        const svg = d3.select('#composicion_chart')
            .append('svg')
            .attr('width', width)
            .attr('height', height)
            .append('g');


        const arc = d3.arc()
            .innerRadius(radius / 1.1)
            .outerRadius(radius);


        const pie = d3.pie()
            .value(function (d) {
                return d.percentage;
            })
            .sort(null);

        let path = svg.selectAll('path')
            .data(pie(datasetWithRest))
            .enter()
            .append('path')
            .attr('d', arc)
            .attr('fill', function (d, i) {
                return color(d.data.label);
            })
            .attr('transform', 'translate(' + (width / 1.6) + ',' + (height / 2) + ')');

        let innerCircle = svg.selectAll('circle')
            .data([1])
            .enter()
            .append('circle')
            .attr('cx', width / 1.6)
            .attr('cy', height / 2)
            .attr('r', radius / 1.5)
            .attr('stroke', '#f0f2f5')
            .attr('stroke-width', '5px')
            .attr('fill', 'transparent');

        //Legend
        let legendCircleRadius = 8,
            legendSpacing = 4;

        let legend = svg.selectAll('legend')
            .data(dataset)
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) {
                let height = legendCircleRadius + legendSpacing;
                let offset = height * dataset.length / 2;
                let horz = 3.5 * legendCircleRadius;
                let vert = i * 1 - offset + 290;
                return 'translate(' + horz + ',' + vert + ')';
            });

        legend.append('circle')
            .attr('r', legendCircleRadius)
            .style('fill', color)
            .style('stroke', color);

        legend.append('text')
            .attr('x', legendCircleRadius + legendSpacing * 2)
            .attr('y', legendCircleRadius - legendSpacing * 3)
            .text(function (d) {
                return d.label;
            });
        legend.append('text')
            .attr('x', legendCircleRadius + legendSpacing * 2)
            .attr('y', legendCircleRadius + legendSpacing * 2)
            .attr('opacity', 0.6)
            .text(function (d) {
                return d.percentage + '%';
            });
    }


    //Resize graph
    $(window).resize(function () {
        if (this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function () {
            $(this).trigger('resizeEnd');
        }, 500);
    });

    $(window).on('resizeEnd', function () {
        drawLineGraph();
        $('#composicion_chart').find('svg').remove();
        drawDonutChart();
    });

    //Display description text to Objetivo Items

    //Resize Description Zone to Items Zone height
    if ($(window).width() > 1000) {
        $('.objetivo_descr').css('height', $('.objetivo_left-block').height());
    }
    else {
        $('.objetivo_descr').css('height', 'auto');
    }

    $('.objetivo_list_item').on('click', function () {
        setActiveItem($(this));
    });

    function setActiveItem(item) {
        $('.objetivo_list_item-active').removeClass('objetivo_list_item-active');
        item.addClass('objetivo_list_item-active');
        getMeasure(item);
    }

    //Setting width for measure bar and content for its popup.
    function getMeasure(item) {
        let itemNum = item.find('.objetivo_list_num').html(),
            itemName = item.find('.objetivo_list_name').html(),
            fillPopUp = $('.objetivo_descr_measure_popup'),
            fillWidth = itemNum * 20 + '%';
        $('.objetivo_descr_measure_fill').css('width', fillWidth);
        $('.objetivo_descr_measure_popup_num').text(itemNum);
        $('.objetivo_descr_measure_popup_text').text(itemName);

        fillPopUp.css('right', '-' + fillPopUp.width() / 2.1 + 'px');
        setDescriptionContent(item, itemName);
    }

    function setDescriptionContent(item, itemName) {
        let itemDescr = item.attr('data-descr');
        $('.objetivo_descr_text').html(itemDescr);
        $('.objetivo_descr_title').html(itemName);
    }

    //Make first item active on load
    setActiveItem($('.objetivo_list_item:first-child'));

    //Show/Hide modal
    $('#modalBtn').on('click', function () {
        $('#detalleModal').toggleClass('detalle_modal_visible');
    });

    $('#regresarAStrategia').on('click', function () {
        $('#detalleModal').toggleClass('detalle_modal_visible');
    });
});
