require ('jquery-touchswipe');
require ('../node_modules/waypoints/lib/jquery.waypoints.min');
require ('lightgallery/dist/js/lightgallery.min');
require('lg-video/dist/lg-video.min');

$(function() {
    $(".carousel-inner").swipe( {
        swipeLeft:function() {
            $(this).parent().carousel('next'); 
        },
        swipeRight: function() {
            $(this).parent().carousel('prev'); 
        },
        threshold:0
    });

$(function() {
    const waypoint = new Waypoint({
        element: $('#bars'),
        handler: function() {
            $("#bars li .bar").each( function() {
                let percentage = $(this).data('percentage');
                $(this).animate({
                    'height' : (percentage * 5.8) + '%'
                }, 1000);
                $(document).off('scroll');
            });
        },
        offset: '100%'
    })
});

let splitLocation = location.pathname.split('/');
let page = splitLocation[2];

$(`.navbar-nav a[href="${page}"]`).addClass("activeLink");


//Video Lightbox
    $('#videoLightbox').lightGallery({
        youtubePlayerParams: {
            autoplay: 1,
            showinfo: 0
        }
    });
});