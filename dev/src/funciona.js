require ('jquery-touchswipe');

$(function () {
    $(".carousel-inner").swipe( {
        swipeLeft:function() {
            $(this).parent().carousel('next');
        },
        swipeRight: function() {
            $(this).parent().carousel('prev');
        },
        threshold:0
    });

    function showTabSingle(head) {
        let conn = $(head).attr('data-content');
        $('.tabs_single_active').removeClass('tabs_single_active');
        $('[data-head="' + conn + '"]').addClass('tabs_single_active');
    };

    $('.tabs_heads_single').on('click', function () {
        let newActive = $(this);
        $('.tabs_heads_single_active').removeClass('tabs_heads_single_active');
        newActive.addClass('tabs_heads_single_active');
        showTabSingle(newActive);
    });

    $('.mob-tab_head').on('click', function () {
        $('.mob-tab_head_active').removeClass('mob-tab_head_active');
        $('.in').removeClass('in');
        $(this).addClass('mob-tab_head_active');
    });

    showTabSingle($('.tabs_heads_single_active'));
});
