
$(function () {
    var map = new GMaps({
        div: '#map',
        lat: 19.4237628,
        lng: -99.2049538,
        zoom: 16,
        disableDefaultUI: true
    });
    var marker = map.addMarker({
        lat: 19.4237628,
        lng: -99.2049538,
        icon: 'img/contactenos/icons/marker.png'
    });

    var infowindow = new google.maps.InfoWindow({
        content: '<p class="map_popup">Montes Urales 749, Lomas de Chapultepec, CDMX 11000</p>'
    });

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    var styles = [
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#444444"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#d0d6e5"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#2b3b4e"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#2b3b4e"
                },
                {
                    "weight": "0.67"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#d0d6e5"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#2b3b4e"
                },
                {
                    "visibility": "on"
                }
            ]
        }
    ]
    map.setOptions({
        styles: styles
    });

    $('.cont_single_head').on('click', function () {
        $(this).toggleClass('cont_single_head_active');
    })
});