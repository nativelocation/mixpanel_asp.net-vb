﻿
Partial Class confirmation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then
            Response.Redirect("datos-de-contacto.aspx")
            Session.Remove("idinversionista")
            Session.Remove("correo_registro")
        End If
    End Sub
End Class
