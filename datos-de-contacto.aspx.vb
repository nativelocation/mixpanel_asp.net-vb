﻿Option Infer On
Imports System.IO
Imports System.Net
Imports System.Threading.Tasks
Imports MailChimp.Net
Imports Newtonsoft.Json



Partial Class datos_de_contacto
    Inherits System.Web.UI.Page
    Public cni As New Data.SqlClient.SqlConnection
    Public SQLi As Data.SqlClient.SqlCommand
    Public rsi As Data.SqlClient.SqlDataReader
    Public inversionista As New ICE_clientes
    Public g As New generales

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        cni.ConnectionString = inversionista.conectdb
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        nombre.Attributes.Add("type", "text") : nombre.Attributes.Add("required", "required")
        telefono.Attributes.Add("type", "tel") : telefono.Attributes.Add("required", "required")
        correo.Attributes.Add("type", "email") : correo.Attributes.Add("required", "required")
        contrasena.Attributes.Add("type", "password") : contrasena.Attributes.Add("required", "required")
    End Sub

    Protected Sub btn_continuar_Click(sender As Object, e As System.EventArgs) Handles btn_continuar.Click
        Dim sSQL, idinversionista As String
        If revisar_existencia() > 0 Then
            lbl_mensaje.Text = "Los datos que ingresate ya existen. Inicia sesión en Mi cuenta"
            Exit Sub
        End If
        sSQL = "insert into Inversionistas (fecha,nombre,telefono,correo,pass,activada,estatus) " &
        "values ('" & inversionista.hora_local & "','" & UCase(nombre.Text) & "','" & telefono.Text & "','" & LCase(Trim(correo.Text)) & "','" & contrasena.Text & "',0,0)"
        SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
        cni.Open()
        Dim rows As Integer = SQLi.ExecuteNonQuery()
        If rows > 0 Then
            Dim sqlIdentity As String = "SELECT @@IDENTITY"
            Dim Identity As New Data.SqlClient.SqlCommand(sqlIdentity, cni)
            idinversionista = (Convert.ToInt32(Identity.ExecuteScalar()))
        End If
        cni.Close()
        Session("idinversionista") = idinversionista
        Session("correo_registro") = LCase(Trim(correo.Text))
        If Not String.IsNullOrEmpty(idinversionista) Then
            AddOrUpdateListMember("us16", "8e4e8013d9f90ca0e02df34423f4b6fd-us16", "7f28b9600b", Session("correo_registro"), UCase(nombre.Text), telefono.Text)
        End If
        Response.Redirect("datos-de-contacto2.aspx")
    End Sub

    Function revisar_existencia() As Integer
        Dim sSQL As String
        sSQL = "select count(*)encontrado from inversionistas where correo='" & LCase(Trim(correo.Text)) & "'"
        SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
        cni.Open() : revisar_existencia = SQLi.ExecuteScalar : cni.Close()
    End Function

    Private Shared Function AddOrUpdateListMember(dataCenter As String, apiKey As String, listId As String, subscriberEmail As String, subscriberName As String, subscriberPhone As String) As String
        Dim sampleListMember = JsonConvert.SerializeObject(New With {
        Key .email_address = subscriberEmail,
        Key .merge_fields = New With {
            Key .EMAIL = subscriberEmail,
            Key .NOMBRE = subscriberName,
            Key .CELULAR = subscriberPhone
        },
        Key .status_if_new = "subscribed"
    })

        Dim hashedEmailAddress = If(String.IsNullOrEmpty(subscriberEmail), "", CalculateMD5Hash(subscriberEmail.ToLower()))
        Dim uri = String.Format("https://{0}.api.mailchimp.com/3.0/lists/{1}/members/{2}", dataCenter, listId, hashedEmailAddress)
        Try
            Using webClient = New WebClient()
                webClient.Headers.Add("Accept", "application/json")
                webClient.Headers.Add("Authorization", Convert.ToString("apikey ") & apiKey)

                Return webClient.UploadString(uri, "PUT", sampleListMember)
            End Using
        Catch we As WebException
            Using sr = New StreamReader(we.Response.GetResponseStream())
                Return sr.ReadToEnd()
            End Using
        End Try
    End Function

    Private Shared Function CalculateMD5Hash(input As String) As String
        ' Step 1, calculate MD5 hash from input.
        Dim md5 = System.Security.Cryptography.MD5.Create()
        Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(input)
        Dim hash As Byte() = md5.ComputeHash(inputBytes)
        ' Step 2, convert byte array to hex string.
        Dim sb = New StringBuilder()
        For Each [byte] As Byte In hash
            sb.Append([byte].ToString("X2"))
        Next
        Return sb.ToString()
    End Function

End Class
