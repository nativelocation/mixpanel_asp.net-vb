﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="index.aspx.vb" Inherits="index" %>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/lightgallery.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/index_test.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-100596471-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-53D5L8T');</script>
    <!-- End Google Tag Manager -->
    <script src="//cdn.mxpnl.com/site_media/js/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script>
        function RegInfo() {
            //alert("hellow");
            distinct_id = window.localStorage.getItem('distinct_id');
            if (distinct_id == null) {
                window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                distinct_id = window.localStorage.getItem('distinct_id');
            }

            var name = document.getElementById("txt_nombre");
            var telephone = document.getElementById("txt_telefono");
            var email = document.getElementById("txt_correo");
            var password = document.getElementById("txt_pass");  
            $("#txt_distinct").val(distinct_id);
            if (name.value !== "" && telephone.value!== "" && email.value !== "" && password !== "" ) {
                if (validateEmail(email.value) && /^\d*$/.test(telephone.value)) {
                    dataLayer.push({
                            'event' : 'register',
                            'Name' : name.value,
                            'UserType' : 'investor',       
                            'Email' : email.value,
                            'Number' : telephone.value
                    });
                }
            }
            return true;
        }
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    </script>
</head>
<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <section class="hero-area">
        <div class="hero-overlay"></div>
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar first"></span>
                        <span class="icon-bar middle"></span>
                        <span class="icon-bar last"></span>
                    </button>
                    <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="funciona.aspx" id="howWorkH" >Cómo Funciona</a></li>
                        <li><a href="buscar-expertos.aspx" id="searchEH" >Buscar Expertos</a></li>
                        <li style="margin-right: 18px;"><a href="preguntas.aspx" id="faqH" >FAQs</a></li>
                        <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                        <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                        <li><a href="Micuenta.aspx" id="myaccountH" >Mi Cuenta</a></li>
                        <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
                    <div class="hero-content index_hero-content">
                        <div class="index_hero-content-half test1_hero-content-half">
                            <h1 class="hero_content_title test1_hero_title test3_hero_content_title">Invierte con los mejores</h1>
                            <p class="hero_content_text test1_hero_text test3_hero_content_text">
                                Encuentra y contrata a los expertos en inversiones que han generado mayores rendimientos en fondos de inversión
                            </p>
                            <div class="test1_index_video-form_wrap">
                                <div class="index_hero-content_video_wrap test1_video">
                                    <div id="videoLightbox" class="test1_lightbox">
                                        <a href="https://www.youtube.com/watch?v=g9IZQ0ALyKg">
                                            <img class="index_hero-content_video test1_video_img" src="img/video.jpg">
                                        </a>
                                    </div>
                                </div>
                                <div class="test1_index_form_wrap">
                                    <form id="test1_reg_form" class="test1_index_form" runat="server">
                                        <h5 class="test1_index_form_title">Regístrate</h5>
                                        <hr class="block_divider test1_form_divider">
                                        <p class="test1_index_form_text">¡Prueba el servicio por 30 días gratis!</p>
                                        <div class="test1_form_input-sm_wrap">
                                            <asp:TextBox ID="txt_nombre" runat="server" placeholder="Nombre" CssClass="test1_index_form_input test1_index_form_input-sm"></asp:TextBox>
                                            <asp:TextBox ID="txt_telefono" runat="server" placeholder="Número de móvil" CssClass="test1_index_form_input test1_index_form_input-sm"></asp:TextBox>
                                        </div>
                                        <asp:TextBox ID="txt_correo" runat="server" placeholder="Correo Electrónico" CssClass="test1_index_form_input" ValidateRequestMode="Enabled" TextMode="Email"></asp:TextBox>
                                        <asp:TextBox ID="txt_pass" runat="server" placeholder="Crea tu Contraseña" CssClass="test1_index_form_input" ValidateRequestMode="Enabled" TextMode="Password"></asp:TextBox>
                                        <asp:TextBox ID="txt_distinct" runat="server" placeholder="Crea tu Contraseña" CssClass="test1_index_form_input_hidden" ValidateRequestMode="Enabled" TextMode="Password"></asp:TextBox>
                                        <asp:Button ID="btn_registro"  OnClientClick="RegInfo();" runat="server" Text="Prueba el servicio" CssClass="btn hero-btn1 test1_form_btn" />
                                        <asp:Label ID="lbl_message" runat="server" Text="" style="background-color:#EA4335; color:white;"></asp:Label>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <section class="home-widgets-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-4">
                    <div class="home-single-widget">
                        <div class="home-widget-img">
                            <img src="img/search-icon.png" alt="">
                        </div>                        
                        <div class="home-widget-content">
                            <h3>Muy Fácil</h3>
                            <p>Selecciona a los expertos fácil y rápido con nuestro buscador inteligente</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="home-single-widget">
                        <div class="home-widget-img">
                            <img src="img/coins-icon.png" alt="">
                        </div>                        
                        <div class="home-widget-content">
                            <h3>Accesible</h3>
                            <p>Invertir como un gran inversionista ahora es posible, desde 25,000 pesos</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="home-single-widget">
                        <div class="home-widget-img">
                            <img src="img/worker-icon.png" alt="">
                        </div>                        
                        <div class="home-widget-content">
                            <h3>Altos Rendimientos</h3>
                            <p>Identificamos a los expertos que han generado mayores rendimientos</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="graph-area">
        <div class="container">
            <h3>Hasta 248% más de rendimiento que los instrumentos tradicionales</h3>

            <div id="chart" class="clearfix">
                <ul id="numbers">
                    <li><span>15%</span></li>
                    <li><span>10%</span></li>
                    <li><span>5%</span></li>
                    <li><span>0%</span></li>
                </ul>

                <ul id="bars">
                    <li>
                        <div class="dots">
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                        </div>
                    </li>
                    <li><div data-percentage="4.17" class="bar"></div><span>Cetes 28 días</span></li>
                    <li>
                        <div class="dots">
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                        </div>
                    </li>
                    <li><div data-percentage="6.20" class="bar"></div><span>BMV Bolsa</span></li>
                    <li>
                        <div class="dots">
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                        </div>
                    </li>
                    <li><div data-percentage="14.56" class="bar"></div><span>Invierte con Expertos</span></li>
                    <li>
                        <div class="dots">
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                            <span>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>


    <section class="invest-steps">
        <div class="container">
            <h2 class="bordered-heading">Invierte en 4 simples pasos</h2>

            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="invest-step-widget">
                        <div class="invest-widget-img">
                            <img src="img/elige-icon.png" alt="">
                        </div>

                        <div class="invest-widget-content">
                            <h3><span>1</span> Elige</h3>
                            <p>
                                Selecciona estrategias de la comunidad de expertos
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="invest-step-widget">
                        <div class="invest-widget-img">
                            <img src="img/registrate-icon.png" alt="">
                        </div>
                        <div class="invest-widget-content">
                            <h3><span>2</span> Regístrate</h3>
                            <p>
                                Ingresa tus datos
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="invest-step-widget">
                        <div class="invest-widget-img">
                            <img src="img/contracto-icon.png" alt="">
                        </div>

                        <div class="invest-widget-content">
                            <h3><span>3</span> Abre tu Contrato</h3>
                            <p>
                                Envía tus documentos para abrir tu contrato
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="invest-step-widget">
                        <div class="invest-widget-img">
                            <img src="img/deposita-icon.png" alt="">
                        </div>

                        <div class="invest-widget-content">
                            <h3><span>4</span> Deposita</h3>
                            <p>
                                Deposita a la institución financiera
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="specifications-area">
        <div class="specifications-overlay"></div>
        <div class="container">
            <div class="row hide-mbl">
                <div class="col-sm-6">
                    <ul class="rounded-check-list list-unstyled">
                        <li>
                            <h3>Transparente</h3>
                            <p>Conoces de forma fácil en dónde inviertes y cómo se comportan tus inversiones.</p>
                        </li>
                        <li>
                            <h3>Atención Personalizada</h3>
                            <p>Ejecutivos especializados te ayudan en lo que necesites.</p>
                        </li>
                        <li>
                            <h3>Regulado</h3>
                            <p>Todos los fondos de inversión son regulados por la CNBV.</p>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <ul class="rounded-check-list list-unstyled">
                        <li>
                            <h3>Diversificado</h3>
                            <p>Puedes diversificar tus inversiones contratando a varios asesores, incrementando tu rendimiento y reduciendo tu riesgo.</p>
                        </li>
                        <li>
                            <h3>Seguro</h3>
                            <p>Depositas directamente en instituciones financieras de reconocido prestigio, nosotros no recibimos tu dinero.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="mbl-spec-slider" class="carousel slide show-mbl" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#mbl-spec-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#mbl-spec-slider" data-slide-to="1" class=""></li>
                    <li data-target="#mbl-spec-slider" data-slide-to="2" class=""></li>
                    <li data-target="#mbl-spec-slider" data-slide-to="3" class=""></li>
                    <li data-target="#mbl-spec-slider" data-slide-to="4" class=""></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="img/fill-1-copy@2x.png" alt="">
                        <h3>Transparente</h3>
                        <p>Conoces de forma fácil en dónde inviertes y cómo se comportan tus inversiones.</p>
                    </div>
                    <div class="item">
                        <img src="img/fill-1-copy@2x.png" alt="">
                        <h3>Atención Personalizada</h3>
                        <p>Ejecutivos especializados te ayudan en lo que necesites.</p>
                    </div>
                    <div class="item">
                        <img src="img/fill-1-copy@2x.png" alt="">
                        <h3>Regulado</h3>
                        <p>Todos los fondos de inversión son regulados por la CNBV.</p>
                    </div>
                    <div class="item">
                        <img src="img/fill-1-copy@2x.png" alt="">
                        <h3>Diversificado</h3>
                        <p>Puedes diversificar tus inversiones contratando a varios asesores, incrementas tu rendimiento y reduces tu riesgo.</p>
                    </div>
                    <div class="item">
                        <img src="img/fill-1-copy@2x.png" alt="">
                        <h3>Seguro</h3>
                        <p>Depositas directamente en instituciones financieras de reconocido prestigio, nosotros no recibimos tu dinero.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="strategies">
        <div class="container">
            <h2 class="bordered-heading">Estrategias manejadas por personas</h2>

            <p>
                Las estrategias tienen un mínimo de inversión de <span>25,000</span> pesos y una comisión para el experto entre 0.5% y 1.5% anual, nosotros te cobramos únicamente 149 pesos por apertura.
            </p>
            <br />
            <div class="fs-btns">
                <button class="btn fs-btn1" onclick="sExpert(); location.href='buscar-expertos.aspx';">Buscar Expertos</button>
                <button class="btn fs-btn2" onclick="hWork(); location.href='funciona.aspx';">Cómo Funciona</button>
            </div>                
        </div>
    </section>
    <section class="expert-cta-area">
        <div class="expert-cta-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-5">
                    <div class="expert-cta-heading">
                        <h3>¿Eres un experto?</h3>                        
                    </div>
                </div>
                <div class="col-sm-6 col-lg-7">
                    <div class="expert-cta-content">
                        <p>Invierte con Expertos te permite ganar dinero como experto independiente.</p>
                        <p>Ayuda a la comunidad de inversionistas a ganar excelentes rendimientos generando portafolios de fondos de inversión.</p>

                        <button class="btn cta-btn" onclick="findmore(); location.href='/expertos';">Conoce más</button>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>
    <footer>
        <div class="container">
            <div class="footer-top-area hidden-xs clearfix">
                <div class="footer-logo-area">
                    <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                                  style="width:125px; height: 50px;"></a>
                    <div class="footer-logo-text">
                        Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                    </div>
                </div>
                <div class="footer-smi">
                    <ul class="list-unstyled list-inline">
                        <li><a href="https://www.facebook.com/invierteconexpertos/" id="sfbB" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/icexpertos" id="stwB" ><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="footer-menu">
                    <ul class="list-instyled list-inline">
                        <li><a href="funciona.aspx" id="howWorkB" >Cómo Funciona</a></li>
                        <li><a href="buscar-expertos.aspx" id="searchEB" >Buscar Expertos</a></li>
                        <li><a href="preguntas.aspx" id="faqB" >Ayuda</a></li>
                        <li><a href="contactenos.aspx">Contacto</a></li>
                    </ul>

                    <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
                </div>
            </div>
            <!-- for mobile -->
            <div class="footer-top-area visible-xs clearfix">
                <div class="footer-menu">
                    <ul class="list-instyled list-inline">
                        <li><a href="funciona.aspx" id="howWorkB" >Cómo Funciona</a></li>
                        <li><a href="buscar-expertos.aspx" id="searchEB" >Buscar Expertos</a></li>
                        <li><a href="preguntas.aspx" id="faqB" >Ayuda</a></li>
                        <li><a href="contactenos.aspx">Contacto</a></li>
                    </ul>
                </div>
                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
                <div class="footer-smi">
                    <ul class="list-unstyled list-inline">
                        <li><a href="https://www.facebook.com/invierteconexpertos/" id="sfbB" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/icexpertos" id="stwB" ><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="footer-logo-area visible-xs">
                    <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                                  style="width:125px; height: 50px;"></a>
                    <div class="footer-logo-text">
                        Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                    </div>
                </div>
                <div class="terms-policy-area">
                    <a href="#">Términos y Condiciones </a> ,
                    <a href="#"> Política de Privacidad</a>
                </div>
                <div class="footer-copyright">2017 All Rights Reserved</div>
            </div>
            <div class="seal-tag">
            <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=FAjqgtyhRxL7NLglEICr5QEoPo2C97wnAUrMtbu95cyA4Sg9k2Z1O2y2lYA8"></script></span>
            </div>
        </div>
    </footer>
    <script src="js/general_bundle.js"></script>
    <script src="js/index_bundle.js"></script>
    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <script>
        function viewAlert() {
            $('#BtnAlert').trigger('click');
        }
        $(document).ready(function () {
            //viewAlert();
        });
    </script>
</body>
</html>
