﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="buscar-expertos-n.aspx.vb" Inherits="buscar_expertos_n" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Invierte - Buscar Expertos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/lightgallery.min.css">
        <link rel="stylesheet" href="css/jquery-ui.min.css">
        <link rel="stylesheet" href="css/general.css">
        <link rel="stylesheet" href="css/buscar.css">
        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<nav class="navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar first"></span>
                <span class="icon-bar middle"></span>
                <span class="icon-bar last"></span>
            </button>
            <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="funciona.aspx">Cómo Funciona</a></li>
                <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                <li style="margin-right: 18px;"><a href="preguntas.aspx">FAQs</a></li>
                <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                <li><a href="Micuenta.aspx">Mi Cuenta</a></li>
                <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
            </ul>
        </div>
    </div>
</nav>
<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<section class="search_wrap">
    <div class="search">
        <div id="searchForm">
            <asp:TextBox ID="txt_search" runat="server" CssClass="search_input" placeholder="Buscar Experto"></asp:TextBox>
            <span class="search_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
            <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                TargetControlID="txt_search" CompletionInterval="10" CompletionSetCount="10" UseContextKey="True"
                ServiceMethod="GetCompletionList" MinimumPrefixLength="1" OnClientItemSelected="IAmSelected">

            </ajaxToolkit:AutoCompleteExtender>
        </div>
    </div>
</section>
<section class="container buscar_cont">
    <div class="buscar_title">
        <h1 class="block_title" style="margin-top: 50px;">Buscar Expertos</h1>
        <hr class="block_divider">
    </div>
    <div id="sec_buscar_main" class="buscar_main-wrap">
    <div class="filter_main-wrap">
    <div class="buscar_video_wrap">
                        <p class="buscar_video_text">Ver video</p>
                        <div id="buscarVideoLightbox">
                            <a href="https://www.youtube.com/watch?v=g9IZQ0ALyKg">
                                <img class="buscar_video" src="img/video.jpg">
                            </a>
                        </div>
                    </div>
    <div class="filtres_wrap" style="-webkit-overflow-scrolling:touch;">
    <span class="filtres-mobile_close">&#10006;</span>
                    <div class="filtres_block_head">FILTRAR:
                        <span class="filtres_block_head_info" id="activeHelp" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                        <i class="fa fa-question-circle-o filtres_block_head_help" aria-hidden="true"></i>
                        Ayuda</span>
                        <img class="filtres-mobile_head_caret" src="img/buscar/icons/white-arrow.svg">
                    </div>
    <div class="filtres_block" id="filters_objetivo">
    <p class="filtres_block_title">Objetivo de Inversíon
                            <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                        </p>
                        <div class="filtres_block_cont">
                            <asp:CheckBoxList ID="chkl_objetivos" runat="server" AutoPostBack="True" class="filtres_obj_list"></asp:CheckBoxList>
                        </div>
        </div>
        <div class="filtres_block">
        <p class="filtres_block_title">Comisión
                                <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                            </p>
                            <div class="filtres_block_cont">
        <ajaxToolkit:SliderExtender RailCssClass="filtres_block_slider" HandleCssClass="ui-slider-handle"  ID="SliderExtender1" runat="server" EnableHandleAnimation="True" TooltipText="Mover el slider" BoundControlID="lbl_comision" TargetControlID="txt_slider" Length="200" Maximum="1.5" Minimum="0.5" Decimals="2" />
        <asp:Label ID="Label1" runat="server" Text="Comisión: "></asp:Label>
        <asp:Label ID="lbl_comision" runat="server" Text=""></asp:Label>
        <asp:TextBox ID="txt_slider" runat="server" AutoPostBack="True"></asp:TextBox>
        </div>
        </div>
        <div class="filtres_block">
        <p class="filtres_block_title">Operadora
                                <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                            </p>
                            <div class="filtres_block_cont filtres_oper_list_wrap">
        <asp:CheckBoxList ID="chkl_operadoras" runat="server" AutoPostBack="True"></asp:CheckBoxList>
        </div>
    </div>
    </div>
    </div>
    <div class="filtres-mobile_head">FILTRAR:
                <span class="filtres_block_head_info">
                        <i class="fa fa-question-circle-o filtres_block_head_help" aria-hidden="true"></i>
                        Ayuda</span>
                <img class="filtres-mobile_head_caret" src="img/buscar/icons/white-arrow.svg">
            </div>
    <div class="estrats_wrap">
    <p class="estrats_wrap_section-title">Ordenar por:</p>
    <div id="vista_botones" class="estrats_ordenar_wrap">
            <asp:Button class="estrats_ordenar estrats_ordenar_active" ID="btn_destacadas" runat="server" Text="Estrategias Destacadas"></asp:Button>
            <asp:Button class="estrats_ordenar" ID="btn_rendimiento" runat="server" Text="Rendimiento"></asp:Button>
            <asp:Button class="estrats_ordenar" ID="btn_comision" runat="server" Text="Comisión"></asp:Button>
        </div>
    <div class="estrats_ordenar-mobile_wrap">
                        <img class="strats_ordenar-mobile_caret" src="img/buscar/icons/white-arrow.svg">
                        <asp:DropDownList runat="server" name="" id="ordenarMobile" class="estrats_ordenar-mobile" AutoPostBack="True">
                            <asp:ListItem Value="0">DESTACADAS</asp:ListItem>
                            <asp:ListItem Value="1">RENDIMIENTO</asp:ListItem>
                            <asp:ListItem Value="2">COMISION</asp:ListItem>
                        </asp:DropDownList>
                    </div>
    <p class="estrats_wrap_section-title">Mostrando Estrategias:</p>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="txt_view" runat="server" />
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="chkl_objetivos" />
                <asp:AsyncPostBackTrigger ControlID="txt_slider" />
                <asp:AsyncPostBackTrigger ControlID="chkl_operadoras" />
                <asp:AsyncPostBackTrigger ControlID="txt_view" />
                <asp:AsyncPostBackTrigger ControlID="btn_destacadas" />
                <asp:AsyncPostBackTrigger ControlID="btn_rendimiento" />
                <asp:AsyncPostBackTrigger ControlID="btn_comision" />
                <asp:AsyncPostBackTrigger ControlID="ordenarMobile" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    </div>
    </div>
</section>
</form>
<hr class="main_divider">
<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>
<div class="buscar_modal_overlay" id="helpModal">
    <div class="help_body">
        <span class="buscar_modal_close">&times;</span>
        <h2 class="block_title" style="margin-top: 0;">Ayuda para usar el filtro</h2>
        <hr class="block_divider">
        <div class="help_group">
            <h3 class="help_group_title">Objetivo de inversion</h3>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpCons">Conservador</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir rendimientos bajos asumiendo un
                        riesgo
                        bajo<br>
                        - Invierten hasta un 10% en Renta Variable (Balsa de Valores)<br>
                        - Baja volatilidad<br>
                        - Horizonte de inversion a 1 anos<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpModer">Moderno</h4>
                    <p class="help_group_single_text">
                        Estretegias recomendadas para personas que quieren recibir rendimientos moderados a sumiendo un
                        riesgo medio<br>
                        - Invierten hasta un 25% en Renta Variable ( Balsa de Valores )<br>
                        - Media volatilidad<br>
                        - Horizonte de inversion a 2 anos<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpEmpr">Emprendedor</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir rendimientos altos asumiendo un
                        riesgo
                        media-alto.<br>
                        - Invierten hasta un 40% en Renta Veriable ( Bolsa de Valores )<br>
                        - Media-Alta volatilidad<br>
                        - Horizonte de inversion a 3 anos<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpAgres">Agresivo</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir rendimientos muy altos asumiendo un
                        riesgo alto.<br>
                        - Invierten hasta un 60% en Renta Veriable ( Bolsa de Valores )<br>
                        - Alta volatilidad<br>
                        - Horizonte de inversion a 4 anos<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpEspec">Especulativo</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir los mas altos rendimientos asumiendo
                        un
                        riesgo muy alto<br>
                        - Invierten hasta un 100% en Renta Veriable ( Bolsa de Valores )<br>
                        - Muy alta volatilidad<br>
                        - Horizonte de inversion a 5 anos<br>
                    </p>
                </div>
            </div>
        </div>
        <hr class="block_divider" style="margin-top: 80px;">
        <div class="help_group">
        <div class="help_group_single help_single-ref">
                        <img class="help_group_single_icon" src="img/buscar/icons/operadora.svg">
                        <div class="help_group_single_cont">
                            <h4 class="help_group_single_title help_group_ref-title">Operadora</h4>
                            <p class="help_group_single_text">
                                Es la institucion financiera que administra alguno de los fondos de inversion que se incluyen en
                                la estrategia.
                            </p>
                        </div>
                    </div>
                    <div class="help_group_single help_single-ref">
                        <img class="help_group_single_icon" src="img/buscar/icons/comision.svg">
                        <div class="help_group_single_cont">
                            <h4 class="help_group_single_title help_group_ref-title">Comision</h4>
                            <p class="help_group_single_text">
                                Cada experto determia el costo por usar su estrategia. La comision as anual y se cobra
                                mensualmente
                                sobre el saldo promedio, el cobro se realiza durante los primeros 5 habiles del mes vencido.
                            </p>
                        </div>
                    </div>
        </div>
        </div>
    </div>
<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayors rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.html">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.html">Buscar Expertos</a></li>
                    <li><a href="preguntas.html">Ayuda</a></li>
                    <li><a href="contactenos.html">Contactos</a></li>
                </ul>

                <button class="btn expert-reg-btn">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.html">Cómo Funciona</a></li>
                    <li><a href="#">Buscar Expertos</a></li>
                    <li><a href="#">Ayuda</a></li>
                    <li><a href="#">Contactos</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayors rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in
                    porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
</footer>
    <script src="js/general_bundle.js"></script>
    <script src="js/buscar_bundle.js"></script>
    <script type="text/javascript">
        function base64_encode(cadena){
            // Encode the String
            var encodedString = btoa(cadena);
            return encodedString;
        }
        function base64_decode(cadena){
            // Decode the String
            var decodedString = atob(cadena);
            return decodedString;
        }
        function IAmSelected(source, eventArgs)
        {
            location.href = "http://invierteconexpertos.mx/buscar-expertos-ex.aspx?search="+ eventArgs.get_text() +"&execution=" + base64_encode(eventArgs.get_value());
        }
    </script>
</body>
</html>
