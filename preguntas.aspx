﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="preguntas.aspx.vb" Inherits="preguntas" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Preguntas Frecuentes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/preguntas.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-100596471-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-53D5L8T');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar first"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar last"></span>
                </button>
                <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li style="margin-right: 18px;"><a href="preguntas.aspx" class="active">FAQs</a></li>
                    <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                    <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                    <li><a href="Micuenta.aspx">Mi Cuenta</a></li>
                    <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
                </ul>
            </div>
        </div>
    </nav>

<section class="container preg_cont_wrap">
    <h1 class="block_title">Preguntas Frecuentes</h1>
    <hr class="block_divider">
    <h2 class="block_subtitle">Todas las estrategias tienen un objetivo de inversión, el cual nunca cambia.</h2>
    <div class="cont_block">
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question1" data-toggle="collapse">¿Qué es Invierte con Expertos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question1"><p>
            Invierte con Expertos es la primera plataforma en México que te permite identificar y contratar a los mejores expertos financieros en México para que tu dinero esté invertido con profesionales y obtengas excelentes rendimientos. Nuestra labor consiste en identificar a los mejores Expertos y presentarte la información de sus estrategias para que sepas cuánto han generado en el pasado y así puedas decidir a qué Expertos te conviene contratar a través de nuestra plataforma. Todas las estrategias invierten únicamente en fondos de inversión de instituciones financieras autorizadas y reguladas por la Comisión Nacional Bancaria y de Valores.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question2" data-toggle="collapse">¿Por qué me conviene Invierte con Expertos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question2"><p>Podrás obtener mayores rendimientos y podrás controlar tu dinero de manera rápida y sencilla. Cuando inviertes tu dinero en un banco o en otra institución financiera, ¿te has preguntado qué tan bueno es tu asesor? Si tus rendimientos son bajos, es probable que tu asesor no sea muy eficiente. En Invierte con Expertos identificamos a los mejores expertos financieros para que puedas invertir de una forma muy sencilla, ya que cuando abres una cuenta con nosotros, nos encargamos de que tu dinero esté invertido de acuerdo a las estrategias de los asesores que hayas seleccionado.
            </p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question3" data-toggle="collapse">¿Cómo elijo a los Expertos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question3"><p>Para elegir a un Experto debes primero seleccionar un Objetivo de Inversión, el cual determina el porcentaje máximo que deseas invertir en renta variable. Posteriormente, te mostramos todas las Estrategias generadas por nuestra comunidad de Expertos y te las ordenamos por el rendimiento que han pagado en el pasado (toma en cuenta que los rendimientos pasados no se garantizan en el futuro). Incluimos también la comisión que cobra el Experto en esa estrategia, la liquidez (cada cuánto puedes retirar tu dinero) y el horizonte de inversión (el plazo que te recomendamos mantener tu dinero invertido).</p>
            <p>Para poder contratar a un Experto primero debes abrir una cuenta de inversión.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question4" data-toggle="collapse">¿Cómo puedo abrir una cuenta en Invierte con Expertos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question4"><p>Para abrir una cuenta con nosotros necesitas ingresar al portal y hacer clic en “Abre tu Cuenta” y proporcionar tu nombre completo, teléfono celular, correo electrónico y la contraseña con la que quieres proteger tu cuenta. </p>
            <p>Posteriormente, te pediremos que nos proporciones los siguientes documentos: identificación oficial, comprobante de domicilio y estado de cuenta bancario de la cuenta que quieres registrar en tu contrato para recibir todos los depósitos que nos solicites (puedes ocultar lo saldos y movimientos) y para abrir tu contrato en Fóndika, que es la institución financiera en donde depositarás tu dinero (abriremos un contrato por cada estrategia que selecciones).</p>
            <p>El costo para abrir tu cuenta en Invierte con Expertos es 149 pesos (pago único por inversionista).</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question6" data-toggle="collapse">¿Dónde deposito mi dinero?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question6"><p>Invierte con Expertos no recibe tu dinero; debes realizar el depósito en Fóndika (www.fondika.com), una Distribuidora de Fondos de Inversión de reconocido prestigio que está autorizada y regulada por la Comisión Nacional Bancaria y de Valores. Es muy importante que al momento de realizar tu deposito ingreses el número de referencia que te indiquemos, ya que cada estrategia cuenta con un número diferente de referencia. De esta manera podremos identificar de forma automática la estrategia que elegiste y podremos invertir tu dinero en ella.</p>
            <p>Para conocer las cuentas y depositar tu dinero, ingresa con tus datos de acceso al portal, dirígete a la sección “Mi Portafolio”, selecciona la estrategia con la cual deseas invertir, haz clic en “Depositar” y sigue las instrucciones.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question7" data-toggle="collapse">¿Cuánto cuesta Invierte con Expertos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question7"><p>Abrir una cuenta en Invierte con Expertos cuesta 149 pesos. Es un cargo único que será descontado de tu primer depósito.</p>
            <p>Posteriormente, debes pagarle al Experto la comisión que se indica en la estrategia de inversión que seleccionaste. Esta comisión es un porcentaje anual de tu saldo que cada mes se te descontará de tu saldo invertido (en base al saldo promedio). </p>
            <p>Es importante que sepas que la comisión designada por el Experto en su estrategia nunca puede incrementarse (sí puede reducirse), por lo que estarás seguro de que no se te cobrará más de lo acordado.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question8" data-toggle="collapse">¿Qué es una Estrategia?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question8"><p>Una estrategia de inversión es un portafolio de fondos de inversión que genera un Experto. Está compuesta por varios fondos de inversión de una o más operadoras de fondos. Cuando un Experto crea una estrategia de inversión, tiene que indicar el Objetivo de Inversión que la estrategia tendrá durante toda la vida de dicha estrategia. Este objetivo indica el porcentaje mínimo y máximo que tendrá en renta variable y es fundamental que sepas que el Experto nunca lo puede modificar.</p>
            <p>Todas las estrategias tienen además una comisión que cobra el Experto en base al dinero que tú inviertas, así como un horizonte de inversión (plazo recomendado para invertir) y liquidez (tiempo en el que puedes hacer retiros).</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question9" data-toggle="collapse">¿Qué es una Comisión?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question9"><p>La comisión aparece en cada estrategia e indica el porcentaje que te cobrará el Experto en base a los recursos que inviertas. Este porcentaje es anual y cada mes se te descontará de tu saldo promedio mensual de forma proporcional. Como ejemplo, si inviertes 100,000 pesos en una estrategia que cobra el 1% anual, cada mes se te descontarían $83.33, y al año un total de 1,000 pesos. Toma en cuenta que este importe puede variar, ya que va en función a tu saldo promedio real, que se compone de tus depósitos más tu rendimiento.</p>
            <p>Es importante comentarte que la comisión indicada por el Experto en su estrategia nunca puede incrementarse (sí puede reducirse), por lo que estarás seguro de que no se te cobrará más de lo acordado.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question10" data-toggle="collapse">¿Qué seguridad tengo?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question10"><p>Tu dinero está resguardado en Fóndika (www.fondika.com), una de las Distribuidoras de Fondos de Inversión más importantes de México, regulada y autorizada por la Comisión Nacional Bancaria y de Valores.</p>
            <p>Además, todo tu dinero se invierte en fondos de inversión operados por instituciones de reconocido prestigio (como por ejemplo Actinver, GBM, Santander, etc.) que también están autorizadas y reguladas por la Comisión Nacional Bancaria y de Valores.</p>
            <p>Los rendimientos de las estrategias no se pueden garantizar, ya que indican el comportamiento que han tenido en el pasado. Ten en cuenta que entre más porcentaje de renta variable tenga la estrategia, podrás obtener mayores minusvalías; es por ello que en cada estrategia, dependiendo de su Objetivo de Inversión, te sugerimos respetar el Horizonte de Inversión (plazo recomendado para mantener tu dinero).</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question11" data-toggle="collapse">¿Cuál es la inversión mínima?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question11"><p>La inversión mínima es de 25,000 pesos por estrategia.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question12" data-toggle="collapse">¿Quién es Fóndika?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question12"><p>Es una de las instituciones financieras más importantes que distribuyen fondos de inversión en México (www.fondika.com). Está autorizada y regulada por la Comisión Nacional Bancaria y de Valores para operar como una Distribuidora de Fondos Inversión en México.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question13" data-toggle="collapse">¿Cómo puedo monitorear mi inversión?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question13"><p>Invierte con Expertos te presentará, en tiempo real, tu saldo y rendimiento de cada una de las estrategias en donde tengas invertido tu dinero. Podrás conocer de manera fácil y rápida el desempeño que tienen tus inversiones por cada estrategia. Ingresa con tus datos de acceso al portal y dirigente a la sección “Mi Portafolio” para revisar esta información. </p>
            <p>También Fóndika te enviará de forma electrónica tu estado de cuenta (uno por cada estrategia), el cual contiene el detalle de tu inversión al cierre de cada mes. </p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question14" data-toggle="collapse">¿Cómo hago un retiro?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question14">
            <p>Puedes solicitar retiros cuantas veces lo requieras. Solo debes tener en cuenta el tiempo en que tus recursos estarán disponibles una vez que solicites el retiro (Liquidez). Para solicitar un retiro, ingresa con tus datos de acceso al portal y dirígete a la sección “Mi Portafolio”. Luego identifica la estrategia de dónde deseas retirar tu dinero, haz clic en “Retirar” y sigue las indicaciones que te aparecen.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question15" data-toggle="collapse">¿Me puedo cambiar de estrategia?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question15"><p>Puedes cambiar de estrategia cuando tú lo decidas, solo debes cumplir con la Liquidez que se indica en cada una. Para cambiar de estrategia, debes hacer clic en el botón “Cambiar” en la estrategia que deseas cambiar y seguir las instrucciones. Toma en cuenta que todo el dinero que esté invertido en esa estrategia será trasladado a la nueva, ya que no podemos realizar cambios parciales.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question16" data-toggle="collapse">¿Cuánto tiempo debe mi dinero permanecer invertido?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question16"><p>No hay un plazo fijo; puedes solicitar retiros todos los días hábiles a cualquier hora. Ten en cuenta que tu dinero será depositado únicamente en la cuenta de banco que hayas registrado previamente cuando abriste tu contrato de inversión y deberás esperar el tiempo indicado en la sección de liquidez para recibir tus recursos. Para solicitar un retiro, haz clic en “Retirar” en la estrategia de la cual quieres retirar dinero.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question17" data-toggle="collapse">¿En dónde está invertido mi dinero?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question17"><p>Tu dinero está invertido en fondos de inversión que pueden ser de deuda o de renta variable (dependiendo de la estrategia que elijas). No invertimos tu dinero directamente ni en acciones, ni en ningún otro instrumento financiero.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question18" data-toggle="collapse">¿Qué es un Objetivo?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question18"><p>Un Objetivo de Inversión es la forma en que clasificamos a todas las estrategias que indica el porcentaje mínimo y máximo que tu estrategia tendrá en renta variable. Ten en cuenta que entre más renta variable posea tu estrategia, podrás obtener mayores rendimientos y se te recomendará invertir durante más tiempo, pero tendrá mayor riesgo y volatilidad.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question19" data-toggle="collapse">¿Qué es Rendimiento?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question19"><p>Rendimiento es el porcentaje de plusvalía (ganancia) o minusvalía (pérdida) que una inversión puede generar durante un período de tiempo. Todos los rendimientos son pasados y no se pueden garantizar en el futuro, ya que dependen de las condiciones de los mercados financieros. Invierte con Expertos calcula los rendimientos de todas las estrategias que los Expertos han generado. Ten en cuenta que deberás restarle la comisión que cobra el Experto para calcular tu rendimiento neto.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question20" data-toggle="collapse">¿Qué es Horizonte de Inversión y Liquidez?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question20"><p>El horizonte de inversión indica el tiempo que te recomendamos mantener tus recursos para que puedas obtener buenos rendimientos. Entre más porcentaje de renta variable tenga tu estrategia, mayor será el tiempo que se te sugerirá invertir en esa estrategia.</p>
            <p>La liquidez te indica el tiempo en el que tendrás disponibles tus recursos en tu cuenta a partir de que hayas solicitado un retiro. Ten en cuenta que se contabilizan en días hábiles y que deberás solicitar tu operación antes de las 12 del día.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question21" data-toggle="collapse">¿Qué es una Clasificación?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question21"><p>Todos los fondos de inversión tienen asignada una clasificación desde el momento en que son creados, la cual es determinada por la Operadora de Fondos que administra los fondos. En cada estrategia te mostramos las diferentes clasificaciones que tienen los fondos de inversión que están incluidos en tu propuesta para que puedas conocer más a detalle dónde está invertido tu dinero.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question22" data-toggle="collapse">¿Qué beneficio obtengo si recomiendo a mis amigos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question22"><p>Tú recibes 250 pesos por cada amigo que recomiendes e invierta en Invierte con Expertos. Tus amigos reciben el beneficio de que no les vamos a cobrar la cuota de apertura.</p>
            </div>
        </div>
        <div class="cont_single">
            <button class="cont_single_head" data-target="#question23" data-toggle="collapse">¿Qué es una Operadora de Fondos?
                <div class="cont_single_head_arrow"></div>
            </button>
            <div class="collapse cont_single_text" id="question23"><p>Es la institución financiera que administra los fondos de inversión contenidos en cada una de las estrategias que los Expertos generan.</p>
            </div>
        </div>
    </div>
</section>
<hr class="main_divider">
<section class="container main_cont_wrap main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>

<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>

                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
</footer>

<script src="js/general_bundle.js"></script>
</body>
</html>