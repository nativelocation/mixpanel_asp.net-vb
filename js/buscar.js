//require ('../node_modules/jquery-ui/ui/widgets/slider');
//require ('jquery-ui-touch-punch');
//require ('lightgallery/dist/js/lightgallery.min');
//require('lg-video/dist/lg-video.min');

$(function () {

    $('#inversionSlider').slider({
        range: 'min',
        max: 500000,
        min: 50000,
        value: 150000,
        slide: function (event, ui) {
        }
    });

    $('#comisionSlider').slider({
        range: true,
        max: 1.25,
        min: 0.25,
        step: 0.01,
        values: [0.50, 1],
        slide: function (event, ui) {
        }
    });

    //Filtres list items selection
    $('.filtres_oper_list_item').on('click', function () {
        $(this).toggleClass('filtres_oper_list_item-selected');
    });

    $('.filtres_obj_list li').on('click', function (e) {
        makeObjActive($(this))
    });

    function makeObjActive (obj){
        obj.toggleClass('filtres_obj_list_item-active');
        if(obj.hasClass('filtres_obj_list_item-active')){
            changeEstratObjColor(obj);
        }
        else{
            changeEstratObjColor($('.filtres_obj_list_item-active'));
        }
    }

    function changeEstratObjColor(obj) {
        let objValue = $('.buscar_estrat_objetivo_value');
        if (obj.length > 0) {
            objValue.css('background-color', $(obj).find('i').css('color'));
            objValue.css('color', 'white');
        }
        else {
            objValue.css('background-color', 'white');
            objValue.css('color', '#f5a623');
        }
    }

    //makeObjActive($('.filtres_obj_list_item:nth-child(1)'));

    //Mobile Filtres Activation
    $('.filtres-mobile_head').on('click', function () {
        $('.navbar').css('display', 'none');
        $('.filtres_wrap').toggleClass('filtres-mobile_wrap');
        $('.filtres-mobile_btn_wrap').toggleClass('filtres-mobile_btn_wrap_visible');
        $('body').addClass('body_no-scroll');
    });

    $('.filtres-mobile_btn-aplicar').on('click', function () {
        $('.navbar').css('display', 'block');
        $('.filtres_wrap').toggleClass('filtres-mobile_wrap');
        $('.filtres-mobile_btn_wrap').toggleClass('filtres-mobile_btn_wrap_visible');
        $('body').removeClass('body_no-scroll');
    });

    $('.filtres-mobile_close').on('click', function () {
        $('.navbar').css('display', 'block');
        $('.filtres_wrap').toggleClass('filtres-mobile_wrap');
        $('.filtres-mobile_btn_wrap').toggleClass('filtres-mobile_btn_wrap_visible');
        $('body').removeClass('body_no-scroll');
    });

    //Selecting active order (desktop)
    $('.estrats_ordenar').on('click', function(){
        $('.estrats_ordenar_active ').removeClass('estrats_ordenar_active ');
        $(this).addClass('estrats_ordenar_active');
    });

    //Mobile Filtres Head Sticky
    let filtresMobPosition = $('.filtres-mobile_head').offset().top,
        $window = $(window);

    let throttle = function (callback, limit) {
        let scrolled = true;
        if (scrolled) {
            callback.call();
            scrolled = false;
            setTimeout(function () {
                scrolled = true
            }, limit)
        }
    };
    if ($window.width() < 1024) {
        $window.scroll(function () {
            throttle(filtresSticky, 150)
        });
    }
    let filtresSticky = function() {
        if($window.scrollTop() >= filtresMobPosition){
            $('.navbar').css('display', 'none');
            $('.filtres-mobile_head').addClass('filtres-mobile_head_sticky');
        }
        else {
            $('.navbar').css('display', 'block');
            $('.filtres-mobile_head').removeClass('filtres-mobile_head_sticky');
        }
    };

    //Toggling filtres section height on mobile
    $(".filtres_block_title").on('click', function () {
        $(this).parent().toggleClass('filtres_block_closed')
    });

    //Open/Close Filtres Help Modal
    const $helpModal = $('#helpModal');
    $('#activeHelp').on('click', function () {
        $('body').addClass('body_no-scroll');
        $helpModal.addClass('buscar_modal_visible');
    });

    $helpModal.on('click', function (e) {
       if($(e.target).is($helpModal)) {
           $helpModal.removeClass('buscar_modal_visible');
           $('body').removeClass('body_no-scroll');
       }
    });

    $('.buscar_modal_close').on('click', function () {
        $('.buscar_modal_visible').removeClass('buscar_modal_visible');
        $('body').removeClass('body_no-scroll');
    });
    // UNUSED CODE for opening, closing estrat's modal and submiting value to that estrat.
    //Showing/Hiding Modal
    //let activeEstrat;
    /*$(".buscar_estrat_head_agregar_btn").on('click', function () {
        $('body').addClass('body_no-scroll');
        $('#agregarModal').addClass('buscar_modal_visible');
        activeEstrat = $(this).parent();
    });

    $(".buscar_estrat_mobile_agregar_btn").on('click', function () {
        $('body').addClass('body_no-scroll');
        $('#agregarModal').addClass('buscar_modal_visible');
        activeEstrat = $(this).parent();
    });

    $('.buscar_modal_close').on('click', function () {
        $('body').removeClass('body_no-scroll');
        $(this).parent().parent().removeClass('buscar_modal_visible');
    });

    //Submitting value
    $('#agregarConfirm').on('click', function () {
        let value = $('#agregarValue').val();
        $('#agregarValue').val('');
        if (isNaN(value) || value <= 0)
            return;

        $('body').removeClass('body_no-scroll');
        $('#agregarModal').removeClass('buscar_modal_visible');
        $(activeEstrat).find($('.buscar_estrat_head_agregar_btn')).removeClass('buscar_estrat_head_agregar_visible');
        $(activeEstrat).find($('.buscar_estrat_mobile_agregar_btn')).hide();
        formatAndPlace(value)
    });

    function formatAndPlace(value) {
        value = '$' + value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $(activeEstrat).find('.buscar_estrat_head_agregar_summ_value').html(value);
        $(activeEstrat).find('.buscar_estrat_head_agregar_summ').addClass('buscar_estrat_head_agregar_visible');
    }*/

//Video Lightbox
    $('#buscarVideoLightbox').lightGallery({
            youtubePlayerParams: {
                autoplay: 1,
                showinfo:0
            }
    }
    );
});