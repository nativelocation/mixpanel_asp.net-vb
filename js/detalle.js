//Display description text to Objetivo Items
//Resize Description Zone to Items Zone height
if ($(window).width() > 1000) {
    $('.objetivo_descr').css('height', $('.objetivo_left-block').height());
}
else {
    $('.objetivo_descr').css('height', 'auto');
}

//$('.objetivo_list_item').on('click', function () {
//    setActiveItem($(this));
//});

function setActiveItem(item) {
    $('.objetivo_list_item-active').removeClass('objetivo_list_item-active');
    item.addClass('objetivo_list_item-active');
    getMeasure(item);
}

//Setting width for measure bar and content for its popup.
function getMeasure(item) {
    let itemNum = item.find('.objetivo_list_num').html(),
        itemName = item.find('.objetivo_list_name').html(),
        fillPopUp = $('.objetivo_descr_measure_popup'),
        fillWidth = itemNum * 20 + '%';
    $('.objetivo_descr_measure_fill').css('width', fillWidth);
    $('.objetivo_descr_measure_popup_num').text(itemNum);
    $('.objetivo_descr_measure_popup_text').text(itemName);

    fillPopUp.css('right', '-' + fillPopUp.width() / 2.1 + 'px');
    setDescriptionContent(item, itemName);
}

function setDescriptionContent(item, itemName) {
    let itemDescr = item.attr('data-descr');
    $('.objetivo_descr_text').html(itemDescr);
    $('.objetivo_descr_title').html(itemName);
}

//Make first item active on load
//setActiveItem($('.objetivo_list_item:first-child'));

//Show/Hide modal
$('#modalBtn').on('click', function () {
    $('#detalleModal').toggleClass('detalle_modal_visible');
});

$('#regresarAStrategia').on('click', function () {
    $('#detalleModal').toggleClass('detalle_modal_visible');
});

