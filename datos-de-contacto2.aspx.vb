﻿
Partial Class datos_de_contacto2
    Inherits System.Web.UI.Page
    Public cni As New Data.SqlClient.SqlConnection
    Public SQLi As Data.SqlClient.SqlCommand
    Public rsi As Data.SqlClient.SqlDataReader
    Public inversionista As New ICE_clientes
    Public g As New generales, idinversionista, parte1, parte2 As String

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Dim correo, cadena() As String
        cni.ConnectionString = inversionista.conectdb
        If String.IsNullOrEmpty(Session("idinversionista")) Or String.IsNullOrEmpty(Session("correo_registro")) Then
            Response.Redirect("datos-de-contacto.aspx")
        Else
            idinversionista = Session("idinversionista")
            correo = Session("correo_registro")
            cadena = Split(correo, "@", 2)
            parte1 = g.base64_encode(cadena(0))
            parte2 = g.base64_encode(cadena(1))
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.UrlReferrer Is Nothing Then
            Response.Redirect("datos-de-contacto.aspx")
            Session.Remove("idinversionista")
            Session.Remove("correo_registro")
        End If

    End Sub
End Class
