﻿Imports Microsoft.VisualBasic
Imports System.Web.UI.Page
Imports System.Globalization
Imports System.Net.Mail

Public Class ICE_clientes
    Public cn, cn2 As New Data.SqlClient.SqlConnection()
    Public SQL1, SQL2 As Data.SqlClient.SqlCommand
    Public rs, rs2 As Data.SqlClient.SqlDataReader
    Public q1, q2 As String

    Function conectdb() As String
        Return "Data Source=69.64.86.166;Initial Catalog=ICE_clientes;User Id=ClientesIce;Password=Db$ce@clientes+;"
    End Function

    Function hora_local() As Date
        Dim sSQL As String, diferencia As Integer, newTime As Date
        cn.ConnectionString = conectdb()
        sSQL = "select valor from sysconfig where id=1"
        SQL1 = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open() : diferencia = SQL1.ExecuteScalar : cn.Close()
        newTime = DateAdd(DateInterval.Hour, diferencia, Now())
        Return newTime
    End Function

    Function nombre_fondo(ByVal idfondo As Integer) As String
        Dim sSQL As String
        cn.ConnectionString = conectdb()
        sSQL = "select nombrefondo from fondo where id='" & idfondo & "'"
        SQL1 = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open() : nombre_fondo = SQL1.ExecuteScalar : cn.Close()
    End Function

    Function operadora_nombrecorto(ByVal idoperadora As Integer) As String
        Dim sSQL As String
        cn.ConnectionString = conectdb()
        sSQL = "select nombrecorto from operadora where id='" & idoperadora & "'"
        SQL1 = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open() : operadora_nombrecorto = SQL1.ExecuteScalar : cn.Close()
    End Function

    Function operadora_nombre(ByVal idoperadora As Integer) As String
        Dim sSQL As String
        cn.ConnectionString = conectdb()
        sSQL = "select nombreOperadora from operadora where id='" & idoperadora & "'"
        SQL1 = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open() : operadora_nombre = SQL1.ExecuteScalar : cn.Close()
    End Function

    Function periodo_carteras() As String
        cn.ConnectionString = conectdb()
        Dim periodo As String
        q1 = "select max(fechaperiodo) from carteras"
        SQL1 = New Data.SqlClient.SqlCommand(q1, cn)
        cn.Open() : periodo = SQL1.ExecuteScalar : cn.Close()
        Return periodo
    End Function

    Function nombre_inversionista(ByVal idinversionista As String) As String
        Dim sSQL As String
        cn.ConnectionString = conectdb()
        sSQL = "select nombre from inversionistas where idinversionista='" & idinversionista & "'"
        SQL1 = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open() : nombre_inversionista = SQL1.ExecuteScalar : cn.Close()
    End Function

    Function email_inversionista(ByVal idinversionista As String) As String
        Dim sSQL As String
        cn.ConnectionString = conectdb()
        sSQL = "select correo from inversionistas where idinversionista='" & idinversionista & "'"
        SQL1 = New Data.SqlClient.SqlCommand(sSQL, cn)
        cn.Open() : email_inversionista = SQL1.ExecuteScalar : cn.Close()
    End Function

    Sub MandaMail(ByVal remitente_email As String, ByVal remitente_nombre As String, ByVal cliente_email As String, ByVal cliente_nombre As String, ByVal asunto As String, ByVal titulo As String, ByVal mensaje As String)
        Dim correo As New System.Net.Mail.MailMessage, smtp As New System.Net.Mail.SmtpClient
        Dim credencial_email, credencial_nombre, pie_correo As String

        Try
            smtp.Host = "relay-hosting.secureserver.net"
            credencial_email = "sistemas@invierteconexpertos.mx"
            credencial_nombre = "$oP0rt3@Expertos"
            smtp.Credentials = New System.Net.NetworkCredential(credencial_email, credencial_nombre)
            correo.From = New System.Net.Mail.MailAddress(remitente_email, remitente_nombre)
            correo.To.Add(cliente_email)
            correo.Bcc.Add("sistemas@invierteconexpertos.mx")
            correo.Subject = asunto
            Dim cuerpo1, cuerpo2 As String
            cuerpo1 = "<p>Hola " & cliente_nombre & ",</p>"
            cuerpo2 = "<p>El equipo de Invierte con Expertos<br><a href='http://invierteconexpertos.mx/expertos'>www.invierteconexpertos.mx</a><br><a href='mailto:info@invierteconexpertos.mx'>info@invierteconexpertos.mx</a></p>"
            correo.Body = cuerpo1 & mensaje & cuerpo2
            correo.IsBodyHtml = True
            correo.Priority = System.Net.Mail.MailPriority.High
            smtp.Send(correo)
        Catch ex As Exception

        End Try
    End Sub

End Class
