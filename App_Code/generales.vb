﻿Imports Microsoft.VisualBasic
Imports System.Web.UI.Page
Imports System.Globalization
Imports System.Net.Mail

Public Class generales
    Function ConvierteFecha(ByVal vFecha As String) As String
        Dim tempFecha As String
        If IsDate(vFecha) = False Or IsDBNull(vFecha) Then
            tempFecha = "No aplica"
        Else
            tempFecha = Month(vFecha) & "/"
            tempFecha = tempFecha & Day(vFecha) & "/"
            tempFecha = tempFecha & Year(vFecha)
        End If

        Return tempFecha
    End Function
    Function ConvierteFecha2(ByVal vFecha As String) As String
        Dim tempFecha As String
        If Not IsDate(vFecha) Or IsDBNull(vFecha) Or vFecha = "" Then
            tempFecha = "No aplica"
        ElseIf Year(vFecha) = 1900 Then
            tempFecha = "No aplica"
        Else

            tempFecha = Day(vFecha) & "/"
            If CInt(Month(vFecha)) = 1 Then
                tempFecha = tempFecha & "Ene/"
            ElseIf CInt(Month(vFecha)) = 2 Then
                tempFecha = tempFecha & "Feb/"
            ElseIf CInt(Month(vFecha)) = 3 Then
                tempFecha = tempFecha & "Mar/"
            ElseIf CInt(Month(vFecha)) = 4 Then
                tempFecha = tempFecha & "Abr/"
            ElseIf CInt(Month(vFecha)) = 5 Then
                tempFecha = tempFecha & "May/"
            ElseIf CInt(Month(vFecha)) = 6 Then
                tempFecha = tempFecha & "Jun/"
            ElseIf CInt(Month(vFecha)) = 7 Then
                tempFecha = tempFecha & "Jul/"
            ElseIf CInt(Month(vFecha)) = 8 Then
                tempFecha = tempFecha & "Ago/"
            ElseIf CInt(Month(vFecha)) = 9 Then
                tempFecha = tempFecha & "Sep/"
            ElseIf CInt(Month(vFecha)) = 10 Then
                tempFecha = tempFecha & "Oct/"
            ElseIf CInt(Month(vFecha)) = 11 Then
                tempFecha = tempFecha & "Nov/"
            ElseIf CInt(Month(vFecha)) = 12 Then
                tempFecha = tempFecha & "Dic/"
            End If
            tempFecha = tempFecha & Year(vFecha)
        End If
        Return tempFecha
    End Function
    Function funFechaNombre(ByVal vFecha As String) As String
        Dim tempFecha As String
        If IsDate(vFecha) = False Then
            tempFecha = "No aplica"
        Else
            tempFecha = Day(vFecha)
            Select Case Month(vFecha)
                Case 1 : tempFecha = tempFecha & " de enero de "
                Case 2 : tempFecha = tempFecha & " de febrero de "
                Case 3 : tempFecha = tempFecha & " de marzo de "
                Case 4 : tempFecha = tempFecha & " de abril de "
                Case 5 : tempFecha = tempFecha & " de mayo de "
                Case 6 : tempFecha = tempFecha & " de junio de "
                Case 7 : tempFecha = tempFecha & " de julio de "
                Case 8 : tempFecha = tempFecha & " de agosto de "
                Case 9 : tempFecha = tempFecha & " de septiembre de "
                Case 10 : tempFecha = tempFecha & " de octubre de "
                Case 11 : tempFecha = tempFecha & " de noviembre de "
                Case 12 : tempFecha = tempFecha & " de diciembre de "
                Case Else : tempFecha = tempFecha & ""
            End Select
            tempFecha = tempFecha & Year(vFecha)
        End If
        Return tempFecha
    End Function
    Function NombreMes(ByVal mes As Integer) As String
        Dim tempFecha As String
        If CInt(mes) = 1 Then tempFecha = "Enero"
        If CInt(mes) = 2 Then tempFecha = "Febrero"
        If CInt(mes) = 3 Then tempFecha = "Marzo"
        If CInt(mes) = 4 Then tempFecha = "Abril"
        If CInt(mes) = 5 Then tempFecha = "Mayo"
        If CInt(mes) = 6 Then tempFecha = "Junio"
        If CInt(mes) = 7 Then tempFecha = "Julio"
        If CInt(mes) = 8 Then tempFecha = "Agosto"
        If CInt(mes) = 9 Then tempFecha = "Septiembre"
        If CInt(mes) = 10 Then tempFecha = "Octubre"
        If CInt(mes) = 11 Then tempFecha = "Noviembre"
        If CInt(mes) = 12 Then tempFecha = "Diciembre"
        Return tempFecha
    End Function

    Function funnombremes(ByVal vFecha)
        funnombremes = ""
        If IsDate(vFecha) = False Then
            Exit Function
        End If
        Select Case Month(vFecha)
            Case 1 : funnombremes = funnombremes & "Ene"
            Case 2 : funnombremes = funnombremes & "Feb"
            Case 3 : funnombremes = funnombremes & "Mar"
            Case 4 : funnombremes = funnombremes & "Abr"
            Case 5 : funnombremes = funnombremes & "May"
            Case 6 : funnombremes = funnombremes & "Jun"
            Case 7 : funnombremes = funnombremes & "Jul"
            Case 8 : funnombremes = funnombremes & "Ago"
            Case 9 : funnombremes = funnombremes & "Sep"
            Case 10 : funnombremes = funnombremes & "Oct"
            Case 11 : funnombremes = funnombremes & "Nov"
            Case 12 : funnombremes = funnombremes & "Dic"
        End Select
        funnombremes = funnombremes & "/" & Year(vFecha)
    End Function
    Function FunHora(ByVal vFecha As String) As String
        If IsDate(vFecha) = False Then
            Return 0
            Exit Function
        Else
            If Hour(vFecha) < 10 Then
                FunHora = FunHora & "0" & Hour(vFecha)
            Else
                FunHora = FunHora & Hour(vFecha)
            End If
            If Minute(vFecha) < 10 Then
                FunHora = FunHora & ":0" & Minute(vFecha)
            Else
                FunHora = FunHora & ":" & Minute(vFecha)
            End If
            Return FunHora
        End If
    End Function
    Function FunHora2(ByVal vFecha As String) As String
        If IsDate(vFecha) = False Then
            Return 0
            Exit Function
        Else
            If Hour(vFecha) < 10 Then
                FunHora2 = FunHora2 & "0" & Hour(vFecha)
            Else
                FunHora2 = FunHora2 & Hour(vFecha)
            End If
            If Minute(vFecha) < 10 Then
                FunHora2 = FunHora2 & ":0" & Minute(vFecha)
            Else
                FunHora2 = FunHora2 & ":" & Minute(vFecha)
            End If
            If Second(vFecha) < 10 Then
                FunHora2 = FunHora2 & ":0" & Second(vFecha)
            Else
                FunHora2 = FunHora2 & ":" & Second(vFecha)
            End If
            Return FunHora2
        End If
    End Function

    Public Function IsValidEmail(ByVal email As String) As Boolean
        Dim expresion As String
        expresion = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
        Return Regex.IsMatch(email, expresion)
    End Function

    Public Base64Chars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" & _
                "abcdefghijklmnopqrstuvwxyz" & _
                "0123456789" & _
                "-_"

    'Codificar base64
    Public Function base64_encode(ByVal strIn As String) As String
        Dim c1, c2, c3, w1, w2, w3, w4, n, strOut As Object
        For n = 1 To Len(strIn) Step 3
            c1 = Asc(Mid(strIn, n, 1))
            c2 = Asc(Mid(strIn, n + 1, 1) + Chr(0))
            c3 = Asc(Mid(strIn, n + 2, 1) + Chr(0))
            w1 = Int(c1 / 4) : w2 = (c1 And 3) * 16 + Int(c2 / 16)
            If Len(strIn) >= n + 1 Then
                w3 = (c2 And 15) * 4 + Int(c3 / 64)
            Else
                w3 = -1
            End If
            If Len(strIn) >= n + 2 Then
                w4 = c3 And 63
            Else
                w4 = -1
            End If
            strOut = strOut + mimeencode(w1) + mimeencode(w2) + _
                      mimeencode(w3) + mimeencode(w4)
        Next
        base64_encode = strOut
    End Function

    Private Function mimeencode(ByVal intIn As Integer) As String
        If intIn >= 0 Then
            mimeencode = Mid(Base64Chars, intIn + 1, 1)
        Else
            mimeencode = ""
        End If
    End Function

    ' Decodificar base 64
    Public Function base64_decode(ByVal strIn As String) As String
        Dim w1, w2, w3, w4, strOut As String, n As Integer
        For n = 1 To Len(strIn) Step 4
            w1 = mimedecode(Mid(strIn, n, 1))
            w2 = mimedecode(Mid(strIn, n + 1, 1))
            w3 = mimedecode(Mid(strIn, n + 2, 1))
            w4 = mimedecode(Mid(strIn, n + 3, 1))
            If w2 >= 0 Then _
                strOut = strOut + _
                    Chr(((w1 * 4 + Int(w2 / 16)) And 255))
            If w3 >= 0 Then _
                strOut = strOut + _
                    Chr(((w2 * 16 + Int(w3 / 4)) And 255))
            If w4 >= 0 Then _
                strOut = strOut + _
                    Chr(((w3 * 64 + w4) And 255))
        Next
        base64_decode = strOut
    End Function

    Private Function mimedecode(ByVal strIn) As Integer
        If Len(strIn) = 0 Then
            mimedecode = -1 : Exit Function
        Else
            mimedecode = InStr(Base64Chars, strIn) - 1
        End If
    End Function

    Sub MandaMail(ByVal remitente_email As String, ByVal remitente_nombre As String, ByVal cliente_email As String, ByVal cliente_nombre As String, ByVal asunto As String, ByVal titulo As String, ByVal mensaje As String)
        Dim correo As New System.Net.Mail.MailMessage, smtp As New System.Net.Mail.SmtpClient
        Dim credencial_email, credencial_nombre, pie_correo As String

        Try
            smtp.Host = "relay-hosting.secureserver.net"
            credencial_email = "sistemas@invierteconexpertos.mx"
            credencial_nombre = "$oP0rt3@Expertos"
            smtp.Credentials = New System.Net.NetworkCredential(credencial_email, credencial_nombre)
            correo.From = New System.Net.Mail.MailAddress(remitente_email, remitente_nombre)
            correo.To.Add(cliente_email)
            correo.Bcc.Add("sistemas@invierteconexpertos.mx")
            correo.Subject = asunto
            Dim cuerpo1, cuerpo2 As String
            cuerpo1 = "<p>Hola " & cliente_nombre & ",</p>"
            cuerpo2 = "<p>El equipo de Invierte con Expertos<br><a href='http://invierteconexpertos.mx/expertos'>invierteconexpertos.mx</a></p>"
            correo.Body = cuerpo1 & mensaje & cuerpo2
            correo.IsBodyHtml = True
            correo.Priority = System.Net.Mail.MailPriority.High
            smtp.Send(correo)
        Catch ex As Exception

        End Try
    End Sub

    Function numeros_aleatorios() As String
        '###################################
        '## GENERANDO NUMEROS ALEATORIOS
        '##################################
        Dim cadena_num, cadena, cadena_sesion As String
        Dim rand As Random
        Dim numero As Int32
        Dim letra As Char
        rand = New Random(Date.Now.Millisecond)
        For i = 0 To 2
            letra = Convert.ToChar(rand.Next(65, 90))
            cadena = cadena & letra
        Next
        cadena_sesion = cadena
        cadena = ""
        cadena_num = ""
        rand = New Random(Date.Now.Millisecond * Date.Now.Millisecond)
        For i = 0 To 2
            cadena_num = Convert.ToChar(rand.Next(48, 57))
            cadena = cadena & cadena_num
        Next
        cadena_sesion = cadena_sesion & cadena
        cadena = ""
        cadena_num = ""
        rand = New Random(Date.Now.Millisecond * Date.Now.Millisecond)
        For i = 0 To 1
            letra = Convert.ToChar(rand.Next(97, 122))
            cadena = cadena & letra
        Next
        cadena_sesion = cadena_sesion & cadena
        '///////
        Return cadena_sesion
    End Function

End Class
