﻿
Partial Class password_recovery
    Inherits System.Web.UI.Page
    Public cni As New Data.SqlClient.SqlConnection
    Public SQLi As Data.SqlClient.SqlCommand
    Public rsi As Data.SqlClient.SqlDataReader
    Public inversionistas As New ICE_clientes, g As New generales

    Private Sub password_recovery_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        cni.ConnectionString = inversionistas.conectdb
    End Sub

    Private Sub password_recovery_Load(sender As Object, e As EventArgs) Handles Me.Load
        correo.Attributes.Add("required", "required")
    End Sub
    Protected Sub btn_continuar_Click(sender As Object, e As EventArgs) Handles btn_continuar.Click
        Dim sSQL, mensaje, pass, nombre As String, encontrado As Integer
        If g.IsValidEmail(LCase(correo.Text)) Then
            sSQL = "select count(*)encontrado from inversionistas where correo='" & LCase(correo.Text) & "'"
            SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
            cni.Open() : encontrado = SQLi.ExecuteScalar : cni.Close()
            If encontrado > 0 Then
                sSQL = "select nombre,pass from inversionistas where correo='" & LCase(correo.Text) & "'"
                SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
                cni.Open()
                rsi = SQLi.ExecuteReader
                While rsi.Read
                    pass = rsi("pass")
                    nombre = rsi("nombre")
                End While
                cni.Close()
                mensaje = "<p>Hemos recibido una solicitud de recuperación de contraseña: <br><ul style='list-style:none;'><li style='list-style:none;'><b>Usuario:</b> " & LCase(correo.Text) & "</li><li style='list-style:none;'><b>Contraseña:</b> " & pass & "</li></ul></p>"
                inversionistas.MandaMail("noreply@invierteconexpertos.mx", "Invierte con Expertos", LCase(correo.Text), UCase(nombre), "Solicitud de recuperación de contraseña", "Recupera tu contraseña", mensaje)
                lbl_correo.Text = "<b>" & LCase(correo.Text) & "</b>"
                formulario.Visible = False
                correct_send.Visible = True
                fail_send.Visible = False
            Else
                fail_send.Visible = True
                correct_send.Visible = False
            End If
        Else
            fail_send.Visible = True
            correct_send.Visible = False
        End If

    End Sub
End Class
