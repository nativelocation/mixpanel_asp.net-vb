﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="buscar-expertos.aspx.vb" Inherits="buscar_expertos" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Buscar Expertos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/lightgallery.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/buscar.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <style>
        .buscar_btn{color:white;border:1px solid #f5a623;background-color:#f5a623;padding:10px 20px;margin-top:15px;outline:none}
        .buscar_btn:focus, .buscar_btn:hover {
            outline: none;
            background-color: #e39109;
            color: white;
            -webkit-transition: .2s;
            -o-transition: .2s;
            transition: .2s;
        }
    </style>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-53D5L8T');</script>
    <!-- End Google Tag Manager -->

    <!-- start Mixpanel -->
    <script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("79a3138bc6cd79dea43bb30e334f8709");</script>

    <script src="//cdn.mxpnl.com/site_media/js/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageview() {
            distinct_id = window.localStorage.getItem('distinct_id');
            if (distinct_id == null) {
                window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                distinct_id = window.localStorage.getItem('distinct_id');
            }
            mixpanel.register({
                'page_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx',
                'page_title':'Invierte - buscar-expertos'
            });
            mixpanel.identify(distinct_id);
            mixpanel.people.increment("page_count");
            mixpanel.track('pageview');
        }
        pageview();

        $(document).ready(function() {
            $("a#searchEH").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('clicked search for experts', {'click_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'header'});
                //Do stuff when clicked
            });
            $("a#searchEB").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('clicked search for experts', {'click_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'bottom'});
                //Do stuff when clicked
            });

            $("a#howWorkH").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('clicked how it works', {'click_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'header'});
            });
            $("a#howWorkB").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('clicked how it works', {'click_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'bottom'});
            });

            $("a#myaccountH").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('clicked open your accout', {'click_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'header'});
            });

            $("a#faqH").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('faq user engagement tracking', {'click_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'header'});
            });
            $("a#faqB").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.people.increment("click_count");
                mixpanel.track('faq user engagement tracking', {'social_url':'https://www.invierteconexpertos.mx/buscar-expertos.aspx', 'click_location': 'bottom'});
            });

            $("a#sfbB").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.track('social link tracking', {'click_url':'https://www.facebook.com/invierteconexpertos'});
            });

            $("a#stwB").click(function() {
                distinct_id = window.localStorage.getItem('distinct_id');
                if (distinct_id == null) {
                    window.localStorage.setItem('distinct_id', Math.floor(Math.random()*1000000000000));
                    distinct_id = window.localStorage.getItem('distinct_id');
                }
                mixpanel.identify(distinct_id);
                mixpanel.track('social link tracking', {'social_url':'https://twitter.com/icexpertos'});
            });
        });
    </script>
    <!-- end Mixpanel -->

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar first"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar last"></span>
                </button>
                <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="funciona.aspx" id="howWorkH" >Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx" id="searchEH" class="active">Buscar Expertos</a></li>
                    <li style="margin-right: 18px;"><a href="preguntas.aspx" id="faqH" >FAQs</a></li>
                    <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                    <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                    <li><a href="Micuenta.aspx" id="myaccountH" >Mi Cuenta</a></li>
                    <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
                </ul>
            </div>
        </div>
    </nav>

<section class="search_wrap">
    <div class="search">
        <form id="searchForm">
            <input type="text" class="search_input" placeholder="Buscar Experto / Estrategia">
            <span class="search_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
            <input type="submit" style="display: none">
        </form>
    </div>
</section>

<section>
<div class="container">
  <!-- Trigger the modal with a button -->
  <button id="BtnAlert" type="button" class="btn btn-info btn-lg hide" data-toggle="modal" data-target="#myModal">Aviso</button>
  <!-- Modal -->
  <div class="modal fade" data-backdrop="static" data-keyboard="false" id="myModal" role="dialog" style="background-color: rgba(0,0,0,.7); padding-top:100px;">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="background-color:#f0f2f5;">
        <div class="modal-body">
            <h3 class="confir_title" style="margin-top:20px;">Invierte con Expertos</h3>
            <p class="confir_text">En muy poco tiempo te avisaremos cuándo puedes comenzar a invertir con los mejores Expertos financieros de México</p>
            <div style="text-align:center;"><button class="btn buscar_btn" onclick="top.location.href='index.aspx';">Ir al Inicio</button></div>
            
        </div>
      </div>
    </div>
  </div>
</div>

</section>

<section class="container buscar_cont">
    <div class="buscar_title">
        <h1 class="block_title" style="margin-top: 50px;">Buscar Expertos</h1>
        <hr class="block_divider">
    </div>
    <div class="buscar_main-wrap">
    <div class="filter_main-wrap">
        <div class="buscar_video_wrap">
                        <p class="buscar_video_text">Ver video</p>
                        <div id="buscarVideoLightbox">
                            <a href="https://www.youtube.com/watch?v=g9IZQ0ALyKg">
                                <img class="buscar_video" src="img/video.jpg">
                            </a>
                        </div>
                    </div>
        <div class="filtres_wrap" style="-webkit-overflow-scrolling:touch;">
            <span class="filtres-mobile_close">&#10006;</span>
            <div class="filtres_block_head">FILTRAR:
                <span class="filtres_block_head_info" id="activeHelp" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                    <i class="fa fa-question-circle-o filtres_block_head_help" aria-hidden="true"></i>
                    Ayuda</span>
                <img class="filtres-mobile_head_caret" src="img/buscar/icons/white-arrow.svg">
            </div>
            <div class="filtres_block" id="filterObjetivo">
                <p class="filtres_block_title">Objetivo de Inversíon
                    <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                </p>
                <div class="filtres_block_cont">
                    <ul class="filtres_obj_list">
                        <li class="filtres_obj_list_item" id="conservador">
                            <span class="filtres_obj_list_num">1</span>Conservador
                            <span class="filtres_obj_list_amount">(78)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </li>
                        <li class="filtres_obj_list_item" id="moderno">
                            <span class="filtres_obj_list_num">2</span>Moderno
                            <span class="filtres_obj_list_amount">(39)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </li>
                        <li class="filtres_obj_list_item" id="emprendedor">
                            <span class="filtres_obj_list_num">3</span>Emprendedor
                            <span class="filtres_obj_list_amount">(20)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </li>
                        <li class="filtres_obj_list_item" id="agresivo">
                            <span class="filtres_obj_list_num">4</span>Agresivo
                            <span class="filtres_obj_list_amount">(56)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </li>
                        <li class="filtres_obj_list_item" id="especulativo">
                            <span class="filtres_obj_list_num">5</span>Especulativo
                            <span class="filtres_obj_list_amount">(85)
                                <i class="fa fa-check" aria-hidden="true"></i>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="filtres_block" id="filterInversion">
                <p class="filtres_block_title">Mínimo de Inversión
                    <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                </p>
                <div class="filtres_block_cont">
                    <div class="filtres_block_slider-value_wrap">
                        <span class="filtres_block_slider-value">$25,000</span>
                        <span class="filtres_block_slider-value" style="float: right">$500,000</span>
                    </div>
                    <div id="inversionSlider" class="filtres_block_slider"></div>
                </div>
            </div>
            <div class="filtres_block" id="filterComision">
                <p class="filtres_block_title">Comisión
                    <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                </p>
                <div class="filtres_block_cont">
                    <div class="filtres_block_slider-value_wrap">
                        <span class="filtres_block_slider-value" id="comisionValue1">0.25%</span>
                        <span class="filtres_block_slider-value" id="comisionValue2" style="float: right;">1.25%</span>
                    </div>
                    <div id="comisionSlider" class="filtres_block_slider"></div>
                </div>
            </div>
            <div class="filtres_block" id="filterOperadora">
                <p class="filtres_block_title">Operadora
                    <img src="img/buscar/icons/arrow.svg" alt="" class="filtres_block_title_arrow">
                </p>
                <div class="filtres_block_cont">
                    <ul class="filtres_oper_list">
                        <li class="filtres_oper_list_item">Actinver <i class="fa fa-check" aria-hidden="true"></i></li>
                        <li class="filtres_oper_list_item">Banamex <i class="fa fa-check" aria-hidden="true"></i></li>
                        <li class="filtres_oper_list_item">Financcess <i class="fa fa-check" aria-hidden="true"></i>
                        </li>
                        <li class="filtres_oper_list_item">Operadora GBM <i class="fa fa-check" aria-hidden="true"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
        <div class="filtres-mobile_btn_wrap">
            <button class="btn filtres-mobile_btn-aplicar">Aplicar Filtros</button>
        </div>
        <div class="filtres-mobile_head">FILTRAR:
            <span class="filtres_block_head_info">
                    <i class="fa fa-question-circle-o filtres_block_head_help" aria-hidden="true"></i>
                    Ayuda</span>
            <img class="filtres-mobile_head_caret" src="img/buscar/icons/white-arrow.svg">
        </div>
        <div class="estrats_wrap">
            <p class="estrats_wrap_section-title">Ordenar por:</p>
            <div class="estrats_ordenar_wrap">
                <div class="estrats_ordenar estrats_ordenar_active"><p>ESTRATEGIAS DESTACADAS</p></div>
                <div class="estrats_ordenar"><p>RENDIMIENTO</p></div>
                <div class="estrats_ordenar"><p>COMISIÓN</p></div>
                <div class="estrats_ordenar"><p>MÍNIMO DE INVERSIÓN</p></div>
            </div>
            <div class="estrats_ordenar-mobile_wrap">
                <img class="strats_ordenar-mobile_caret" src="img/buscar/icons/white-arrow.svg">
                <select name="" id="ordenarMobile" class="estrats_ordenar-mobile">
                    <option value="destacadas">ESTRATEGIAS DESTACADAS</option>
                    <option value="rendimento">RENDIMIENTO</option>
                    <option value="comisión">COMISIÓN</option>
                    <option value="mínimi">MÍNIMO DE INVERSIÓN</option>
                </select>
            </div>
            <p class="estrats_wrap_section-title">Mostrando 52 Estrategias:</p>
            <div class="buscar_estrat">
                <div class="estrat_body_wrap">
                    <div class="buscar_estrat_head">
                        <div class="estrat_head_titles_wrap">
                            <p class="buscar_estrat_head_title">
                                <span>Estrategia:</span> AE120
                            </p>
                            <span> / &nbsp;</span>
                            <p class="buscar_estrat_head_title">
                                <span>Experto:</span> fede11
                            </p>
                        </div>
                        <button class="btn buscar_estrat_head_agregar_btn buscar_estrat_head_agregar_visible"
                                onclick="location.href='detalle.aspx';">
                            Ver Detalle & Invertir
                        </button>
                        <p class="buscar_estrat_head_agregar_summ">Agregaste <span
                                class="buscar_estrat_head_agregar_summ_value"></span></p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_rendimiento">
                        <p class="buscar_estrat_term">Rendimiento</p>
                        <p class="buscar_estrat_percent" style="font-weight:600;">20.5%</p>
                        <p class="buscar_estrat_term" style="margin-top: -16px;">Últimos 12M</p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_comision">
                        <span class="buscar_estrat_subtitle" style="margin-top:20px;">Comisión</span>
                        <div class="buscar_estrat_value_wrap estrat_comision_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_comision_value">1.20%</span>
                            <span class="buscar_estrat_value_sm-text">(anual)</span>
                        </div>
                        <hr class="estrat_block_comision_mob-divider">
                        <span class="buscar_estrat_subtitle" style="margin-top:20px;">Mínimo</span>
                        <div class="buscar_estrat_value_wrap estrat_minimo_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_minimo_value">$50.000</span>
                            <span class="buscar_estrat_value_sm-text">pesos</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_objetivo">
                        <span class="buscar_estrat_subtitle">Objetivo de Inversión</span>
                        <span class="buscar_estrat_objetivo_value">4</span>
                        <div style="margin-top: 10px; text-align: center;">
                            <span class="buscar_estrat_subtitle">Liquidez</span><br />
                            <span class="buscar_estrat_big-value">72hrs</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_diver">
                        <span class="buscar_estrat_subtitle" style="margin-bottom: 8px;">Diversificación</span>
                        <p>
                            <span class="buscar_estrat_big-value buscar_estrat_fondos_value">5</span>
                            <span class="buscar_estrat_subtitle">Fondos</span>
                        </p>
                        <p><span class="buscar_estrat_big-value buscar_estrat_operadoras_value">2</span>
                            <span class="buscar_estrat_subtitle">Operadoras</span>
                        </p>
                    </div>
                    <button class="btn buscar_estrat_mobile_agregar_btn" onclick="location.href='detalle.aspx';">
                        Ver Detalle & Invertir
                    </button>
                </div>
            </div>
            <div class="buscar_estrat">
                <div class="estrat_body_wrap">
                    <div class="buscar_estrat_head">
                        <div class="estrat_head_titles_wrap">
                            <p class="buscar_estrat_head_title">
                                <span>Estrategia</span> AE120
                            </p>
                            <p class="buscar_estrat_head_title">
                                <span>Experto</span> fede11
                            </p>
                        </div>
                        <button class="btn buscar_estrat_head_agregar_btn buscar_estrat_head_agregar_visible"
                                onclick="location.href='detalle.aspx';">
                            Ver Detalle & Invertir
                        </button>
                        <p class="buscar_estrat_head_agregar_summ">Agregaste <span
                                class="buscar_estrat_head_agregar_summ_value"></span></p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_rendimiento">
                        <p class="buscar_estrat_term">Rendimiento</p>
                        <p class="buscar_estrat_percent">20.5%</p>
                        <p class="buscar_estrat_term">Últimos 12M</p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_comision">
                        <span class="buscar_estrat_subtitle">Comisión</span>
                        <div class="buscar_estrat_value_wrap estrat_comision_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_comision_value">1.20%</span>
                            <span class="buscar_estrat_value_sm-text">(anual)</span>
                        </div>
                        <hr class="estrat_block_comision_mob-divider">
                        <span class="buscar_estrat_subtitle">Minomo</span>
                        <div class="buscar_estrat_value_wrap estrat_minimo_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_minimo_value">$50.000</span>
                            <span class="buscar_estrat_value_sm-text">pesos</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_objetivo">
                        <span class="buscar_estrat_subtitle">Objetivo de Inversión</span>
                        <span class="buscar_estrat_objetivo_value">4</span>
                        <div>
                            <span class="buscar_estrat_subtitle">Liquidez:</span>
                            <span class="buscar_estrat_big-value buscar_estrat_liquidez_value">72hrs</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_diver">
                        <span class="buscar_estrat_subtitle">Diversificación</span>
                        <p>
                            <span class="buscar_estrat_big-value buscar_estrat_fondos_value">5</span>
                            <span class="buscar_estrat_subtitle">Fondos</span>
                        </p>
                        <p><span class="buscar_estrat_big-value buscar_estrat_operadoras_value">2</span>
                            <span class="buscar_estrat_subtitle">Operadoras</span>
                        </p>
                    </div>
                    <button class="btn buscar_estrat_mobile_agregar_btn" onclick="location.href='detalle.aspx';">
                        Ver Detalle & Invertir
                    </button>
                </div>
            </div>
            <div class="buscar_estrat">
                <div class="estrat_body_wrap">
                    <div class="buscar_estrat_head">
                        <div class="estrat_head_titles_wrap">
                            <p class="buscar_estrat_head_title">
                                <span>Estrategia</span> AE120
                            </p>
                            <p class="buscar_estrat_head_title">
                                <span>Experto</span> fede11
                            </p>
                        </div>
                        <button class="btn buscar_estrat_head_agregar_btn buscar_estrat_head_agregar_visible"
                                onclick="location.href='detalle.aspx';">
                            Ver Detalle & Invertir
                        </button>
                        <p class="buscar_estrat_head_agregar_summ">Agregaste <span
                                class="buscar_estrat_head_agregar_summ_value"></span></p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_rendimiento">
                        <p class="buscar_estrat_term">Rendimiento</p>
                        <p class="buscar_estrat_percent">20.5%</p>
                        <p class="buscar_estrat_term">Últimos 12M</p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_comision">
                        <span class="buscar_estrat_subtitle">Comisión</span>
                        <div class="buscar_estrat_value_wrap estrat_comision_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_comision_value">1.20%</span>
                            <span class="buscar_estrat_value_sm-text">(anual)</span>
                        </div>
                        <hr class="estrat_block_comision_mob-divider">
                        <span class="buscar_estrat_subtitle">Minomo</span>
                        <div class="buscar_estrat_value_wrap estrat_minimo_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_minimo_value">$50.000</span>
                            <span class="buscar_estrat_value_sm-text">pesos</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_objetivo">
                        <span class="buscar_estrat_subtitle">Objetivo de Inversión</span>
                        <span class="buscar_estrat_objetivo_value">4</span>
                        <div>
                            <span class="buscar_estrat_subtitle">Liquidez:</span>
                            <span class="buscar_estrat_big-value buscar_estrat_liquidez_value">72hrs</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_diver">
                        <span class="buscar_estrat_subtitle">Diversificación</span>
                        <p>
                            <span class="buscar_estrat_big-value buscar_estrat_fondos_value">5</span>
                            <span class="buscar_estrat_subtitle">Fondos</span>
                        </p>
                        <p><span class="buscar_estrat_big-value buscar_estrat_operadoras_value">2</span>
                            <span class="buscar_estrat_subtitle">Operadoras</span>
                        </p>
                    </div>
                    <button class="btn buscar_estrat_mobile_agregar_btn" onclick="location.href='detalle.aspx';">
                        Ver Detalle & Invertir
                    </button>
                </div>
            </div>
            <div class="buscar_estrat">
                <div class="estrat_body_wrap">
                    <div class="buscar_estrat_head">
                        <div class="estrat_head_titles_wrap">
                            <p class="buscar_estrat_head_title">
                                <span>Estrategia</span> AE120
                            </p>
                            <p class="buscar_estrat_head_title">
                                <span>Experto</span> fede11
                            </p>
                        </div>
                        <button class="btn buscar_estrat_head_agregar_btn buscar_estrat_head_agregar_visible"
                                onclick="location.href='detalle.aspx';">
                            Ver Detalle & Invertir
                        </button>
                        <p class="buscar_estrat_head_agregar_summ">Agregaste <span
                                class="buscar_estrat_head_agregar_summ_value"></span></p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_rendimiento">
                        <p class="buscar_estrat_term">Rendimiento</p>
                        <p class="buscar_estrat_percent">20.5%</p>
                        <p class="buscar_estrat_term">Últimos 12M</p>
                    </div>
                    <div class="buscar_estrat_block estrat_block_comision">
                        <span class="buscar_estrat_subtitle">Comisión</span>
                        <div class="buscar_estrat_value_wrap estrat_comision_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_comision_value">1.20%</span>
                            <span class="buscar_estrat_value_sm-text">(anual)</span>
                        </div>
                        <hr class="estrat_block_comision_mob-divider">
                        <span class="buscar_estrat_subtitle">Minomo</span>
                        <div class="buscar_estrat_value_wrap estrat_minimo_wrap">
                            <span class="buscar_estrat_big-value buscar_estrat_minimo_value">$50.000</span>
                            <span class="buscar_estrat_value_sm-text">pesos</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_objetivo">
                        <span class="buscar_estrat_subtitle">Objetivo de Inversión</span>
                        <span class="buscar_estrat_objetivo_value">4</span>
                        <div>
                            <span class="buscar_estrat_subtitle">Liquidez:</span>
                            <span class="buscar_estrat_big-value buscar_estrat_liquidez_value">72hrs</span>
                        </div>
                    </div>
                    <div class="buscar_estrat_block estrat_block_diver">
                        <span class="buscar_estrat_subtitle">Diversificación</span>
                        <p>
                            <span class="buscar_estrat_big-value buscar_estrat_fondos_value">5</span>
                            <span class="buscar_estrat_subtitle">Fondos</span>
                        </p>
                        <p><span class="buscar_estrat_big-value buscar_estrat_operadoras_value">2</span>
                            <span class="buscar_estrat_subtitle">Operadoras</span>
                        </p>
                    </div>
                    <button class="btn buscar_estrat_mobile_agregar_btn" onclick="location.href='detalle.aspx';">
                        Ver Detalle & Invertir
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="buscar_modal_overlay" id="helpModal">
    <div class="help_body">
        <span class="buscar_modal_close">&times;</span>
        <h2 class="block_title" style="margin-top: 0;">Ayuda para usar el filtro</h2>
        <hr class="block_divider">
        <div class="help_group">
            <h3 class="help_group_title">Objetivo de Inversión</h3>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpCons">Conservador</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir rendimientos bajos asumiendo un
                        riesgo
                        bajo<br>
                        - Invierten hasta un 10% en Renta Variable (Bolsa de Valores)<br>
                        - Baja volatilidad<br>
                        - Horizonte de inversión a 1 año<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpModer">Moderno</h4>
                    <p class="help_group_single_text">
                        Estretegias recomendadas para personas que quieren recibir rendimientos moderados asumiendo un
                        riesgo medio<br>
                        - Invierten hasta un 25% en Renta Variable (Bolsa de Valores)<br>
                        - Media volatilidad<br>
                        - Horizonte de inversion a 2 años<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpEmpr">Emprendedor</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir rendimientos altos asumiendo un
                        riesgo
                        media-alto.<br>
                        - Invierten hasta un 40% en Renta Veriable (Bolsa de Valores)<br>
                        - Media-Alta volatilidad<br>
                        - Horizonte de inversión a 3 años<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpAgres">Agresivo</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir rendimientos muy altos asumiendo un
                        riesgo alto.<br>
                        - Invierten hasta un 60% en Renta Veriable (Bolsa de Valores)<br>
                        - Alta volatilidad<br>
                        - Horizonte de inversión a 4 años<br>
                    </p>
                </div>
            </div>
            <div class="help_group_single">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title" id="helpEspec">Especulativo</h4>
                    <p class="help_group_single_text">
                        Estrategias recomendadas para personas que quieren recibir los más altos rendimientos asumiendo
                        un riesgo muy alto<br>
                        - Invierten hasta un 100% en Renta Veriable (Bolsa de Valores)<br>
                        - Muy alta volatilidad<br>
                        - Horizonte de inversión a 5 años<br>
                    </p>
                </div>
            </div>
        </div>
        <hr class="block_divider" style="margin-top: 80px;">
        <div class="help_group">
            <h3 class="help_group_title">Términos de Referencia</h3>
            <div class="help_group_single help_single-ref">
                <img class="help_group_single_icon" src="img/buscar/icons/inversion.svg">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title help_group_ref-title">Mínimo de inversion</h4>
                    <p class="help_group_single_text">
                        El importe mínimo es determinado por el experto y es la cantidad mínima que se requiere para
                        invertir en la estrategia.
                    </p>
                </div>
            </div>
            <div class="help_group_single help_single-ref">
                <img class="help_group_single_icon" src="img/buscar/icons/operadora.svg">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title help_group_ref-title">Operadora</h4>
                    <p class="help_group_single_text">
                        Es la institucion financiera que administra alguno de los fondos de inversión que se incluyen en
                        la estrategia.
                    </p>
                </div>
            </div>
            <div class="help_group_single help_single-ref">
                <img class="help_group_single_icon" src="img/buscar/icons/comision.svg">
                <div class="help_group_single_cont">
                    <h4 class="help_group_single_title help_group_ref-title">Comisión</h4>
                    <p class="help_group_single_text">
                        Cada experto determina el costo por utilizar su estrategia. La comisión es anual y se cobra    
                        mensualmente sobre el saldo promedio. El cobro se realiza durante los primeros 5 días hábiles 
                        siguientes al mes vencido.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="buscar_modal_overlay" style="display: none;" id="agregarModal">
    <div class="buscar_modal_body">
        <span class="buscar_modal_close">&times;</span>
        <h3 class="buscar_modal_title" id="estrategiaName1">Estrategia AE120</h3>
        <p class="buscar_agregar-modal_text">Importe Minomo: <span>$50,000</span></p>
        <label for="agregarValue">Indicia el importe que deseas invertir en la estrategia:</label>
        <input class="buscar_modal_input" id="agregarValue">
        <div class="buscar_modal_btn-wrap">
            <button class="btn buscar_modal_btn" id="agregarConfirm">Agregar al Portafolio</button>
        </div>
    </div>

</div>

<div class="buscar_modal_overlay" style="display: none;" id="objetivoModal">
    <div class="buscar_modal_body">
        <span class="buscar_modal_close">&times;</span>
        <h3 class="buscar_modal_title">Objetivo de Inversión</h3>
        <div class="buscar_modal_table_wrap">
            <table class="buscar_modal_table">
                <tr>
                    <th>Objetivo</th>
                    <th>Máximo en Renta Variable</th>
                    <th>Volafilidad</th>
                    <th>Tiempo Sugerido</th>
                </tr>
                <tr>
                    <td>Conservador</td>
                    <td>10%</td>
                    <td>Baja</td>
                    <td>1 año</td>
                </tr>
                <tr>
                    <td>Moderado</td>
                    <td>25%</td>
                    <td>Media</td>
                    <td>2 año</td>
                </tr>
                <tr>
                    <td>Emprendedor</td>
                    <td>40%</td>
                    <td>Media-Alta</td>
                    <td>3 años</td>
                </tr>
                <tr>
                    <td>Agresivo</td>
                    <td>60%</td>
                    <td>Alta</td>
                    <td>4 años</td>
                </tr>
                <tr>
                    <td>Especulativo</td>
                    <td>100%</td>
                    <td>Muy Alta</td>
                    <td>5 años</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<hr class="main_divider">
<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>

<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/" id="sfbB" ><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos" id="stwB" ><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx" id="howWorkB" >Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx" id="searchEB" >Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx" id="faqB" >Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>

                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx" id="howWorkB" >Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx" id="searchEB" >Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx" id="faqB" >Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/" id="sfbB" ><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos" id="stwB" ><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in
                    porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
</footer>
<script src="js/general_bundle.js"></script>
<script src="js/buscar_bundle.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#BtnAlert").trigger("click");
    });
</script>
</body>
</html>