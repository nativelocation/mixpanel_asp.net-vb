﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="portafolio.aspx.vb" Inherits="inversionistas_portafolio" %>

<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com -->
<!--  Last Published: Wed Apr 05 2017 22:14:20 GMT+0000 (UTC)  -->
<html data-wf-page="58cc50f6e175a0d719c4e56f" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Portafolio</title>
  <meta content="Portafolio" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="../expertos/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="../expertos/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="../expertos/css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="../expertos/js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-form-wrapper">
    <div class="close" data-ix="close"></div>
    <div class="login-wrapper">
      <div class="login-photo"></div>
      <div class="form w-tabs">
        <div class="login-back w-tab-menu">
          <a class="register-round w-inline-block w-tab-link" data-w-tab="Tab 2">
            <div class="login-text">Regístrate</div>
          </a>
          <a class="login-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
            <div class="login-text">Inicia Sesión</div>
          </a>
        </div>
        <div class="w-tab-content">
          <div class="w-tab-pane" data-w-tab="Tab 2">
            <div class="login-back-bottom register">
              <p class="brown-center-14px">Crea tu cuenta en el portal de Expertos y comienza a registrar tus estrategias</p><a class="button reg w-button" href="#">Regístrate Gratis</a>
            </div>
          </div>
          <div class="w--tab-active w-tab-pane" data-w-tab="Tab 1">
            <div class="login-back-bottom">
              <div class="w-form">
                <form class="login-form" data-name="Email Form" id="email-form" name="email-form">
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="../expertos/images/Envelope---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Correo Electr Nico 2" id="correo-electr-nico-2" maxlength="256" name="correo-electr-nico-2" placeholder="correo electrónico" required="required" type="email">
                  </div>
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="../expertos/images/lock---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Contrase A 2" id="contrase-a-2" maxlength="256" name="contrase-a-2" placeholder="contraseña" type="password">
                  </div>
                  <div class="checkbox-field w-checkbox w-clearfix">
                    <input class="checkbox w-checkbox-input" data-name="Checkbox 5" id="checkbox-5" name="checkbox-5" type="checkbox">
                    <label class="small-right-text w-form-label" for="checkbox-5">Recuérdame</label>
                  </div>
                  <input class="button login w-button" data-wait="Please wait..." type="submit" value="Ingresar">
                </form>
                <div class="w-form-done">
                  <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                  <div>Oops! Something went wrong while submitting the form</div>
                </div>
              </div><a class="small-center-text" href="#">¿Olvidaste tu contraseña?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="light-section">
    <div class="nav-bar nb-light w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="../index.aspx"><img src="../expertos/images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link nl-light w-nav-link" href="#">Buscar Expertos</a><a class="nav-link nl-light w-nav-link" href="portafolio.aspx">Portafolio</a><a class="nav-link nl-light w-nav-link" href="movimientos.aspx">Movimientos</a><a class="nav-link nl-light w-nav-link" href="comisiones.aspx">Comisiones</a><a class="nav-link nl-light w-nav-link" href="herramientas.aspx">Herramientas</a><a class="mbb-light menu-bar-button w-button" href="recomienda.aspx">Recomienda y Gana</a><a class="nav-link nl-hide nl-light w-nav-link" href="#">Сerrar sesión</a>
        </nav>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="estrategies-top et-light">
      <div class="heading-wrapper">
        <div>Hola, Diego!</div>
        <h2 class="heading-white-28px hw-light left-align">Portafolio</h2>
      </div>
      <div class="div-block-3"><a class="dark-link" href="#">Cerrar Sesión</a>
        <div class="buttons-wrapper-2 bw-2-light">
          <div class="depositar-wrapper w-clearfix">
            <div class="pw-1">
              <div class="popups-wrapper pw-1">
                <div class="window-light-close" data-ix="light-window-close"></div>
                <div class="pw-first-first">
                  <div class="port-window pw-first-first">
                    <div class="window-menu-bar">
                      <div class="window-title">Depositar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <div class="window-info-wrapper"><img class="image" src="../expertos/images/safebox.svg">
                      <div class="text-block">Avisanos por este medio cuando hayas depositado dinero a tu cuenta. Si quieres conocer las cuentas autorizadas da <a class="light-link">clic quí</a>
                      </div>
                    </div>
                    <div class="form-wrapper w-form">
                      <form class="w-clearfix window-form-light" data-name="Email Form 2" id="email-form-2" name="email-form-2">
                        <label class="window-form-label" for="Ingresa-2">Ingresa la cantidad que ya depositaste:</label>
                        <input class="form-light-field w-input" data-name="Ingresa 2" id="Ingresa-2" maxlength="256" name="Ingresa-2" type="text">
                        <label class="window-form-label" for="field-5">En que Banco depositaste:</label>
                        <select class="select-field w-select" data-name="Field 5" id="field-5" name="field-5">
                          <option value="">Select one...</option>
                          <option value="First">First Choice</option>
                          <option value="Second">Second Choice</option>
                          <option value="Third">Third Choice</option>
                        </select>
                        <label class="window-form-label" for="field-2">Ingresa la fecha que hiciste tu deposito:</label>
                        <div class="form-date-wrapper">
                          <select class="day select-field w-select" data-name="Field 6" id="field-6" name="field-6">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                          </select>
                          <select class="month select-field w-select" data-name="Field 7" id="field-7" name="field-7">
                            <option value="Jan">January</option>
                            <option value="Dec">December</option>
                            <option value="Feb">February</option>
                            <option value="Mar">March</option>
                          </select>
                          <select class="select-field w-select year" data-name="Field 8" id="field-8" name="field-8">
                            <option value="2017">2017</option>
                            <option value="2016">2016</option>
                            <option value="2015">2015</option>
                            <option value="2014">2014</option>
                          </select>
                        </div>
                        <div class="divider-form"></div>
                        <input class="form-light-button w-button" data-ix="second-window-appear" data-wait="Please wait..." type="submit" value="Reportar Deposito">
                      </form>
                      <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                      </div>
                      <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pw-second-first">
                  <div class="port-window pw-second-first">
                    <div class="window-menu-bar">
                      <div class="window-title">Depositar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <h4 class="window-heading">Confirma la Operación</h4>
                    <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/safebox.svg">
                      <div class="text-block">Reportar Deposito
                        <a href="#" class="light-link"></a>
                      </div>
                    </div>
                    <div class="div-block">
                      <div class="text-block-2">Depositar: <span class="main-text">$50,000 pesos</span>
                      </div>
                      <div class="text-block-2">Fecha de Deposito: <span>&nbsp;<span class="main-text">24 septiembre 2016</span></span>
                      </div>
                      <div class="last-one text-block-2">Banco donde depositaste:&nbsp; <span class="main-text">BBVA BANCOMER</span>
                      </div>
                    </div>
                    <div class="buttom-wrapper-form-ligh w-clearfix">
                      <div class="divider-form"></div><a class="form-light-button w-button" data-ix="third-window-appear" href="#">Reportar Deposito</a>
                      <div class="dark small-text">Los recurs se verán reflejando en un lapso no mayor
                        <br>a 24 horas hábilies</div>
                    </div>
                  </div>
                </div>
                <div class="pw-third-first">
                  <div class="port-window pw-third-first">
                    <div class="window-menu-bar">
                      <div class="window-title">Depositar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <h4 class="window-heading">Operación realizada satisfactoriamente</h4>
                    <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/safebox.svg">
                      <div class="text-block">Puedes monitorear el status de la operación en la&nbsp; sección «MoVimientos» de tu cuenta. Los recursos se verán reflejando en un lapso no mayor a 24 horas hábiles después de haberlos depositado.
                        <a href="#" class="light-link"></a>
                      </div>
                    </div>
                    <div class="buttom-wrapper-form-ligh w-clearfix">
                      <div class="divider-form"></div><a class="form-light-button w-button" data-ix="fourth-window-appear" href="#">Cerrar</a>
                    </div>
                  </div>
                </div>
                <div class="pw-fourth-first">
                  <div class="port-window pw-fourth-first">
                    <div class="window-menu-bar">
                      <div class="window-title">Depositar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <h4 class="window-heading">Depositar a tu cuenta</h4>
                    <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/safebox.svg">
                      <div class="text-block">Para depositar en tu cuenta, debes realizar una transferencia a las siguientes cuentas
                        <a href="#" class="light-link"></a>
                      </div>
                    </div>
                    <div class="estrrr t-light table-block tb-integrate">
                      <div class="table-underline tu-dark"></div>
                      <div class="table">
                        <div class="head-string table-string">
                          <div class="table-item ti-third tt-light">
                            <div>Banco</div>
                          </div>
                          <div class="table-item tt-light">
                            <div>Cuenta</div>
                          </div>
                          <div class="_1231 table-item ti-fourth tt-light">
                            <div>Referencia</div>
                          </div>
                        </div>
                        <div class="table-underline tu-dark"></div>
                        <div class="table-string ts-first">
                          <div class="table-item ti-third tt-light">
                            <div>BBVA Bancomer</div>
                          </div>
                          <div class="table-item tt-light">
                            <div>015812831</div>
                          </div>
                          <div class="_1231 table-item ti-fourth tt-light">
                            <div>3333534-1</div>
                          </div>
                        </div>
                        <div class="table-string">
                          <div class="table-item ti-third tt-light">
                            <div>Santander</div>
                          </div>
                          <div class="table-item tt-light">
                            <div>015812831</div>
                          </div>
                          <div class="_1231 table-item ti-fourth tt-light">
                            <div>3333534-1</div>
                          </div>
                        </div>
                        <div class="table-string">
                          <div class="table-item ti-third tt-light">
                            <div>IXE Banorte</div>
                          </div>
                          <div class="table-item tt-light">
                            <div>015812831</div>
                          </div>
                          <div class="_1231 table-item ti-fourth tt-light">
                            <div>3333534-1</div>
                          </div>
                        </div>
                        <div class="table-string">
                          <div class="table-item ti-third tt-light">
                            <div>Banamex</div>
                          </div>
                          <div class="table-item tt-light">
                            <div>015812831</div>
                          </div>
                          <div class="_1231 table-item ti-fourth tt-light">
                            <div>3333534-1</div>
                          </div>
                        </div>
                        <div class="table-string">
                          <div class="table-item ti-third tt-light">
                            <div>HSBC</div>
                          </div>
                          <div class="table-item tt-light">
                            <div>015812831</div>
                          </div>
                          <div class="_1231 table-item ti-fourth tt-light">
                            <div>3333534-1</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="mobile mobile-table-light table-block tbb-trans">
                      <div class="table-underline tu-dark"></div>
                      <div class="mobile-table">
                        <div class="mobile-table-string mt-heading-string">
                          <div class="table-dark-text">19 dec, 2016</div>
                        </div>
                        <div class="mobile-table-string">
                          <div class="mt-label mtl-dark">Cuenta:</div>
                          <div class="mt-info mti-dark">01581231</div>
                        </div>
                        <div class="mobile-table-string">
                          <div class="mt-label mtl-dark">Referencia:</div>
                          <div class="mt-info mti-dark">33334534-1</div>
                        </div>
                      </div>
                      <div class="table-underline tu-dark"></div>
                      <div class="mobile-table">
                        <div class="mobile-table-string mt-heading-string">
                          <div class="table-dark-text">20 dec, 2016</div>
                        </div>
                        <div class="mobile-table-string">
                          <div class="mt-label mtl-dark">Cuenta:</div>
                          <div class="mt-info mti-dark">01581231</div>
                        </div>
                        <div class="mobile-table-string">
                          <div class="mt-label mtl-dark">Referencia:</div>
                          <div class="mt-info mti-dark">$33334534-1</div>
                        </div>
                      </div>
                      <div class="table-underline tu-dark"></div>
                    </div>
                    <div class="buttom-wrapper-form-ligh w-clearfix"><a class="form-light-button w-button" href="#">Enviar por Correo Electrónico</a>
                    </div>
                  </div>
                </div>
              </div>
            </div><a class="flb-yellow form-light-button w-button" data-ix="window-light-appear" href="#">Depositar</a>
          </div>
          <div class="retirar-wrapper">
            <div class="pw-2">
              <div class="popups-wrapper pw-2">
                <div class="window-light-close" data-ix="light-window-close"></div>
                <div class="pw-second">
                  <div class="port-window pw-second">
                    <div class="window-menu-bar">
                      <div class="window-title">Retirar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/money.svg">
                      <div class="div-block-2">
                        <div class="text-block">Disponible: <span class="dollar-s">$20,000.00</span>
                          <a href="#" class="light-link"></a>
                        </div>
                        <div class="w-form">
                          <form class="form-3" data-name="Email Form 3" id="email-form-3" name="email-form-3">
                            <label class="window-form-label" for="node">Ingresa la cantidad que quieres retirar:</label>
                            <input class="form-light-field long w-input" data-name="$" id="node" maxlength="256" placeholder="$" type="text">
                          </form>
                          <div class="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                          </div>
                          <div class="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="buttom-wrapper-form-ligh w-clearfix">
                      <div class="divider-form"></div><a class="form-light-button w-button" data-ix="third-window-appear-2" href="#">Retirar</a>
                      <div class="dark small-text">Tu deposito se verá reflejando en un lapso no mayor
                        <br> a 24 horas hábilies</div>
                    </div>
                  </div>
                </div>
                <div class="pw-third">
                  <div class="port-window pw-third">
                    <div class="window-menu-bar">
                      <div class="window-title">Retirar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <h4 class="window-heading">Confirma la Operación</h4>
                    <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/money.svg">
                      <div class="text-block">Retirar en tu cuenta <span class="dollar-s">$20,000</span> pesos
                        <a href="#" class="light-link"></a>
                      </div>
                    </div>
                    <div class="buttom-wrapper-form-ligh w-clearfix">
                      <div class="divider-form"></div><a class="form-light-button w-button" data-ix="fourth-window-appear-2" href="#">Retirar</a>
                      <div class="dark small-text">Los recursos se verán reflejando en un lapso no mayor
                        <br>a 24 horas hábiles después de haberlos depositado.</div>
                    </div>
                  </div>
                </div>
                <div class="pw-fourth">
                  <div class="port-window pw-fourth">
                    <div class="window-menu-bar">
                      <div class="window-title">Retirar</div>
                      <div class="window-cross" data-ix="light-window-close">x</div>
                    </div>
                    <h4 class="window-heading">Operación realizada satisfactoriamente</h4>
                    <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/money.svg">
                      <div class="text-block">Puedes monitorear el estatus de la opración en la sección «Movimientos» de tu cuenta.
                        <a href="#" class="light-link"></a>
                      </div>
                    </div>
                    <div class="buttom-wrapper-form-ligh w-clearfix">
                      <div class="divider-form"></div><a class="form-light-button w-button" data-ix="fourth-window-appear" href="#">Cerrar</a>
                      <div class="dark small-text">Los recursos se verán reflejando en un lapso no mayor
                        <br>a 24 horas hábiles después de haberlos depositado.</div>
                    </div>
                  </div>
                </div>
              </div>
            </div><a class="flb-yellow form-light-button ret w-button" data-ix="window-light-appear-2" href="#">Retirar</a>
          </div>
        </div>
      </div>
    </div>
    <div class="info-blocks-wrapper">
      <div class="ib-light info-block">
        <div class="info-price ip-light"><span class="dollar">$</span>350,001.00</div>
        <div class="info-question-wrapper">
          <div class="info-text it-light">Saldo</div>
          <div class="popup-wrapper">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
          </div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon_1.svg" width="16">
        </div>
      </div>
      <div class="plus">=</div>
      <div class="ib-light info-block trans">
        <div class="info-price ip-light"><span class="dollar">$</span>300,000.50</div>
        <div class="info-question-wrapper">
          <div class="info-text it-light">Invertido</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon_1.svg" width="16">
          <div class="popup-wrapper">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
          </div>
        </div>
      </div>
      <div class="plus">+</div>
      <div class="ib-light info-block trans">
        <div class="info-price ip-light"><span class="dollar"></span><span class="dollar">$</span>50,000.50</div>
        <div class="info-question-wrapper">
          <div class="info-text it-light">Disponible</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon_1.svg" width="16">
          <div class="popup-wrapper">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
          </div>
        </div>
      </div>
    </div>
    <div class="graphic-wrapper gw-light">
      <div class="gm-light graphic-menu">
        <div class="gh-light graphic-heading" data-ix="comp-dissapear">Estrategia AE120</div>
        <div class="graphic-menu-points">
          <a class="gmp-light graphic-menu-point w-inline-block" href="#"><img class="menu-icon" src="../expertos/images/pie-chart.svg" width="18">
            <div class="gmt-light graphic-menu-text">Invertir</div>
          </a>
          <a class="gmp-light graphic-menu-point w-inline-block" data-ix="comp-appear" href="#"><img class="menu-icon" src="../expertos/images/composition_1.svg" width="19">
            <div class="gmt-light graphic-menu-text">Composición</div>
          </a>
          <a class="gmp-light graphic-menu-point w-inline-block" href="#"><img class="menu-icon" src="../expertos/images/cash.svg" width="16">
            <div class="gmt-light graphic-menu-text">Vender</div>
          </a>
        </div>
      </div>
      <div class="g-light graphic">
        <div class="graphic-section gs-light">
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$120 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$80 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$60 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$40 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$20 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$0</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="empty graphic-line-text"></div>
            <div class="graphic-date-wrapper">
              <div class="gbt-hide graphic-bottom-text tb-light">42 000</div>
              <div class="gbt-hide graphic-bottom-text tb-light">42 100</div>
              <div class="gbt-hide graphic-bottom-text tb-light">42 200</div>
              <div class="graphic-bottom-text tb-light">42 300</div>
              <div class="graphic-bottom-text tb-light">42 400</div>
              <div class="graphic-bottom-text tb-light">42 500</div>
              <div class="graphic-bottom-text tb-light">42 600</div>
              <div class="graphic-bottom-text tb-light">42 700</div>
            </div>
          </div>
          <div class="p-blue point" data-ix="window-appear">
            <div class="pip pp-blue" data-ix="window-appear"></div>
            <div class="point-window">
              <div class="point-window-block">
                <div class="window-date">4 oct, 2016</div>
              </div>
            </div>
          </div><img class="graphic-path" src="../expertos/images/Path-Yellow.svg" width="570"><img class="mobile-graphic-path" src="../expertos/images/Path-Yellow-Mobile.svg">
        </div>
        <div class="stats-wrapper">
          <div class="stat-block top">
            <div class="info-price ip-18px ip-light"><span class="dollar-s">$</span>150,250.50</div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Saldo</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="stat-block">
            <div class="info-price ip-18px ip-light">21,54&nbsp;<span class="dollar-s">%</span>
            </div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Rendimiento</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="stat-block">
            <div class="info-price ip-18px ip-light"><span class="dollar-s">$</span>27,205.05</div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Plusvalía/Minusvalía</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="stat-block">
            <div class="info-price ip-18px ip-light">4/Dic/2018</div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Plazo Sugerido</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="composition-wrapper cw-light">
        <div class="estrrr table-block tb-integrate">
          <div class="table-heading">
            <h3 class="table-heading th-dark">Composición de Estrategia EA200</h3>
            <div class="table-date td-light">19 dec, 2016</div>
          </div>
          <div class="table-underline tu-dark"></div>
          <div class="table">
            <div class="head-string table-string">
              <div class="table-item ti-second ti2-light">
                <div>Fondo</div>
              </div>
              <div class="table-item ti-third tt-light">
                <div>Operadora</div>
              </div>
              <div class="table-item ti-dark">
                <div>Clasificatión</div>
              </div>
            </div>
            <div class="table-underline tu-dark"></div>
            <div class="table-string">
              <div class="table-item ti-second ti2-light">
                <div>GMBCREB</div>
              </div>
              <div class="table-item ti-third tt-light">
                <div>Operadora GBM</div>
              </div>
              <div class="table-item ti-dark">
                <div>Acciones Descrecional</div>
              </div>
            </div>
            <div class="table-string">
              <div class="table-item ti-second ti2-light">
                <div>GMBCREB</div>
              </div>
              <div class="table-item ti-third tt-light">
                <div>Operadora GBM</div>
              </div>
              <div class="table-item ti-dark">
                <div>Acciones México</div>
              </div>
            </div>
            <div class="table-string">
              <div class="table-item ti-second ti2-light">
                <div>GMBCREB</div>
              </div>
              <div class="table-item ti-third tt-light">
                <div>Operadora GBM</div>
              </div>
              <div class="table-item ti-dark">
                <div>Acciones Internacionales</div>
              </div>
            </div>
            <div class="table-string">
              <div class="table-item ti-second ti2-light">
                <div>GMBCREB</div>
              </div>
              <div class="table-item ti-third tt-light">
                <div>Operadora GBM</div>
              </div>
              <div class="table-item ti-dark">
                <div>Acciones Estados Unidos</div>
              </div>
            </div>
          </div>
        </div>
        <div class="mobile mobile-table-light table-block">
          <div>
            <h3 class="table-heading th-dark">Composición de Estrategia EA200</h3>
          </div>
          <div class="table-underline tu-dark"></div>
          <div class="mobile-table">
            <div class="mobile-table-string mt-dark mt-heading-string">
              <div>19 dec, 2016</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">%:</div>
              <div class="mt-info mti-dark">$21,105</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Fondo:</div>
              <div class="mt-info mti-dark">$35,052,025</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Operadora:</div>
              <div class="mt-info mti-dark">Operadora GBM</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Rendimiento:</div>
              <div class="mt-info mti-dark">20.55%</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Pluslavía:</div>
              <div class="mt-info mti-dark">$25,250</div>
            </div>
          </div>
          <div class="table-underline tu-dark"></div>
          <div class="mobile-table">
            <div class="mobile-table-string mt-dark mt-heading-string">
              <div>20 dec, 2016</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">%:</div>
              <div class="mt-info mti-dark">$21,105</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Fondo:</div>
              <div class="mt-info mti-dark">$35,052,025</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Operadora:</div>
              <div class="mt-info mti-dark">Operadora GBM</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Rendimiento:</div>
              <div class="mt-info mti-dark">20.55%</div>
            </div>
            <div class="mobile-table-string">
              <div class="mt-label mtl-dark">Pluslavía:</div>
              <div class="mt-info mti-dark">$25,250</div>
            </div>
          </div>
          <div class="table-underline tu-dark"></div>
        </div>
      </div>
    </div>
    <div class="graphic-wrapper gw-light">
      <div class="gm-light graphic-menu">
        <div class="gh-light graphic-heading" data-ix="comp-dissapear-2">Estrategia AE120</div>
        <div class="graphic-menu-points">
          <a class="gmp-light graphic-menu-point w-inline-block" data-ix="comprar-appear" href="#"><img class="menu-icon" src="../expertos/images/cart.svg" width="18">
            <div class="gmt-light graphic-menu-text">Comprar</div>
          </a>
          <a class="gmp-light graphic-menu-point w-inline-block" data-ix="comp-appear-2" href="#"><img class="menu-icon" src="../expertos/images/composition_1.svg" width="19">
            <div class="gmt-light graphic-menu-text">Composición</div>
          </a>
          <a class="gmp-light graphic-menu-point w-inline-block" href="#"><img class="menu-icon" src="../expertos/images/cash.svg" width="16">
            <div class="gmt-light graphic-menu-text">Vender</div>
          </a>
          <div class="comprar-wrapper">
            <div class="comprar-wrapper popups-wrapper">
              <div class="window-light-close" data-ix="light-window-close"></div>
              <div class="comprar-first">
                <div class="port-window">
                  <div class="window-menu-bar">
                    <div class="window-title">Comprar</div>
                    <div class="window-cross" data-ix="light-window-close">x</div>
                  </div>
                  <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/cart.svg" width="50">
                    <div class="div-block-2">
                      <div class="tb-main text-block">Disponible: <span class="dollar-s">$20,000.00</span>
                        <a href="#" class="light-link"></a>
                      </div>
                      <div class="db-light div-block">
                        <div class="tb2-light text-block-2">Depositar:&nbsp; <span class="main-text">$50,000 pesos</span>
                        </div>
                        <div class="tb2-light text-block-2">Fecha de Deposito: <span>&nbsp;<span class="main-text">24 septiembre 2016</span></span>
                        </div>
                        <div class="tb2-light text-block-2">Banco donde depositaste:&nbsp; <span class="main-text">BBVA BANCOMER</span>
                        </div>
                      </div>
                      <div class="w-form">
                        <form class="form-3" data-name="Email Form 3" id="email-form-3" name="email-form-3">
                          <label class="window-form-label" for="node">Ingresa la cantidad que quieres retirar:</label>
                          <input class="form-light-field long w-input" data-name="$" id="node" maxlength="256" placeholder="$" type="text">
                        </form>
                        <div class="w-form-done">
                          <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div class="w-form-fail">
                          <div>Oops! Something went wrong while submitting the form</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="buttom-wrapper-form-ligh w-clearfix">
                    <div class="divider-form"></div><a class="form-light-button w-button" data-ix="comprar-second" href="#">Comprar</a>
                    <div class="dark small-text">La compra se realizará de acuerdo a la política de cada fondo
                      <br>que se incluya en la estrategia</div>
                  </div>
                </div>
              </div>
              <div class="comprar-second">
                <div class="comprar-second port-window">
                  <div class="window-menu-bar">
                    <div class="window-title">Comprar</div>
                    <div class="window-cross" data-ix="light-window-close">x</div>
                  </div>
                  <h4 class="window-heading">Confirma la Operación</h4>
                  <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/cart.svg" width="50">
                    <div class="div-block-2">
                      <div class="tb-main text-block">Estrategia: <span>AE120</span>
                        <a href="#" class="light-link"></a>
                      </div>
                      <div class="db-light div-block">
                        <div class="tb2-light text-block-2">Comprar <span class="dollar-s">&nbsp;$20,000</span> &nbsp;pesos para invertirlos en la Estrategia&nbsp; <span class="dollar-s">AE120</span><span class="main-text"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="buttom-wrapper-form-ligh w-clearfix">
                    <div class="divider-form"></div><a class="form-light-button w-button" data-ix="comprar-third-appear" href="#">Comprar</a>
                    <div class="dark small-text">La operación se lleverá a cabo en un horario de operación, de acuerdo
                      <br>a la política de operación de cada fondo que integra la estrategia,
                      <br>en plazo máximo de 7 días hábiles</div>
                  </div>
                </div>
              </div>
              <div class="comprar-third">
                <div class="comprar-third port-window">
                  <div class="window-menu-bar">
                    <div class="window-title">Comprar</div>
                    <div class="window-cross" data-ix="light-window-close">x</div>
                  </div>
                  <h4 class="window-heading">Operación realizada satisfactoriamente</h4>
                  <div class="secondd window-info-wrapper"><img class="image" src="../expertos/images/cart.svg" width="50">
                    <div class="text-block">Puedes monitorear el estatus de la opración en la sección «Movimientos» de tu cuenta.
                      <a href="#" class="light-link"></a>
                    </div>
                  </div>
                  <div class="buttom-wrapper-form-ligh w-clearfix">
                    <div class="divider-form"></div><a class="form-light-button w-button" data-ix="fourth-window-appear" href="#">Cerrar</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="g-light graphic">
        <div class="graphic-section gs-light">
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$120 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$80 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$60 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$40 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$20 000</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="glt-light graphic-line-text">
              <div class="tb-light">$0</div>
            </div>
            <div class="gls-light graphic-line-shape"></div>
          </div>
          <div class="graphic-line">
            <div class="empty graphic-line-text"></div>
            <div class="graphic-date-wrapper">
              <div class="gbt-hide graphic-bottom-text tb-light">42 000</div>
              <div class="gbt-hide graphic-bottom-text tb-light">42 100</div>
              <div class="gbt-hide graphic-bottom-text tb-light">42 200</div>
              <div class="graphic-bottom-text tb-light">42 300</div>
              <div class="graphic-bottom-text tb-light">42 400</div>
              <div class="graphic-bottom-text tb-light">42 500</div>
              <div class="graphic-bottom-text tb-light">42 600</div>
              <div class="graphic-bottom-text tb-light">42 700</div>
            </div>
          </div>
          <div class="p-blue point" data-ix="window-appear">
            <div class="pip pp-blue" data-ix="window-appear"></div>
            <div class="point-window">
              <div class="point-window-block">
                <div class="window-date">4 oct, 2016</div>
              </div>
            </div>
          </div><img class="graphic-path" src="../expertos/images/Path-Yellow.svg" width="570"><img class="mobile-graphic-path" src="../expertos/images/Path-Yellow-Mobile.svg">
        </div>
        <div class="stats-wrapper">
          <div class="stat-block top">
            <div class="info-price ip-18px ip-light"><span class="dollar-s">$</span>150,250.50</div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Saldo</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="stat-block">
            <div class="info-price ip-18px ip-light">21,54&nbsp;<span class="dollar-s">%</span>
            </div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Rendimiento</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="stat-block">
            <div class="info-price ip-18px ip-light"><span class="dollar-s">$</span>27,205.05</div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Plusvalía/Minusvalía</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
          <div class="stat-block">
            <div class="info-price ip-18px ip-light">4/Dic/2018</div>
            <div class="popup-wrapper">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
            </div>
            <div class="info-question-wrapper iq-mobile">
              <div class="info-text it-light it-small">Plazo Sugerido</div><img class="question-icon" data-ix="popup-appear" height="16" src="../expertos/images/Question-Icon.svg" width="16">
              <div class="popup-wrapper">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="cw-2">
        <div class="composition-wrapper cw-2 cw-light">
          <div class="estrrr table-block tb-integrate">
            <div class="table-heading">
              <h3 class="table-heading th-dark">Composición de Estrategia EA200</h3>
              <div class="table-date td-light">19 dec, 2016</div>
            </div>
            <div class="table-underline tu-dark"></div>
            <div class="table">
              <div class="head-string table-string">
                <div class="table-item ti-second ti2-light">
                  <div>Fondo</div>
                </div>
                <div class="table-item ti-third tt-light">
                  <div>Operadora</div>
                </div>
                <div class="table-item ti-dark">
                  <div>Clasificatión</div>
                </div>
              </div>
              <div class="table-underline tu-dark"></div>
              <div class="table-string">
                <div class="table-item ti-second ti2-light">
                  <div>GMBCREB</div>
                </div>
                <div class="table-item ti-third tt-light">
                  <div>Operadora GBM</div>
                </div>
                <div class="table-item ti-dark">
                  <div>Acciones Descrecional</div>
                </div>
              </div>
              <div class="table-string">
                <div class="table-item ti-second ti2-light">
                  <div>GMBCREB</div>
                </div>
                <div class="table-item ti-third tt-light">
                  <div>Operadora GBM</div>
                </div>
                <div class="table-item ti-dark">
                  <div>Acciones México</div>
                </div>
              </div>
              <div class="table-string">
                <div class="table-item ti-second ti2-light">
                  <div>GMBCREB</div>
                </div>
                <div class="table-item ti-third tt-light">
                  <div>Operadora GBM</div>
                </div>
                <div class="table-item ti-dark">
                  <div>Acciones Internacionales</div>
                </div>
              </div>
              <div class="table-string">
                <div class="table-item ti-second ti2-light">
                  <div>GMBCREB</div>
                </div>
                <div class="table-item ti-third tt-light">
                  <div>Operadora GBM</div>
                </div>
                <div class="table-item ti-dark">
                  <div>Acciones Estados Unidos</div>
                </div>
              </div>
            </div>
          </div>
          <div class="mobile mobile-table-light table-block">
            <div>
              <h3 class="table-heading th-dark">Composición de Estrategia EA200</h3>
            </div>
            <div class="table-underline tu-dark"></div>
            <div class="mobile-table">
              <div class="mobile-table-string mt-dark mt-heading-string">
                <div>19 dec, 2016</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">%:</div>
                <div class="mt-info mti-dark">$21,105</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Fondo:</div>
                <div class="mt-info mti-dark">$35,052,025</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Operadora:</div>
                <div class="mt-info mti-dark">Operadora GBM</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Rendimiento:</div>
                <div class="mt-info mti-dark">20.55%</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Pluslavía:</div>
                <div class="mt-info mti-dark">$25,250</div>
              </div>
            </div>
            <div class="table-underline tu-dark"></div>
            <div class="mobile-table">
              <div class="mobile-table-string mt-dark mt-heading-string">
                <div>20 dec, 2016</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">%:</div>
                <div class="mt-info mti-dark">$21,105</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Fondo:</div>
                <div class="mt-info mti-dark">$35,052,025</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Operadora:</div>
                <div class="mt-info mti-dark">Operadora GBM</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Rendimiento:</div>
                <div class="mt-info mti-dark">20.55%</div>
              </div>
              <div class="mobile-table-string">
                <div class="mt-label mtl-dark">Pluslavía:</div>
                <div class="mt-info mti-dark">$25,250</div>
              </div>
            </div>
            <div class="table-underline tu-dark"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="f-light footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="../expertos/images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="fl-light footer-link" href="../index.aspx">Home</a><a class="fl-light footer-link" href="#">Portafolio</a><a class="fl-light footer-link" href="#">Movimentos</a><a class="fl-light footer-link" href="#">Comisiones</a><a class="fl-light footer-link" href="../faq.aspx">Herramientas</a><a class="fl-light footer-link" data-ix="open">Login</a>
      </div>
      <div class="footer-column">
        <p class="fa-light footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>Mexico CDMX 11000
          <a href="#" class="footer-link fl-light">experto@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="#"><img height="28" src="../expertos/images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="#"><img height="24" src="../expertos/images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="../expertos/images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="25" src="../expertos/images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="../expertos/js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>