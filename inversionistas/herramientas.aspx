﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="herramientas.aspx.vb" Inherits="inversionistas_herramientas" %>

<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com -->
<!--  Last Published: Wed Apr 05 2017 22:14:20 GMT+0000 (UTC)  -->
<html data-wf-page="58dcdf9ed33f36ec64a44065" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Herramientass</title>
  <meta content="Herramientass" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="../expertos/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="../expertos/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="../expertos/css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="../expertos/js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-form-wrapper">
    <div class="close" data-ix="close"></div>
    <div class="login-wrapper">
      <div class="login-photo"></div>
      <div class="form w-tabs">
        <div class="login-back w-tab-menu">
          <a class="register-round w-inline-block w-tab-link" data-w-tab="Tab 2">
            <div class="login-text">Regístrate</div>
          </a>
          <a class="login-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
            <div class="login-text">Inicia Sesión</div>
          </a>
        </div>
        <div class="w-tab-content">
          <div class="w-tab-pane" data-w-tab="Tab 2">
            <div class="login-back-bottom register">
              <p class="brown-center-14px">Crea tu cuenta en el portal de Expertos y comienza a registrar tus estrategias</p><a class="button reg w-button" href="#">Regístrate Gratis</a>
            </div>
          </div>
          <div class="w--tab-active w-tab-pane" data-w-tab="Tab 1">
            <div class="login-back-bottom">
              <div class="w-form">
                <form class="login-form" data-name="Email Form" id="email-form" name="email-form">
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="../expertos/images/Envelope---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Correo Electr Nico 2" id="correo-electr-nico-2" maxlength="256" name="correo-electr-nico-2" placeholder="correo electrónico" required="required" type="email">
                  </div>
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="../expertos/images/lock---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Contrase A 2" id="contrase-a-2" maxlength="256" name="contrase-a-2" placeholder="contraseña" type="password">
                  </div>
                  <div class="checkbox-field w-checkbox w-clearfix">
                    <input class="checkbox w-checkbox-input" data-name="Checkbox 5" id="checkbox-5" name="checkbox-5" type="checkbox">
                    <label class="small-right-text w-form-label" for="checkbox-5">Recuérdame</label>
                  </div>
                  <input class="button login w-button" data-wait="Please wait..." type="submit" value="Ingresar">
                </form>
                <div class="w-form-done">
                  <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                  <div>Oops! Something went wrong while submitting the form</div>
                </div>
              </div><a class="small-center-text" href="#">¿Olvidaste tu contraseña?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="light-section">
    <div class="nav-bar nb-light w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="../index.aspx"><img src="../expertos/images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link nl-light w-nav-link" href="#">Buscar Expertos</a><a class="nav-link nl-light w-nav-link" href="portafolio.aspx">Portafolio</a><a class="nav-link nl-light w-nav-link" href="movimientos.aspx">Movimientos</a><a class="nav-link nl-light w-nav-link" href="comisiones.aspx">Comisiones</a><a class="nav-link nl-light w-nav-link" href="herramientas.aspx">Herramientas</a><a class="mbb-light menu-bar-button w-button" href="recomienda.aspx">Recomienda y Gana</a><a class="nav-link nl-hide nl-light w-nav-link" href="#">Сerrar sesión</a>
        </nav>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="estrategies-top et-light">
      <div class="heading-wrapper">
        <div>Hola, Diego!</div>
        <h2 class="heading-white-28px hw-light left-align">Herramientas</h2>
      </div>
      <div class="div-block-3"><a class="dark-link" href="#">Cerrar Sesión</a>
      </div>
    </div>
    <div class="center mobile mobile-table-light table-block">
      <div class="table-underline tu-dark"></div>
      <div class="mobile-table">
        <div class="mobile-table-string mt-dark mt-heading-string">
          <div class="table-dark-text">15/Nov/2016</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Estatus:</div>
          <div class="mt-info mti-yellow">En Proceso</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Tipo de Movimento:</div>
          <div class="mt-info mti-dark">Compra</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Importe:</div>
          <div class="mt-info mti-dark">4,695.32 MX$</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Comentarios de la Operación:</div>
          <div class="mt-info mti-dark">Compra de Estrategia EA120</div>
        </div>
      </div>
      <div class="table-underline tu-dark"></div>
      <div class="mobile-table">
        <div class="mobile-table-string mt-dark mt-heading-string">
          <div class="table-dark-text">15/Nov/2016</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Estatus:</div>
          <div class="mt-info mti-green">Procesada</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Tipo de Movimento:</div>
          <div class="mt-info mti-dark">Compra</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Importe:</div>
          <div class="mt-info mti-dark">4,695.32 MX$</div>
        </div>
        <div class="mobile-table-string">
          <div class="mt-label mtl-dark">Comentarios de la Operación:</div>
          <div class="mt-info mti-dark">Compra de Estrategia EA120</div>
        </div>
      </div>
      <div class="table-underline tu-dark"></div>
    </div>
    <div class="lf-wrapper">
      <div class="light white-small-wrapper">
        <h3 class="heading-right-24px">Actualizar tus Datos de Contacto</h3>
        <div class="brown-bold-12px place">Correo Electrónico:</div>
        <div class="input-block no-margin" data-ix="icons">
          <input class="estrat text-field w-input" data-name="Correo Electrónico" id="Correo-Electr-nico-3" maxlength="256" name="Correo-Electr-nico" required="required" type="email">
        </div>
        <div class="brown-bold-12px place">Teléfono de Contacto:</div>
        <div class="dob input-block no-margin" data-ix="icons">
          <input class="estrat text-field w-input" data-name="Teléfono de Contacto" id="Tel-fono-de-Contacto" maxlength="256" name="Tel-fono-de-Contacto" required="required" type="text">
        </div><a class="sb-light small-button w-button" href="#">Actualizar</a>
      </div>
      <div class="light white-small-wrapper">
        <h3 class="heading-right-24px">Cambiar su Contraseña</h3>
        <div class="brown-bold-12px place">Contraseña Anterior:</div>
        <div class="input-block no-margin" data-ix="icons">
          <input class="estrat text-field w-input" data-name="Contraseña Anterior:" id="Contrase-a-Anterior" maxlength="256" name="Contrase-a-Anterior" required="required" type="password">
        </div>
        <div class="brown-bold-12px place">Contraseña Nueva:</div>
        <div class="dob input-block no-margin" data-ix="icons">
          <input class="estrat text-field w-input" data-name="Tel Fono De Contacto 2" id="Tel-fono-de-Contacto-2" maxlength="256" name="Tel-fono-de-Contacto-2" required="required" type="password">
        </div>
        <div class="brown-bold-12px place">Confirme la Nueva Contraseña:</div>
        <div class="input-block no-margin" data-ix="icons">
          <input class="estrat text-field w-input" id="node-2" maxlength="256" required="required" type="password">
        </div><a class="sb-light small-button w-button" href="#">Actualizar</a>
      </div>
    </div>
  </div>
  <div class="f-light footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="../expertos/images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="fl-light footer-link" href="../index.aspx">Home</a><a class="fl-light footer-link" href="#">Portafolio</a><a class="fl-light footer-link" href="#">Movimentos</a><a class="fl-light footer-link" href="#">Comisiones</a><a class="fl-light footer-link" href="../faq.aspx">Herramientas</a><a class="fl-light footer-link" data-ix="open">Login</a>
      </div>
      <div class="footer-column">
        <p class="fa-light footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>Mexico CDMX 11000
          <a href="#" class="footer-link fl-light">experto@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="#"><img height="28" src="../expertos/images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="#"><img height="24" src="../expertos/images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="../expertos/images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="25" src="../expertos/images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="../expertos/js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>