﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="detalle.aspx.vb" Inherits="inversionistas_detalle" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Mi Portafolio</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/detalle.css">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script type="text/javascript" src="../fusioncharts/fusioncharts.js"></script>     
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar first"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar last"></span>
                </button>
                <a class="navbar-brand" href="portafolio.aspx"><img src="../img/logo.svg" alt=""></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos-n.aspx">Buscar Expertos</a></li>
                    <li style="margin-right: 18px;"><a href="#">FAQs</a></li>
                    <li><a class="btn expert-reg-btn" href="#" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                    <li><a class="btn reg-btn" href="#">Regístrate como Experto</a></li>
                    <li><a href="#">Mi Cuenta</a></li>
                    <li><a href="#">Recomienda y Gana</a></li>
                </ul>
            </div>
        </div>
    </nav>
<form id="form1" runat="server">
<section class="search_wrap">
    <div class="search">
        <div id="searchForm">
            <input type="text" class="search_input" placeholder="Buscar Experto / Estrategia">
            <span class="search_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
            <input type="submit" style="display: none">
        </div>
    </div>
    <div class="container detalle_sticky">
        <h3 class="detalle_sticky_title">Estrategias Detalle</h3>
        <%--<button class="btn detalle_sticky_btn" id="stickyBtnInvertir">Invertir</button>--%>
        <asp:Button ID="stickyBtnInvertir" runat="server" Text="Agregar al Portafolio" CssClass="btn detalle_sticky_btn" />
        <%--<button class="btn detalle_sticky_btn" id="stickyBtnRegresar">Regresar</button>--%>
        <asp:Button ID="stickyBtnRegresar" runat="server" Text="Regresar" CssClass="btn detalle_sticky_btn" />
    </div>
</section>

<div class="detalle_modal_overlay" id="detalleModal" role="dialog">
    <div class="detalle_modal_body">
        <h3 class="detalle_modal_title">Invertir</h3>
        <p>Estrategia: <span id="estrategiaName1"><%=Mid(clave, 12) %></span><br>
            Estrategia: <span id="estrategiaName2"><%=expertos.alias_experto(idexperto) %></span></p>
        <p>La estrategia se encuenta en tu Portfolio</p>
        <button class="btn detalle_modal_btn" id="irAPortafolio">Ir a mi Portafolio</button>
        <button class="btn detalle_modal_btn" id="regresarAStrategia">Regresar a la Strategia</button>
    </div>
</div>

<section class="container detalle_cont">
    <%--<button class="btn detalle_estrat_btn" id="modalBtn">Agregar al Portafolio</button>--%>
    <div style="margin:50px auto 0; padding: 10px 35px;" ></div>
    <div class="detalle_estrat">
        <div class="detalle_estrat_head">
            <p class="detalle_estrat_head_title">
                <span>Estrategia</span> <%=Mid(clave, 12) %>
            </p>
            <p class="detalle_estrat_head_title">
                <span>Experto</span> <%=expertos.alias_experto(idexperto) %>
            </p>
            <span class="detalle_estrat_head_date">Alta <%=g.ConvierteFecha2(fecha_alta) %></span>
        </div>
        <div class="detalle_estrat_block estrat_block_rendiment">
            <p class="detalle_estrat_term">Rendimiento</p>
            <p class="detalle_estrat_percent"><%=FormatNumber(rend_acum, 2)  %>%</p>
            <p class="detalle_estrat_term">Desde el <%=g.ConvierteFecha2(fecha_alta) %></p>
        </div>
        <hr class="estrat_mobile_divider">
        <div class="detalle_estrat_block estrat_block_comision">
            <span class="detalle_estrat_subtitle">Comisión</span>
            <div class="detalle_estrat_value_wrap estrat_comision_wrap">
                <span class="detalle_estrat_big-value detalle_estrat_comision_value"><%=FormatNumber(comision, 2) %>%</span>
                <span class="detalle_estrat_value_sm-text">(anual)</span>
            </div>
            <span class="detalle_estrat_subtitle">Mínimo</span>
            <div class="detalle_estrat_value_wrap estrat_minimo_wrap">
                <span class="detalle_estrat_big-value detalle_estrat_minimo_value"><%=FormatNumber(25000, 0) %></span>
                <span class="detalle_estrat_value_sm-text">pesos</span>
            </div>
        </div>
        <div class="detalle_estrat_block estrat_block_objetivo">
            <span class="detalle_estrat_subtitle">Objetivo de Inversión</span>
            <span class="detalle_estrat_objetivo_value" id="nObjetivo" runat="server"><%=perfil %></span>
            <p class="detalle_estrat_term">Objetivo</p>
            <div>
                <span class="detalle_estrat_subtitle">Liquidez:</span>
                <span class="detalle_estrat_liquidez_value detalle_estrat_big-value">72hrs</span>
            </div>
        </div>
        <div class="detalle_estrat_block estrat_block_diver">
            <span class="detalle_estrat_subtitle">Diversificación</span>
            <p class="detalle_estrat_big-value detalle_estrat_fondos_value"><%=n_fondos %> <span>Fondos</span></p>
            <p class="detalle_estrat_big-value detalle_estrat_operadoras_value"><%=n_oper  %> <span>Operadoras</span></p>
        </div>
    </div>
    <div class="detalle_block desempeno">
        <p class="detalle_block_title">
            Desempeño de la Estrategia
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <%grafica_estrategia()%>
        <div>
            <asp:Literal ID="myGraphArea" runat="server"></asp:Literal></div>
    </div>
    <div class="detalle_block rendimentos">
        <p class="detalle_block_title">
            Rendimientos al Día
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <div class="detalle_block_table_wrap">
            <table class="detalle_block_table">
                <tr>
                    <th>Al <%=g.funFechaNombre(fechaact) %></th>
                    <th>Estrategia</th>
                    <th>Cetes</th>
                </tr>
                <%rendimientos_dia() %>
            </table>
        </div>
    </div>
    <div class="detalle_block composicion">
        <p class="detalle_block_title">
            Composicion de la Estrategia
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <%grafica_composicion() %>
        <div><div class="detalle_block_subtitle" style="width:100px; height:10px; background-color:#2b3b4e;">&nbsp;</div>Renta Variable: <%=p_rv %>%</div><br />
        <div><div class="detalle_block_subtitle" style="width:100px; height:10px; background-color:#c7c7c7;">&nbsp;</div>Deuda: <%=p_deuda %>%</div>
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    </div>
    <div class="detalle_block horizonte">
        <p class="detalle_block_title">
            Horizonte de Inversíon y Liquidez
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <div class="horizonte_block">
            <p class="horizonte_block_text">Tu sugerimos
                invertir duranre :</p>
            <p class="detalle_block_big-value" id="horizonte_value"><%=horizonte %></p>
            <p class="horizonte_block_text">Horizonte</p>
        </div>
        <hr class="horizonte_divider">
        <div class="horizonte_block">
            <p class="horizonte_block_text">Si necesitas retirar tu dinero
                estará disponible en: </p>
            <p class="detalle_block_big-value" id="liquidez_value"><%=liquidex %></p>
            <p class="horizonte_block_text">Liquidez</p>
        </div>
    </div>
    <div class="detalle_block comision">
        <p class="detalle_block_title">
            Comisíon
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <p class="detalle_block_big-value" id="comission_value"><%=FormatNumber(comision, 2) %>%</p>
        <p class="detalle_block_text">La comisíon es annual y se cobra mensualmente sobre el saldo
            promedio, el
            cobro se realiza durante los primeros 5 hábiles del mes vercido.
            Ejemplo: si inviertes $100,000 pesos, pagarías cada mes $104.16
        </p>
    </div>
    <div class="detalle_block objetivo">
        <div class="objetivo_block objetivo_left-block">
            <p class="detalle_block_title">
                Objetivo de Inversíon
                <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
            </p>
            <ul class="detalle_block_list objetivo_list">
                <%objetivos_inversion() %>
            </ul>
        </div>
        <hr class="objetivo_divider">
        <div class="objetivo_block objetivo_descr">
            <p class="objetivo_descr_title"></p>
            <p class="objetivo_descr_text"></p>
            <div class="objetivo_descr_measure">
                <div class="objetivo_descr_measure_fill">
                    <div class="objetivo_descr_measure_popup">
                        <div class="objetivo_descr_measure_popup_cont">
                            <span class="objetivo_descr_measure_popup_num">1</span>
                            <span class="objetivo_descr_measure_popup_text"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="detalle_block clasific">
        <p class="detalle_block_title">
            Clasificaciones de Fondos
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <ul class="detalle_block_list">
            <%clasificaciones() %>
        </ul>
    </div>
    <div class="detalle_block operadoras">
        <p class="detalle_block_title">
            Operadoras que Participan
            <span class="detalle_block_title_info" data-info="Lorem ipsum dolor sit amet, consectetur adipiscing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
            </span>
        </p>
        <ul class="detalle_block_list">
            <%operadoras() %>
        </ul>
    </div>
</section>
</form>

<hr class="main_divider">
<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="../img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="../img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>

<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="../img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>

                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="../img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="../js/general_bundle.js"></script>
<script src="../js/detalle.js"></script>
<script>
    var objetivoSel=<%=perfil%>;
    var itemTag;
    if (objetivoSel==1){ itemTag="#conservador";}
    if (objetivoSel==2){ itemTag="#moderado";}
    if (objetivoSel==3){ itemTag="#emprendedor";}
    if (objetivoSel==4){ itemTag="#agresivo";}
    if (objetivoSel==5){ itemTag="#especulativo";}
    setActiveItem($(itemTag));
</script>
</body>
</html>