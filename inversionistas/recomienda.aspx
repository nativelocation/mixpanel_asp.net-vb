﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="recomienda.aspx.vb" Inherits="inversionistas_recomienda" %>

<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com -->
<!--  Last Published: Wed Apr 05 2017 22:14:20 GMT+0000 (UTC)  -->
<html data-wf-page="58dce5d589a66e1d7479b8a3" data-wf-site="5898439cf29bebaa63a0fb15">
<head>
  <meta charset="utf-8">
  <title>Recomienda</title>
  <meta content="Recomienda" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="../expertos/css/normalize.css" rel="stylesheet" type="text/css">
  <link href="../expertos/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="../expertos/css/invierte.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">
      WebFont.load({
          google: {
              families: ["Raleway:100,200,300,regular,500,600,700,800,900"]
          }
      });
  </script>
  <script src="../expertos/js/modernizr.js" type="text/javascript"></script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>
  <div class="login-form-wrapper">
    <div class="close" data-ix="close"></div>
    <div class="login-wrapper">
      <div class="login-photo"></div>
      <div class="form w-tabs">
        <div class="login-back w-tab-menu">
          <a class="register-round w-inline-block w-tab-link" data-w-tab="Tab 2">
            <div class="login-text">Regístrate</div>
          </a>
          <a class="login-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
            <div class="login-text">Inicia Sesión</div>
          </a>
        </div>
        <div class="w-tab-content">
          <div class="w-tab-pane" data-w-tab="Tab 2">
            <div class="login-back-bottom register">
              <p class="brown-center-14px">Crea tu cuenta en el portal de Expertos y comienza a registrar tus estrategias</p><a class="button reg w-button" href="#">Regístrate Gratis</a>
            </div>
          </div>
          <div class="w--tab-active w-tab-pane" data-w-tab="Tab 1">
            <div class="login-back-bottom">
              <div class="w-form">
                <form class="login-form" data-name="Email Form" id="email-form" name="email-form">
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="../expertos/images/Envelope---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Correo Electr Nico 2" id="correo-electr-nico-2" maxlength="256" name="correo-electr-nico-2" placeholder="correo electrónico" required="required" type="email">
                  </div>
                  <div class="input-block" data-ix="icons"><img class="login-icon" height="20" src="../expertos/images/lock---simple-line-icons.svg" width="20">
                    <input class="text-field w-input" data-name="Contrase A 2" id="contrase-a-2" maxlength="256" name="contrase-a-2" placeholder="contraseña" type="password">
                  </div>
                  <div class="checkbox-field w-checkbox w-clearfix">
                    <input class="checkbox w-checkbox-input" data-name="Checkbox 5" id="checkbox-5" name="checkbox-5" type="checkbox">
                    <label class="small-right-text w-form-label" for="checkbox-5">Recuérdame</label>
                  </div>
                  <input class="button login w-button" data-wait="Please wait..." type="submit" value="Ingresar">
                </form>
                <div class="w-form-done">
                  <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                  <div>Oops! Something went wrong while submitting the form</div>
                </div>
              </div><a class="small-center-text" href="#">¿Olvidaste tu contraseña?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="light-section">
    <div class="nav-bar nb-light w-nav" data-animation="default" data-collapse="medium" data-duration="400">
      <div class="menu-container w-container">
        <a class="menu-logo w-nav-brand" href="../index.aspx"><img src="../expertos/images/Logo.svg">
        </a>
        <nav class="mobile-menu w-nav-menu" role="navigation"><a class="nav-link nl-light w-nav-link" href="#">Buscar Expertos</a><a class="nav-link nl-light w-nav-link" href="portafolio.aspx">Portafolio</a><a class="nav-link nl-light w-nav-link" href="movimientos.aspx">Movimientos</a><a class="nav-link nl-light w-nav-link" href="comisiones.aspx">Comisiones</a><a class="nav-link nl-light w-nav-link" href="herramientas.aspx">Herramientas</a><a class="mbb-light menu-bar-button w-button" href="recomienda.aspx">Recomienda y Gana</a><a class="nav-link nl-hide nl-light w-nav-link" href="#">Сerrar sesión</a>
        </nav>
        <div class="menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="estrategies-top et-light">
      <div class="heading-wrapper">
        <div>Hola, Diego!</div>
        <h2 class="heading-white-28px hw-light left-align">Recomienda y Gana</h2>
      </div>
      <div class="div-block-3"><a class="dark-link" href="#">Cerrar Sesión</a>
      </div>
    </div>
    <div class="tabs w-tabs" data-duration-in="300" data-duration-out="100">
      <div class="tabs-menu w-tab-menu">
        <a class="tab-link w--current w-inline-block w-tab-link" data-w-tab="Tab 1">
          <div>¿Cómo funciona?</div>
        </a>
        <a class="tab-link-2 w-inline-block w-tab-link" data-w-tab="Tab 2">
          <div>Recomienda</div>
        </a>
        <a class="tab-link-3 w-inline-block w-tab-link" data-w-tab="Tab 3">
          <div>Consulta</div>
        </a>
      </div>
      <div class="w-tab-content">
        <div class="w--tab-active w-tab-pane" data-w-tab="Tab 1">
          <div class="light-tab white-small-wrapper">
            <h3 class="heading-3"><strong>Sencillo.</strong> En tres simples pasos</h3>
            <div class="ibw-l info-blocks-wrapper">
              <div class="ib-light ibl2 info-block"><img class="image-2" height="80" src="../expertos/images/login.svg" width="80">
                <div class="info-price ip-light"><span class="dollar"></span>Ingresa</div>
                <div class="info-question-wrapper">
                  <div class="info-text it-light itl-c">Inicia sesión con tu cuenta de y haz clic en «Recomienda y Gana»</div>
                  <div class="popup-wrapper">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
                  </div>
                </div>
              </div>
              <div class="ib-light ibl2 info-block"><img class="image-2" height="80" src="../expertos/images/speech-bubble.svg" width="80">
                <div class="info-price ip-light"><span class="dollar"></span>Recomienda</div>
                <div class="info-question-wrapper">
                  <div class="info-text it-light itl-c">Selecciona a tus contractos desde Gmail, Yahoo! O escribe los correos de tus amigos
                    <br>
                    <br>Tus amigos reciben un código que deben proporcionar cuando se registren</div>
                  <div class="popup-wrapper">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
                  </div>
                </div>
              </div>
              <div class="ib-light ibl2 info-block"><img class="image-2" height="80" src="../expertos/images/piggy-bank.svg" width="80">
                <div class="info-price ip-light"><span class="dollar"></span><span class="dollar"></span>Gana dinero</div>
                <div class="info-question-wrapper">
                  <div class="info-text it-light itl-c">Recibe automáticamente $400 pesos por casa referido que invierta</div>
                  <div class="popup-wrapper">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="line"></div>
            <h3 class="heading-3"><strong>Beneficios.</strong> Todos ganan</h3>
            <div class="ibw-l info-blocks-wrapper">
              <div class="_30 ib-light info-block"><img class="image-2 side" height="120" src="../expertos/images/worker.svg" width="120">
                <div class="info-price ip-light"><span class="dollar"></span>Tu ganas</div>
                <div class="info-question-wrapper">
                  <div class="info-text it-light itl-c">Recibes $250 pesos por cada amigo que invierta</div>
                  <div class="popup-wrapper">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
                  </div>
                </div>
              </div>
              <div class="_50 ib-light info-block"><img class="image-2" height="120" src="../expertos/images/high-five.svg" width="120">
                <div class="info-price ip-light"><span class="dollar"></span>Tus amigos</div>
                <div class="info-question-wrapper">
                  <div class="info-text it-light itl-c">Consique el primer ano gratis de servicio Solo por recomendarles a Invierte con Expertos, no tendrán que pagar al experto(s) que contraten durante los primeros 30 días.</div>
                  <div class="popup-wrapper">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse varius enim in eros elementum tristique.&nbsp;</p>
                  </div>
                </div><a class="sb-light small-button w-button" href="#">Empieza a recomendar</a>
              </div>
            </div>
          </div>
        </div>
        <div class="w-tab-pane" data-w-tab="Tab 2">
          <div class="light-tab white-small-wrapper">
            <h4 class="heading-left-brown-14px">Recomienda por Correo Electrónico</h4>
            <p class="br-margin brown-right-14px">Envia por correo electrónico a todos tus amigos tu código promocional, para que tu y ellos ganen
              <br>¿Qué correo utilizas?</p>
            <div class="refers-wrapper">
              <div class="gmail refer"><img height="17" src="../expertos/images/Group.svg">
                <div class="refer-t">Gmail</div>
              </div>
              <div class="refer yahoo"><img height="17" src="../expertos/images/Fill-2.svg">
                <div class="refer-t">Yahoo</div>
              </div>
              <div class="otro refer"><img height="17" src="../expertos/images/mail-1.svg">
                <div class="refer-t">Otro</div>
              </div>
            </div>
            <div class="line"></div>
            <h4 class="heading-left-brown-14px">Recomienda por redes sociales</h4>
            <p class="br-margin brown-right-14px">Publica en tus redes sociales información de Prosuma junto con tu código promocional para que lo utilicen tus amigos</p>
            <div class="refers-wrapper">
              <div class="facebook refer"><img height="17" src="../expertos/images/facebook-1.svg">
                <div class="refer-t">Facebook</div>
              </div>
              <div class="refer twitter"><img height="17" src="../expertos/images/twitter-1.svg">
                <div class="refer-t">Twitter</div>
              </div>
            </div>
            <div class="line"></div><a class="long sb-light small-button w-button" href="#">Consulta el Estatus de tus Referidos</a>
          </div>
        </div>
        <div class="w-tab-pane" data-w-tab="Tab 3">
          <div class="light-tab white-small-wrapper">
            <h4 class="heading-left-brown-14px">Consulta el estatus de tus amigos</h4>
            <p class="br-margin brown-right-14px">Te mostramos el estatus que tiene cada uno de tus amigos en Prosuma para que conozcas si ya se refistraron (Registrado), ya invirtieron (Cerrado), ya no quieren invertir (Cancelado) o no participa (No Califica) ya que Presuma ya contaba con sus datos antes de que tu nos los dieras</p>
            <div class="kkoa mobile recom table-block tb-light-tab-mobile">
              <div class="mobile-table">
                <div class="mobile-table-string mt-heading-string">
                  <div><a href="#" class="table-big-link">jcarlo@hotmail.com</a>
                  </div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Envío:</div>
                  <div class="mt-info">2/Feb/17</div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Estatus:</div>
                  <div class="mt-info">Enviado (Recordar)</div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Beneficio:</div>
                  <div class="mt-info">$0</div>
                </div>
              </div>
              <div class="table-underline"></div>
              <div class="mobile-table">
                <div class="mobile-table-string mt-heading-string">
                  <div><a href="#" class="table-big-link">jcarlo@hotmail.com</a>
                  </div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Envío:</div>
                  <div class="mt-info">2/Feb/17</div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Estatus:</div>
                  <div class="mt-info">Registrado</div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Beneficio:</div>
                  <div class="mt-info">No Aplica</div>
                </div>
              </div>
              <div class="table-underline"></div>
              <div class="mobile-table">
                <div class="mobile-table-string mt-heading-string">
                  <div><a href="#" class="table-big-link">pqrodriguez@telmex.com</a>
                  </div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Envío:</div>
                  <div class="mt-info">5/Nov/16</div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Estatus:</div>
                  <div class="mt-info">No Califica</div>
                </div>
                <div class="mobile-table-string">
                  <div class="mt-label">Beneficio:</div>
                  <div class="mt-info">$0</div>
                </div>
              </div>
            </div>
            <div class="kkoa table-block tb-light-tab">
              <div class="table-underline"></div>
              <div class="table">
                <div class="head-string table-string">
                  <div class="first-one table-item">
                    <div>Correo Electrónico</div>
                  </div>
                  <div class="second-one table-item">
                    <div>Envío</div>
                  </div>
                  <div class="table-item tti-third">
                    <div>Estatus</div>
                  </div>
                  <div class="table-item tti-fourth">
                    <div>Beneficio</div>
                  </div>
                </div>
                <div class="table-underline"></div>
                <div class="table-string ts-first">
                  <div class="first-one table-item">
                    <div><a href="../expertos/comisions-2.aspx" class="table-link">jcarlo@hotmail.com</a>
                    </div>
                  </div>
                  <div class="second-one table-item">
                    <div>$21,105</div>
                  </div>
                  <div class="table-item tti-third">
                    <div>Enviado (Recordar)</div>
                  </div>
                  <div class="_222 table-item ti-fourth">
                    <div>No Aplica</div>
                  </div>
                </div>
                <div class="table-string ts-first">
                  <div class="first-one table-item">
                    <div><a href="../expertos/comisions-2.aspx" class="table-link">pqrodriguez@telmex.com</a>
                    </div>
                  </div>
                  <div class="second-one table-item">
                    <div>$21,105</div>
                  </div>
                  <div class="table-item tti-third">
                    <div>Registrado</div>
                  </div>
                  <div class="_222 table-item ti-fourth">
                    <div>$0</div>
                  </div>
                </div>
                <div class="table-string ts-first">
                  <div class="first-one table-item">
                    <div><a href="../expertos/comisions-2.aspx" class="table-link">pqrodriguez@telmex.com</a>
                    </div>
                  </div>
                  <div class="second-one table-item">
                    <div>$21,105</div>
                  </div>
                  <div class="table-item tti-third">
                    <div>No Califica</div>
                  </div>
                  <div class="_222 table-item ti-fourth">
                    <div>No Aplica</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="f-light footer">
    <div class="footer-columns-wrapper">
      <div class="footer-column"><img src="../expertos/images/Logo.svg"><a class="footer-gray-link" href="#">Términos y Condiciones</a><a class="footer-gray-link" href="#">Política de Privacidad</a>
      </div>
      <div class="footer-column"><a class="fl-light footer-link" href="../index.aspx">Home</a><a class="fl-light footer-link" href="#">Portafolio</a><a class="fl-light footer-link" href="#">Movimentos</a><a class="fl-light footer-link" href="#">Comisiones</a><a class="fl-light footer-link" href="../faq.aspx">Herramientas</a><a class="fl-light footer-link" data-ix="open">Login</a>
      </div>
      <div class="footer-column">
        <p class="fa-light footer-address">Montes Urales 749&nbsp;
          <br>Lomas De Chapultepec
          <br>Mexico CDMX 11000
          <a href="#" class="footer-link fl-light">experto@invierteconexpertos.mx</a>
        </p>
      </div>
      <div class="footer-column icons">
        <a class="social-icon w-inline-block" href="#"><img height="28" src="../expertos/images/facebook-logo.svg">
        </a>
        <a class="ex social-icon w-inline-block" href="#"><img height="24" src="../expertos/images/twitter-logo.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="34" src="../expertos/images/google-plus.svg">
        </a>
        <a class="social-icon w-inline-block" href="#"><img height="25" src="../expertos/images/youtube-logo.svg">
        </a>
      </div>
    </div>
    <div class="footer-privacidad">
      <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar&nbsp; Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</p>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="../expertos/js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>