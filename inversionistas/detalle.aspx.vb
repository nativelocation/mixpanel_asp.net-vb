﻿Imports FusionCharts.Charts

Partial Class inversionistas_detalle
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public g As New generales, expertos As New Ice_expertos
    Public idexperto, idestrategia, name_estrategia, fecha_alta, liquidex, horizonte As String, p_deuda, p_rv As Integer
    Public clave As String, monto, perfil, max_rv, n_fondos, n_oper, idpublicacion As Integer, comision, rendimiento, rend_acum As Double
    Public identificador, proviene, fechaact As String

    Private Sub detalle_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        Dim sSQL As String, existe As Integer
        cne.ConnectionString = expertos.conectdb : cne2.ConnectionString = expertos.conectdb
        idestrategia = Request.QueryString(g.base64_encode("Estrategia"))
        idexperto = Request.QueryString(g.base64_encode("Experto"))
        idestrategia = g.base64_decode(idestrategia)
        idexperto = g.base64_decode(idexperto)
        proviene = Request.QueryString(g.base64_encode("proviene"))
        sSQL = "select fechaact from actualizacion where id=4"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : fechaact = SQLe.ExecuteScalar : cne.Close()

        sSQL = "select count(*) from clientes where idcliente='" & idestrategia & "' and idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : existe = SQLe.ExecuteScalar : cne.Close()
        If existe = 0 Then : salida(proviene) : End If
        detalle_estrategia()
    End Sub

    Private Sub detalle_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (Session("invesionista_login") Is Nothing) Or (Session("invesionista_login") <> "autorized") Then
            If proviene = "buscarexp" Then
                Response.Redirect("../login.aspx?" & g.base64_encode("Estrategia") & "=" & g.base64_encode(idestrategia) & "&" & g.base64_encode("Experto") & "=" & g.base64_encode(idexperto) & "&" & g.base64_encode("proviene") & "=" & proviene & "&search=" & Request.QueryString("search") & "&execution=" & Request.QueryString("execution"))
            ElseIf proviene = "buscarn" Then
                Response.Redirect("../login.aspx?" & g.base64_encode("Estrategia") & "=" & g.base64_encode(idestrategia) & "&" & g.base64_encode("Experto") & "=" & g.base64_encode(idexperto) & "&" & g.base64_encode("proviene") & "=" & proviene)
            End If
        Else
            proviene = Request.QueryString(g.base64_encode("proviene"))
            If Request.UrlReferrer Is Nothing Then
                salida(proviene)
            End If
        End If
    End Sub

    Function salida(ByVal parametro As String)
        If parametro = "buscarn" Then
            Response.Redirect("buscar-expertos-n.aspx")
        ElseIf parametro = "buscarexp" Then
            Response.Redirect("buscar-expertos-ex.aspx?search=" & Request.QueryString("search") & "&execution=" & Request.QueryString("execution"))
        Else
            Response.Redirect("buscar-expertos-n.aspx")
        End If
    End Function

    Function detalle_estrategia()
        Dim sSQL, fechaact, sHTML As String

        sSQL = "select t1.idpublicacion,t1.idestrategia,t1.idexperto,t1.fecha," &
        "a.clave, a.monto, a.rendimiento, perfil2, a.max_rv, a.comision,isnull(a.comen_asesor,'N/D')liquidez,a.fecha as fecha_creada  " &
        "from (Select Rank() over(order by idestrategia)NroRank,row_number()over(order by idestrategia,fecha desc)NroRow, " &
        "pp.* from publicacion_paquete pp inner join publicacion_estrategias pe On pp.idpublicacion=pe.idpublicacion " &
        "where pp.estatus=2)t1 inner join asesorias a On t1.idestrategia=a.idasesoria " &
        "where t1.NroRank=t1.NroRow And t1.idestrategia='" & idestrategia & "' and t1.idexperto='" & idexperto & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            clave = rse("clave") : monto = rse("monto") : perfil = rse("perfil2") : fecha_alta = rse("fecha_creada")
            max_rv = rse("max_rv") : comision = rse("comision") : rendimiento = rse("rendimiento")
            idpublicacion = rse("idpublicacion") : liquidex = rse("liquidez")
        End While
        cne.Close()

        Dim color_objetivo As String
        Select Case perfil
            Case 1 : color_objetivo = "rgb(151, 205, 87)"
            Case 2 : color_objetivo = "rgb(245, 166, 35)"
            Case 3 : color_objetivo = "rgb(219, 40, 55)"
            Case 4 : color_objetivo = "rgb(115, 191, 247)"
            Case 5 : color_objetivo = "rgb(110, 96, 234)"
        End Select
        nObjetivo.Attributes.Add("style", "background-color:" & color_objetivo & ";")
        sSQL = "select objetivo,horizonte from objetivos where id='" & perfil & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            horizonte = rse("horizonte")
            identificador = LCase(rse("objetivo"))
        End While

        cne.Close()

        If horizonte = "1" Then
            horizonte = horizonte & " año"
        Else
            horizonte = horizonte & " años"
        End If

        sHTML = ""
        sSQL = "select fechaact from actualizacion where id=4"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : fechaact = SQLe.ExecuteScalar : cne.Close()

        'Calculando la composicion de fondos
        sSQL = "select count(distinct op.nombrecorto)fondos,count(distinct f.idoperadora)operadoras " &
        "from (publicacion_estrategias p inner join fondo f On p.idfondo=f.id)inner join operadora op " &
        "on f.idoperadora=op.id where p.idpublicacion='" & idpublicacion & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            n_fondos = rse("fondos")
            n_oper = rse("operadoras")
        End While
        cne.Close()

        'Calculando el rendimiento anualizado
        sSQL = "select rendimientoaacum from posicion_cliente where idcliente='" & idestrategia & "' and fecha='" & Format(CDate(fechaact), "yyyy-MM-dd") & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            rend_acum = rse("rendimientoaacum")
        End While
        cne.Close()
    End Function

    Function grafica_estrategia()
        '///////////////////////////////////////
        Dim xmlData2 As New StringBuilder(), fechatmp, sHTML, sSQL2 As String
        Dim maximo, minimo As Integer
        maximo = 105000
        minimo = 90000
        xmlData2.Append("<chart caption='' subcaption='' lineThickness='4' showValues='0'  showPercentValues='1' xAxisName='' yAxisName='' ")
        xmlData2.Append("basefontcolor='#2b3b4e' canvasBgAlpha='0' bgColor='403936' bgAlpha='0' borderAlpha='0' showBorder='0' showAlternateVGridColor='0' ")
        xmlData2.Append("DivlineThickness='1' canvasBorderAlpha='0' lineColor='E39109' toolTipColor='403936' xAxisNameFontSize='6' ")
        xmlData2.Append("formatNumberScale='0' anchorRadius='0' divLineAlpha='20' divLineColor='2b3b4e' divLineIsDashed='0' showAlternateHGridColor='0' numberPrefix='$' ")
        xmlData2.Append("numdivlines='10' numvdivlines='0' adjustDiv='0' yAxisValueDecimals='0' yAxisMaxValue='" & maximo & "' yAxisMinValue='" & minimo & "' decimals='3' forcedecimals='3' labelDisplay='ROTATE' slantLabels='1'> ")
        Dim hay_posicion As Integer
        sSQL2 = "select count(fecha)registros from posicion_cliente where idcliente='" & idestrategia & "'"
        SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
        cne2.Open() : hay_posicion = SQLe2.ExecuteScalar : cne2.Close()

        sSQL2 = "select fecha from posicion_cliente where idcliente='" & idestrategia & "' order by fecha"
        SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
        cne2.Open()
        rse2 = SQLe2.ExecuteReader
        While rse2.Read
            fechatmp = rse2("fecha")
            xmlData2.Append("<set label='" & fechatmp & "' value='" & posicion_un_dia(fechatmp, idestrategia) & "'/>")
        End While
        cne2.Close()
        xmlData2.Append("</chart>")
        ' Create the chart
        Dim draws As New Chart
        ' Setting chart id
        draws.SetChartParameter(Chart.ChartParameter.chartId, "Estrategia")
        ' Setting chart type to Column 3D chart
        draws.SetChartParameter(Chart.ChartParameter.chartType, "line")
        ' Setting chart width to 800px
        draws.SetChartParameter(Chart.ChartParameter.chartWidth, "100%")
        ' Setting chart height to 500px
        draws.SetChartParameter(Chart.ChartParameter.chartHeight, "400px")
        ' Setting chart background color 
        draws.SetChartParameter(Chart.ChartParameter.bgColor, "transparent")
        ' Setting chart data as XML String 
        draws.SetData(xmlData2.ToString)
        ' Render chart
        myGraphArea.Text = draws.Render()
        If hay_posicion > 0 Then
            sHTML &= myGraphArea.Text
        Else
            sHTML &= "<div style='font-size:smaller; color:gray; height:400px; padding-top:125px; text-align:center;'>Tu estrategia se publicará en 24 horas hábiles siguientes a su fecha de alta, a partir de este momento empezaremos a contabilizar las plusvalías/minusvalías que genere durante el tiempo.</div>"
        End If
        '///////////////////////////////////////
    End Function

    Function posicion_un_dia(ByVal fechap As String, ByVal idcliente As String) As Double
        Dim cny As New Data.SqlClient.SqlConnection
        Dim SQLy As Data.SqlClient.SqlCommand
        Dim rsy As Data.SqlClient.SqlDataReader
        Dim vSQLy, fechaptmp As String
        Dim total_fondos, importeefectivo, importe As Double
        cny.ConnectionString = expertos.conectdb
        'Verifica si es habil
        If habiles(fechap) = 0 Then
            fechaptmp = fechaCalculadaV(fechap, 1)
        Else
            fechaptmp = fechap
        End If
        'Se obtiene la posicion total en fondos
        vSQLy = "select isnull(sum(pos.importe),0)importe " &
        "from (select pos.idfondo,pos.idoperacion,sum(pos.titulos)titulos,(sum(pos.titulos)*pf.precio) as importe,pf.nombrefondo,pf.precio,op.nombrecorto " &
        "from (((select cyv.idfondo,cyv.idoperacion,case when cyv.tipoO=2 then (sum(cyv.importeNcod))*-1 else sum(cyv.importeNcod) end as importeNcod," &
        "case when cyv.tipoO=2 then (sum(cyv.tituloscod))*-1 else sum(cyv.tituloscod) end as titulos,cyv.tipoO " &
        "from cyv where idcliente='" & idcliente & "' and idfondo<>'1442' and estatus=4  and fechaLcod<='" & fechaptmp & "' " &
        "group by cyv.idfondo,cyv.idoperacion,cyv.tipoO)pos inner join posicion_fondo pf on pos.idoperacion=pf.idoperacion " &
        "and pf.idcliente='" & idcliente & "' and pos.idfondo=pf.idfondo) inner join fondo f on pos.idfondo=f.id) inner join operadora op on f.idoperadora=op.id " &
        "where pf.fecha='" & fechaptmp & "' group by pos.idfondo,pos.idoperacion,pf.nombrefondo,pf.precio,op.nombrecorto)pos"
        SQLy = New Data.SqlClient.SqlCommand(vSQLy, cny)
        cny.Open() : total_fondos = SQLy.ExecuteScalar : cny.Close()

        'Se indica el importe en efectivo de las operaciones liquidadas
        vSQLy = "select pos.idfondo,sum(importeNcod)importe,pf.nombrefondo " &
        "from (((select cyv.idfondo,cyv.idoperacion,case when cyv.tipoO=2 then (sum(cyv.importeNcod))*-1 else sum(cyv.importeNcod) end as importeNcod, " &
        "case when cyv.tipoO=2 then (sum(cyv.tituloscod))*-1 else sum(cyv.tituloscod) end as titulos,cyv.tipoO " &
        "from cyv where idcliente='" & idcliente & "' and idfondo='1442' and estatus=4 and fechaLcod<='" & fechaptmp & "' " &
        "group by cyv.idfondo,cyv.idoperacion,cyv.tipoO)pos inner join posicion_fondo pf on pos.idoperacion=pf.idoperacion " &
        "and pf.idcliente='" & idcliente & "' and pos.idfondo=pf.idfondo) inner join fondo f on pos.idfondo=f.id) inner join operadora op on f.idoperadora=op.id " &
        "where pf.fecha='" & fechaptmp & "' group by pos.idfondo,pf.nombrefondo"
        SQLy = New Data.SqlClient.SqlCommand(vSQLy, cny)
        cny.Open()
        rsy = SQLy.ExecuteReader
        While rsy.Read
            importeefectivo = CDbl(rsy("importe"))
        End While
        cny.Close()
        importe = importeefectivo + total_fondos
        posicion_un_dia = importe
    End Function

    Function habiles(ByVal fecha As String) As Integer
        Dim vSQLx As String, encontrado As Integer
        Dim cn As New Data.SqlClient.SqlConnection
        Dim SQL1 As Data.SqlClient.SqlCommand
        Dim rs As Data.SqlClient.SqlDataReader
        cn.ConnectionString = expertos.conectdb
        If Weekday(fecha) = 1 Or Weekday(fecha) = 7 Then
            habiles = 0
        Else
            vSQLx = "Select count(diafestivo) as EXPR from diasfestivos where diafestivo='" & fecha & "'"
            SQL1 = New Data.SqlClient.SqlCommand(vSQLx, cn)
            cn.Open()
            encontrado = SQL1.ExecuteScalar
            cn.Close()
            If encontrado = 1 Then
                habiles = 0
            Else
                habiles = 1
            End If
        End If
    End Function

    Function fechaCalculadaV(ByVal fechaI As String, ByVal dias As Integer) As String
        Dim fecha1, cadenafecha, fechaq As String, combinacionfecha, validacion, cuentadias, encontrado As Integer
        fecha1 = fechaI
        combinacionfecha = dias
        Dim cnx As New Data.SqlClient.SqlConnection
        Dim SQLx As Data.SqlClient.SqlCommand
        Dim vSQLx As String
        cnx.ConnectionString = expertos.conectdb
        validacion = 0
        Do While validacion = 0
            cuentadias = 1
            Do While cuentadias <= CInt(combinacionfecha)
                fecha1 = DateAdd("d", -1, fecha1)
                cadenafecha = Month(fecha1) & "/" & Day(fecha1) & "/" & Year(fecha1)
                fecha1 = CDate(cadenafecha)
                fechaq = Month(fecha1) & "/" & Day(fecha1) & "/" & Year(fecha1)
                vSQLx = "Select count(diafestivo) as EXPR from diasfestivos where diafestivo='" & fechaq & "'"
                SQLx = New Data.SqlClient.SqlCommand(vSQLx, cnx)
                cnx.Open() : encontrado = SQLx.ExecuteScalar : cnx.Close()
                If Weekday(fecha1) = 1 Or Weekday(fecha1) = 7 Or encontrado = 1 Then
                Else
                    cuentadias = cuentadias + 1
                End If
            Loop
            validacion = 1
        Loop
        fechaCalculadaV = fecha1
    End Function

    Function grafica_composicion()
        'Consulta con el maximo en RV
        Dim sSQL, sDeuda, sRentaV As String, total As Double

        sSQL = "select p.idfondo,p.porcentaje,isnull(f.max_rv,0)max_rv,((p.porcentaje*f.max_rv)/100)poderacion_rv " &
        "from publicacion_estrategias p inner join fondo f On p.idfondo=f.id " &
        "where p.idpublicacion=1"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            p_rv = p_rv + FormatNumber(rse("poderacion_rv"), 0)
        End While
        cne.Close()
        p_deuda = 100 - p_rv
        If p_rv < 10 Then : sRentaV = "0" & p_rv : Else : sRentaV = p_rv : End If
        If p_deuda < 10 Then : sDeuda = "0" & p_deuda : Else : sDeuda = p_deuda : End If

        Dim xmlData2 As New StringBuilder
        xmlData2.Append("<chart palettecolors='#c7c7c7,#2b3b4e' enableMultiSlicing='0' ")
        xmlData2.Append("bgcolor='#ffffff' bgAlpha='0' showborder='0' use3dlighting='0' showshadow='0' enablesmartlabels='0' startingangle='50' ")
        xmlData2.Append("showlabels='0' alignLegendWithCanvas='1' legendAllowDrag='0' showValues='0' ")
        xmlData2.Append("showpercentvalues='1' showlegend='0' legendBgAlpha='0' legendshadow='0' legendborderalpha='0' showtooltip='0' decimals='0' ")
        xmlData2.Append("usedataplotcolorforlabels='1'>")
        xmlData2.Append("<set label='Deuda' value='" & sDeuda & "' isSliced='1' />")
        xmlData2.Append("<set label='Renta Variable' value='" & sRentaV & "' />")
        xmlData2.Append("</chart>")

        ' Create the chart
        Dim draws As New Chart
        ' Setting chart id
        draws.SetChartParameter(Chart.ChartParameter.chartId, "composicion")
        ' Setting chart type to Column 3D chart
        draws.SetChartParameter(Chart.ChartParameter.chartType, "doughnut2d")
        ' Setting chart width to 800px
        draws.SetChartParameter(Chart.ChartParameter.chartWidth, "100%")
        ' Setting chart height to 500px
        draws.SetChartParameter(Chart.ChartParameter.chartHeight, "50%")
        ' Setting chart background color 
        draws.SetChartParameter(Chart.ChartParameter.bgColor, "transparent")
        ' Setting chart data as XML String 
        draws.SetData(xmlData2.ToString)
        ' Render chart
        Literal2.Text = draws.Render()
    End Function

    Function objetivos_inversion()
        Dim sSQL, sHTML, clase As String
        sSQL = "select id,objetivo,rv,horizonte,color from objetivos --where id='" & perfil & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        sHTML = ""
        While rse.Read
            If CInt(perfil) = CInt(rse("id")) Then
                clase = "objetivo_list_item objetivo_list_item-active"
                identificador = LCase(rse("objetivo"))
            Else
                clase = "objetivo_list_item"
                identificador = rse("id")
            End If
            'clase = "objetivo_list_item"
            'identificador = LCase(rse("objetivo"))
            sHTML = sHTML & "<li Class='" & clase & "' id='" & identificador & "' style='cursor:auto;' data-descr='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            sHTML = sHTML & "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
            sHTML = sHTML & "Duis aute irure dolor In reprehenderit In voluptate velit esse cillum dolore eu fugiat nulla pariatur."
            sHTML = sHTML & "Excepteur sint occaecat cupidatat non proident, sunt In culpa qui officia deserunt mollit anim id est laborum.'>"
            sHTML = sHTML & "<span Class='objetivo_list_num'>" & rse("id") & "</span>"
            sHTML = sHTML & "<span Class='objetivo_list_name'>" & rse("objetivo") & "</span>"
            sHTML = sHTML & "<i Class='fa fa-check' aria-hidden='true'></i>"
            sHTML = sHTML & "</li>"
        End While
        cne.Close()
        Response.Write(sHTML)
    End Function

    Function clasificaciones()
        Dim sSQL, sHTML As String
        sSQL = "select distinct sc.clasificacion " &
        "from (publicacion_estrategias p inner join fondo f On p.idfondo=f.id)inner join sia_clasificacion sc " &
        "on f.sia_idclasificacion=sc.idclasificacion " &
        "where p.idpublicacion='" & idpublicacion & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        sHTML = ""
        While rse.Read
            sHTML = sHTML & "<li class='detalle_block_list_item'>" & rse("clasificacion") & "</li>"
        End While
        cne.Close()
        Response.Write(sHTML)
    End Function

    Function operadoras()
        Dim sSQL, sHTML As String
        sSQL = "select distinct op.nombrecorto " &
        "from (publicacion_estrategias p inner join fondo f on p.idfondo=f.id)inner join operadora op " &
        "on f.idoperadora=op.id " &
        "where p.idpublicacion='" & idpublicacion & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        sHTML = ""
        While rse.Read
            sHTML = sHTML & "<li class='detalle_block_list_item'>" & rse("nombrecorto") & "</li>"
        End While
        cne.Close()
        Response.Write(sHTML)
    End Function

    Protected Sub stickyBtnInvertir_Click(sender As Object, e As EventArgs) Handles stickyBtnInvertir.Click

    End Sub
    Protected Sub stickyBtnRegresar_Click(sender As Object, e As EventArgs) Handles stickyBtnRegresar.Click
        salida(proviene)
    End Sub

    Function rendimientos_dia()
        Dim sSQL, sHTML As String
        Dim rend30, rend60, rend90, rend_actual, rend_anioant As String
        Dim cete30, cete60, cete90, cete_actual, cete_anioant As String
        sSQL = "select f.id, f.clave, f.serie, f.rend1d, f.rend7d, f.rend30d, f.rend60d, f.rend90d, f.rend120d, " &
        "f.rend180d, f.rend360d, f.rend720d, f.rend1080d, f.rendd from fondo f where (f.id='2438') " &
        "order by f.clave, f.serie"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        While rse.Read
            If IsDBNull(rse("rend30d")) Then : cete30 = "N/A" : Else : cete30 = FormatNumber(rse("rend30d"), 2) : End If
            If IsDBNull(rse("rend60d")) Then : cete60 = "N/A" : Else : cete60 = FormatNumber(rse("rend60d"), 2) : End If
            If IsDBNull(rse("rend90d")) Then : cete90 = "N/A" : Else : cete90 = FormatNumber(rse("rend90d"), 2) : End If
            If IsDBNull(rse("rendd")) Then : cete_actual = "N/A" : Else : cete_actual = FormatNumber(rse("rendd"), 2) : End If
            If IsDBNull(rse("rend360d")) Then : cete_anioant = "N/A" : Else : cete_anioant = FormatNumber(rse("rend360d"), 2) : End If
        End While
        cne.Close()

        sSQL = "Select f.id, f.clave, f.serie, f.rend1d, f.rend7d, f.rend30d, f.rend60d, f.rend90d, f.rend120d, " &
        "f.rend180d, f.rend360d, f.rend720d, f.rend1080d, f.rendd,p.porcentaje " &
        "from (publicacion_estrategias p inner join fondo f On p.idfondo=f.id)inner join operadora op " &
        "On f.idoperadora=op.id " &
        "where p.idpublicacion='" & idpublicacion & "'"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        sHTML = ""
        While rse.Read
            If IsDBNull(rse("rend30d")) Then : rend30 = "N/A" : Else : rend30 = FormatNumber(rse("rend30d"), 2) : End If
            If IsDBNull(rse("rend60d")) Then : rend60 = "N/A" : Else : rend60 = FormatNumber(rse("rend60d"), 2) : End If
            If IsDBNull(rse("rend90d")) Then : rend90 = "N/A" : Else : rend90 = FormatNumber(rse("rend90d"), 2) : End If
            If IsDBNull(rse("rendd")) Then : rend_actual = "N/A" : Else : rend_actual = FormatNumber(rse("rendd"), 2) : End If
            If IsDBNull(rse("rend360d")) Then : rend_anioant = "N/A" : Else : rend_anioant = FormatNumber(rse("rend360d"), 2) : End If

            sHTML = "<tr>" &
                    "<td>Últimos 30 días</td>" &
                    "<td>" & rend30 & "%</td>" &
                    "<td>" & cete30 & "%</td>" &
                    "</tr>" &
                    "<tr>" &
                    "<td>Últimos 60 días</td>" &
                    "<td>" & rend60 & "%</td>" &
                    "<td>" & cete60 & "%</td>" &
                    "</tr>" &
                    "<tr>" &
                    "<td>Últimos 90 días</td>" &
                    "<td>" & rend90 & "%</td>" &
                    "<td>" & cete90 & "%</td>" &
                    "</tr>" &
                    "<tr>" &
                    "<td>" & Year(Now()) & "</td>" &
                    "<td>" & rend_actual & "%</td>" &
                    "<td>" & cete_actual & "%</td>" &
                    "</tr>" &
                    "<tr>" &
                    "<td>Último año</td>" &
                    "<td>" & rend_anioant & "%</td>" &
                    "<td>" & cete_anioant & "%</td>" &
                    "</tr>"
        End While
        cne.Close()
        Response.Write(sHTML)
    End Function
End Class
