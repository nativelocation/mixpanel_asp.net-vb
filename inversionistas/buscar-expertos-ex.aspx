﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="buscar-expertos-ex.aspx.vb" Inherits="inversionistas_buscar_expertos_ex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Invierte - Buscar Expertos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/lightgallery.min.css">
        <link rel="stylesheet" href="../css/jquery-ui.min.css">
        <link rel="stylesheet" href="../css/general.css">
        <link rel="stylesheet" href="../css/buscar.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<nav class="navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar first"></span>
                <span class="icon-bar middle"></span>
                <span class="icon-bar last"></span>
            </button>
            <a class="navbar-brand" href="index.aspx"><img src="../img/logo.svg" alt=""></a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="funciona.html">CÓMO FUNCIONA</a></li>
                <li><a href="buscar-expertos.html">BUSCAR EXPERTOS</a></li>
                <li><a href="preguntas.html">AYUDA</a></li>
                <li><a class="btn expert-reg-btn" href="datos-de-contacto.html">Abre tu Cuenta</a></li>
                <li><a class="btn reg-btn" href="#">Regístrate como Experto</a></li>
                <li><a href="portafolio.html">Mi Cuenta</a></li>
                <li><a href="recomienda.html">Recomienda y gana</a></li>
            </ul>
        </div>
    </div>
</nav>
<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<section class="search_wrap">
    <div class="search">
        <div id="searchForm">
            <asp:TextBox ID="txt_search" runat="server" CssClass="search_input" placeholder="Buscar Experto"></asp:TextBox>
            <span class="search_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
            <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" 
                TargetControlID="txt_search" CompletionInterval="10" CompletionSetCount="10" UseContextKey="True"
                ServiceMethod="GetCompletionList" MinimumPrefixLength="1" OnClientItemSelected="IAmSelected">

            </ajaxToolkit:AutoCompleteExtender>
        </div>
    </div>
</section>
<section class="container buscar_cont">
    <div class="buscar_title">
        <h1 class="block_title" style="margin-top: 50px;">Buscar Expertos</h1>
        <hr class="block_divider">
    </div>
    <div id="sec_buscar_main" class="buscar_main-wrap">
    
    <div class="estrats_wrap" style="margin:auto;">
    <p style="text-align:right;"><asp:Button ID="btn_regresar" runat="server" Text="Regresar" style="text-align: center; font-size: 16px; 
    transition: all 0.4s; outline: none; padding: 10px 35px; margin-right: 5px; color: white; background-color: #f5a623; border: none; border-radius: 5px;" /></p>
    <p class="estrats_wrap_section-title">Mostrando Estrategias:</p>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="txt_view" runat="server" />
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </ContentTemplate>
            <Triggers>                
                <asp:AsyncPostBackTrigger ControlID="txt_view" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    </div>
    </div>
</section>
</form>
<hr class="main_divider">
<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="../img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="../img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>


<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="../img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayors rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.html">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.html">Buscar Expertos</a></li>
                    <li><a href="preguntas.html">Ayuda</a></li>
                    <li><a href="contactenos.html">Contactos</a></li>
                </ul>

                <button class="btn expert-reg-btn">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.html">Cómo Funciona</a></li>
                    <li><a href="#">Buscar Expertos</a></li>
                    <li><a href="#">Ayuda</a></li>
                    <li><a href="#">Contactos</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="../img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayors rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in
                    porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
</footer>
    <script src="../js/general_bundle.js"></script>
    <script type="text/javascript">
        function base64_encode(cadena){
            // Encode the String
            var encodedString = btoa(cadena);
            return encodedString;
        }
        function base64_decode(cadena){
            // Decode the String
            var decodedString = atob(cadena);
            return decodedString;
        }
        function IAmSelected(source, eventArgs)
        {
            location.href = "http://invierteconexpertos.mx/buscar-expertos-ex.aspx?search="+ eventArgs.get_text() +"&execution=" + base64_encode(eventArgs.get_value());
        }
    </script>
</body>
</html>
