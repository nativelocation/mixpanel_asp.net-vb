﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="confirmation.aspx.vb" Inherits="confirmation" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Confirmación</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/confirmacion.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-100596471-1', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-53D5L8T');</script>
    <!-- End Google Tag Manager -->
    <style>
        .buscar_btn{color:white;border:1px solid #f5a623;background-color:#f5a623;padding:10px 20px;margin-top:15px;outline:none}
        .buscar_btn:focus, .buscar_btn:hover {
            outline: none;
            background-color: #e39109;
            color: white;
            -webkit-transition: .2s;
            -o-transition: .2s;
            transition: .2s;
        }
    </style>
</head>
<body style="background-color:White;">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<section class="container confir_wrap">
    <img src="img/confirmacion/icons/confirmation_window.svg" alt="Confirmación" class="confir_icon">
    <h1 class="confir_title">¡Bienvenido! Tu cuenta ha sido creada</h1>
    <p class="confir_text">En muy poco tiempo te avisaremos cuándo puedes comenzar a invertir con los mejores Expertos financieros de México.</p>
    <div style="text-align:center;"><button class="btn buscar_btn" onclick="top.location.href='index.aspx';">Ir al inicio</button>    </div>
    
</section>
<script src="js/general_bundle.js"></script> 
    <script>
    ga('send', 'event', 'Encuestas', 'Clic', 'Pre-registro');
    </script>
</body>
</html>


