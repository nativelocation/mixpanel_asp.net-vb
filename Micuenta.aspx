﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Micuenta.aspx.vb" Inherits="Micuenta" %>

<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Invierte - Abre tu Cuenta</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/datos.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-100596471-1', 'auto');
    ga('send', 'pageview');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-53D5L8T');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar first"></span>
                    <span class="icon-bar middle"></span>
                    <span class="icon-bar last"></span>
                </button>
                <a class="navbar-brand" href="index.aspx"><img src="img/logo.svg" alt=""></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li style="margin-right: 18px;"><a href="preguntas.aspx">FAQs</a></li>
                    <li><a class="btn expert-reg-btn" href="datos-de-contacto.aspx" style="background-color:#f5a623;">Prueba el Servicio</a></li>
                    <li><a class="btn reg-btn" href="/expertos">Regístrate como Experto</a></li>
                    <li><a href="Micuenta.aspx">Mi Cuenta</a></li>
                    <li><a href="recomienda.aspx">Recomienda y Gana</a></li>
                </ul>
            </div>
        </div>
    </nav>
 
<section class="container contacto_wrap">
    <h1 class="block_title">Mi Cuenta</h1>
    <hr class="block_divider">
    <div class="datos_block" style="text-align:center;">
        <h1 class="confir_title">En muy poco tiempo te avisaremos cuándo puedes comenzar a invertir con los mejores Expertos financieros de México.</h1><br />
        <%--<p class="confir_text">En muy poco tiempo te avisaremos cuándo puedes comenzar a invertir con los mejores Expertos financieros de México.</p>        --%>
        <%--<img src="img/confirmacion/icons/confirmation_window.svg" width="20%" alt="Confirmación" class="confir_icon">--%>
        <%--<h1 class="confir_title">¡Bienvenido!<br />Tu cuenta ha sido creada</h1>--%>
        <div style="text-align: center;">
            <button class="btn buscar_btn" style="width:200px; height:50px; background-color:#f5a623; color:#fff;"  onclick="top.location.href='index.aspx';">Ir al inicio</button>
        </div>
    </div>
</section>

<hr class="main_divider">
<section class="container main_contacts">
    <h2 class="contacts_title">¿Dudas?<br>
        Estamos para ayudarte</h2>
    <div class="contacts_single">
        <img src="img/login/icons/phone_icon.svg" alt="phone" class="contacts_single_img">
        <p class="contacts_single_text">55-5001-9965</p>
    </div>
    <div class="contacts_single">
        <img src="img/login/icons/mail_icon.svg" alt="email" class="contacts_single_img">
        <p class="contacts_single_text">info@invierteconexpertos.mx</p>
    </div>
</section>
<footer>
    <div class="container">
        <div class="footer-top-area hidden-xs clearfix">
            <div class="footer-logo-area">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>

                <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            </div>
        </div>
        <!-- for mobile -->
        <div class="footer-top-area visible-xs clearfix">
            <div class="footer-menu">
                <ul class="list-instyled list-inline">
                    <li><a href="funciona.aspx">Cómo Funciona</a></li>
                    <li><a href="buscar-expertos.aspx">Buscar Expertos</a></li>
                    <li><a href="preguntas.aspx">Ayuda</a></li>
                    <li><a href="contactenos.aspx">Contacto</a></li>
                </ul>
            </div>
            <button class="btn expert-reg-btn" onclick="location.href='expertos/';">Regístrate como Experto</button>
            <div class="footer-smi">
                <ul class="list-unstyled list-inline">
                    <li><a href="https://www.facebook.com/invierteconexpertos/"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/icexpertos"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="footer-logo-area visible-xs">
                <a href="index.aspx" class="footer-logo"><img src="img/logo.svg" alt=""
                                                              style="width:125px; height: 50px;"></a>
                <div class="footer-logo-text">
                    Encuentra y contrata a los expertos que han generado mayores rendimientos en fondos de inversión
                </div>
            </div>
            <div class="footer-bottom-text">
                <p>
                    Duis ultricies mi at arcu porttitor interdum. Proin ullamcorper nunc interdum sodales auctor.
                    Maecenas
                    tempor suscipit tellus, sit amet rhoncus tellus auctor ut. Nulla consequat vitae arcu in porttitor.
                    Etiam tincidunt quam ut ex vulputate, in elementum ipsum porttitor. Mauris egestas fermentum
                    eleifend.
                    Praesent lacus sapien, molestie eget dapibus sit amet, egestas eget magna. Cras vitae ultrices
                    turpis.
                    Curabitur vel accumsan nisl, at pharetra libero. Nunc eu facilisis turpis. Nullam tortor risus,
                    mollis
                    at lorem ac, sagittis auctor justo.
                </p>
            </div>

            <div class="terms-policy-area">
                <a href="#">Términos y Condiciones </a> ,
                <a href="#"> Política de Privacidad</a>
            </div>
            <div class="footer-copyright">2017 All Rights Reserved</div>
        </div>
    </div>
</footer>

<script src="js/general_bundle.js"></script>
</body>
</html>