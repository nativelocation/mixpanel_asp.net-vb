﻿
Partial Class FileNotFound
    Inherits System.Web.UI.Page

    Protected Sub Page_InitComplete(sender As Object, e As System.EventArgs) Handles Me.InitComplete
        Dim word As String = "/expertos/"
        Dim URL_referencia As String
        Dim encontrado As Integer
        URL_referencia = Request.QueryString("aspxerrorpath")
        encontrado = InStr(1, URL_referencia, word)
        If encontrado > 0 Then
            Response.Redirect("expertos/index.aspx")
        Else
            Response.Redirect("index.aspx")
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

    End Sub
End Class
