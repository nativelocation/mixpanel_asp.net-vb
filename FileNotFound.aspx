﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FileNotFound.aspx.vb" Inherits="FileNotFound" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>No hemos encontrado la página que intentas visitar</p>
        <p><a href="index.aspx">Ir a Inicio</a></p>
    </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(function () {
            if (window != window.top) { //Checks whether the custom error page is loading inside an iframe
                window.top.location = window.location; //Set the top location to custom error page location
            }
        });
    </script>
</body>
</html>
