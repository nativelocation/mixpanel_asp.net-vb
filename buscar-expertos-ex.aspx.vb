﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Collections.Generic

Partial Class buscar_expertos_ex
    Inherits System.Web.UI.Page
    Public cne, cne2 As New Data.SqlClient.SqlConnection
    Public SQLe, SQLe2 As Data.SqlClient.SqlCommand
    Public rse, rse2 As Data.SqlClient.SqlDataReader
    Public expertos As New Ice_expertos, g As New generales
    Public idexperto, alias_exp As String

    Private Sub buscar_expertos_n_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        cne.ConnectionString = expertos.conectdb
        cne2.ConnectionString = expertos.conectdb
        If String.IsNullOrEmpty(Request.QueryString("execution")) Or String.IsNullOrEmpty(Request.QueryString("search")) Then
            Response.Redirect("buscar-expertos-n.aspx")
        End If
        idexperto = g.base64_decode(Request.QueryString("execution"))
        txt_view.Value = "1"
        verificar_objetivos()
    End Sub

    <System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()>
    Public Shared Function GetCompletionList(prefixText As String, count As Integer, contextKey As String) As String()
        Dim connectionString As String = ConfigurationManager.ConnectionStrings("ICEConnection").ConnectionString
        Dim conn As New SqlConnection(connectionString), sSQL As String
        sSQL = "SELECT TOP " & count & " ex.idexperto,ex.alias FROM expertos ex inner join " &
        "(select distinct idexperto from publicacion_paquete where estatus=2)texp " &
        "On ex.idexperto=texp.idexperto " &
        "WHERE Alias Like '" & prefixText & "%'"
        ' Try to use parameterized inline query/sp to protect sql injection
        Dim cmd As New SqlCommand(sSQL, conn)

        Dim oReader As SqlDataReader
        conn.Open()
        Dim CompletionSet As New List(Of String)()
        oReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        While oReader.Read()
            'CompletionSet.Add(oReader("Alias").ToString())
            CompletionSet.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(oReader("Alias").ToString(), oReader("idexperto").ToString()))
        End While
        Return CompletionSet.ToArray()
    End Function

    Public Function estrategias_expertos(ByVal comision As String, ByVal condicion As String, ByVal condicion_op As String)
        Dim sSQL, sSQL2, sHTML, fechaact, ordenar_por, destacadas, color_objetivo As String
        Dim paginacion, ini_page, fin_page, total_row, npage, show_page, mostrar_vista As Integer
        paginacion = 10 : mostrar_vista = txt_view.Value
        sSQL = "select count(t1.idpublicacion)registros " &
        "from (select Rank() over(order by idestrategia)NroRank,row_number()over(order by idestrategia,fecha desc)NroRow, " &
        "pp.* from publicacion_paquete pp inner join publicacion_estrategias pe on pp.idpublicacion=pe.idpublicacion " &
        "where pp.estatus=2)t1 where t1.NroRank=t1.NroRow"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : total_row = SQLe.ExecuteScalar : cne.Close()
        npage = total_row / paginacion
        If String.IsNullOrEmpty(Request.QueryString("page")) Then : show_page = 1 : Else : show_page = Request.QueryString("page") : End If

        If (show_page - 1) >= 0 Then
            If (show_page - 1) <= npage Then
                fin_page = paginacion * show_page
                ini_page = fin_page - (paginacion - 1)
            Else
                fin_page = paginacion
                ini_page = fin_page - (paginacion - 1)
            End If
        Else
            fin_page = paginacion
            ini_page = fin_page - (paginacion - 1)
        End If

        sSQL = "select fechaact from actualizacion where id=4"
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open() : fechaact = SQLe.ExecuteScalar : cne.Close()

        If Not String.IsNullOrEmpty(condicion) Then
            condicion = "and (" & condicion & ")"
        End If
        If Not String.IsNullOrEmpty(condicion_op) Then
            condicion_op = "and (" & condicion_op & ") "
        End If

        destacadas = " "
        ordenar_por = "sum(search.rendaacum) desc"


        sSQL = "select search.idpublicacion,search.idestrategia,search.idexperto,search.fecha," &
        "search.clave, search.perfil2, search.max_rv, search.comision,sum(search.rendaacum)rend " &
        "from (Select rank() over(order by d1.idestrategia)NroRank,ROW_NUMBER() over(order by d1.idestrategia,d2.rendaacum desc)NroRow,d1.*,d2.* " &
        "from (select t2.idpublicacion,t2.idestrategia,t2.idexperto,t2.fecha, " &
        "a.clave, a.monto, a.rendimiento, a.perfil2, a.max_rv, a.comision " &
        "from (select row_number() over(order by data.idpublicacion)ROWNUMBER,data.idpublicacion,data.idestrategia,data.idexperto,data.fecha " &
        "from (select t1.idpublicacion,t1.idestrategia,t1.idexperto,t1.fecha " &
        "from (select Rank() over(order by idestrategia)NroRank,row_number()over(order by idestrategia,fecha desc)NroRow, " &
        "pp.* from publicacion_paquete pp inner join publicacion_estrategias pe on pp.idpublicacion=pe.idpublicacion " &
        "where pp.estatus=2)t1 where t1.NroRank=t1.NroRow) as data)t2 inner join asesorias a on t2.idestrategia=a.idasesoria " &
        "where (t2.ROWNUMBER between " & ini_page & " and " & total_row & ") and a.comision<=" & comision & " " & condicion & destacadas & ")d1 inner join " &
        "(select pf.idcliente, (pf.idfondo)fondos,((pf.porcentajediafondo*rend360a)/100)rendaacum,(f.idoperadora)operadoras " &
        "from posicion_fondo pf inner join fondo f on pf.idfondo=f.id " &
        "where pf.fecha='" & Format(CDate(fechaact), "yyyy-MM-dd") & "' and idfondo<>1442)d2 on d1.idestrategia=d2.idcliente " & condicion_op & ")search " &
        "where search.idexperto='" & idexperto & "' group by " &
        "search.idpublicacion,search.idestrategia,search.idexperto,search.fecha," &
        "search.clave, search.monto, search.rendimiento, search.perfil2, search.max_rv, search.comision " &
        "order by " & ordenar_por & " "
        'Response.Write(sSQL)
        'Label2.Text = sSQL
        SQLe = New Data.SqlClient.SqlCommand(sSQL, cne)
        cne.Open()
        rse = SQLe.ExecuteReader
        sHTML = ""
        While rse.Read
            Dim n_fondos, n_oper As Integer, rendimiento As Double
            ''Calulando la composicion y el rendimiento ponderado
            'sSQL2 = "select count(pf.idfondo)fondos,sum((pf.porcentajediafondo*rend360a)/100)rendaacum,count(distinct(f.idoperadora))operadoras " &
            '"from posicion_fondo pf inner join fondo f on pf.idfondo=f.id " &
            '"where pf.idcliente='" & rse("idestrategia") & "' and pf.fecha='" & Format(CDate(fechaact), "yyyy-MM-dd") & "' and idfondo<>1442"
            'SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            'cne2.Open()
            'rse2 = SQLe2.ExecuteReader
            'While rse2.Read
            '    n_fondos = rse2("fondos")
            '    rendimiento = rse2("rendaacum")
            '    n_oper = rse2("operadoras")
            'End While
            'cne2.Close()

            'Calculando la composicion de fondos
            sSQL2 = "select count(distinct op.nombrecorto)fondos,count(distinct f.idoperadora)operadoras " &
            "from (publicacion_estrategias p inner join fondo f On p.idfondo=f.id)inner join operadora op " &
            "on f.idoperadora=op.id where p.idpublicacion='" & rse("idpublicacion") & "'"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                n_fondos = rse2("fondos")
                n_oper = rse2("operadoras")
            End While
            cne2.Close()

            'Calculando el rendimiento anualizado
            sSQL2 = "select rendimientoaacum from posicion_cliente where idcliente='" & rse("idestrategia") & "' and fecha='" & Format(CDate(fechaact), "yyyy-MM-dd") & "'"
            SQLe2 = New Data.SqlClient.SqlCommand(sSQL2, cne2)
            cne2.Open()
            rse2 = SQLe2.ExecuteReader
            While rse2.Read
                rendimiento = rse2("rendimientoaacum")
            End While
            cne2.Close()

            Select Case rse("perfil2")
                Case 1 : color_objetivo = "rgb(151, 205, 87)"
                Case 2 : color_objetivo = "rgb(245, 166, 35)"
                Case 3 : color_objetivo = "rgb(219, 40, 55)"
                Case 4 : color_objetivo = "rgb(115, 191, 247)"
                Case 5 : color_objetivo = "rgb(110, 96, 234)"
            End Select
            sHTML = sHTML & "<div class='buscar_estrat'>"
            sHTML = sHTML & "<div class='estrat_body_wrap'>"
            sHTML = sHTML & "<div class='buscar_estrat_head'>"
            sHTML = sHTML & "<div class='estrat_head_titles_wrap'>"
            sHTML = sHTML & "<p class='buscar_estrat_head_title'><span>Estrategia:</span> " & Mid(rse("clave"), 12) & "</p><span> / &nbsp;</span>"
            sHTML = sHTML & "<p class='buscar_estrat_head_title'><span>Experto:</span> " & expertos.alias_experto(rse("idexperto")) & "</p>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<a class='btn buscar_estrat_head_agregar_btn buscar_estrat_head_agregar_visible' href='inversionistas/detalle.aspx?" & g.base64_encode("Estrategia") & "=" & g.base64_encode(rse("idestrategia")) & "&" & g.base64_encode("Experto") & "=" & g.base64_encode(rse("idexperto")) & "&" & g.base64_encode("proviene") & "=buscarexp&search=" & Request.QueryString("search") & "&execution=" & Request.QueryString("execution") & "'>"
            sHTML = sHTML & "Ver Detalles</a>"
            sHTML = sHTML & "<p class='buscar_estrat_head_agregar_summ'>Agregaste <span"
            sHTML = sHTML & "class='buscar_estrat_head_agregar_summ_value'></span></p>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<div class='buscar_estrat_block estrat_block_rendimiento'>"
            sHTML = sHTML & "<p class='buscar_estrat_term'>Rendimiento</p>"
            sHTML = sHTML & "<p class='buscar_estrat_percent' style='font-weight:600;'>" & FormatNumber(rendimiento, 2) & "%</p>"
            sHTML = sHTML & "<p class='buscar_estrat_term' style='margin-top: -16px;'>Desde el " & g.ConvierteFecha2(rse("fecha")) & "</p>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<div class='buscar_estrat_block estrat_block_comision'>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle' style='margin-top:20px;'>Comisión</span>"
            sHTML = sHTML & "<div class='buscar_estrat_value_wrap estrat_comision_wrap'>"
            sHTML = sHTML & "<span class='buscar_estrat_big-value buscar_estrat_comision_value'>" & FormatNumber(rse("comision"), 2) & "%</span>"
            sHTML = sHTML & "<span class='buscar_estrat_value_sm-text'> (anual)</span>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<hr class='estrat_block_comision_mob-divider'>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle' style='margin-top:20px;'>Mínimo</span>"
            sHTML = sHTML & "<div class='buscar_estrat_value_wrap estrat_minimo_wrap'>"
            sHTML = sHTML & "<span class='buscar_estrat_big-value buscar_estrat_minimo_value'>$" & FormatNumber(25000, 0) & "</span>"
            sHTML = sHTML & "<span class='buscar_estrat_value_sm-text'> pesos</span>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<div class='buscar_estrat_block estrat_block_objetivo'>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle'>Objetivo de Inversión</span>"
            sHTML = sHTML & "<span class='buscar_estrat_objetivo_value' style='background-color: " & color_objetivo & "; color: white;'>" & rse("perfil2") & "</span>"
            sHTML = sHTML & "<div style='margin-top: 10px; text-align: center;'>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle'>Liquidez</span><br />"
            sHTML = sHTML & "<span class='buscar_estrat_big-value'>72hrs</span>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<div class='buscar_estrat_block estrat_block_diver'>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle' style='margin-bottom: 8px;'>Diversificación</span>"
            sHTML = sHTML & "<p>"
            sHTML = sHTML & "<span class='buscar_estrat_big-value buscar_estrat_fondos_value'>" & n_fondos & "</span>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle'> Fondos</span>"
            sHTML = sHTML & "</p>"
            sHTML = sHTML & "<p><span class='buscar_estrat_big-value buscar_estrat_operadoras_value'>" & n_oper & "</span>"
            sHTML = sHTML & "<span class='buscar_estrat_subtitle'> Operadoras</span>"
            sHTML = sHTML & "</p>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "<button class='btn buscar_estrat_mobile_agregar_btn' onclick='location.href='detalle.aspx?" & g.base64_encode("Estrategia") & "=" & g.base64_encode(rse("idestrategia")) & "&" & g.base64_encode("Experto") & "=" & g.base64_encode(rse("idexperto")) & "';'>"
            sHTML = sHTML & "Ver(Detalle & Invertir)"
            sHTML = sHTML & "</button>"
            sHTML = sHTML & "</div>"
            sHTML = sHTML & "</div>"
        End While
        cne.Close()
        Literal1.Text = sHTML
    End Function

    Function verificar_objetivos()
        Dim cadena_principal, cadena_operadoras As String
        Dim contador As Integer = 1, comision_val As Double
        cadena_principal = ""
        cadena_operadoras = ""
        comision_val = 1.5
        estrategias_expertos(comision_val, cadena_principal, cadena_operadoras)
    End Function
    Protected Sub btn_regresar_Click(sender As Object, e As EventArgs) Handles btn_regresar.Click
        Response.Redirect("buscar-expertos-n.aspx")
    End Sub

    Private Sub buscar_expertos_ex_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class
