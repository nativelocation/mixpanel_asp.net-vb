﻿Option Infer On
Imports System.IO
Imports System.Net
Imports System.Threading.Tasks
Imports MailChimp.Net
Imports Newtonsoft.Json

Imports System.Timers

Partial Class index
    Inherits System.Web.UI.Page
    Public cni As New Data.SqlClient.SqlConnection
    Public SQLi As Data.SqlClient.SqlCommand
    Public rsi As Data.SqlClient.SqlDataReader
    Public g As New generales, inversionista As New ICE_clientes
    Private aTimer As System.Timers.Timer
    
    Private Sub index_InitComplete(sender As Object, e As EventArgs) Handles Me.InitComplete
        cni.ConnectionString = inversionista.conectdb
    End Sub

    Protected Sub btn_registro_Click(sender As Object, e As EventArgs) Handles btn_registro.Click     
        Dim sSQL, idinversionista As String 
        If revisar_existencia() = 0 Then
            sSQL = "insert into Inversionistas (fecha,nombre,telefono,correo,pass,activada,estatus) " &
            "values ('" & inversionista.hora_local & "','" & UCase(txt_nombre.Text) & "','" & txt_telefono.Text & "','" & LCase(Trim(txt_correo.Text)) & "','" & txt_pass.Text & "',0,0)"
            SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
            cni.Open()
            Dim rows As Integer = SQLi.ExecuteNonQuery()
            If rows > 0 Then
                Dim sqlIdentity As String = "SELECT @@IDENTITY"
                Dim Identity As New Data.SqlClient.SqlCommand(sqlIdentity, cni)
                idinversionista = (Convert.ToInt32(Identity.ExecuteScalar()))
            End If
            cni.Close() 
            Session("idinversionista") = idinversionista
            Session("correo_registro") = LCase(Trim(txt_correo.Text))
            If Not String.IsNullOrEmpty(idinversionista) Then
                AddOrUpdateListMember("us16", "8e4e8013d9f90ca0e02df34423f4b6fd-us16", "7f28b9600b", LCase(txt_correo.Text), UCase(txt_nombre.Text), txt_telefono.Text)
            End If

            Dim jsonproduct = JsonConvert.SerializeObject(New With {
                Key .event = "register",
                Key .properties = New With {
                    Key .distinct_id = txt_distinct.Text,
                    Key .token = "79a3138bc6cd79dea43bb30e334f8709",
                    Key .name = txt_nombre.Text,
                    Key .email = txt_correo.Text,
                    Key .phone = txt_telefono.Text,
                    Key .user_type = "investor"
                }
            })
            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(jsonproduct)
            Dim webClient As New System.Net.WebClient
            Dim result As String = webClient.DownloadString("http://api.mixpanel.com/track/?data=" & System.Convert.ToBase64String(byt))
            Response.Redirect("datos-de-contacto2.aspx")
        Else
            lbl_message.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Los datos han sido registrados con anterioridad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
        End If
    End Sub

    Private Sub index_Load(sender As Object, e As EventArgs) Handles Me.Load
        txt_correo.Attributes.Add("required", "required") : txt_correo.Attributes.Add("type", "email")
        txt_nombre.Attributes.Add("required", "required")
        txt_pass.Attributes.Add("required", "required")
        txt_telefono.Attributes.Add("required", "required")
    End Sub

    Function revisar_existencia() As Integer
        Dim sSQL As String
        sSQL = "select count(*)encontrado from inversionistas where correo='" & LCase(Trim(txt_correo.Text)) & "'"
        SQLi = New Data.SqlClient.SqlCommand(sSQL, cni)
        cni.Open() : revisar_existencia = SQLi.ExecuteScalar : cni.Close()
    End Function

    Private Shared Function AddOrUpdateListMember(dataCenter As String, apiKey As String, listId As String, subscriberEmail As String, subscriberName As String, subscriberPhone As String) As String
        Dim sampleListMember = JsonConvert.SerializeObject(New With {
        Key .email_address = subscriberEmail,
        Key .merge_fields = New With {
            Key .EMAIL = subscriberEmail,
            Key .NOMBRE = subscriberName,
            Key .CELULAR = subscriberPhone
        },
        Key .status_if_new = "subscribed"
    })

        Dim hashedEmailAddress = If(String.IsNullOrEmpty(subscriberEmail), "", CalculateMD5Hash(subscriberEmail.ToLower()))
        Dim uri = String.Format("https://{0}.api.mailchimp.com/3.0/lists/{1}/members/{2}", dataCenter, listId, hashedEmailAddress)
        Try
            Using webClient = New WebClient()
                webClient.Headers.Add("Accept", "application/json")
                webClient.Headers.Add("Authorization", Convert.ToString("apikey ") & apiKey)

                Return webClient.UploadString(uri, "PUT", sampleListMember)
            End Using
        Catch we As WebException
            Using sr = New StreamReader(we.Response.GetResponseStream())
                Return sr.ReadToEnd()
            End Using
        End Try
    End Function

    Private Shared Function CalculateMD5Hash(input As String) As String
        ' Step 1, calculate MD5 hash from input.
        Dim md5 = System.Security.Cryptography.MD5.Create()
        Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(input)
        Dim hash As Byte() = md5.ComputeHash(inputBytes)
        ' Step 2, convert byte array to hex string.
        Dim sb = New StringBuilder()
        For Each [byte] As Byte In hash
            sb.Append([byte].ToString("X2"))
        Next
        Return sb.ToString()
    End Function
End Class
